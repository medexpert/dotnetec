﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Xml;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;

/// <summary>
/// Summary description for IviewData
/// </summary>
[Serializable()]
public class IviewData
{
    public IviewData()
    {
        //
        // TODO: Add constructor logic here
        //
        ReportHdrs = new ArrayList();
        ReportHdrStyles = new ArrayList();
        ReportFtrs = new ArrayList();
        ReportFtrStyles = new ArrayList();
        HiddenCols = new ArrayList();
        IViewWhenEmpty = string.Empty;
        paramNames = new ArrayList();
        paramCaption = new ArrayList();
        paramNameType = new ArrayList();
        paramValsOnLoad = new ArrayList();
        paramChangedVals = new ArrayList();
        ShowHiddengridCols = new ArrayList();
        HiddenCols = new ArrayList();
        FilterXml = string.Empty;
        IsSumField = new ArrayList();
        IsRunningTotal = new ArrayList();
        GolbalVarName = new ArrayList();
        GolbalVarValue = new ArrayList();
        SubtTotalonTop = new ArrayList();
        SubtColName = new ArrayList();
        SubtCaption = new ArrayList();
        SubtHeader = new ArrayList();
        SubtFooter = new ArrayList();
        SubtOrderNo = new ArrayList();
        SubtSpaceFooter = new ArrayList();
        SubtPageBreak = new ArrayList();
        RunningTotalValue = new DataTable();
        GrandTotalRow = new DataTable();
        RunningPageCount = new int();
        ColNoRepeat = new ArrayList();
        ArrParamType = new ArrayList();
        customBtnIV = new ArrayList();
    }


    #region privatevariables

    private ArrayList colZeroOff;

    private ArrayList colNoRepeat;

    private ArrayList arrParamType;
    public ArrayList customBtnIV;

    public ArrayList ArrParamType
    {
        get { return arrParamType; }
        set { arrParamType = value; }
    }

    private int totalRows;

    private int directDbtotalRows;

    private string hdntotalRows;

    private bool isPerfXxml;
    private string iVType;
    public string IVType
    {
        get { return iVType; }
        set { iVType = value; }
    }
    private bool isParameterExist;

    public bool IsParameterExist
    {
        get { return isParameterExist; }
        set { isParameterExist = value; }
    }

    public string HdnTotalRows
    {
        get { return hdntotalRows; }
        set { hdntotalRows = value; }
    }

    private string grdPageSize = "1000";

    private ArrayList ivCaption;

    private string currentPageNo;

    private bool paramsExist;

    private int gridWidth;

    private string axp_refresh;

    private string sortDir;

    //TODOP: use only one of the below property (ivname, iname, tid)
    private string ivname;

    private string iName;

    private string tid;

    private string iviewCaption;

    private bool isIviewStagLoad;

    private bool isFilterEnabled;

    private List<DataTable> stagTables;

    private string rxml;

    private string fromHyperLink;

    private string myViewName;

    private ArrayList reportHdrs;

    private ArrayList reportHdrStyles;

    private ArrayList reportFtrs;

    private ArrayList reportFtrStyles;

    private string iViewWhenEmpty;

    private string srNoColumName;

    private string toolbarHtml;

    private string filterText;

    private string ires;

    private bool isWordWithHtml;


    private string iviewFooter;

    public string IviewFooter
    {
        get { return iviewFooter; }
        set { iviewFooter = value; }
    }

    private StringBuilder paramHtml;

    public StringBuilder ParamHtml
    {
        get { return paramHtml; }
        set { paramHtml = value; }
    }



    public ArrayList PivotMergeHeaders { get; set; }
    public ArrayList PivotStartCol { get; set; }
    public ArrayList PivotEndCol { get; set; }

    private bool isDirectDBcall;

    private string structureXml;

    private DataSet dsDataSetDB;

    private DataTable dsIviewConfig;

    private ArrayList golbalVarName;

    private ArrayList golbalVarValue;


    public ArrayList ColNoRepeat
    {
        get { return colNoRepeat; }
        set { colNoRepeat = value; }
    }


    public ArrayList GolbalVarName
    {
        get { return golbalVarName; }
        set { golbalVarName = value; }
    }

    public ArrayList GolbalVarValue
    {
        get { return golbalVarValue; }
        set { golbalVarValue = value; }
    }

    public DataSet DsDataSetDB
    {
        get { return dsDataSetDB; }
        set { dsDataSetDB = value; }
    }

    public DataTable DsIviewConfig
    {
        get { return dsIviewConfig; }
        set { dsIviewConfig = value; }
    }


    public string StructureXml
    {
        get { return structureXml; }
        set { structureXml = value; }
    }

    public bool IsDirectDBcall
    {
        get { return isDirectDBcall; }
        set { isDirectDBcall = value; }
    }


    public bool IsWordWithHtml
    {
        get { return isWordWithHtml; }
        set { isWordWithHtml = value; }
    }


    public string iRes
    {
        get { return ires; }
        set { ires = value; }
    }

    public int TotalRows
    {
        get { return totalRows; }
        set { totalRows = value; }
    }

    public int DirectDbtotalRows
    {
        get { return directDbtotalRows; }
        set { directDbtotalRows = value; }
    }

    public string SrNoColumName
    {
        get { return srNoColumName; }
        set { srNoColumName = value; }
    }

    public string IViewWhenEmpty
    {
        get { return iViewWhenEmpty; }
        set { iViewWhenEmpty = value; }
    }

    public List<DataTable> StagTables
    {
        get { return stagTables; }
        set { stagTables = value; }
    }

    public bool IsIviewStagLoad
    {
        get { return isIviewStagLoad; }
        set { isIviewStagLoad = value; }
    }

    public bool IsFilterEnabled
    {
        get { return isFilterEnabled; }
        set { isFilterEnabled = value; }
    }

    private DataTable dtCurrentdata;

    public DataTable DtCurrentdata
    {
        get { return dtCurrentdata; }
        set { dtCurrentdata = value; }
    }

    private string sessioKey;

    public string SessioKey
    {
        get { return sessioKey; }
        set { sessioKey = value; }
    }
    private string toolbarbtn;
    public string ToolBarBtn
    {
        get { return toolbarbtn; }
        set { toolbarbtn = value; }
    }




    private ArrayList colFld;

    private ArrayList headName;

    private ArrayList colWidth;

    private ArrayList colType;

    private ArrayList colHide;

    private ArrayList colDec;

    private ArrayList colApplyComma;


    private ArrayList colHlink;

    private ArrayList colHlinkPop;

    private ArrayList colRefreshParent;

    private IDictionary<string, string> actRefreshParent;

    private ArrayList colHlinktype;

    private ArrayList colMap;

    private ArrayList colHAction;

    private ArrayList colAlign;

    private ArrayList colNoPrint;

    private string menubreadcrumb;

    private ArrayList hiddenCols;

    private ArrayList showHideCols;

    private ArrayList showHiddengridCols;

    private ArrayList paramNames;

    private ArrayList paramCaption;

    private ArrayList paramNameType;

    private ArrayList paramValsOnLoad;

    private ArrayList paramChangedVals;



    private bool colAxp_format;

    private ArrayList axp_format;

    private ArrayList formatCols;

    private ArrayList colStyle;


    private ArrayList subtTotalonTop;
    private ArrayList subtColName;
    private ArrayList subtCaption;
    private ArrayList subtHeader;
    private ArrayList subtFooter;
    private ArrayList subtOrderNo;
    private ArrayList subtSpaceFooter;
    private ArrayList subtPageBreak;
    private ArrayList isSumField;


    private DataTable runningTotalValue;
    private DataTable grandTotalRow;
    private ArrayList isRunningTotal;
    private int runningPageCount;

    public DataTable GrandTotalRow
    {
        get { return grandTotalRow; }
        set { grandTotalRow = value; }
    }

    public int RunningPageCount
    {
        get { return runningPageCount; }
        set { runningPageCount = value; }
    }
    public DataTable RunningTotalValue
    {
        get { return runningTotalValue; }
        set { runningTotalValue = value; }
    }

    public ArrayList IsRunningTotal
    {
        get { return isRunningTotal; }
        set { isRunningTotal = value; }
    }

    private bool isGrandTotal;

    private string sqlQuery;
    private string iviewParamString;
    private string associatedTStruct;
    private string axpAutoSplit;
    private string axpIviewDisableSplit;
    Dictionary<string, string> actBtnNavigation;
    Dictionary<string, string> hypLnkNavigation;
    private string colsToFilter;
    private string filterXml;
    private int webServiceTimeout = 100000;
    LogFile.Log logobj = new LogFile.Log();

    #endregion


    #region publicvariables

    public bool IsGrandTotal
    {
        get { return isGrandTotal; }
        set { isGrandTotal = value; }
    }

    public string SqlQuery
    {
        get { return sqlQuery; }
        set { sqlQuery = value; }
    }

    public ArrayList IsSumField
    {
        get { return isSumField; }
        set { isSumField = value; }
    }


    public ArrayList SubtTotalonTop
    {
        get { return subtTotalonTop; }
        set { subtTotalonTop = value; }
    }


    public ArrayList SubtColName
    {
        get { return subtColName; }
        set { subtColName = value; }
    }

    public ArrayList SubtCaption
    {
        get { return subtCaption; }
        set { subtCaption = value; }
    }

    public ArrayList SubtHeader
    {
        get { return subtHeader; }
        set { subtHeader = value; }
    }

    public ArrayList SubtFooter
    {
        get { return subtFooter; }
        set { subtFooter = value; }
    }

    public ArrayList SubtOrderNo
    {
        get { return subtOrderNo; }
        set { subtOrderNo = value; }
    }

    public ArrayList SubtSpaceFooter
    {
        get { return subtSpaceFooter; }
        set { subtSpaceFooter = value; }
    }

    public ArrayList SubtPageBreak
    {
        get { return subtPageBreak; }
        set { subtPageBreak = value; }
    }



    #endregion


    #region publicvariables

    public string IviewParamString
    {
        get { return iviewParamString; }
        set { iviewParamString = value; }

    }

    public bool IsPerfXml
    {
        get { return isPerfXxml; }
        set { isPerfXxml = value; }

    }

    public ArrayList ParamCaption
    {
        get { return paramCaption; }
        set { paramCaption = value; }
    }

    public ArrayList ParamNames
    {
        get { return paramNames; }
        set { paramNames = value; }
    }

    public ArrayList ParamNameType
    {
        get { return paramNameType; }
        set { paramNameType = value; }
    }

    public ArrayList ParamValsOnLoad
    {
        get { return paramValsOnLoad; }
        set { paramValsOnLoad = value; }
    }

    public ArrayList ParamChangedVals
    {
        get { return paramChangedVals; }
        set { paramChangedVals = value; }
    }

    public ArrayList ShowHiddengridCols
    {
        get { return showHiddengridCols; }
        set { showHiddengridCols = value; }
    }

    public ArrayList ShowHideCols
    {
        get { return showHideCols; }
        set { showHideCols = value; }
    }
    public string Tid
    {
        get { return tid; }
        set { tid = value; }
    }

    public string IName
    {
        get { return iName; }
        set { iName = value; }
    }

    public string Ivname
    {
        get { return ivname; }
        set { ivname = value; }
    }


    public string SortDir
    {
        get { return sortDir; }
        set { sortDir = value; }
    }

    public string Axp_refresh
    {
        get { return axp_refresh; }
        set { axp_refresh = value; }
    }


    public int GridWidth
    {
        get { return gridWidth; }
        set { gridWidth = value; }
    }

    public bool ParamsExist
    {
        get { return paramsExist; }
        set { paramsExist = value; }
    }

    public string CurrentPageNo
    {
        get { return currentPageNo; }
        set { currentPageNo = value; }
    }


    public ArrayList IvCaption
    {
        get { return ivCaption; }
        set { ivCaption = value; }
    }

    public string GrdPageSize
    {
        get { return grdPageSize; }
        set { grdPageSize = value; }

    }

    public string ResultXml
    {
        get { return rxml; }
        set { rxml = value; }

    }

    public string IviewCaption
    {
        get { return iviewCaption; }
        set { iviewCaption = value; }
    }

    public string Menubreadcrumb
    {
        get { return menubreadcrumb; }
        set { menubreadcrumb = value; }
    }
    //align
    public ArrayList ColAlign
    {
        get { return colAlign; }
        set { colAlign = value; }
    }
    //hlaction
    public ArrayList ColHAction
    {
        get { return colHAction; }
        set { colHAction = value; }
    }
    //map
    public ArrayList ColMap
    {
        get { return colMap; }
        set { colMap = value; }
    }
    //hltype
    public ArrayList ColHlinktype
    {
        get { return colHlinktype; }
        set { colHlinktype = value; }
    }
    //pop
    public ArrayList ColHlinkPop
    {
        get { return colHlinkPop; }
        set { colHlinkPop = value; }
    }
    //refresh
    public ArrayList ColRefreshPrent
    {
        get { return colRefreshParent; }
        set { colRefreshParent = value; }
    }
    ///r1/param1/Refresh
    public IDictionary<string, string> ActRefreshParent
    {
        get { return actRefreshParent; }
        set { actRefreshParent = value; }
    }
    //hlink
    public ArrayList ColHlink
    {
        get { return colHlink; }
        set { colHlink = value; }
    }
    //hide
    public ArrayList ColHide
    {
        get { return colHide; }
        set { colHide = value; }
    }
    //dec
    public ArrayList ColDec
    {
        get { return colDec; }
        set { colDec = value; }
    }
    //always false
    public ArrayList ColApplyComma
    {
        get { return colApplyComma; }
        set { colApplyComma = value; }
    }

    //type
    public ArrayList ColType
    {
        get { return colType; }
        set { colType = value; }
    }
    //width
    public ArrayList ColWidth
    {
        get { return colWidth; }
        set { colWidth = value; }
    }
    //col caption
    public ArrayList HeadName
    {
        get { return headName; }
        set { headName = value; }
    }
    //col id
    public ArrayList ColFld
    {
        get { return colFld; }
        set { colFld = value; }
    }
    //col id starts with
    public ArrayList ColNoPrint
    {
        get { return colNoPrint; }
        set { colNoPrint = value; }
    }
    //always false if not directDB
    public ArrayList ColZeroOff
    {
        get { return colZeroOff; }
        set { colZeroOff = value; }
    }

    public string FromHyperLink
    {
        get { return fromHyperLink; }
        set { fromHyperLink = value; }
    }

    public ArrayList ReportHdrs
    {
        get { return reportHdrs; }
        set { reportHdrs = value; }
    }

    public ArrayList ReportHdrStyles
    {
        get { return reportHdrStyles; }
        set { reportHdrStyles = value; }
    }

    public ArrayList ReportFtrs
    {
        get { return reportFtrs; }
        set { reportFtrs = value; }
    }

    public ArrayList ReportFtrStyles
    {
        get { return reportFtrStyles; }
        set { reportFtrStyles = value; }
    }

    public string MyViewName
    {
        get { return myViewName; }
        set { myViewName = value; }
    }

    public ArrayList HiddenCols
    {
        get { return hiddenCols; }
        set { hiddenCols = value; }
    }

    public string ToolbarHtml
    {
        get { return toolbarHtml; }
        set { toolbarHtml = value; }
    }

    public string FilterText
    {
        get { return filterText; }
        set { filterText = value; }
    }

    public string ColsToFilter
    {
        get { return colsToFilter; }
        set { colsToFilter = value; }

    }

    public string FilterXml
    {
        get { return filterXml; }
        set { filterXml = value; }
    }

    public ArrayList Axp_format
    {
        get { return axp_format; }
        set { axp_format = value; }
    }
    public bool ColAxp_format
    {
        get { return colAxp_format; }
        set { colAxp_format = value; }
    }
    public ArrayList FormatCols
    {
        get { return formatCols; }
        set { formatCols = value; }
    }
    public ArrayList ColStyle
    {
        get { return colStyle; }
        set { colStyle = value; }
    }


    public string AssociatedTStruct
    {
        get { return associatedTStruct; }
        set { associatedTStruct = value; }

    }

    public string AxpIsAutoSplit
    {
        get { return axpAutoSplit; }
        set { axpAutoSplit = value; }

    }

    public string AxpIviewDisableSplit
    {
        get { return axpIviewDisableSplit; }
        set { axpIviewDisableSplit = value; }

    }
    public Dictionary<string, string> ActBtnNavigation
    {
        get { return actBtnNavigation; }
        set { actBtnNavigation = value; }
    }
    public Dictionary<string, string> HypLnkNavigation
    {
        get { return hypLnkNavigation; }
        set { hypLnkNavigation = value; }
    }

    public int WebServiceTimeout
    {
        get { return webServiceTimeout > 0 ? webServiceTimeout : 100000; }
        set { webServiceTimeout = value; }
    }

    public bool noVisibleParam { get; set; }

    public bool showParam = false;

    //if iview web service will return row count or not
    public bool getIviewRowCount = false;
    //page limit for iview data call
    public int iviewDataWSRows = 1000;
    //real page size returned by web service
    public int realRowCount = 0;
    //wheather we received last page or not;
    public bool lastPageCached = false;
    //page no being opened after caching
    public ArrayList newPagesArray = new ArrayList();
    //real page size in the page number respective of newPagesArray
    public List<int> realPageSize = new List<int>();
    //page size including grand total + sub total ++++++ respective of newPagesArray
    public ArrayList pageSizeWithGTandST = new ArrayList();
    //cached dataset with tables as pages
    public DataSet dsIvPages = new DataSet();
    //current wabservice cached page no
    public int cachedPage = 0;
    //param string for cached data = "";
    public string paramCacheString = "";
    #endregion

    //exportVerticalAlign = top|middle|bottom
    public string exportVerticalAlign = "middle";

    public string purposeString = string.Empty;

    Util.Util objUtil = new Util.Util();
    #region Functions

    public DataTable GetVisibleColsData(DataTable dt)
    {
        int shcol = -1;
        int shgcol = -1;
        string colName = string.Empty;
        for (int i = dt.Columns.Count - 1; i >= 0; i--)
        {
            colName = colFld[i].ToString();
            if ((ShowHiddengridCols != null) && (ShowHiddengridCols.Count > 0))
                shgcol = ShowHiddengridCols.IndexOf(colName);

            if ((showHideCols != null) && (showHideCols.Count > 0))
                shcol = showHideCols.IndexOf(colName);

            if ((shgcol == -1) && (colHide[i].ToString() == "true") || (colName == "axrowtype") || (shcol > -1))
                dt.Columns.RemoveAt(i);

        }
        return dt;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="paramValues">name~value¿name~value</param>
    /// <param name="paramName"></param>
    /// <returns></returns>
    public string GetParamValue(string paramValues, string paramName)
    {
        string pValue = string.Empty;
        if (paramValues != string.Empty)
        {
            string[] arrParams = paramValues.Split('¿');
            string pVal = string.Empty;
            int idx = -1;
            for (int i = 0; i < arrParams.Length; i++)
            {
                if (arrParams[i].ToString() != string.Empty)
                {
                    pVal = arrParams[i].ToString();
                    idx = pVal.IndexOf("~");
                    if (pVal.Substring(0, idx) == paramName)
                    {
                        pValue = pVal.Substring(idx + 1);
                    }
                }
            }
        }
        return pValue;
    }


    /// <summary>
    /// To return IView data as datatable
    /// </summary>
    /// <param name="iName">IView name</param>
    /// <param name="pageNo">To load page number</param>
    /// <param name="recsPerPage">Records per page to be displayed</param>
    /// <returns></returns>
    public object GetData(string IvName, int pageNo, int recsPerPage, string paramX, string purposeString = "")
    {
        string iXml = string.Empty;
        string errlog = string.Empty;
        string result = string.Empty;

        string title = string.Empty;
        string subTitle = string.Empty;
        string customText = string.Empty;
        string footer = string.Empty;
        DataSet dst = new DataSet();
        string fileName = "GetIViewFromObject-" + IvName;

        string sessionId = string.Empty;
        bool IsDbPagination = false;
        if (HttpContext.Current.Session["AxDbPagination"] != null)
            IsDbPagination = Convert.ToBoolean(HttpContext.Current.Session["AxDbPagination"].ToString().ToLower());
        string proj = HttpContext.Current.Session["Project"].ToString();
        ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();


        if (sessionId == "")
            sessionId = HttpContext.Current.Session.SessionID;
        errlog = logobj.CreateLog("Call to GetIView Web Service for page no " + pageNo, sessionId, fileName, "");
        if (IsDbPagination == true && pageNo.ToString() != "1" && HttpContext.Current.Session["iv_noofpages"] != null)
        {
            int.TryParse(HttpContext.Current.Session["iv_noofpages"].ToString(), out totalRows);
            iXml = "<root " + purposeString + " name ='" + IvName + "' axpapp = '" + proj + "' sessionid = '" + sessionId + "' appsessionkey='" + HttpContext.Current.Session["AppSessionKey"].ToString() + "' username='" + HttpContext.Current.Session["username"].ToString() + "' trace = '" + errlog + "' pageno='" + pageNo + "' pagesize='" + recsPerPage.ToString() + "' firsttime='" + (pageNo < 2 ? "yes" : "no") + "' sqlpagination='" + IsDbPagination.ToString().ToLower() + "' totalrows='" + totalRows + "' getrowcount='" + getIviewRowCount.ToString().ToLower() + "' gettotalrows='false' smartview='true'><params>" + paramX + "</params>";
        }
        else
        {
            iXml = "<root " + purposeString + " name ='" + IvName + "' axpapp = '" + proj + "' sessionid = '" + sessionId + "' appsessionkey='" + HttpContext.Current.Session["AppSessionKey"].ToString() + "' username='" + HttpContext.Current.Session["username"].ToString() + "' trace = '" + errlog + "' pageno='" + pageNo + "' pagesize='" + recsPerPage.ToString() + "' firsttime='" + (pageNo < 2 ? "yes" : "no") + "' sqlpagination='" + IsDbPagination.ToString().ToLower() + "' getrowcount='" + getIviewRowCount.ToString().ToLower() + "' gettotalrows='false' smartview='true'><params>" + paramX + "</params>";
        }

        iXml += HttpContext.Current.Session["axApps"].ToString() + HttpContext.Current.Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString() + "</root> ";

        //Call service
        try
        {
            result = objWebServiceExt.CallGetIViewWS(IvName, iXml, StructureXml, WebServiceTimeout);
        }
        catch (Exception ex)
        {
            logobj.CreateLog("Error in GetData => " + ex.Message + "\n\r Call to GetIView Web Service for page no " + pageNo, sessionId, fileName, "");
        }
        string errMsg = objUtil.ParseXmlErrorNode(result);

        if (errMsg != string.Empty && pageNo > 1)
        {
            return errMsg;
        }
        else
        {
            return result;
        }
    }

    /// <summary>
    /// Storing the IView HyperLinks for Staggered Loading
    /// </summary>
    /// <returns>DataRow</returns>

    public DataRow GetHyperLinks(DataRow row, string catName, IviewData iviewObj, int rIndex, DataTable drv)
    {
        string @params = null;

        DataTable navigationInfo = new DataTable();

        if (HttpContext.Current.Session["iNavigationInfoTable"] != null)
        {
            navigationInfo = (DataTable)HttpContext.Current.Session["iNavigationInfoTable"];
        }

        for (int idx = 0; idx <= iviewObj.ColHide.Count - 1; idx++)
        {
            string sstr = null;
            string fstr = string.Empty;
            string navigateTo = string.Empty;
            string UrlProp = "";

            @params = "";

            if (idx > row.ItemArray.Length - 1)
                continue;

            string rowVal = null;
            rowVal = drv.Rows[rIndex][idx].ToString();
            if (iviewObj.ColHide[idx].ToString() == "false")
            {
                //Below code checks for custom navigation based on AxpStructConfig table - Test for without column and with column all possible values

                if (iviewObj.HypLnkNavigation != null && iviewObj.HypLnkNavigation.ContainsKey(iviewObj.headName[idx].ToString().ToLower()))
                {
                    UrlProp = iviewObj.HypLnkNavigation[headName[idx].ToString().ToLower()];
                    UrlProp = UrlProp.ToLower();
                }


                if (iviewObj.ColMap[idx].ToString() != "-")
                {
                    // spilt colmap 
                    string[] spiltOut = iviewObj.ColMap[idx].ToString().Split(',');
                    int s = 0;
                    for (s = 0; s < spiltOut.Length; s++)
                    {
                        if (spiltOut[s].ToString().IndexOf("=:") == -1)
                            continue;
                        int pos;

                        pos = (spiltOut[s].ToString().IndexOf("=:", 0) + 1);

                        fstr = spiltOut[s].ToString().Substring(0, (pos - 1));
                        if (fstr.Contains("."))
                        {
                            string[] strFs = fstr.Split('.');
                            fstr = strFs[1];
                        }

                        sstr = spiltOut[s].ToString().Substring((pos + 1));
                        int g = 0;
                        string sstrval = string.Empty;
                        g = iviewObj.ColFld.IndexOf(sstr);
                        if (g != -1 && g <= iviewObj.ColFld.Count - 1)
                        {
                            try
                            {
                                sstrval = drv.Rows[rIndex][g].ToString();


                            }
                            catch (Exception ex)
                            {
                                // MessageBox("Iview cannot be loaded fully since, Structure definition is not proper.");
                            }
                        }
                        else
                        {
                            int paramIndx = iviewObj.paramNames.IndexOf(sstr);
                            if (paramIndx > -1)
                            {
                                sstrval = iviewObj.ParamChangedVals[paramIndx].ToString();
                            }
                        }

                        if (!string.IsNullOrEmpty(sstrval))
                        {
                            sstrval = objUtil.ReverseCheckSpecialChars(sstrval);
                            sstrval = objUtil.CheckUrlSpecialChars(sstrval);
                        }

                        if (g != -1 && g <= iviewObj.ColType.Count - 1)
                        {
                            if (iviewObj.ColType[g].ToString() == "d")
                            {
                                if (!string.IsNullOrEmpty(sstrval))
                                {
                                    if (sstrval.Contains("-"))
                                    {
                                        //string[] dateformat = sstrval.Split('-');
                                        //sstrval = dateformat[0] + "/" + dateformat[1] + "/" + dateformat[2].Substring(0, 4);
                                        try
                                        {
                                            DateTime.Parse(sstrval);
                                            string[] dateformat = sstrval.Split(' ')[0].Split('-');
                                            sstrval = dateformat[0] + "/" + dateformat[1] + "/" + dateformat[2];
                                        }
                                        catch (Exception ex)
                                        {
                                            //not a date so return as it is
                                        }

                                    }
                                    string tdt = null;
                                    tdt = sstrval;
                                    sstrval = tdt.ToString();
                                }
                            }
                        }


                        @params = @params + System.Web.HttpContext.Current.Server.HtmlEncode("&" + fstr + "=" + sstrval);

                    }
                }
            }
            //@params = @params.Replace("#", "%23");
            //REPLACE # not &# with %23
            @params = Regex.Replace(@params, "(?<!&)#", "%23");
            @params = @params.Replace("+", "%2b");
            //Below replacing %3C to &l%3Ct; as '<' symbol even in encoded format cannot be recieved in the ivtstload and ivtoivload page
            @params = @params.Replace("%3C", "l%3tC;");
            string torecid = "&torecid=false";
            string hlt = string.Empty;
            string isRefreshParent = "false";

            string refresh = string.Empty;
            if (Convert.ToString(iviewObj.Axp_refresh) == "true")
            {
                refresh = "&axp_refresh=" + Convert.ToString(iviewObj.Axp_refresh);
            }
            else if (iviewObj.ColRefreshPrent[idx].ToString() != "-")
            {
                isRefreshParent = iviewObj.ColRefreshPrent[idx].ToString();
            }
            string isRefreshParentAction = "false";
            rowVal = rowVal.Replace("&nbsp;", " ");
            if (!(rowVal.StartsWith("<") && rowVal.EndsWith(">")) && !(rowVal.Contains("<") && (rowVal.Contains("</") || rowVal.Contains("/>"))))
                rowVal = objUtil.CheckSpecialChars(rowVal);
            if (colHAction[idx].ToString() != "-")
            {
                if (rowVal != "0")
                {
                    try
                    {
                        isRefreshParentAction = iviewObj.ActRefreshParent[colHAction[idx].ToString()];
                    }
                    catch (Exception ex) { }
                    row[idx] = "<a href=\"javascript:callHLaction('" + colHAction[idx].ToString() + "'," + Convert.ToInt32(catName) + ",'" + iName + "','" + UrlProp + "');setRefreshParent('" + isRefreshParentAction.ToLower() + "');\" class=l3>" + rowVal + "</a>";
                }
            }

            // set for hyper links
            if (iviewObj.ColHlink[idx].ToString() != "-" & drv.Rows[rIndex][idx].ToString() != "Grand Total" & row.ItemArray[1].ToString() != "gtot" & row[1].ToString() != "stot" & row[1].ToString() != "subhead")
            {
                string hl = null;
                string na = null;
                string isPop = string.Empty;
                string className = string.Empty;

                className = " class=l2";
                if (row[1].ToString() == "subhead")
                {
                    className = " class=ivHeaderLink";
                }
                hl = iviewObj.ColHlink[idx].ToString().Substring(0, 1);
                na = iviewObj.ColHlink[idx].ToString().Substring(1);
                if (iviewObj.ColHlinkPop[idx].ToString() == "True")
                {
                    isPop = "true";
                }


                if (hl == ":")
                {
                    try
                    {
                        na = drv.Rows[rIndex][na].ToString();
                    }
                    catch (Exception ex)
                    { }
                    //string istst = string.Empty;
                    hl = string.Empty;
                    if (!string.IsNullOrEmpty(na))
                    {
                        hl = na.ToString().Substring(0, 1);
                        na = na.ToString().Substring(1);
                    }
                }

                if (hl == "p")
                {
                    hlt = "&hltype=" + iviewObj.ColHlinktype[idx].ToString();
                    navigateTo = string.Empty;
                    if (isPop == "true")
                    {
                        if (rowVal != "0")
                            row[idx] = "<a onclick='javascript:LoadPopPage(\"Actionpage.aspx?name=" + na + hlt + @params + refresh + "\",\"\",\"\",\"" + UrlProp + "\");' class=\"handCur l2\" >" + rowVal + "</a>";
                    }
                    else
                    {
                        if (rowVal != "0")
                            row[idx] = "<a href='./Actionpage.aspx?name=" + na + hlt + @params + "' " + className + " >" + rowVal + "</a>";
                    }

                }
                else if (hl == "t")
                {
                    hlt = "&hltype=" + iviewObj.ColHlinktype[idx].ToString();
                    navigateTo = "ivtstload.aspx?tstname=";
                    if (isPop == "true")
                    {
                        if (rowVal != "0")
                        {
                            row[idx] = "<a onclick='javascript:SetColumnName(\"" + sstr + "\",\"" + Convert.ToInt32(catName) + "\",\"" + true + "\");setRefreshParent(\"" + isRefreshParent.ToLower() + "\");LoadPopPage(\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + refresh + "\",\"\",\"\",\"" + UrlProp + "\");' data-url=\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + refresh + "\" class=\"handCur l2\"  >" + rowVal + "</a>";
                        }
                    }
                    else
                    {
                        if (rowVal != "0")
                        {

                            row[idx] = "<a onclick='javascript:SetColumnName(\"" + sstr + "\",\"" + Convert.ToInt32(catName) + "\",\"" + true + "\");LoadTstFrmIview(\"" + "./ivtstload.aspx?tstname=" + na + "\",\"" + @params + hlt + torecid + "\",\"" + na + "\",\"" + UrlProp + "\");' data-url=\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + "\" " + className + " >" + rowVal + "</a>";
                        }
                    }


                }
                else if (hl == "i")
                {
                    if (iviewObj.ColType[idx].ToString() == "d")
                    {
                        if (rowVal.Contains("-"))
                        {
                            string[] dateformat = rowVal.Split('-');
                            rowVal = dateformat[0] + "/" + dateformat[1] + "/" + dateformat[2].Substring(0, 4);
                        }
                        string tdt = null;
                        tdt = rowVal;
                        rowVal = tdt.ToString();
                    }

                    hlt = "&hltype=" + iviewObj.ColHlinktype[idx].ToString();
                    hlt = string.Empty;
                    torecid = string.Empty;
                    navigateTo = string.Empty;
                    if (isPop == "true")
                    {
                        if (rowVal != "0")
                            row[idx] = "<a onclick='javascript:LoadPopPage(\"ivtoivload.aspx?ivname=" + na + @params + hlt + torecid + "\",\"\",\"\",\"" + UrlProp + "\");' data-url=\"ivtoivload.aspx?ivname=" + na + @params + hlt + torecid + "\" class=\"handCur l2\"  >" + rowVal + "</a>";
                    }
                    else
                    {
                        string urlStr = "ivtoivload.aspx?ivname=" + na;
                        @params = @params.Replace("%26", "--.--");
                        if (!row[1].ToString().Contains("stot") && (rowVal != "0"))
                            row[idx] = "<a onclick=\"javascript:OpenIviewFromIv('" + urlStr + "','" + @params + hlt + torecid + "','" + na + "','" + UrlProp + "');\"" + className + ">" + rowVal + "</a>";
                    }

                }
                //else if (hl == ":")
                //{
                //    hlt = "&hltype=" + iviewObj.ColHlinktype[idx].ToString();
                //    try
                //    {
                //        na = drv.Rows[rIndex][na].ToString();
                //    }
                //    catch (Exception ex)
                //    { }
                //    string istst = string.Empty;
                //    if (!string.IsNullOrEmpty(na))
                //    {
                //        istst = na.ToString().Substring(0, 1);
                //        na = na.ToString().Substring(1);
                //    }

                //    navigateTo = string.Empty;
                //    if (isPop == "true")
                //    {
                //        if (istst == "t")
                //        {
                //            navigateTo = "ivtstload.aspx?tstname=";
                //            if (rowVal != "0")
                //            {

                //                row[idx] = "<a onclick='javascript:SetColumnName(\"" + sstr + "\",\"" + Convert.ToInt32(catName) + "\",\"" + true + "\");setRefreshParent(\"" + isRefreshParent.ToLower() + "\");LoadPopPage(\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + refresh + "\",\"\",\"\",\"" + UrlProp + "\");' data-url=\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + refresh + "\" class=\"handCur l2\"  >" + rowVal + "</a>";
                //            }
                //        }
                //        else if (istst == "i")
                //        {
                //            if (rowVal != "0")
                //            {

                //                row[idx] = "<a onclick='javascript:LoadPopPage(\"ivtoivload.aspx?ivname=" + na + @params + hlt + torecid + "\",\"\",\"\",\"" + UrlProp + "\");' data-url=\"ivtoivload.aspx?ivname=" + na + @params + hlt + torecid + "\" class=\"handCur l2\"  >" + rowVal + "</a>";

                //            }
                //        }
                //        else if (istst == "p")
                //        {
                //            if (rowVal != "0")
                //                row[idx] = "<a onclick='javascript:LoadPopPage(\"Actionpage.aspx?name=" + na + hlt + @params + refresh + "\",\"\",\"\",\"" + UrlProp + "\");' class=\"handCur l2\" >" + rowVal + "</a>";
                //        }
                //    }
                //    else
                //    {
                //        if (istst == "t")
                //        {
                //            navigateTo = "ivtstload.aspx?tstname=";
                //            if (rowVal != "0")
                //            {


                //                row[idx] = "<a onclick='javascript:SetColumnName(\"" + sstr + "\",\"" + Convert.ToInt32(catName) + "\",\"" + true + "\");LoadTstFrmIview(\"" + "./ivtstload.aspx?tstname=" + na + "\",\"" + @params + hlt + torecid + "\",\"" + na + "\",\"" + UrlProp + "\");' data-url=\"ivtstload.aspx?tstname=" + na + @params + hlt + torecid + "\" " + className + " >" + rowVal + "</a>";
                //            }
                //        }
                //        else if (istst == "i")
                //        {
                //            string urlStr = "ivtoivload.aspx?ivname=" + na;
                //            @params = @params.Replace("%26", "--.--");
                //            if (!row[1].ToString().Contains("stot") && (rowVal != "0"))
                //                row[idx] = "<a onclick=\"javascript:OpenIviewFromIv('" + urlStr + "','" + @params + hlt + torecid + "','" + na + "','" + UrlProp + "');\"" + className + ">" + rowVal + "</a>";
                //        }
                //        else if (istst == "p")
                //        {
                //            if (rowVal != "0")
                //                row[idx] = "<a href='./Actionpage.aspx?name=" + na + hlt + @params + "' " + className + " >" + rowVal + "</a>";
                //        }
                //    }

                //}

                //navigation from hyperlink code.. left unimplemented, so not implemente in js
                string navUrl = "";
                if (!string.IsNullOrEmpty(navigateTo) && !(Convert.ToString(HttpContext.Current.Session["AxIsPop"]) == "IviewPop") && sstr != null)
                {
                    navUrl = navigateTo + na + @params + hlt + torecid;
                    if (navigationInfo.Columns.Contains(sstr))
                    {
                        if (navigationInfo.Columns.IndexOf(sstr) == 0)
                            navigationInfo.Rows.Add();
                        navigationInfo.Rows[navigationInfo.Rows.Count - 1][sstr] = navUrl;
                    }
                    else
                    {
                        navigationInfo.Columns.Add(sstr, typeof(string));
                        if (navigationInfo.Rows.Count == 0)
                            navigationInfo.Rows.Add(navUrl);
                        else
                            navigationInfo.Rows[navigationInfo.Rows.Count - 1][sstr] = navUrl;
                    }
                }
            }
        }

        return row;
    }

    /// <summary>
    /// Get IView columns Font style from axp_format column
    /// </summary>
    /// <param name="colAxp_format">{colname=font-family~font size~font weight~color,colname=font-family~font size~font weight~color }</param>
    /// <param name="paramName"></param>
    /// <returns></returns>
    public void GetIviewColFontStyle(IviewData iviewObj, string colFldVal)
    {
        if (iviewObj.FormatCols != null)
            iviewObj.FormatCols.Clear();
        else
            iviewObj.FormatCols = new ArrayList();
        if (iviewObj.ColStyle != null)
            iviewObj.ColStyle.Clear();
        else
            iviewObj.ColStyle = new ArrayList();

        if (colFldVal != string.Empty)
        {
            string[] fontStyle = colFldVal.Split(',');
            for (int indx = 0; indx < fontStyle.Length; indx++)
            {
                string[] cols = fontStyle[indx].Split('=');
                if (cols.Length > 0)
                {
                    iviewObj.FormatCols.Add(cols[0].ToString());
                    iviewObj.ColStyle.Add(cols[1].ToString());
                }
            }
        }
    }

    public ArrayList SetIviewColFontStyle(int rowIndx, IviewData iviewObj, int styleColIndx)
    {
        string[] fontStyles = new string[10];
        string strStyle = iviewObj.ColStyle[styleColIndx].ToString();

        if (strStyle != string.Empty)
        {
            fontStyles = strStyle.Split('~');
        }

        ArrayList styleVal = new ArrayList(fontStyles);
        return styleVal;

    }
    #endregion

    #region subtotal,Running total & Grand total

    ///<summary>
    ///<para>Function to apply sub total for datarows based on the array values fetched from structure xml </para>
    ///</summary>
    ///

    public DataSet OrderGridColumns(DataSet DsDataSetDB)
    {
        try
        {
            if (ColFld != null && DsDataSetDB != null)
            {
                DataColumnCollection columns = DsDataSetDB.Tables[0].Columns;
                for (int i = 0; i <= (ColFld.Count - 1); i++)
                {
                    string colname = ColFld[i].ToString();
                    if (columns.Contains(colname))
                        DsDataSetDB.Tables[0].Columns[colname].SetOrdinal(i);

                }
            }
        }
        catch (Exception ex)
        {
            //logobj.CreateLog("ordering grid columns -" + ex.Message + "", sid, "GetIviewXml-DB", string.Empty);

        }
        return DsDataSetDB;
    }

    private string GetSubTotHdrCaption(DataTable dt, string subCaptionTxt)
    {
        //subCaptionTxt=	"{course_code} -- {course_name}"	        
        string[] strTxt = subCaptionTxt.Split('}');
        StringBuilder strCaption = new StringBuilder();
        for (int i = 0; i < strTxt.Length; i++)
        {
            if (strTxt[i] != "")
            {
                string prefix = "";
                int idx = strTxt[i].ToString().IndexOf('{');
                if (idx != -1)
                {
                    if (idx != 0)
                        prefix = strTxt[i].Substring(0, idx);
                    if (prefix != "")
                        strCaption.Append(prefix + dt.Rows[0][strTxt[i].Substring(idx + 1)].ToString());
                    else
                        strCaption.Append(dt.Rows[0][strTxt[i].Substring(idx + 1)].ToString());
                }
            }
        }



        return strCaption.ToString();
    }

    public DataSet ApplySubTotal(DataSet dsOriginalData, bool IsIview, int gridPageSize = -1)
    {
        if (IsIview == true)
        {
            dsOriginalData = OrderGridColumns(dsOriginalData);
        }
        DateTime starttime = DateTime.Now;
        if (dsOriginalData.Tables[0].Rows.Count == 0)
            return dsOriginalData;
        DataTable dtrtv = new DataTable();
        if (IsIview)
            dtrtv = RunningTotalValue;

        if (RunningTotalValue.Rows.Count == 0)
        {
            RunningTotalValue = dsOriginalData.Tables[0].Clone();
            DataRow dr = RunningTotalValue.NewRow();
            RunningTotalValue.Rows.Add(dr);
            RunningPageCount = 0;
        }

        if (SubtColName.Count == 0 && (IsGrandTotal == null || IsGrandTotal == false) && !IsRunningTotal.Contains("True"))
        {
            return dsOriginalData;
        }
        else if (SubtColName.Count == 0 && IsGrandTotal == true)
        {
            if (IsRunningTotal.Contains("True"))
            {
                DataSet ds = ApplyRunningTotal(dsOriginalData, IsIview);
                return dsOriginalData = ApplyGrandTotal(ds, dsOriginalData, IsIview, Convert.ToInt32(gridPageSize));
            }
            else
            {
                return dsOriginalData = ApplyGrandTotal(dsOriginalData, dsOriginalData, IsIview, Convert.ToInt32(gridPageSize));
            }
        }
        else if (SubtColName.Count == 0 && IsRunningTotal.Contains("True"))
        {
            DataSet ds = ApplyRunningTotal(dsOriginalData, IsIview);
            return ds;
        }


        ArrayList subHeaderValues = new ArrayList();
        ArrayList subFooterValues = new ArrayList();
        DataSet dsRowRemove = new DataSet();
        DataSet dsRowData = new DataSet();
        DataSet dsFooterData = new DataSet();
        DataSet dsFullData = new DataSet();
        DataSet dsFinalData = new DataSet();
        try
        {
            dsRowRemove.Tables.Add(dsOriginalData.Tables[0].Copy());
            dsRowData.Tables.Add(dsOriginalData.Tables[0].Clone());
            dsFooterData.Tables.Add(dsOriginalData.Tables[0].Clone());
            dsFullData.Tables.Add(dsOriginalData.Tables[0].Copy());
            dsFinalData.Tables.Add(dsOriginalData.Tables[0].Clone());
            //reqired columns for sub total lstcol based on this only getting the sum of data by particular column
            List<string> lstcol = new List<string>();
            foreach (DataColumn col in dsOriginalData.Tables[0].Columns)
            {
                if (ColType[col.Ordinal].ToString() == "n")
                {
                    if (IsSumField[col.Ordinal].ToString().ToLower() == "true")
                        lstcol.Add(col.ColumnName);
                }
            }
            int rowhdCount = 0;
            int hdrowsCount = 0;
            bool rowFlag = false;
            for (int Orgrc = 0; Orgrc < dsOriginalData.Tables[0].Rows.Count; Orgrc++)
            {
                rowhdCount++;
                if (Orgrc == 0)
                {
                    #region Header & First record
                    bool hdFlag = false;
                    if (dsRowData.Tables[0].Rows.Count == 0)
                    {
                        hdrowsCount = 0;
                    }
                    for (int subhCount = 0; subhCount < SubtColName.Count; subhCount++)
                    {
                        string colName = SubtColName[SubtOrderNo.IndexOf(subhCount.ToString())].ToString();
                        string colCaption = SubtCaption[SubtOrderNo.IndexOf(subhCount.ToString())].ToString();
                        if (rowhdCount == 1)  // First header 
                        {
                            DataRow dr = dsRowData.Tables[0].NewRow();
                            for (int colCount = 0; colCount < dsRowData.Tables[0].Columns.Count; colCount++)
                            {
                                if (dsRowData.Tables[0].Columns[colCount].ToString().ToUpper() == colCaption.ToUpper())
                                {
                                    subHeaderValues.Add(dsOriginalData.Tables[0].Rows[0][colName].ToString());
                                    if (!string.IsNullOrEmpty(SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString()))
                                    {
                                        // var colh = SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString().Split('{')[1].Split('}')[0];
                                        dr[colCaption] = GetSubTotHdrCaption(dsOriginalData.Tables[0], SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString());
                                    }
                                    else
                                        dr[colCaption] = string.Empty;
                                }
                                else if (dsRowData.Tables[0].Columns[colCount].ToString().ToLower() == "axrowtype")
                                {
                                    dr[dsRowData.Tables[0].Columns[colCount].ToString()] = "subhead";
                                }
                            }
                            dsRowData.Tables[0].Rows.Add(dr);
                            hdrowsCount++;
                        }
                        else if (subHeaderValues[subhCount].ToString() != dsOriginalData.Tables[0].Rows[0][colName].ToString()) // checking previous header and current header subHeaderValues.Count > 0 &&
                        {
                            hdFlag = true;
                            DataRow dr = dsRowData.Tables[0].NewRow();
                            for (int colCount = 0; colCount < dsRowData.Tables[0].Columns.Count; colCount++)
                            {
                                if (dsRowData.Tables[0].Columns[colCount].ToString().ToUpper() == colCaption.ToUpper())
                                {
                                    subHeaderValues[subhCount] = dsOriginalData.Tables[0].Rows[0][colName].ToString();
                                    if (!string.IsNullOrEmpty(SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString()))
                                    {
                                        //var colh = SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString().Split('{')[1].Split('}')[0];
                                        dr[colCaption] = GetSubTotHdrCaption(dsOriginalData.Tables[0], SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString());
                                    }
                                    else
                                        dr[colCaption] = string.Empty;
                                }
                                else if (dsRowData.Tables[0].Columns[colCount].ToString().ToLower() == "axrowtype")
                                {
                                    dr[dsRowData.Tables[0].Columns[colCount].ToString()] = "subhead";
                                }
                            }
                            dsRowData.Tables[0].Rows.Add(dr);
                            hdrowsCount++;
                        }
                        else if (hdFlag == true) // checking previous header and current header, sumtimes sub condtions may be change 
                        {
                            hdFlag = false;
                            DataRow dr = dsRowData.Tables[0].NewRow();
                            for (int colCount = 0; colCount < dsRowData.Tables[0].Columns.Count; colCount++)
                            {
                                if (dsRowData.Tables[0].Columns[colCount].ToString().ToUpper() == colCaption.ToUpper())
                                {
                                    subHeaderValues[subhCount] = dsOriginalData.Tables[0].Rows[0][colName].ToString();
                                    if (!string.IsNullOrEmpty(SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString()))
                                    {
                                        //var colh = SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString().Split('{')[1].Split('}')[0];
                                        dr[colCaption] = GetSubTotHdrCaption(dsOriginalData.Tables[0], SubtHeader[SubtOrderNo.IndexOf(subhCount.ToString())].ToString());
                                    }
                                    else
                                        dr[colCaption] = string.Empty;
                                }
                                else if (dsRowData.Tables[0].Columns[colCount].ToString().ToLower() == "axrowtype")
                                {
                                    dr[dsRowData.Tables[0].Columns[colCount].ToString()] = "subhead";
                                }
                            }
                            dsRowData.Tables[0].Rows.Add(dr);
                            hdrowsCount++;
                        }
                    }
                    dsRowData.Tables[0].Rows.Add(dsOriginalData.Tables[0].Rows[Orgrc].ItemArray);
                    dsRowRemove.Tables[0].Rows.RemoveAt(Orgrc);
                    subFooterValues.Clear();
                    for (int colCount = 0; colCount < SubtColName.Count; colCount++)
                    {
                        string colName = SubtColName[SubtOrderNo.IndexOf(colCount.ToString())].ToString();
                        subFooterValues.Add(dsOriginalData.Tables[0].Rows[0][colName].ToString());
                    }
                    #endregion

                    #region last row footer
                    if (dsRowRemove.Tables[0].Rows.Count == 0 && dsRowData.Tables[0].Rows.Count != 0) // last row sub footer sum and adding rows
                    {
                        DataTable dstt = dsFullData.Tables[0].Copy();
                        DataTable dstts = dsFullData.Tables[0].Clone();
                        for (int colCount = 0; colCount < SubtColName.Count; colCount++)
                        {
                            string col = SubtColName[SubtOrderNo.IndexOf(colCount.ToString())].ToString();
                            if (subFooterValues[colCount].ToString() != "")
                            {
                                string strFilterExp = col.ToUpper() + " = '" + subFooterValues[colCount].ToString().ToUpper() + "'";
                                DataTable query = dstt.Select(strFilterExp).CopyToDataTable();
                                DataRow dr = dstts.NewRow();
                                int ilstc = 0;
                                for (int rowCol = 0; rowCol < query.Columns.Count; rowCol++)
                                {
                                    try
                                    {
                                        if (query.Columns[rowCol].ToString().ToUpper() == col.ToUpper())
                                        {
                                            try
                                            {
                                                dr[col] = "";
                                            }
                                            catch
                                            {
                                                dr[col] = DBNull.Value;
                                            }
                                        }
                                        else if (lstcol.Count > ilstc)
                                        {
                                            if (query.Columns[rowCol].ToString() == lstcol[ilstc].ToString())
                                            {
                                                dr[query.Columns[rowCol].ToString()] = query.Compute("SUM([" + lstcol[ilstc] + "])", "");
                                                ilstc++;
                                            }
                                        }
                                        else
                                        {
                                            dr[query.Columns[rowCol].ToString()] = "";
                                        }
                                    }
                                    catch
                                    {
                                        dr[query.Columns[rowCol].ToString()] = DBNull.Value;
                                    }
                                }
                                dstt.Rows.Clear();
                                dstt = query;
                                dstts.Rows.Add(dr);
                            }
                        }


                        for (int ColName = 0; ColName < SubtColName.Count; ColName++) // based on footer condition adding sum records 
                        {
                            DataRow dr = dsFooterData.Tables[0].NewRow();
                            string colcat = SubtCaption[SubtOrderNo.IndexOf(((SubtColName.Count) - (ColName + 1)).ToString())].ToString();
                            int ilstc = 0;
                            for (int rowCol = 0; rowCol < dsRowData.Tables[0].Columns.Count; rowCol++)
                            {
                                try
                                {
                                    if (dsRowData.Tables[0].Columns[rowCol].ToString().ToUpper() == colcat.ToUpper())
                                    {
                                        try
                                        {
                                            dr[colcat] = SubtFooter[SubtOrderNo.IndexOf(((SubtColName.Count) - (ColName + 1)).ToString())].ToString();
                                        }
                                        catch
                                        {
                                            dr[colcat] = DBNull.Value;
                                        }
                                    }
                                    else if (lstcol.Count > ilstc)
                                    {
                                        if (dsRowData.Tables[0].Columns[rowCol].ToString() == lstcol[ilstc].ToString())
                                        {
                                            dr[dsRowData.Tables[0].Columns[rowCol].ToString()] = dstts.Rows[((SubtColName.Count) - (ColName + 1))][rowCol];
                                            ilstc++;
                                        }
                                        else if (dsRowData.Tables[0].Columns[rowCol].ToString().ToLower() == "axrowtype")
                                        {
                                            dr[dsRowData.Tables[0].Columns[rowCol].ToString()] = "stot";
                                        }
                                    }
                                    else if (dsRowData.Tables[0].Columns[rowCol].ToString().ToLower() == "axrowtype")
                                    {
                                        dr[dsRowData.Tables[0].Columns[rowCol].ToString()] = "stot";
                                    }
                                    else
                                    {
                                        dr[dsRowData.Tables[0].Columns[rowCol].ToString()] = "";
                                    }
                                }
                                catch
                                {
                                    dr[dsRowData.Tables[0].Columns[rowCol].ToString()] = DBNull.Value;
                                }
                            }
                            dsFooterData.Tables[0].Rows.Add(dr);
                        }
                        rowFlag = true;
                    }
                    #endregion
                }
                else  // for second data row or more data rows  
                {
                    #region records & footer
                    bool footerRowFlag = false;
                    int subConCount = 0, subConTrueCount = 0;
                    for (int ConCount = 0; ConCount < SubtColName.Count; ConCount++)
                    {
                        string colName = SubtColName[SubtOrderNo.IndexOf(ConCount.ToString())].ToString();
                        subFooterValues[ConCount] = dsOriginalData.Tables[0].Rows[0][colName].ToString();
                        if (dsOriginalData.Tables[0].Rows[Orgrc - 1][colName].ToString() == dsOriginalData.Tables[0].Rows[Orgrc][colName].ToString()) // both previous row & current row with condition is same
                        {
                            subConCount++;
                            subConTrueCount++;
                            if ((SubtColName.Count - 1) == ConCount)
                            {
                                if (dsOriginalData.Tables[0].Rows.Count - 1 != Orgrc)
                                {
                                    if (dsOriginalData.Tables[0].Rows[Orgrc][colName].ToString() != dsOriginalData.Tables[0].Rows[Orgrc + 1][colName].ToString() && subConCount > 1)
                                    {
                                        subConCount--;
                                    }
                                }
                                dsRowData.Tables[0].Rows.Add(dsOriginalData.Tables[0].Rows[Orgrc].ItemArray);
                                dsRowRemove.Tables[0].Rows.RemoveAt(0);

                                if (IsRunningTotal.Contains("True"))
                                {
                                    for (int rowColName = 0; rowColName < dsRowData.Tables[0].Columns.Count; rowColName++)
                                    {
                                        if (IsRunningTotal[rowColName].ToString() == "True")
                                        {
                                            var sumval = 0;
                                            if (int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) > RunningPageCount)
                                            {
                                                if (RunningTotalValue.Rows.Count == 0)
                                                {
                                                    sumval += 0;
                                                }
                                                else if (RunningTotalValue.Rows.Count == int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()))
                                                {
                                                    sumval += (string.IsNullOrEmpty(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString())) ? 0 : int.Parse(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString());
                                                }
                                                sumval += (string.IsNullOrEmpty(dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName].ToString())) ? 0 : int.Parse(dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName].ToString());
                                                if (Orgrc + hdrowsCount == 2)
                                                {
                                                    dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName] = sumval;
                                                }
                                                sumval += int.Parse(dsRowData.Tables[0].Rows[Orgrc + hdrowsCount][rowColName].ToString());

                                                if (int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) == RunningTotalValue.Rows.Count)
                                                {
                                                    DataRow dr = RunningTotalValue.NewRow();
                                                    dr[dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()] = sumval;
                                                    RunningTotalValue.Rows.Add(dr);
                                                }
                                                else
                                                {
                                                    RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString())][dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()] = sumval;
                                                }
                                            }
                                            else
                                            {
                                                if (Orgrc == 1)
                                                {
                                                    sumval += (string.IsNullOrEmpty(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString())) ? 0 : int.Parse(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsRowData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString());
                                                    if (RunningTotalValue.Rows.Count > int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()))
                                                    {
                                                        if (RunningTotalValue.Rows.Count == int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) + 1)
                                                        {

                                                        }
                                                        else
                                                        {
                                                            RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) + 1].Delete();
                                                        }
                                                    }
                                                }
                                                sumval += (string.IsNullOrEmpty(dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName].ToString())) ? 0 : int.Parse(dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName].ToString());
                                                if (Orgrc + hdrowsCount == 2)
                                                {
                                                    dsRowData.Tables[0].Rows[(Orgrc + hdrowsCount) - 1][rowColName] = sumval;
                                                }
                                                sumval += int.Parse(dsRowData.Tables[0].Rows[Orgrc + hdrowsCount][rowColName].ToString());
                                            }
                                            dsRowData.Tables[0].Rows[Orgrc + hdrowsCount][rowColName] = sumval;
                                        }
                                    }
                                }

                                if (dsOriginalData.Tables[0].Rows.Count - 1 != Orgrc)
                                {
                                    if (dsOriginalData.Tables[0].Rows[Orgrc][colName].ToString() == dsOriginalData.Tables[0].Rows[Orgrc + 1][colName].ToString())
                                    {
                                        footerRowFlag = true;
                                        rowFlag = false;
                                    }
                                    else
                                    {
                                        footerRowFlag = false;
                                        rowFlag = true;
                                    }
                                }
                                else
                                {
                                    rowFlag = true;
                                }
                            }
                        }
                        else // both previous row & current row with condition is not Equal break the condition 
                        {
                            if (subConCount == 0)
                            {
                                subConCount = SubtColName.Count;
                            }
                            else
                            {
                                subConCount++;
                            }
                            rowFlag = true;
                            break;
                        }
                    }
                    if (footerRowFlag == false) // sum of footer rows data
                    {
                        #region sum of footer rows data
                        DataTable dstt = new DataTable();
                        if (SubtColName.Count == 1)
                        {
                            dstt = dsRowData.Tables[0].Copy();
                        }
                        else
                        {
                            dstt = dsFullData.Tables[0].Copy();
                        }
                        DataTable dstts = dsFullData.Tables[0].Clone();
                        for (int scolName = 0; scolName < SubtColName.Count; scolName++)
                        {
                            string col = SubtColName[SubtOrderNo.IndexOf(scolName.ToString())].ToString();
                            if (subFooterValues[scolName].ToString() != "")
                            {
                                string strFilterExp = col.ToUpper() + " = '" + subFooterValues[scolName].ToString().ToUpper() + "'";
                                DataTable query = dstt.Select(strFilterExp).CopyToDataTable();

                                DataRow dr = dstts.NewRow();
                                int ilstc = 0;
                                for (int rowCol = 0; rowCol < query.Columns.Count; rowCol++)
                                {
                                    try
                                    {
                                        if (query.Columns[rowCol].ToString().ToUpper() == col.ToUpper())
                                        {
                                            dr[col] = "";
                                        }
                                        else if (lstcol.Count > ilstc)
                                        {
                                            if (query.Columns[rowCol].ToString() == lstcol[ilstc].ToString())
                                            {
                                                dr[query.Columns[rowCol].ToString()] = query.Compute("SUM([" + lstcol[ilstc] + "])", "");
                                                ilstc++;
                                            }
                                        }
                                        else
                                        {
                                            dr[query.Columns[rowCol].ToString()] = "";
                                        }
                                    }
                                    catch
                                    {
                                        dr[query.Columns[rowCol].ToString()] = DBNull.Value;
                                    }
                                }

                                dstt.Rows.Clear();
                                dstt = query;
                                dstts.Rows.Add(dr);
                            }
                        }
                        #endregion

                        #region binding footer rows
                        for (int scolName = 0; scolName < SubtColName.Count; scolName++) // binding footer rows with text & sum values
                        {
                            if (SubtColName.Count == subConCount && subConTrueCount != 0)
                            {
                                subConCount = 1;
                            }
                            if (subConCount > scolName)
                            {
                                if (SubtFooter[SubtOrderNo.IndexOf((SubtColName.Count - (scolName + 1)).ToString())].ToString() != "")
                                {
                                    DataRow dr = dsFooterData.Tables[0].NewRow();
                                    string colCaption = SubtCaption[SubtOrderNo.IndexOf((SubtColName.Count - (scolName + 1)).ToString())].ToString();
                                    int ilstc = 0;
                                    for (int rowColName = 0; rowColName < dsRowData.Tables[0].Columns.Count; rowColName++)
                                    {
                                        try
                                        {
                                            if (dsRowData.Tables[0].Columns[rowColName].ToString().ToUpper() == colCaption.ToUpper())
                                            {
                                                try
                                                {
                                                    dr[colCaption] = SubtFooter[SubtOrderNo.IndexOf((SubtColName.Count - (scolName + 1)).ToString())].ToString();
                                                }
                                                catch (Exception ex)
                                                {
                                                    dr[colCaption] = DBNull.Value;
                                                }

                                            }
                                            else if (lstcol.Count > ilstc)
                                            {
                                                if (dsRowData.Tables[0].Columns[rowColName].ToString() == lstcol[ilstc].ToString())
                                                {
                                                    if (dsRowData.Tables[0].Columns[rowColName].ToString() != "SNO")
                                                    {
                                                        dr[dsRowData.Tables[0].Columns[rowColName].ToString()] = dstts.Rows[(SubtColName.Count - (scolName + 1))][rowColName];
                                                    }
                                                    ilstc++;
                                                }
                                                else if (dsRowData.Tables[0].Columns[rowColName].ToString().ToLower() == "axrowtype")
                                                {
                                                    dr[dsRowData.Tables[0].Columns[rowColName].ToString()] = "stot";
                                                }
                                            }
                                            else if (dsRowData.Tables[0].Columns[rowColName].ToString().ToLower() == "axrowtype")
                                            {
                                                dr[dsRowData.Tables[0].Columns[rowColName].ToString()] = "stot";
                                            }
                                            else
                                            {
                                                dr[dsRowData.Tables[0].Columns[rowColName].ToString()] = "";
                                            }
                                        }
                                        catch
                                        {
                                            dr[dsRowData.Tables[0].Columns[rowColName].ToString()] = DBNull.Value;
                                        }
                                    }

                                    dsFooterData.Tables[0].Rows.Add(dr);
                                }
                                bool colspaft = Convert.ToBoolean(SubtSpaceFooter[SubtOrderNo.IndexOf((SubtColName.Count - (scolName + 1)).ToString())].ToString());
                                if (colspaft == true)
                                {
                                    DataRow drs = dsFooterData.Tables[0].NewRow();
                                    drs[1] = "emptyRow";
                                    dsFooterData.Tables[0].Rows.Add(drs);
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                }
                if (rowFlag == true) // making final dataset and clearing remaing datasets 
                {
                    #region Final Dataset
                    rowFlag = false;
                    dsFinalData.Tables[0].Merge(dsRowData.Tables[0]);
                    dsRowData.Tables[0].Rows.Clear();
                    dsFinalData.Tables[0].Merge(dsFooterData.Tables[0]);
                    dsFooterData.Tables[0].Rows.Clear();
                    dsOriginalData.Tables[0].Rows.Clear();
                    dsOriginalData = dsRowRemove.Copy();
                    dsOriginalData.AcceptChanges();
                    Orgrc = -1;
                    #endregion
                }
            }

            if (IsGrandTotal == true) // Grand total method 
            {
                dsFinalData = ApplyGrandTotal(dsFinalData, dsFullData, IsIview, Convert.ToInt32(gridPageSize));
            }
            dsFullData.Clear();
            if (!IsIview)
                RunningPageCount = int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString());
        }
        catch (Exception ex)
        {
            logobj.CreateLog("Exception in ApplySubTotal : " + ex.Message, HttpContext.Current.Session["nsessionid"].ToString(), "openiview-" + iName, string.Empty);
            HttpContext.Current.Response.Redirect(Constants.ERRPATH + ex.Message);
        }
        DsDataSetDB = dsFinalData;
        return dsFinalData;
    }

    ///<summary>
    ///<para>Function to apply running  total for datarows based on the array values fetched from structure xml </para>
    ///</summary>
    public DataSet ApplyRunningTotal(DataSet dsFinalData, bool IsIview)
    {

        DateTime starttime = DateTime.Now;
        if (IsRunningTotal.Contains("True"))
        {
            for (int rowCount = 0; rowCount < dsFinalData.Tables[0].Rows.Count; rowCount++)
            {
                for (int rowColName = 0; rowColName < dsFinalData.Tables[0].Columns.Count; rowColName++)
                {
                    if (IsRunningTotal[rowColName].ToString() == "True")
                    {
                        var sumval = 0;
                        if (rowCount > 0)
                        {
                            sumval = int.Parse(dsFinalData.Tables[0].Rows[rowCount - 1][rowColName].ToString());
                        }
                        sumval += int.Parse(dsFinalData.Tables[0].Rows[rowCount][rowColName].ToString());
                        if (int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) > RunningPageCount)
                        {
                            if (RunningTotalValue.Rows.Count == 0)
                            {
                                sumval += 0;
                            }
                            else if (RunningTotalValue.Rows.Count == int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()))
                            {
                                sumval += (string.IsNullOrEmpty(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString())) ? 0 : int.Parse(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString());
                            }

                            if (int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) == RunningTotalValue.Rows.Count)
                            {
                                DataRow dr = RunningTotalValue.NewRow();
                                dr[dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()] = sumval;
                                RunningTotalValue.Rows.Add(dr);
                            }
                            else
                            {
                                RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString())][dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()] = sumval;
                            }
                        }
                        else
                        {
                            if (rowCount == 0)
                            {
                                sumval += (string.IsNullOrEmpty(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString())) ? 0 : int.Parse(RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) - 1][dsFinalData.Tables[0].Columns[rowColName].ColumnName.ToString()].ToString());
                                if (RunningTotalValue.Rows.Count > int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()))
                                {
                                    if (RunningTotalValue.Rows.Count == int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) + 1)
                                    {
                                    }
                                    else
                                    {
                                        RunningTotalValue.Rows[int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()) + 1].Delete();
                                    }
                                }
                            }
                        }
                        dsFinalData.Tables[0].Rows[rowCount][rowColName] = sumval;
                    }
                }
            }
        }
        if (!IsIview)
            RunningPageCount = int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString());


        return dsFinalData;
    }

    ///<summary>
    ///<para>Function to apply grand total for datarows based on the array values fetched from structure xml </para>
    ///</summary>
    public DataSet ApplyGrandTotal(DataSet dsFinalData, DataSet dsFullDataGT, bool IsIview, int gridPageSize)
    {
        if (dsFullDataGT.Tables[0].Rows.Count > 0)
        {
            DataTable dtgtr = new DataTable();
            if (IsIview)
                dtgtr = GrandTotalRow;

            //var pageNo = decimal.Parse(HttpContext.Current.Session["AxDefaultPageSize"].ToString().Trim());
            var pageNo = Convert.ToDecimal(gridPageSize);
            //var recCount = decimal.Parse(DsDataSetDB.Tables[1].Rows[0]["IVIEWCOUNT"].ToString());
            var recCount = decimal.Parse(directDbtotalRows.ToString());
            var fnCount = Math.Ceiling(recCount / pageNo);
            if (fnCount <= 0)
                fnCount = 1;
            if (fnCount == int.Parse(HttpContext.Current.Session["iv_noofpages"].ToString()))
            {
                bool gdflag = false;
                DataRow dr = dsFinalData.Tables[0].NewRow();
                foreach (DataColumn col in dsFullDataGT.Tables[0].Columns)
                {
                    try
                    {
                        if (ColType[col.Ordinal].ToString() == "n" && dsFinalData.Tables[0].Columns[col.Ordinal].ToString() != "SNO")
                        {
                            if (IsSumField[col.Ordinal].ToString().ToLower() == "true")
                                if (GrandTotalRow.Rows.Count == 0)
                                {
                                    dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "");
                                }
                                else
                                {
                                    if (Convert.ToInt32(ColDec[col.Ordinal].ToString()) == 0)
                                    {
                                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = Int64.Parse(GrandTotalRow.Rows[0][col.Ordinal].ToString()) + Int64.Parse(dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "").ToString());

                                    }
                                    else
                                    {
                                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = decimal.Parse(GrandTotalRow.Rows[0][col.Ordinal].ToString()) + decimal.Parse(dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "").ToString());
                                    }
                                }
                        }
                        else
                        {

                            if (ColType[col.Ordinal].ToString() == "c" && gdflag == false && col.Caption.ToLower() != "rowno" && col.Caption.ToLower() != "axrowtype")
                            {
                                gdflag = true;
                                dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = "Grand Total";
                            }
                            if (col.Caption.ToLower() == "axrowtype")
                            {
                                dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = "gtot";
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        logobj.CreateLog("Exception in ApplyGrandTotal : " + ex.Message, HttpContext.Current.Session["nsessionid"].ToString(), "openiview-" + iName, string.Empty);
                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = DBNull.Value;
                    }
                }
                dsFinalData.Tables[0].Rows.Add(dr);
            }
            else
            {
                if (GrandTotalRow.Rows.Count == 0)
                {
                    GrandTotalRow = dsFinalData.Tables[0].Clone();
                }
                bool gdflag = false;
                DataRow dr = GrandTotalRow.NewRow();
                foreach (DataColumn col in dsFullDataGT.Tables[0].Columns)
                {
                    try
                    {
                        if (ColType[col.Ordinal].ToString() == "n" && dsFinalData.Tables[0].Columns[col.Ordinal].ToString() != "SNO")
                        {
                            if (IsSumField[col.Ordinal].ToString().ToLower() == "true")
                                if (GrandTotalRow.Rows.Count == 0)
                                {
                                    dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "");
                                }
                                else
                                {
                                    if (Convert.ToInt32(ColDec[col.Ordinal].ToString()) == 0)
                                    {
                                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = Int64.Parse(GrandTotalRow.Rows[0][col.Ordinal].ToString()) + Int64.Parse(dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "").ToString());

                                    }
                                    else
                                    {
                                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = decimal.Parse(GrandTotalRow.Rows[0][col.Ordinal].ToString()) + decimal.Parse(dsFullDataGT.Tables[0].Compute("SUM([" + col.Caption + "])", "").ToString());
                                    }
                                }
                        }
                        else
                        {

                            if (ColType[col.Ordinal].ToString() == "c" && gdflag == false && col.Caption.ToLower() != "rowno" && col.Caption.ToLower() != "axrowtype")
                            {
                                gdflag = true;
                                dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = "Grand Total";
                            }
                            if (col.Caption.ToLower() == "axrowtype")
                            {
                                dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = "gtot";
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        logobj.CreateLog("Exception in ApplyGrandTotal : " + ex.Message, HttpContext.Current.Session["nsessionid"].ToString(), "openiview-" + iName, string.Empty);
                        dr[dsFinalData.Tables[0].Columns[col.Ordinal].ToString()] = DBNull.Value;
                    }
                }
                if (GrandTotalRow.Rows.Count == 0)
                {
                    GrandTotalRow.Rows.Add(dr);
                }
                else
                {
                    GrandTotalRow.Rows.RemoveAt(0);
                    GrandTotalRow.Rows.Add(dr);
                }
            }

            if (IsIview)
                grandTotalRow = dtgtr;
        }
        return dsFinalData;
    }

    #endregion

    public Boolean IsStructureUpdated(string ivUpdateOn, string ivName)
    {
        string isDevInstance = string.Empty;
        if (HttpContext.Current.Session["AxDevInstance"] != null)
            isDevInstance = HttpContext.Current.Session["AxDevInstance"].ToString();

        if (isDevInstance == "true")
        {
            string proj = HttpContext.Current.Session["project"].ToString();
            string sessionID = HttpContext.Current.Session["nsessionid"].ToString();

            System.Globalization.CultureInfo culture = default(System.Globalization.CultureInfo);
            culture = new System.Globalization.CultureInfo("en-GB");
            string errorLog = logobj.CreateLog("Loading Structure.", sessionID, "IsUpdatedIview", "new");

            string dbTimeSql = string.Empty, dbTimeRes = string.Empty, fileName = string.Empty;
            ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
            dbTimeSql = "<sqlresultset axpapp='" + proj + "' sessionid='" + sessionID + "' trace='" + errorLog + "' appsessionkey='" + HttpContext.Current.Session["AppSessionKey"].ToString() + "' username='" + HttpContext.Current.Session["username"].ToString() + "'><sql>select updatedon upd from iviews where name ='" + ivName + "' and blobno=1</sql>";

            dbTimeSql += HttpContext.Current.Session["axApps"].ToString() + HttpContext.Current.Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString() + "</sqlresultset>";
            fileName = "IviewStructure-" + ivName;
            logobj.CreateLog("Calling DB to check structure is updated.", sessionID, fileName, "");
            //Call service
            dbTimeRes = objWebServiceExt.CallGetChoiceWS(ivName, dbTimeSql);

            if (dbTimeRes.Contains(Constants.ERROR) == true)
            {
                dbTimeRes = dbTimeRes.Replace(Constants.ERROR, "");
                dbTimeRes = dbTimeRes.Replace("</error>", "");
                dbTimeRes = dbTimeRes.Replace("\n", "");
                throw (new Exception(dbTimeRes));
            }
            else
            {
                string dbResult = dbTimeRes.ToUpper();
                XmlDocument xmlDoc = new XmlDocument();
                XmlNode dbUpdatedtime = null;
                xmlDoc.LoadXml(dbResult);
                dbUpdatedtime = xmlDoc.SelectSingleNode("//RESPONSE/ROW/UPD");

                string dbUpdDate = dbUpdatedtime.InnerText;
                DateTime dbDate;
                if (HttpContext.Current.Session["axdb"] != null)
                {
                    if (HttpContext.Current.Session["axdb"].ToString().ToLower() == "ms sql")
                        culture = new System.Globalization.CultureInfo("en-US");
                }


                try
                {
                    dbDate = DateTime.Parse(dbUpdDate, culture);
                }
                catch (System.FormatException ex)
                {

                    dbDate = DateTime.Parse(dbUpdDate);
                }

                DateTime cacheTime;
                string objTime = ivUpdateOn;
                if (object.ReferenceEquals(objTime, ""))
                    objTime = "01/01/2000 00:00:01 AM";

                try
                {
                    cacheTime = DateTime.Parse(objTime, culture);
                }
                catch (System.FormatException ex)
                {
                    cacheTime = DateTime.Parse(objTime);
                }

                logobj.CreateLog("    Cache time is " + cacheTime + ", DB time is " + dbDate + ", transid-" + ivName, sessionID, "GetIvCacheDetails", "");
                return dbDate > cacheTime;
            }
        }
        else
        {
            return false;
        }
    }
    // Code to write properties from xml

    public void GetAxpStructConfig(IviewData iviewObj)
    {

        Dictionary<string, string> TempHypLnkNavigation = new Dictionary<string, string>();
        Dictionary<string, string> TempActBtnNavigation = new Dictionary<string, string>();
        DataTable dsConfig = iviewObj.DsIviewConfig;
        var strConfigProps = dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "navigation")
                   .Select(x => new { hyperLnkName = x.Field<string>("HLINK"), propVal = x.Field<string>("PROPSVAL"), btnProp = x.Field<string>("SBUTTON") }).ToList();
        var strAutoSPlit = dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "autosplit")
                   .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
        if (strAutoSPlit.Count > 0)
        {
            if (HttpContext.Current.Session["MobileView"] != null && HttpContext.Current.Session["MobileView"].ToString() == "True")
                iviewObj.AxpIsAutoSplit = "false";
            else
                iviewObj.AxpIsAutoSplit = strAutoSPlit[0].splitVal;
        }

        try
        {

            iviewDataWSRows = Convert.ToInt32(dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "fetchsize").Select(x => x.Field<string>("PROPSVAL")).FirstOrDefault());
            iviewDataWSRows = iviewDataWSRows < 1 ? 1000 : iviewDataWSRows;
            GrdPageSize = iviewDataWSRows.ToString();
        }
        catch (Exception ex)
        {
        }

        try
        {
            exportVerticalAlign = dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "exportverticalalign").Select(x => x.Field<string>("PROPSVAL")).FirstOrDefault().ToString();
        }
        catch (Exception ex)
        {
        }

        try
        {
            WebServiceTimeout = Convert.ToInt32(dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "webservice timeout").Select(x => x.Field<string>("PROPSVAL")).FirstOrDefault());
        }
        catch (Exception ex)
        {
        }

        try
        {
            var strDisSPlit = dsConfig.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "disablesplit")
               .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
            if (strDisSPlit.Count > 0)
                iviewObj.AxpIviewDisableSplit = strDisSPlit[0].splitVal;
            if (strConfigProps.Count > 0)
            {
                for (int i = 0; i < strConfigProps.Count; i++)
                {
                    if (strConfigProps[i].hyperLnkName != null)
                    {
                        if (TempHypLnkNavigation.ContainsKey(strConfigProps[i].hyperLnkName))
                            TempHypLnkNavigation[strConfigProps[i].hyperLnkName] = strConfigProps[i].propVal;
                        else
                            TempHypLnkNavigation.Add(strConfigProps[i].hyperLnkName, strConfigProps[i].propVal);
                    }
                    else if (strConfigProps[i].btnProp != null)
                    {
                        if (TempActBtnNavigation.ContainsKey(strConfigProps[i].btnProp))
                            TempActBtnNavigation[strConfigProps[i].btnProp] = strConfigProps[i].propVal;
                        else
                            TempActBtnNavigation.Add(strConfigProps[i].btnProp, strConfigProps[i].propVal);
                    }
                }
                iviewObj.HypLnkNavigation = TempHypLnkNavigation;
                iviewObj.ActBtnNavigation = TempActBtnNavigation;
            }
        }
        catch (Exception ex)
        {
            logobj.CreateLog("Exception while getting config data from DB-" + ex.Message + "", HttpContext.Current.Session["nsessionid"].ToString(), "GetAxpStructConfig-" + IName, "new");
        }

    }
}

