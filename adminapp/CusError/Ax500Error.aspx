﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Ax500Error.aspx.cs" Inherits="aspx_Ax500Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom Error</title>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />

    <!-- Forcefully closing the Dimmer -->
    <script type="text/javascript">
        window.parent.closeFrame();
    </script>
</head>
<body onload="window.parent.closeFrame();" dir='<%=direction%>'>
    <form id="form1" runat="server">
        <div>
            <h3 class="error err">
                <asp:Label ID="lblOops" runat="server" meta:resourcekey="lblOops"> Oops! there seems to be some problem. Please try later.</asp:Label>
            </h3>
        </div>
    </form>
</body>
</html>
