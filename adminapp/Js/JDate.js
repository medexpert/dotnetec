﻿var dtCulture = eval(callParent('glCulture'));
var dtFormat = "";


var dateLocale = new Array();
var dateFormat = new Array();

dateLocale[0] = "en-us";
dateFormat[0] = "mm/dd/yyyy";
dateLocale[1] = "en-gb";
dateFormat[1] = "dd/mm/yyyy";


function GetDateFormat(culture) {
    var dtStrFormat = "";
    var ind = $j.inArray(culture.toLowerCase(), dateLocale);
    if (ind != -1) {
        dtStrFormat = dateFormat[ind];
    } else {
        dtStrFormat = "dd/mm/yyyy";
    }
    return dtStrFormat;
}

function GetDateStr(date, oldFormat, newFormat) {
    if (oldFormat == newFormat)
        return date;
        
    var dd = "";
    var mm = "";
    var yy = "";
    var dtStr = date.split("/");
    var newDt = "";
    
    if (oldFormat == "dd/mm/yyyy") {
        dd = dtStr[0];
        mm = dtStr[1];
        yy = dtStr[2];
    }
    else {
        dd = dtStr[1];
        mm = dtStr[0];
        yy = dtStr[2];
    }

    if (newFormat == "dd/mm/yyyy") {
        newDt = dd + "/" + mm + "/" + yy;
    }
    else {
        newDt = mm + "/" + dd + "/" + yy;
    }

    return newDt;
}

//Destination format is dd/mm/yyyy.
//function DateSaveFormat(date) {
//    if (dtCulture.toLowerCase() == "en-gb")
//        return date;
//    else if (dtCulture.toLowerCase() == "en-us") {
//        var newDt = GetDateStr(date, "mm/dd/yyyy", "dd/mm/yyyy");
//        return newDt;
//    }
//    else {
//        //needs to be handled
//    }
//}

//source date string is always in the dd/mm/yyyy format.
function DateDisplayFormat(date) {
    if (date == "")
        return date;
    if (dtCulture.toLowerCase() == "en-gb")
        return date;
    else if (dtCulture.toLowerCase() == "en-us") {
        //convert from dd/mm/yyyy to mm/dd/yyyy       
        var newDt = GetDateStr(date, "dd/mm/yyyy", "mm/dd/yyyy");
        return newDt;
    }
    else {
        //needs to be handled
    }
}

