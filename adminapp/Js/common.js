﻿CheckSuccCallBackValidate();

function CheckSuccCallBackValidate() {
    var returnValue = false;
    var pervsid = eval(callParent('hdnpsid'));
    if (typeof pervsid != "undefined" && pervsid != "") {
        getSession("nsessionid", function (data) {
            if (data.d != undefined && data.d != pervsid) {
                var cllMainPage = callParentNew("", "getParent");
                cllMainPage.location.href = "../aspx/mainnew.aspx";
                returnValue = true;
            }
        });
    }
    return returnValue;
}

function SuccCallBackValidate(result) {
    if (typeof result != "undefined" && result == "Duplicate session") {
        CheckSuccCallBackValidate();
        return true;
    }
    return false;
}

try {
    var mainPageUrl = "";
    var isMainPage = true;
    var linkHref = window.location.href;
    if (linkHref.toLowerCase().indexOf("/aspx/tstruct.aspx") != -1 || linkHref.toLowerCase().indexOf("/aspx/iview.aspx") != -1 || linkHref.toLowerCase().indexOf("/aspx/ivtoivload.aspx") != -1 || linkHref.toLowerCase().indexOf("/aspx/ivtstload.aspx") != -1) {
        var parentPage = callParentNew("", "getParent");
        if (parentPage != undefined && parentPage.document != undefined)
            mainPageUrl = parentPage.document.URL;

        if (mainPageUrl == undefined || mainPageUrl == "" || mainPageUrl.toLowerCase().indexOf("/aspx/mainnew.aspx") == -1) {
            isMainPage = false;
            mainPageUrl = GetMainPageUrl();
        }

        if (!isMainPage && mainPageUrl.toLowerCase().indexOf("/aspx/mainnew.aspx") > -1) {
            LoadMainPage(mainPageUrl, linkHref);
        }
    }
} catch (ex) {
    console.warn("Error in GetMainPageUrl call : " + ex.message);
}

function LoadMainPage(mainPageUrl, linkHref) {
    try {
        //local storage for new tab pageload is not used since it is loading duplicate pages when multiple new tabs are opened simultaneously.
        var useLocalStorage = false;
        if (useLocalStorage && typeof (Storage) !== "undefined") {

            if (localStorage["PageToLoad_" + projName] == undefined || localStorage["PageToLoad_" + projName] == "")
                localStorage["PageToLoad_" + projName] = linkHref;
            else
                localStorage["PageToLoad_" + projName] = localStorage["PageToLoad_" + projName] + "♠" + linkHref;
            mainPageUrl += "#url=local";
        } else {
            SetSession({
                key: "PageToLoad",
                val: linkHref,
                appendVal: true,
                delimiter: "♠",
                cb: function (data) {
                    mainPageUrl += "#url=sess";
                    window.location.href = mainPageUrl;
                }
            })
        }
    } catch (ex) {
        console.warn("Error in LoadMainPage call : " + ex.message);
    }
}

function GetMainPageUrl() {
    var mainPageUrl = "";
    try {
        var useLocalStorage = false;
        if (useLocalStorage && typeof (Storage) !== "undefined") {
            mainPageUrl = localStorage["MainPageUrl_" + projName];
        } else {
            var appUrl = window.location.href;
            if (appUrl.indexOf("/aspx/") !== -1) {
                appUrl = appUrl.substring(0, appUrl.indexOf("/aspx/") + 6);
                mainPageUrl = appUrl + "Mainnew.aspx";
            }
        }
    } catch (ex) {
        console.warn("Error in GetMainPageUrl call : " + ex.message);
    }
    return mainPageUrl;
}

function ClearRedisKeys(rsKeys) {
    var clearrsKeys = rsKeys.split("*");
    var settings = redisNodeApiSettings();
    settings.url = eval(callParent('nodeApi')) + "clearkeys";
    settings.data.rKey = JSON.stringify(clearrsKeys); //keys to clear
    settings.data.rUtl = eval(callParent('redisUtl')); //reddis utl
    $.ajax(settings).done(function (response) {
        if (response.status == true) { }
    });
}

function PeriodicRefreshCache() {
    var settings = redisNodeApiSettings();
    settings.url = eval(callParent('nodeApi')) + "periodicrefresh";
    settings.data.utl = eval(callParent('utl'));
    settings.data.rUtl = eval(callParent('redisUtl'));
    $.ajax(settings).done(function (response) {
        if (response.status == true) { }
    });
}

var redisNodeApiSettings = function () {
    return {
        "async": true,
        "crossDomain": true,
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {}
    }
}

function commonReadyTasks() {
    if (typeof appGlobalVarsObject == "undefined") {
        appGlobalVarsObject = callParentNew("appGlobalVarsObject");
    }

    closeParentFrame();
    ShowDimmer("false");
    checkSuccessAxpertMsg();

    try{
        if(window.frameElement.id == "middle1"){
            callParentNew("middle1URL=", window.location.href);
        }
    }catch(ex){}

    resetMainPageUI();
}


/**
 * @description: common main page reset functionality
 * @author Prashik
 * @date 2019-10-18
 */
function resetMainPageUI() {
    try {
        $(callParentNew(appGlobalVarsObject._CONSTANTS.search.staging.div.substr(1), "id")).val("");
        $(callParentNew(appGlobalVarsObject._CONSTANTS.search.staging.divparent.substr(1), "class")).removeClass("open");
    } catch (ex) { }

    try {

        var homePageBody = $(callParentNew("homePageBody", "class"));
        if (homePageBody && !homePageBody.hasClass("enableExpandCollapse")) {
            homePageBody.removeClass("overlay-open");
            homePageBody.find(".overlay").fadeOut();
        }
    } catch (ex) { }
}

function validateFilesize() {
    var attachement = document.getElementById('filMyFile');
    attachement.onchange = function () {
        var fileType = "File";
        var attachmentSizeMB = callParentNew("axAttachmentSize", "axAttachmentSize") == undefined ? 1 : callParentNew("axAttachmentSize", "axAttachmentSize");
        var fileUploadLimit = attachmentSizeMB * 1024 * 1024;
        if ($j("#imgUploadLimit").length)
            fileType = "Image";
        for (var i = 0; i < attachement.files.length; i++) {
            var file = attachement.files[i];
            if (file == null || file.size > fileUploadLimit) {
                if (file == null) {
                    if (fileType == "File") $("#fileuploadsts").text("[" + eval(callParent('lcm[80]')).replace("{0}", attachmentSizeMB) + "]");
                    else $("#fileuploadsts").text("[" + eval(callParent('lcm[81]')).replace("{0}", attachmentSizeMB) + "]");
                } else {
                    if (fileType == "File") $("#fileuploadsts").text("[" + eval(callParent('lcm[82]')).replace("{0}", attachmentSizeMB) + "]");
                    else $("#fileuploadsts").text("[" + eval(callParent('lcm[83]')).replace("{0}", attachmentSizeMB) + "]");
                }
                ResetFileUploadProperties();
            } else {
                var fileName = file.name;
                if (fileName.indexOf("+") > -1) {
                    $("#fileuploadsts").text("[FileName should not contain '+' character.]");
                    ResetFileUploadProperties();
                } else if (fileName.indexOf(".") == -1) {
                    $("#fileuploadsts").text(eval(callParent('lcm[362]')));
                    ResetFileUploadProperties();
                }


                else {
                    if (fileType == "File") $("#fileuploadsts").text("[Click 'Attach' button]");
                    else $("#fileuploadsts").text("[Click 'Upload' button]");
                    $("#fileuploadsts").css('color', 'Green');
                    $(".file-upload.active .file-select .file-select-button").css({ "background": "#3fa46a" });
                    $(".file-upload.active .file-select").css({ "border-color": "#3fa46a" });
                }
            }
        }
    }
}

function ValidateFileExtension(filename, fileType, CustomList) {
    var fileExtensionImage = ['gif', 'png', 'jpg', 'jpeg', 'bmp', 'tiff', 'tif', 'heic', 'jfif'];
    //var filename = $("#filMyFile").val();
    var fileExtension = ['bmp', 'jpg', 'jpeg', 'pjpeg', 'png', 'doc', 'gif', 'docx', 'xml', 'xls', 'ppt', 'pdf', 'txt', 'xlsx', 'pwd', 'tiff', 'tif', 'dib', 'jfif', 'heic', 'xlxs', 'csv'];
    var ext = filename.split('.').pop().toLowerCase();

    if (fileType == "image") {
        if ($.inArray(ext, fileExtensionImage) == -1)
            return false;
        else
            return true;
    }
    if (fileType == "file") {

        if ($.inArray(ext, fileExtension) == -1)
            return false;
        else
            return true;
    }
    if (fileType == "CustomList") {

        if ($.inArray(ext, CustomList == -1))
            return false;
        else
            return true;
    }
}

function ResetFileUploadProperties() {
    $("#fileuploadsts").css('color', '#DB2222');
    $("#filMyFile")[0].value = "";
    $("#noFile")[0].textContent = eval(callParent('lcm[66]'));
    $(".file-upload.active .file-select .file-select-button").css({ "background": "grey" });
    $(".file-upload.active .file-select").css({ "border-color": "grey" });
}

function checkSuccessAxpertMsg() {
    // window.content.localStorage["axpertSuccessMessage"];
    var SuccessMsg = window.localStorage.getItem("axpertSuccessMessage");
    if (SuccessMsg) {
        showAlertDialog('success', SuccessMsg)
        valoutput = window.localStorage.removeItem("axpertSuccessMessage");
    }
}


$(document).click(function (e) {
    if (document.URL.indexOf("aspx/mainnew.aspx") === -1) {
        callParentNew("createMobileQRcode(destroy)", "function")
    }


    var container = $('#menuContentWrapper', window.parent.document);
    var btnContainer = $('#hamburgerMenuIcon', window.parent.document);
    //var themeContainer = $(".panel-control,#customizer", window.parent.document);

    // if the target of the click isn't the container nor a descendant of the container
    if (!$(e.target).hasClass('menuName') && !$(e.target).hasClass('hasSubChildIcon') && !$(e.target).hasClass('menuBgIcon') && !$(e.target).hasClass('hasSubChild') && !$(e.target).hasClass('hirarchy') && $(e.target).attr("id") != "menuContentWrapper" && $(e.target).attr("id") != "hamburgerMenuIcon" && $(e.target).parents("#hamburgerMenuIcon").length === 0 && $(e.target).parents("#menuContentWrapper").length === 0 && $(e.target).parents('.mnuPagination').length === 0) {
        if (btnContainer.hasClass('is-active')) {
            //container.hide();
            btnContainer.find('button').click();
            return;
        }
    }

    // if the target of the click isn't the container nor a descendant of the container
    try {
        if (window["currentThemeColor"] === undefined) {
            var themeContainer = $(".panel-control,#customizer", window.parent.document);
            if (!themeContainer.is(e.target) && themeContainer.has(e.target).length === 0) {
                if ($('#customizer', window.parent.document).hasClass('panel-open')) {
                    $('#customizer', window.parent.document).removeClass('panel-open');
                }
            };
        } else if (window["currentThemeColor"]) {
            var themeContainer = $(".panel-control,#customizer");
            if (!themeContainer.is(e.target) && themeContainer.has(e.target).length === 0) {
                if ($('#customizer').hasClass('panel-open')) {
                    $('#customizer').removeClass('panel-open');
                }
            };
        }
    } catch (ex) {
        console.log(ex.message);
    }
});

//disabling backdrop for jquery confirm boxes//
function disableBackDrop(task, disESCKey) {
    if (task == "destroy") {
        jQuery(document).unbind("keyup.cnfrmPopupKU");
        jQuery(document).unbind("keydown.cnfrmPopupKD")
        return;
    }

    jQuery(document).bind("keyup.cnfrmPopupKU", function (event) { });
    jQuery(document).bind("keydown.cnfrmPopupKD", function (event) {
        var elemntsToCheck = 'button[tabindex!="-1"],a[tabindex!="-1"],input[tabindex!="-1"],select[tabindex!="-1"],textarea[tabindex!="-1"]';
        if (!event.shiftKey && event.keyCode == 9) {
            //tab key
            if ($(document.activeElement)[0] == $(".jconfirm-buttons button:last")[0]) {
                event.preventDefault()
                $(".jconfirm-box").find(elemntsToCheck).first().focus();
            }
        } else if (event.shiftKey && event.keyCode == 9) {
            if ($(document.activeElement)[0] == $(".jconfirm-box").find(elemntsToCheck).first()[0]) {
                event.preventDefault();
                $(".jconfirm-box").find(elemntsToCheck).last().focus();
            }
        } else if (event.keyCode == 27) {
            event.preventDefault();
            if (disESCKey)
                $(".jconfirm-box .jconfirm-buttons .coldbtn").click();
        }

    })
    $(".jconfirm-buttons button").each(function () {
        var txt = $(this).text();
        $(this).prop('title', txt.charAt(0).toUpperCase() + txt.slice(1))
    });
    $(".jconfirm-buttons .hotbtn").focus();

}



function callParent(name, type) {
    var hirarchy = "window";
    try {
        if (typeof eval(name) === "undefined") {
            hirarchy = hirarchy + ".parent";
        }
    } catch (ex) {
        hirarchy = hirarchy + ".parent";
    }
    var maxParentToCheck = 10;
    var reachedCntPlcHlder = false;

    for (var i = 0; i < maxParentToCheck; i++) {
        try {
            if (!reachedCntPlcHlder && checkForCntPlcHldrFrame(hirarchy)) {
                reachedCntPlcHlder = true;
            } else if (reachedCntPlcHlder) {
                break;
                return false;
            }
            if (type == "id" || type == "class") {
                var byName = "";
                type == "id" ? byName = "Id" : byName = "sByClassName";
                if (eval(hirarchy + ".document.getElementBy" + byName + "('" + name + "')") == null) {
                    hirarchy = hirarchy + ".parent";
                    continue;
                } else {
                    return hirarchy + ".document";
                    break;
                }
            } else {
                if (type == 'function') {
                    var fParams = name.substr(name.indexOf('(') + 1, name.indexOf(')')).slice(0, -1);
                    var checkName = name.substr(0, name.indexOf('('));
                    //tst.substr(tst.indexOf('(') + 1, tst.indexOf(')')).slice(0, -1)

                } else {
                    checkName = name;
                }
                if (typeof eval(hirarchy + "." + checkName) == 'undefined') {
                    hirarchy = hirarchy + ".parent";
                    continue;
                } else {
                    if (type == 'function') {
                        return fParams == "" ? hirarchy + "." + checkName + "()" : hirarchy + "." + checkName + "('" + fParams + "')";
                    } else {
                        return hirarchy + "." + name;
                    }
                }
            }
        } catch (ex) {
            hirarchy = hirarchy + ".parent";
            continue;
            //console.log(ex.message);
        }
    }


}

function checkForCntPlcHldrFrame(hirarchy) {
    return eval(hirarchy + ".leftMenuWrapper") != undefined ? true : false;
}

//Author - ManiKanta
//changes 
//if type=id,class then instead of hirarchy elem will be returned ===> $("#mani",callParentNew("mani","id")) -> $(callParentNew("mani","id"));
//to set the value to a variable need to call as callParentNew("varName=","value"); first parameter need to end with '='
//@usage
//-->@function<-- type needs to be function ==> callParentNew("funcNam(withParams)","function") ==>@returns the executed value of the function
//-->@elem/class<-- type need to be class ==> callParentNew("className","class") ==>@returns the *JAVASCRIPT* object
//-->@elem/id<-- type need to be id ==> callParentNew("ID","id") ==>@returns the *JAVASCRIPT* object **To user both and class as JQUERY object need to call as $(callParentNew("idClassName","id/class"));
//-->@variable<-- need to pass type ==> callParentNew("variableName") ==>@returns value of the variable



function callParentNew(name = "", type) {
    var hirarchy = window;
    //verifying if name ends with equalto
    let isAssigningValue = false;
    name = name.trim();
    if (name.substr(name.length - 1) === "=") {
        name = name.slice(0, -1);
        name = name.trim();
        isAssigningValue = true;
    }

    try {
        if (typeof window[name] === "undefined") {
            hirarchy = window.parent;
        }
    } catch (ex) {
        hirarchy = window.parent;
    }
    var maxParentToCheck = 10;
    var reachedCntPlcHlder = false;

    for (var i = 0; i < maxParentToCheck; i++) {
        try {
            if (!reachedCntPlcHlder && checkForCntPlcHldrFrameNew(hirarchy)) {
                reachedCntPlcHlder = true;
            } else if (reachedCntPlcHlder) {
                break;
                return false;
            }
            if (type == "id" || type == "class") {
                var byName = type == "id" ? "ById" : "sByClassName";
                var elem = hirarchy.document["getElement" + byName](name);
                if (elem == null) {
                    hirarchy = hirarchy.parent;
                    continue;
                } else {
                    return elem;
                    break;
                }
            } else {
                if (type == 'function') {
                    var fParams = name.substr(name.indexOf('(') + 1, name.indexOf(')')).slice(0, -1);
                    var checkName = name.substr(0, name.indexOf('('));
                    //tst.substr(tst.indexOf('(') + 1, tst.indexOf(')')).slice(0, -1)

                } else if (type == 'getParent') {
                    checkName = name || 'leftMenuWrapper';
                } else {
                    checkName = name;
                }

                if (typeof hirarchy[checkName] === "undefined") {
                    hirarchy = hirarchy.parent;
                    continue;
                } else {
                    if (type == 'function') {
                        return hirarchy[checkName].apply(this, fParams.split(','));//This = > presetn context
                    } else {
                        if (type === 'getParent') {
                            return hirarchy;
                        }
                        if (isAssigningValue) {
                            hirarchy[name] = type;
                            return true;
                        }
                        return hirarchy[name];
                    }
                }
            }
        } catch (ex) {
            hirarchy = hirarchy.parent;
            continue;
            //console.log(ex.message);
        }
    }


}

function checkForCntPlcHldrFrameNew(hirarchy) {
    return hirarchy.leftMenuWrapper != undefined ? true : false;
}

function chooseFileUpload(event) {
    if (event.keyCode == 13)
        $('#filMyFile').click();
};




function valSessByApi(response) {
    //if (response.statusCode == "401")
    //    parent.location.href = "sess.aspx";
    if ((response.statusCode === 401 && response.errMsg.toLowerCase() == "not a valid session") || response.statusCode === 403) {
        //for 403 access forbidden will come
        parent.location.href = "sess.aspx";
    }
}

function closeRemodalPopupOnEsc() {
    if ($j("#popupIframeRemodal", parent.document).attr("src") != undefined) {
        $j(document).keyup(function (e) {
            if (e.keyCode === 27) {
                $j('.remodal-close', parent.document).click();
            }
        });
    }

}

function IFrameModalDialogTabEvents(inputFirst) {
    var elemntsToCheck = 'button[tabindex!="-1"],a[tabindex!="-1"],input[tabindex!="-1"],select[tabindex!="-1"],radio[tabindex!="-1"]';
    var inputs = $('#form1').find(elemntsToCheck).filter(':visible').not(':disabled');
    firstInput = inputs.first();
    lastInput = inputs.last();
    if (inputFirst != "")
        $("." + inputFirst).addClass("firstFocusable");
    else
        firstInput.addClass("firstFocusable");
    lastInput.addClass("lastFocusable");
    $(".firstFocusable").focus();

    $(".lastFocusable").on('keydown.tabRot', function (e) {
        if ((e.which === 9 && !e.shiftKey)) {
            e.preventDefault();
            $(".firstFocusable").focus();
        }
    });
    $(".firstFocusable").on('keydown.tabRot', function (e) {
        if ((e.which === 9 && e.shiftKey)) {
            e.preventDefault();
            $(".lastFocusable").focus();
        }
    });
    $('.file-select').on("keydown", function (event) {
        if (event.which == 13) {
            $('#filMyFile').click();
        }
    })

}

//-------------
//To display an IFrame/HTML content in a Bootstrap modal dialog
//parameters
//-------------
//header: header of the dialog
//modalType: modal size - large(lg), medium(md), small(sm), extra small(xs) - optional - default: md
//height: height of the modal(px or %) - optional - default: 250px
//isIFrame: iframe or html content - true if it is iframe, false otherwise
//htmlContentOriFrameUrl: iFrame url or html content
//requiredConfirm: confirm option when click on cancel - true or false - optional - default: false
//dialogLoadEvent:  load event, triggers once the dialog loaded completely - optional
//dialogUnloadEvent: unload event, triggers before the dialog closes - optional
//-------------
//example: displayBootstrapModalDialog("Change Password", "md","250px", true, "../aspx/cpwd.aspx?remark=chpwd")
var dialogWindow = "";

function displayBootstrapModalDialog(header, modalType, height, isIFrame, htmlContentOriFrameUrl, requiredConfirm, dialogLoadEvent, dialogUnloadEvent) {

    height = height == "" || height == undefined ? "250px" : height;
    modalType = modalType == "" || modalType == undefined ? "md" : modalType;
    requiredConfirm = requiredConfirm == "" || requiredConfirm == undefined ? false : true;
    closeIcon = requiredConfirm ? "confirm-modal-close" : "modal";
    langAlignment = eval(callParent('gllangType')) === "en" ? "right" : "left"; // modal close icon alignment - English(right) & Arabic(left)

    var modalId = "divModal" + header.split(" ").join("");
    var iFrameId = "iFrame" + header.split(" ").join("");
    if ($("#" + modalId).next().hasClass("modal-backdrop"))
        $("#" + modalId).next().remove();
    $("#" + modalId).remove();

    var modalHtml = '';
    setTimeout(function () {
        modalHtml += '<div class="custom-dialog modal fade" id="' + modalId + '" role="dialog" data-iframe-src="' + (isIFrame ? htmlContentOriFrameUrl : '') + '"  data-keyboard="false" data-confirm-leave="' + requiredConfirm + '" data-iframe-id="' + iFrameId + '"><div class="modal-dialog modal-' + modalType + '"><div class="modal-content"><div class="modal-header"> <button id="btnModalClose" type="button" class="close" title="' + eval(callParent('lcm[249]')) + '" data-dismiss=' + closeIcon + ' style="float:' + langAlignment + '">&times;</button><h4 class="modal-title" id="divModalHeader">' + header + '</h4></div><div class="modal-body">';

        if (isIFrame)
            modalHtml += '<iframe src="' + htmlContentOriFrameUrl + '" style="width: 100%;height: ' + height + ';border: none;" id="' + iFrameId + '"></iframe>';
        else
            modalHtml += '<div id="htmlContentOriFrameUrl" style="height:' + height + '">' + (typeof htmlContentOriFrameUrl == "string" ? htmlContentOriFrameUrl : "") + '</div>';
        $("body").append(modalHtml);
        typeof htmlContentOriFrameUrl == "object" ? $("#htmlContentOriFrameUrl").append(htmlContentOriFrameUrl) : "";
        //call back function for modal load event 
        $("#" + modalId).on("shown.bs.modal", function () {
            if (dialogLoadEvent)
                dialogLoadEvent(); //triggers once the dialog is loaded
        });

        //call back function for modal unload event
        $("#" + modalId).on("hide.bs.modal", function () {
            if (dialogUnloadEvent)
                dialogUnloadEvent(); //triggers before dialog closes
        });

        //for any reason if the iframe modal dialog is not opened then remove disabled option for utilities & settings menu after 10 sec
        setTimeout(function () {
            removeUnclickableMenuCssClass();
        }, 10000)

        if (isIFrame) {
            iframe = eval(callParent(iFrameId, "id") + ".getElementById('" + iFrameId + "')");
            if (iframe != null) {
                iframe.onload = function () { //activate events once iframe modal is loaded
                    $("#" + modalId).modal("show").draggable({
                        handle: ".modal-header"
                    });
                    removeUnclickableMenuCssClass();

                    //if any error occurs in form then it will redirect to err.aspx page, to avoid go back option from modal dialog we are hiding goback button. otherwise it will go bcak to previous opened forms in modal dialog itself
                    var mdlIframeSrc = $("#" + modalId).attr("data-iframe-src");
                    var curIframeSrc = $(iframe).contents().find("#form1").attr("action");
                    if (typeof curIframeSrc != "undefined" && mdlIframeSrc.indexOf(curIframeSrc.substr(1)) < 0) {
                        $(iframe).contents().find("#goback").hide();
                    }

                    dialogWindow = document.getElementById(iFrameId);

                    $(iframe.contentWindow.document).keydown(function (e) {
                        if (e.which == 27) { //close popup on click of escape key
                            var requiredConfirm = $('.modal').filter(".in").attr("data-confirm-leave") === "true";
                            var iFrameId = $('.modal').filter(".in").attr("data-iframe-id");
                            if (requiredConfirm && dialogWindow != undefined && dialogWindow.contentWindow.checkIfFormChanges != undefined && dialogWindow.contentWindow.checkIfFormChanges()) { //if required confirm dialog displays it, otherwise close current dialog
                                dialogWindow.contentWindow.ConfirmLeave();
                            } else
                                closeModalDialog();
                        }
                    });

                    if (requiredConfirm) {
                        $("#" + modalId).on('shown.bs.modal', function () {
                            //display confirm dialog if user clicks on modal close icon
                            $("[data-dismiss='confirm-modal-close']").unbind('click').bind('click', function () {
                                if (dialogWindow != undefined && dialogWindow.contentWindow.checkIfFormChanges != undefined && dialogWindow.contentWindow.checkIfFormChanges())
                                    dialogWindow.contentWindow.ConfirmLeave();
                                else
                                    closeModalDialog();
                                actionsClicked = ""
                            });

                            if ($('.modal.in').length > 0) {
                                $(".navbar-inner").unbind('click').bind('click', function (e) { //display confirm dialog if user clicks on header menu 
                                    //$(e.target)[0].id != "settingdropdown" &&
                                    if ($(e.target)[0].id != "Li1") {
                                        if (dialogWindow != undefined && dialogWindow.contentWindow.checkIfFormChanges != undefined && dialogWindow.contentWindow.checkIfFormChanges())
                                            if ($(e.target).closest('li').length > 0 && $(e.target).closest('li')[0].id == "li_Logout") {
                                                dialogWindow.contentWindow.removeConfirmDialog();
                                            }
                                            else
                                                dialogWindow.contentWindow.ConfirmLeave();
                                        else
                                            closeModalDialog();
                                    }
                                });
                            }

                            $("body :not(.modal-header)").find(".modal").unbind('click').bind('click', function (e) {
                                //display confirm dialog if user clicks on outside but not Modal header
                                if ($(e.target)[0].id == modalId && dialogWindow != undefined && dialogWindow.contentWindow.checkIfFormChanges != undefined && dialogWindow.contentWindow.checkIfFormChanges())
                                    dialogWindow.contentWindow.checkIfFormChanges();
                                else
                                    closeModalDialog();
                                actionsClicked = ""
                            });

                            $("body").find(".modal").unbind('click').bind('click', function (e) {
                                //if form is still loading, prevent if user clicks on outside the dialog
                                if ($(e.target).hasClass("fade") && $(e.target).hasClass("in") && dialogWindow != undefined && dialogWindow.contentWindow.waitDialogUntilFormLoad != undefined && dialogWindow.contentWindow.waitDialogUntilFormLoad) {
                                    return;
                                }
                                //display confirm dialog if user clicks on modal outside
                                if ($(e.target).hasClass("fade") && $(e.target).hasClass("in") && dialogWindow != undefined && dialogWindow.contentWindow.checkIfFormChanges != undefined && dialogWindow.contentWindow.checkIfFormChanges())
                                    dialogWindow.contentWindow.ConfirmLeave();
                                else if ($(e.target).hasClass("fade") && $(e.target).hasClass("in"))
                                    closeModalDialog();
                                actionsClicked = ""
                            });
                        });
                    } else { //close modal dialog if user clicks on modal outside
                        $("#" + modalId).on('shown.bs.modal', function () {
                            $(".navbar-inner").unbind('click').bind('click', function () {
                                $(".navbar-inner").unbind('click');
                                if ($('.modal.in').length > 0) {
                                    closeModalDialog();
                                }
                            })
                        }).on("hidden.bs.modal", function () {
                            $(".navbar-inner").unbind('click');
                            closeModalDialog();

                        }).on("hide.bs.modal", function () {
                            $(".navbar-inner").unbind('click');

                            if ($("#" + modalId).next().hasClass("modal-backdrop"))
                                $("#" + modalId).next().remove();
                            setTimeout(function () {
                                $("#" + modalId).remove();
                            }, 300)
                        });
                    }
                };
            }
        } else {
            $("#" + modalId).modal("show").draggable({
                handle: ".modal-header"
            });
            $("body").keydown(function (e) {
                if (e.which == 27) {
                    closeModalDialog();
                    e.stopPropagation();
                }
            });
        }
    }, 200);
}

//To close the Bootstrap modal dialog
function closeModalDialog() {
    if ($(".jconfirm-box").length) {
        $(".jconfirm-box .jconfirm-buttons .coldbtn").click();
        return;
    }

    var count = 0;
    $(".modal").each(function () {
        count++;
    });
    //to remove active modal dialog
    if ($($(".modal")[count - 1]).next().hasClass("modal-backdrop"))
        $($(".modal")[count - 1]).next().remove();
    $($(".modal")[count - 1]).modal("hide");

    setTimeout(function () {
        $($(".modal")[count - 1]).remove();
        //if multiple popup's are opened, if any dialog is closed then make the current dialog as iframeid
        iFrameId = $(".custom-dialog").attr("data-iframe-id");
        dialogWindow = document.getElementById(iFrameId);
    }, 300)
    removeUnclickableMenuCssClass();
}

/**
 * generic LoadEvent Callback to remove headers from IView and TStruct.
 * @author Prashik
 * @Date   2019-06-27T11:45:39+0530
 */
function removePageHeaders() {
    var glType = gllangType;
    var isRTL = false;
    var styleObj = {};
    if (glType == "ar") {
        isRTL = true;
        styleObj = {
            "color": "rgb(255, 255, 255)",
            "opacity": "1",
            "position": "absolute",
            "top": "4px",
            "left": "20px",
            "background-color": "rgb(173, 167, 167)",
            "width": "25px",
            "height": "25px",
            "border-radius": "25px"
        };
    }
    else {
        isRTL = false;
        styleObj = {
            "color": "rgb(255, 255, 255)",
            "opacity": "1",
            "position": "absolute",
            "top": "4px",
            "right": "20px",
            "background-color": "rgb(173, 167, 167)",
            "width": "25px",
            "height": "25px",
            "border-radius": "25px"
        };
    }

    var divId = $(".custom-dialog.modal[id^=divModal]:eq(0)").attr("id");
    var divIframeId = $(".custom-dialog.modal[id^=divModal]:eq(0)").data("iframeId");

    $("#" + divId + " .modal-content .modal-body").css({ "padding": "0px" });
    $("#" + divId + " .modal-content .modal-header").hide();
    $("#" + divId + " .modal-content .modal-body").prepend($("#" + divId + " .modal-content #btnModalClose").detach());
    $("#" + divId + " .modal-content #btnModalClose").css(styleObj);

    $("#" + divId + " .modal-dialog").css({ "margin": "0px" }).parent().css({ "top": "25px" });
}

$(document).ready(function () {
    $("div").scroll(function () {
        $('#ui-datepicker-div').hide();
    });

    //var parFrame = $("#axpiframe", parent.document);
    //var midFrm = $("#middle1", parent.document);    
    //if(parFrame.length > 0 && !eval(callParent('IsContentAxpIframe'))){
    //    parFrame[0].style.display = "none";
    //    parFrame[0].src = "";
    //    if(midFrm.length > 0)
    //        midFrm[0].style.width = "100%";
    //}

    //if Import data, Export data, Config app, Fast data utility menu drop down clicked then make the settings dropdown unclickable until the dialog opens
    $(".modal-dialog-events, .modal-dialog-events-settings").click(function () {
        $("#settingdropdown, #ExportImportCogIcon").addClass("menu-not-allowed");
    });

    ////if Change password/Trace file log menu drop down clicked then make the utilities dropdown unclickable until the dialog opens
    //$(".modal-dialog-events-settings").click(function () {
    //    $("#ExportImportCogIcon").addClass("menu-not-allowed");
    //})


});

$(window).bind("load", function () {
    if ($(window.frameElement).hasClass('bootStrapModal')) return false;
    if (window && window.frameElement && window.frameElement.id == "popupIframeRemodal") return false;
    if (window && window.frameElement && window.frameElement.id.indexOf('iFrame') === 0) return false;
    var newLoadUrl = window.location.href;
    callParentNew("updateAppLinkObj(" + newLoadUrl + ")", "function");
});

//when settings dropdown option & utilities down option clicks on same time 2 popup's will appear at a time, to avoid this once click on one dropdown making other dropdown unclickable, once the first popup is loaded successfully then remove that dropdown unclickable class
function removeUnclickableMenuCssClass() {
    $("#settingdropdown, #ExportImportCogIcon").removeClass("menu-not-allowed");
}


function getSession(key, cb) {
    $.ajax({
        type: "POST",
        url: "../WebService.asmx/GetSession",
        cache: false,
        async: false,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({ key: key }),
        dataType: "json",
        success: function (data) {
            if (typeof cb == "function") {
                cb(data);
            }
        },
    });
}

function SetSession({ key, val, cb, appendVal = false, delimiter = "" }) {
    try {
        $.ajax({
            type: "POST",
            url: "../WebService.asmx/SetSession",
            cache: false,
            async: true,
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify({ key: key, val: val, appendVal: appendVal, delimiter: delimiter }),
            dataType: "json",
            success: function (data) {
                if (typeof cb == "function") {
                    cb(data);
                }
            },
        });
    } catch (ex) {
        console.warn("Error in SetSession Ajax call : " + ex.message);
    }
}

function GetNewTabPageSess({ key, cb }) {
    try {
        $.ajax({
            type: "POST",
            url: "../WebService.asmx/GetNewTabPageSess",
            cache: false,
            async: true,
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify({ key: key }),
            dataType: "json",
            success: function (data) {
                if (typeof cb == "function") {
                    cb(data);
                }
            },
        });
    } catch (e) {
        console.warn("Error in GetNewTabPageSess Ajax call : " + ex.message);
    }
}

//Moved this function from CommonBuilder.js to here.
function tabFocusEventHandler(task, parentId, loopInsideParent, enableForParentOnly, exceptions) {
    if (task === "unbind") {
        $(document).off("keydown.loopingInsideParent" + parentId);
        return;
    }

    var allFocusableElems = "a:visible,input:enabled,select:enabled,button:enabled,textarea:enabled";
    var parentElem = $("#" + parentId);
    var allFocusableElemsList = parentElem.find(allFocusableElems).not("[tabindex='-1']");
    allFocusableElemsList.first().focus();
    if (loopInsideParent) {
        var firstFocusableElem = allFocusableElemsList.first();
        var lastFocusableElem = allFocusableElemsList.last();
        $(document).off("keydown.loopingInsideParent" + parentId);

        var docToEnable;
        if (enableForParentOnly != undefined && enableForParentOnly)
            docToEnable = parentElem;
        else
            docToEnable = $(document);

        docToEnable.on("keydown.loopingInsideParent" + parentId, function (event) {
            if (!event.shiftKey && event.keyCode == 9) {
                //tab key
                if ($(document.activeElement)[0] == lastFocusableElem[0]) {
                    event.preventDefault()
                    firstFocusableElem.focus();
                }
            } else if (event.shiftKey && event.keyCode == 9) {
                //tab key + shift
                if ($(document.activeElement)[0] == firstFocusableElem[0]) {
                    event.preventDefault();
                    lastFocusableElem.focus();
                }
            }
        })
    }
}

function UnlockConfigApp({ forceUnlock = false, cb }) {
    try {
        $.ajax({
            type: "POST",
            url: "../WebService.asmx/UnlockConfigApp",
            data: JSON.stringify({ forceUnlock: forceUnlock }),
            cache: false,
            async: true,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (typeof cb == "function") {
                    cb(data);
                }
            },
        });
    }
    catch (ex) {
        console.warn("Error in UnlockConfigApp : " + ex.message);
    }
}
var AxClColors = {
    //clNone: "#1FFFFFFF",
    clNone: "transparent",
    clAqua: "#00FFFF",
    clBlack: "#000000",
    clBlue: "#0000FF",
    clCream: "#FFFBF0",
    clDkGray: "#808080",
    clFuchsia: "#FF00FF",
    clGray: "#808080",
    clGreen: "#008000",
    clLime: "#00FF00",
    clLtGray: "#C0C0C0",
    clMaroon: "#800000",
    clMedGray: "#A0A0A4",
    clMoneyGreen: "#C0DCC0",
    clNavy: "#000080",
    clOlive: "#808000",
    clPurple: "#800080",
    clRed: "#FF0000",
    clSilver: "#C0C0C0",
    clSkyBlue: "#A6CAF0",
    clTeal: "#008080",
    clWhite: "#FFFFFF",
    clYellow: "#FFFF00",
};


function CheckSpecialChars(str) {
    if (str == undefined || str == null)
        return "";

    str = str.replace(/&/g, '&amp;');
    str = str.replace(/</g, '&lt;');
    str = str.replace(/>/g, '&gt;');
    str = str.replace(/'/g, '&apos;');
    str = str.replace(/"/g, '&quot;');
    return str;
}

function ReverseCheckSpecialChars(str) {

    if (str == undefined || str == null)
        return "";

    str = str.replace(/&amp;/g, '&');
    str = str.replace(/&lt;/g, '<');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&apos;/g, '\'');
    str = str.replace(/&quot;/g, '"');
    str = str.replace(/&nbsp;/g, ' ');
    return str;
}


function ReverseCheckSpecialCharsInQuery(str) {
    if (str == undefined || str == null)
        return "";

    str = str.replace(/&amp;/g, '&');
    str = str.replace(/&lt;/g, '<');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&apos;/g, '\'');
    str = str.replace(/&quot;/g, '"');
    return str;
}


function CheckUrlSpecialChars(str) {
    if (str == undefined || str == null)
        return "";
    str = str.replace(/%/g, '%25');
    str = str.replace(/#/g, '%23');
    str = str.replace(/&/g, '%26');
    str = str.replace(/'/g, '%27');
    str = str.replace(/\"/g, '%22');
    str = str.replace(/\+/g, '%2b');
    str = str.replace(/</g, '%3C');
    str = str.replace(/\\/g, '%5C');
    return str;
}

function findGetParameter(parameterName, locationData = location.search) {
    var result = null,
        tmp = [];
    locationData = locationData || "";
    locationData
        .substr(locationData.indexOf("?") + 1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0].toLowerCase() === parameterName.toLowerCase()) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('Mobile') !== -1);
}

function getCaseInSensitiveJsonProperty(obj, propertyName) {
    let reg = new RegExp(`^${propertyName}$`, "i");
    return Object.keys(obj).reduce((result, key) => {
        if (reg.test(key)) result.push(obj[key]);
        return result;
    }, []);
}

/**
 * @description : Load Css and Js files dynamically and execute callback on completion
 * @author Prashik
 * @date 2019-08-28
 * @function loadAndCall
 */
const loadAndCall = (function (_opts) {
    var _options = {
        files: {
            css: [],// ["/Css/file.css", "/Css/file.css"]
            js: []// ["/Js/file.js", "/Js/file2.js"]
        },
        callBack: "",
        mapCustomPath: false,
        win: window
    }
    $.extend(_options, _opts);
    var _callBackValidator = 0
    var _allJsLoadedValidator = function () {
        _callBackValidator++;
        if (typeof _options.callBack === "function" && _callBackValidator >= (_options.files.css.length +
            _options.files.js.length)) {
            _options.callBack();
        }
    }
    var _addFilesAndEvents = function (_addedFile, _pathUrl, _fileType) {
        if (_addedFile.readyState) {
            //IE
            _addedFile.onreadystatechange = function () {
                if (_addedFile.readyState == "loaded" ||
                    _addedFile.readyState == "complete") {
                    _addedFile.onreadystatechange = null;
                    _allJsLoadedValidator();
                }
            };
        } else {
            //Others
            _addedFile.onload = function () {
                _allJsLoadedValidator();
            };
        }
        if (_fileType == "css") {
            _addedFile.href = _pathUrl;
        } else if (_fileType == "js") {
            _addedFile.src = _pathUrl;
        }
        _options.win.document.getElementsByTagName("head")[0].appendChild(_addedFile);
    }
    var _folderPath = `..${(_options.mapCustomPath ? ("/" + callParentNew("thmProj")) : "")}`;
    _options.files.css.forEach(_cssPath => {
        _cssPath = _folderPath + _cssPath;
        if (!$(`link[href='${_cssPath}']`).length) {
            var _link = _options.win.document.createElement("link")
            _link.rel = "stylesheet";
            _addFilesAndEvents(_link, _cssPath, "css");
        }
    });
    _options.files.js.map(_jsPath => {
        _jsPath = _folderPath + _jsPath;
        if (!$(`script[src='${_jsPath}']`).length) {
            var _script = _options.win.document.createElement("script")
            _script.type = "text/javascript";
            _addFilesAndEvents(_script, _jsPath, "js");
        }
    }).length || _allJsLoadedValidator();
});

/**
 * @description : Render an templete dynamically on demand in templetes exact container
 * @author Prashik
 * @date 2019-08-28
 * @function loadAndCall
 */
const renderTemplete = function (templateContent) {
    const template = document.querySelector(templateContent);
    if (template) {
        const node = document.importNode((template.content || createElementFromHTML(template.innerHTML)) , true);
        if (node) {
            var temp = template.parentElement.insertBefore(node, template);
        }
    }
}

function createElementFromHTML(htmlString) {
    var div = document.createElement('div');
    div.innerHTML = htmlString.trim();
    return div.firstChild; 
}