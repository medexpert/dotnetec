﻿//We are using this js file in iview,listview,Listview & saveas files.
//-----------------------------List of functions in this file--------------------------------------
//ChangeTheme() -Function to change the theme of the page.
//Adjustwin(window)-Adjust the size of parent window according to the current window size
//pgReload()-Function to click the new button from iview ,display the tstruct page. 
//CheckAll() -Function to check all the row once the header row is checked. 
//ChkHdrCheckbox() -Function to check if the header checkbox is checked then check all.  
//-------------------------------------------------------------------------------------------------
var traceSplitChar = "♦";
var traceSplitStr = "♦♦";
var NewTree = new Array();
var arrCheckedFilter = new Array();
var loadPop;
var isSqlPagination;
var parafilterfix = {};
var isDepsProcessed = true;
var formSubmited = false;
var strTimeTaken = "";
var arrcheckedvalues = new Array();
var timePickerSec = false;
var accordionHeight = 0;
var firstTimeScroll = true;
var windowResizer;
var beforeResizeHeight = "";
var ivirColumnTypeObj = ""; //if this variable is empty its not an interactive report
var ivirDataTableApi, ivirDatatable;
var isChkBox = "";
var rowTypeExist = false;
var gl_language = "";
var validateParamOnGo = false;
var isPlDepParBound = false;
var iframeindex = -1;
var HeaderText, ColumnType, FieldName, HideColumn, HideColumnLength, filteredColumns, hiddenColumnIndex, ivComps, ivHeadRows, ivActions, ivConfigurations, ivDatas = [];
var initAdvFilters = true;
var isPerf = false;
var tableWidth = 0;
var minCellWidth = 10;
var listViewCheckBoxSize = 40;
var configNavProp = "default";
var enableCardsUi = false;

var actionCallFlag = Math.random();
var actionCallbackFlag = actionCallFlag;

var classicView = false;

var recordsExist = false;

var trimIviewData = true;

var viewNavigationData = [];

var cellHeaderConf = {};
var bulkDownloadEnabled = false;
//to close all popups on unload of ListView
$j(window).on("unload", function () {
    if (loadPop && !loadPop.closed) {
        loadPop.close();
    }
    callParentNew("removeOverlayFromBody()", "function"); //if any remodal popup is opened & user clicks on browser back button then remove window - header, footer & menu overlay css
});

var isLnkClicked = false;

//This function clears the cache files on unload and on opening another iview.
function DeleteIviewCacheFiles() {

    // on unload deletes all existing cached iview files.
    try {

        ASB.WebService.DeleteIviewCacheFiles(iName);
    } catch (ex) { }
}


function SetLnkClick() {
    isLnkClicked = true;
}


function dvToolBarFix() {
    //parafilterfix
    if ($j("#dvToolbar").text() == "" || $j("#dvToolbar").text() == null || $j("#dvToolbar").text() == undefined) {

        $j("#dvToolbar").parent().css("display", "none");
    } else {

        $j("#dvToolbar").parent().css("display", "inline-block");
    }
    if ($j("#FilterValues").text() == "" || $j("#FilterValues").text() == null || $j("#FilterValues").text() == undefined) {
        $j("#dvSelectedFilters").css("display", "none");
    } else {
        $j("#dvSelectedFilters").css("display", "inline-block");
    }
}


/**
 * Return Relemnt from multiple elements with data property and its value
 * @author Prashik
 * @Date   2019-04-11T10:27:23+0530
 * @param  {string}                 prop : data property name
 * @param  {string}                 val  : data property value
 * @return {object}                      : filtered element
 */
$.fn.filterByData = function (prop, val) {
    var $self = this;
    if (typeof val === 'undefined') {
        return $self.filter(
            function () { return typeof $(this).data(prop) !== 'undefined'; }
        );
    }
    return $self.filter(
        function () { return $(this).data(prop) == val; }
    );
};

$.fn.single_double_click = function (single_click_callback, double_click_callback, timeout) {
    return this.each(function () {
        var clicks = 0,
            self = this;
        $(this).on("click", function (event) {
            clicks++;
            if (clicks == 1) {
                setTimeout(function () {
                    if (clicks == 1) {
                        single_click_callback.call(self, event);
                    } else {
                        double_click_callback.call(self, event);
                    }
                    clicks = 0;
                }, timeout || 300);
            }
        });
    });
}

//to check scrollbar exists or not for an element
$.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.height();
}


/**
 * page load callback function trigerred from c# for an update panel's postback and not of postback page loads and should be used instead of $(document).ready() function since that won't call on postback
 * @author Prashik
 * @Date   2019-04-11T10:31:38+0530
 */
function pageLoad(sender, args) {
    try {
        pageLoadIvHook();
    } catch (ex) { }

    enableCardsUi = isMobileDevice();

    $(".animationLoading").show();
    if (typeof getAjaxIviewData != "undefined" && getAjaxIviewData) {
        $("#dvSqlPages").show();
        $("#nextPrevBtns").hide();
    }
    if (typeof capturedButton1Submit != "undefined" && capturedButton1Submit == true) {
        capturedButton1Submit = false;
        $("#button1").click();
    }

    if (typeof totRowCount != "undefined" && totRowCount != "" && $("#getIviewRecordCountVal").length > 0) {
        $("#lblNoOfRecs").html(" of " + totRowCount);
    }

    $("table#GridView1").each(function () {
        var jTbl = $(this);
        if (typeof getAjaxIviewData != "undefined" && !getAjaxIviewData) {


            if (jTbl.find("tbody>tr>th").length > 0) {
                jTbl.find("tbody").before("<thead></thead>");
                jTbl.find("tbody>tr").each(function () {
                    var thElem = $(this);
                    if (thElem.find("th").length > 0) {
                        jTbl.find("thead:first").append(thElem);
                    }
                });
                jTbl.find("tbody>tr>th").parent("tr").remove();
            }

            //table width 100%;
            //if (widthOfGrid <= $(window).width())
            //    $(".gridData").css("width", "100%");


            //if (ivirColumnTypeObj != "") {

            if (jTbl.find("table#GridView1 tfoot").length == 0) {
                var thLength = jTbl.find("thead tr:last th").length;
                var tfootHtml = "<tfoot>"
                for (var i = 0; i < thLength; i++) {
                    tfootHtml += "<td></td>"
                }
                tfootHtml += "</tfoot>";
                jTbl.find("tbody").before(tfootHtml);
            }
            //}
        }

    });

    $("#tasks").html(callParentNew('lcm')[407] + " " + "<span class='caret'></span>").prop({ 'title': callParentNew('lcm')[407] });
    $(".print").prop({ 'title': callParentNew('lcm')[408] }).text(callParentNew('lcm')[408]);
    $("#ivInSearchInput").attr({ 'placeholder': callParentNew('lcm')[415] });
    /**
     * Entry point for initializing jquery Datatable
     * @author Prashik
     * @Date   2019-04-11T10:40:40+0530
     */

    if (document.title == "Iview" && typeof getAjaxIviewData != "undefined" && getAjaxIviewData) {
        var iVData = $("#hdnIViewData").val();
        if (iVData != "") {
            try {
                if (iVData) {
                    createIvir(iVData);
                    try {
                        nxtScrollSize = 100; //default record size for datatable initilization
                        defaultRecsPerPage = 100; //fetch size value
                        dtTotalRecords = getDtRecordCount();
                        checkIfNextDBRowsExist(true);

                    } catch (ex) {
                        console.log(ex.message);
                    }
                    //$("#hdnIViewData").val("");
                }
            } catch (e) {

            }
        }

    } else if (document.title == "Iview" && typeof ivirHeaderData != "undefined" && typeof getAjaxIviewData != "undefined" && !getAjaxIviewData) {
        if (Object.keys(ivirHeaderData).length)
            createIvir(JSON.stringify(ivirHeaderData));
    }





    if (document.title == "Iview" && $("table#GridView1").length != 0) {
        if (ivirColumnTypeObj != "") {
            //$("table#GridView1").hide();
            $("table#GridView1").removeAttr('cellspacing rules border style');
            $("table#GridView1").addClass('ivirMainGrid stripe row-border hover order-column');
            if(!enableCardsUi)
                $("table#GridView1").css({ "box-shadow": "initial", "border-bottom": "1px black solid" });
            $("#ivContainer").addClass("interactiveReport");
            $(".iviewTableWrapper").addClass('interactiveReportWrapper')
        }
        //else {

    }




    $('#Filterscollapse').on('shown.bs.collapse', function () {
        $("#myFilters").removeClass("disabled");
        $("#searchBar").find(".ccolapsee").removeClass("glyphicon-plus icon-arrows-plus").addClass("glyphicon-minus icon-arrows-minus");
        //iviewTableWrapperHeightFix();
        //if (ivirColumnTypeObj == "")
        //    cloneTheGridHeader(false, "no");
    }).on('hidden.bs.collapse', function () {
        $("#myFilters").removeClass("disabled");
        $("#searchBar").find(".ccolapsee").removeClass("glyphicon-minus icon-arrows-minus").addClass("glyphicon-plus icon-arrows-plus");
        //iviewTableWrapperHeightFix();
        //if (ivirColumnTypeObj == "")
        //    cloneTheGridHeader(false, "no");
    });
    if (ivirColumnTypeObj == "") {
        $('#Filterscollapse').off('show.bs.collapse').off('hide.bs.collapse');
        $('#Filterscollapse').on('show.bs.collapse', function () {
            if ($("#myFilters").hasClass("disabled"))
                return;
            $("#myFilters").addClass("disabled");
            if (accordionHeight != 0) {
                // $(".gridData.clonedHtml").animate({ "top": (parseInt($(".gridData.clonedHtml").css('top')) + accordionHeight) + "px" }, 300);
            }
        }).on('hide.bs.collapse', function () {
            if ($("#myFilters").hasClass("disabled"))
                return;
            $("#myFilters").addClass("disabled");
            //$(".gridData.clonedHtml").animate({ "top": (parseInt($(".gridData.clonedHtml").css('top')) - accordionHeight) + "px" }, 300);
        });

        $(window).resize(function () {
            //cloneTheGridHeader(false, "yes");
        });
    }
    if (document.title == "Iview") {
        timePickerSecChecker();
        $(window).resize(function () {
            //cloneTheGridHeader(false, "yes");
            setSmartViewHeight();
        });
    }




    $("#lnkPrev").single_double_click(function (e) {
        EnablePageDimmer(e);
    }, function (e) {
        EnablePageDimmer(e);
        return;
    });

    $("#lnkNext").single_double_click(function (e) {
        EnablePageDimmer(e);
    }, function (e) {
        EnablePageDimmer(e);
        return;
    });

    FocusOnFirstFieldParam();


    $("a").keyup(function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (event.keyCode == 13) {
            $(this).click();
        }
    });

    if (!$("#nextPrevBtns").is(":visible") && document.title.toLowerCase() == "iview") {
        //$("#ivContainer.interactiveReport #dvRefreshParam, #ivContainer.interactiveReport ul#iconsUl li:not(#myFiltersLi):not(#ivInSearch):not(#filterColumnSelect):not(.searchColWrapper):not(#filterWrapper):not(#ivirCustomActionButtons):not(#customActionBtn):not(.dropDownButton__item):not(.actionWrapper):not(.gropuedBtn):not(.liNew):not(.liDelete)").hide();
        classicView = false;
        //$("#chkall").attr("onclick", "javascript:ivirCheckAll(this)");
        //$("input[name=chkItem]:checkbox").attr("onclick", "javascript:ivirChkHdrCheckbox(this);");
        $("#chkall").attr("onclick", "javascript:CheckAll()");
        $("input[name=chkItem]:checkbox").attr("onclick", "javascript:ChkHdrCheckbox();");
    } else {
        classicView = true;
        $("#chkall").attr("onclick", "javascript:CheckAll()");
        $("input[name=chkItem]:checkbox").attr("onclick", "javascript:ChkHdrCheckbox();");
    }

    $("#recPerPage").click(function () {
        lastSelRecPerPage = $("#recPerPage option:selected");
    });

    if (ivirColumnTypeObj == "") {
        //cloneTheGridHeader(true);
        scrollCloneHeader();
        var maxTimeWaitCloneHeader = 2000; // 2 seconds
        var timeWaitCloneHeader = 0;

        var interval = setInterval(function () {
            if ($('.gridData.clonedHtml thead').is(':visible')) {
                //  Clone Header visible
                checkHeightOfTable();
                clearInterval(interval);
            } else {
                if (timeWaitCloneHeader > maxTimeWaitCloneHeader) {
                    clearInterval(interval);
                    return;
                }
                timeWaitCloneHeader += 100;
            }
        }, 200);
        $(".animationLoading").hide();
    } else {
        if ($(ivirTable).length > 0) {
            //createIvirDataTable();
            //if (!ivirDataTableApi)
            getSavedConditions();
        } else {
            $(".animationLoading").hide();
        }
    }
    //  $("#middle1", eval(callParent('middle1', 'id'))).show();
    $("table#GridView1").show();


    //if ($("#hdnShowParams").val() == "true") {
    //    parafilterfix.myFilters = true;
    //}
    //else {
    //    parafilterfix.myFilters = false;
    //}

    /**
     * delay any callback by some interval while continous keyup
     * @author Prashik
     * @Date   2019-02-28T15:12:10+0530
     * @param  {function}               callback    callback function
     * @param  {int}                 ms         delay time in milliseconds
     */
    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    /**
     * search callback function for gridview
     * @author Prashik
     * @Date   2019-02-28T15:14:27+0530
     * @param  {object}                 e   : event object
     */
    var searchCallBack = function (e) {
        if (typeof ivirDataTableApi == "undefined") {
            //is classic iview
            var value = $(this).val().toLowerCase();
            $("#GridView1 tbody tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        } else {
            //if(typeof e.keyCode != "undefined"){
            //    resetMoreFilters();
            //    pushSearchStringtoFilters();
            //}

            //ivirDataTableApi.draw();
            ivirDataTableApi.search('').columns().search('').draw();
            ivirDataTableApi.search($(this).val()).draw();
        }
    }

    $("#ivInSearchInput").on("keyup", delay(searchCallBack, 500)).on("mouseover",
        function () {
            $(this).focus()
        });


    if (document.title == "Iview") {// && !args._isPartialLoad
        if (!showParam || recordsExist) {
            //setTimeout(function () {$('#Filterscollapse').collapse('hide')},1000);
            $('#myFilters').addClass("collapsed").attr("aria-expanded", "false")
            $('#Filterscollapse').removeClass("in").attr("aria-expanded", "false")
        } else {
            //setTimeout(function () {$('#Filterscollapse').collapse('show')},1000);
            $('#myFilters').removeClass("collapsed").attr("aria-expanded", "true")
            $('#Filterscollapse').addClass("in").attr("aria-expanded", "true")
        }
    }


    assignclassnames();

    ShowDimmer(false);
}

window.onbeforeunload = BeforeWindowClose;
function BeforeWindowClose() {
    var ivKey = $j("#hdnKey").val();
    //eval(callParent('IsContentAxpIframe') + "= false");
    try {
        if (document.title.toLowerCase() == "iview")
            ASB.WebService.DeleteIviewDataObj(ivKey);
    }
    catch (ex) {
    }
}


/**
 * generate params XML node for webservice call
 * @author Prashik
 * @Date   2019-04-11T10:38:57+0530
 * @return {string}                 : params XML node for webservice call
 */
function generateParamX() {
    var paramString = $("#hdnparamValues").val();
    var splitParamString = "";
    var returnParamX = "";
    if (typeof paramString != "undefined" && paramString != "") {
        splitParamString = paramString.replace(/¿/g, '♣').split('♣');


        var i = 0;
        $("#param").val("");
        for (i = 0; i <= splitParamString.length - 1; i++) {
            if (splitParamString[i] != "") {
                var arrparam = splitParamString[i].toString().split('~');
                var pName = CheckSpecialCharsInStr(arrparam[0].toString());
                var pValue = CheckSpecialCharsInStr(arrparam[1].toString());
                if (pValue.indexOf("&amp;grave;") >= 0) {
                    pValue = pValue.replace(/&amp;grave;/g, '~');
                }
                returnParamX = returnParamX + "<" + pName + ">";
                returnParamX = returnParamX + pValue;
                returnParamX = returnParamX + "</" + pName + ">";

                //if (pName == "mrefresh")
                //{
                //    if (pValue != "")
                //        refreshTime = pValue;
                //}

                //UpdateParamValues(pName, pValue);


                //ArrParamType count mismatch in ReplaceParamvalues since params are also added here
                //objIview.ArrParamType.Add("");
                //if (objIview.ParamCaption.Count > 0 && objIview.ParamCaption.toString() != "")
                //{
                //    paramss[objIview.ParamCaption[i].toString()] = pValue;
                //}

                pValue = pValue.replace(/~/g, '&grave;');
                if ($("#param").val() != "") {
                    $("#param").val($("#param").val() + "~" + pName + "♠" + pValue);
                }
                else {
                    $("#param").val(pName + "♠" + pValue);
                }
            }
        }

    }
    return returnParamX;

}

function onRecPerPageChange(event) {
    if ($("#recPerPage").val() == "0" && allowRecPerPageChange == false) {
        lastSelRecPerPage.prop("selected", true);

        var cutMsg = eval(callParent('lcm[390]'));
        var glType = eval(callParent('gllangType'));
        var isRTL = false;
        if (glType == "ar")
            isRTL = true;
        else
            isRTL = false;
        var ConfirmDeleteCB = $.confirm({
            title: eval(callParent('lcm[155]')),
            onContentReady: function () {
                disableBackDrop('bind');
            },
            backgroundDismiss: 'false',
            rtl: isRTL,
            escapeKey: 'buttonB',
            content: cutMsg,
            buttons: {
                buttonA: {
                    text: eval(callParent('lcm[282]')),
                    btnClass: 'hotbtn',
                    action: function () {
                        ConfirmDeleteCB.close();
                        ShowDimmer(true);
                        $("#recPerPage option[value=0]").prop("selected", true);
                        allowRecPerPageChange = true;
                        //$("#recPerPage").trigger("change");
                        setTimeout('__doPostBack(\'recPerPage\',\'\')', 0);
                    }
                },
                buttonB: {
                    text: eval(callParent('lcm[192]')),
                    btnClass: 'coldbtn',
                    action: function () {
                        disableBackDrop('destroy');
                        allowRecPerPageChange = false;
                        ShowDimmer(false);
                        return;
                    }
                }
            }
        });

        allowRecPerPageChange = false;
        return false;
    } else {
        setTimeout('__doPostBack(\'recPerPage\',\'\')', 0);
    }
    allowRecPerPageChange = false;
    //setTimeout('__doPostBack(\'recPerPage\',\'\')', 0)
    return true;
}


/**
 * get records count for iview dynamically from webservice
 * @author Prashik
 * @Date   2019-04-11T10:40:40+0530
 */
function getIviewRecordCount() {
    try {
        if (totRowCount == "" && $("#getIviewRecordCountVal").length > 0) {
            $("#getIviewRecordCountVal").addClass("fa-spin");
            ASB.WebService.GetIviewRecordCount(iName, $j("#hdnparamValues").val(), isListView, function (e, t) {
                //Success
                if (e.indexOf("<totalrows>") == 0) {
                    var closingIndex = e.indexOf("</totalrows>");
                    closingIndex = closingIndex > -1 ? closingIndex : e.length;
                    totRowCount = parseInt(e.substring("<totalrows>".length, closingIndex), 10);
                    $("#lblNoOfRecs").html(" " + totRowCount);

                } else {

                }
                $("#getIviewRecordCountVal").hide();
            }, function (e) {
                //Failure
                $("#getIviewRecordCountVal").hide();
            });
        }
    } catch (ex) {
        $("#getIviewRecordCountVal").hide();
        console.log(ex.message);
    }
}

function timePickerSecChecker() {
    //For time picker expression to enable seconds should be given in format as {HH:mm:ss} all numeric equivalent to {00:00:00} or to disable seconds it should be given as {HH:mm} all numeric equivalent to {00:00}
    if (timePickerSec == false) {
        for (var j = 0; j < parentArr.length; j++) {
            if (typeof (Expressions[j]) == "string" && Expressions[j] != "" && Expressions[j].charAt(0) == "{" && Expressions[j].charAt(Expressions[j].length - 1) == "}" && (Expressions[j]).indexOf(":") > -1) {
                var expTime = (Expressions[j]).slice(1, -1).split(':');
                expTime.forEach(function (value, i) {
                    //if (parseInt(expTime, 10) == expTime) {
                    if (!isNaN(value)) {
                        if (i == (expTime.length - 1) && expTime.length == 3) {
                            timePickerSec = true;
                            return;
                            //break;
                        }
                    }
                });
            }
        }
    }
}

//function setHeightOfTable() {
//    if (beforeResizeHeight != $(".gridData.clonedHtml").outerHeight()) {
//        var mrgnTop = parseInt($(".gridData:not('.clonedHtml')").css("margin-top"));
//        var heightOftable = $(".gridData:not('.clonedHtml')").outerHeight();
//        $(".gridData:not('.clonedHtml')").css({ "margin-top": mrgnTop - (beforeResizeHeight - $(".gridData.clonedHtml").outerHeight()) + "px", "height": heightOftable + (beforeResizeHeight - $(".gridData.clonedHtml").outerHeight()) + "px" });
//        beforeResizeHeight = "";
//    }
//}
function checkHeightOfTable() {
    if ($("#Filterscollapse").hasClass("in")) {
        $("#searchBar").find(".ccolapsee").addClass("glyphicon-minus icon-arrows-minus");
        iviewTableWrapperHeightFix();
    } else {
        $("#searchBar").find(".ccolapsee").addClass("glyphicon-plus icon-arrows-plus");
        iviewTableWrapperHeightFix();
    }
}

function cloneTheGridHeader(isClowningNotDone, setTopOfClone) {
    if (isClowningNotDone && $('.gridData.clonedHtml').length == 0) {
        var theadHtml = "<table class='gridData clonedHtml'>";
        theadHtml += "<thead>";
        theadHtml += $(".gridData thead").html();
        theadHtml += "</thead>";
        theadHtml += "</table>";
        $(theadHtml).insertBefore($(".gridData"));
    }
    if (document.title.toLowerCase() == "iview")
        $(".iviewTableWrapper .dvContent").css("width", "100%");
    if (document.title.toLowerCase() == "list iview")
        $(".iviewTableWrapper .pnlgrid").css("width", "100%");



    $(".gridData:not('.clonedHtml') thead th").each(function (index, el) {
        var widthOfTh = $(this).outerWidth();
        $(".gridData.clonedHtml thead th").eq(index).css({ "width": widthOfTh, "max-width": widthOfTh });
    });



    var widthOFGrid = $(".gridData:not('.clonedHtml')").outerWidth();
    $(".gridData.clonedHtml").css({ "width": widthOFGrid, "min-width": widthOFGrid });
    if (document.title.toLowerCase() == "iview")
        $(".iviewTableWrapper .dvContent").css("width", widthOFGrid);
    if (document.title.toLowerCase() == "list iview")
        $(".iviewTableWrapper .pnlgrid").css("width", widthOFGrid);
    $(".gridData:not('.clonedHtml') thead").css("visibility", "hidden");

    //if (isClowningNotDone)
    try {
        if (!isNaN(parseInt($(".gridData:not('.clonedHtml')").css('top')))) {
            //$(".gridData.clonedHtml").css("top", parseInt($(".gridData:not('.clonedHtml')").css('top')) + "px");
            //$(".gridData:not('.clonedHtml')").css("margin-top", "-" + $(".gridData:not('.clonedHtml') thead").outerHeight() + "px");
        }
    } catch (ex) { }
    //if ($(".gridData:not('.clonedHtml')").length > 0 && setTopOfClone == "yes")
    //    $(".gridData.clonedHtml").css("top", $(".gridData:not('.clonedHtml')")[0].offsetTop + $(".gridData:not('.clonedHtml') tbody")[0].offsetTop + "px");

}

function scrollCloneHeader() {
    //  SetGridHeaderWidth(dcNo);


    //var content = $j(".gridData:not('.clonedHtml')");
    var content = $j(".iviewTableWrapper");
    //(document.title.toLowerCase() == "iview") ? content = $j(".iviewTableWrapper") : content = $j(".iviewTableWrapper");
    var headers = $j(".gridData.clonedHtml");
    //gridDc-gridHd
    content.scroll(function () {
        var leftScrolled;
        //firstTimeScroll ? (leftScrolled = parseInt(headers.css("left"))+content.scrollLeft(),firstTimeScroll=false) : leftScrolled = content.scrollLeft();
        //if (document.title == "Iview")
        //    leftScrolled = 10 - content.scrollLeft();
        //else
        var isDirectionRtl = $("body").hasClass("btextDir-rtl");
        leftScrolled = 0 - content.scrollLeft();
        if (isDirectionRtl) {
            if (jQuery.browser.msie) {
                var scrollElem = ".dvContent";
                if (document.title == "List IView") {
                    scrollElem = "#Panel2";
                }
                var wdth = content.outerWidth();
                wdth = $j(scrollElem).outerWidth() - wdth;
                leftScrolled = -wdth - leftScrolled;
            } else {
                var scrollElem = ".dvContent";
                if (document.title == "List IView") {
                    scrollElem = "#divLContainer";
                }
                if ($(".iviewTableWrapper").outerHeight() <= $(scrollElem).outerHeight())
                    leftScrolled = leftScrolled + 16;
            }
        }
        headers.css("left", leftScrolled + "px");
        //}
    });
}

function FocusOnFirstFieldParam(focusIndex) {
    var paramFocused = false;
    if ($("#Filterscollapse").hasClass("in")) {
        if (focusIndex == undefined) {
            focusIndex = 0;
        }
        var focusObj;
        focusObj = $j("#myFiltersBody").find(':input[type!="hidden"]:input[type!="submit"],textarea,select').filter(":visible:enabled").eq(focusIndex);
        if (focusObj.hasClass("date") && focusObj.val() == "") {
            if (focusObj.length > 0) {
                focusObj.focus();
                paramFocused = true;
            }
        } else if (!focusObj.hasClass("date")) {
            if (focusObj.length > 0) {
                focusObj.focus();
                paramFocused = true;
            }
        } else {
            FocusOnFirstFieldParam(focusIndex + 1);
        }
    } else {
        FocusOnFirstFieldIView();
    }
    if (!paramFocused) {
        FocusOnFirstFieldIView();
    }
}

function FocusOnFirstFieldIView() {
    var objId = ".gridData";
    if ($j(objId).length > 0) {
        var focusObj;
        focusObj = $j(".gridData").find(':input[type="checkbox"],a').first();
        if (focusObj.length > 0) {
            focusObj.focus();
        }
    }
}

function iviewTableWrapperHeightFix() {
    if (($("#accordion").height()) > accordionHeight) {
        accordionHeight = $("#accordion").height();
    }
    //var heightOfHeader = $j(".gridData.clonedHtml thead").outerHeight();
    if (ivirColumnTypeObj == "") {
        if (!$(".iviewTableWrapper").hasClass('marginSetted')) {
            //$(".iviewTableWrapper").css("margin-top", heightOfHeader).addClass('marginSetted');
            $(".iviewTableWrapper").addClass('marginSetted');
            var heightOfSubHeader = $("#ivCap1").outerHeight(true) || 0;
            //$(".iviewTableWrapper").css({ "height": $(".iviewTableWrapper").outerHeight() - ($(".gridData.clonedHtml").outerHeight() + 32 + heightOfSubHeader), "overflow": "auto" });
            $(".iviewTableWrapper").css({ "overflow": "auto" });
        }
    }
    adjustRecordsHeight();
}

function adjustRecordsHeight() {
    try {
        var el = $(".iviewTableWrapper")[0];
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;
        var heightDifference = 0;

        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }

        if ((top + height) > ($(window).height() + window.pageYOffset)) {
            heightDifference = (top + height) - ($(window).height() + window.pageYOffset);
            $(".iviewTableWrapper").css("height", ($(".iviewTableWrapper").outerHeight() - heightDifference) + "px");
        } else if ((top + height) <= ($(window).height() + window.pageYOffset)) {
            heightDifference = ($(window).height() + window.pageYOffset) - (top + height);
            $(".iviewTableWrapper").css("height", ($(".iviewTableWrapper").outerHeight() + heightDifference) + "px");
        }
        $("#noRecccord").css("height", ($(window).height() - top) + "px");
        if (ivirColumnTypeObj == "") {
            //cloneTheGridHeader(false, "no");
        }
    } catch (ex) { }
}
var pickStatus = true;
$j(document).ready(function () {
    //console.clear();
    let isDisabledSplit = ``; 
    //if (document.title == "Iview" && typeof getAjaxIviewData != "undefined" && !getAjaxIviewData) {
        //isDisabledSplit = $("#hdnDisableSplit").val();
    //}
    //else {
        if (ivConfigurations) {
            try {
                isDisabledSplit = getCaseInSensitiveJsonProperty(ivConfigurations.filter((val, ind) => {
                    var thisVal = getCaseInSensitiveJsonProperty(val, "PROPS");
                    return thisVal && thisVal.toString() && thisVal.toString().toLowerCase() == "disablesplit"
                })[0], ["PROPSVAL"]).toString();
            } catch (ex) { }
        }else{
            isDisabledSplit = $("#hdnDisableSplit").val();
        }
    //}


    if (isDisabledSplit != undefined && isDisabledSplit.toLowerCase() == "false") {
        $(callParentNew("split-btn", "class")).removeClass("hide");
        $(callParentNew("split-btn-vertical", "class")).removeClass("hide");
    }
    else if (isDisabledSplit != undefined && isDisabledSplit.toLowerCase() == "true") {
       // $(callParentNew("split-btn", "class")).addClass("hide");
       // $(callParentNew("split-btn-vertical", "class")).addClass("hide");
        $(callParentNew("splitIcon", "id")).addClass("hide");
    }
    else if (isDisabledSplit != undefined && isDisabledSplit.toLowerCase() == "") {
        $(callParentNew("splitIcon", "id")).removeClass("hide");
    }

    $j(document).delegate("#myFilters", "click", function () {
        //if (parafilterfix.myFilters == false || parafilterfix.myFilters == null) {
        //    parafilterfix.myFilters = true;
        //    $("#hdnShowParams").val("true")
        //} else {
        //    parafilterfix.myFilters = false;
        //    $("#hdnShowParams").val("false")
        //}
        checkSuccessAxpertMsg();
    });
    var stTime = new Date();
    // ChangeTheme();
    window.parent.AxIvFilterMinHeader = new Array();
    if (document.title == "Iview") {
        GetUserLocale();
        AddDatePicker();
        AddTimePicker();
        //To pass parameter for drilldown iview
        if ($j("#hdnparamValues").val().indexOf("¿") > -1)
            SetParamValues($j("#hdnparamValues").val());
        // CreateIvFilter();
        //for popup back forward buttons will not be availabe
        if (window.opener && !window.opener.closed && $j(".backbutton").length > 0)
            $j(".backbutton").children().hide();
    }
    if (document.title != "Save As") {
        load();
    }
    if (document.title == "List IView") {
        CallLViewFunctions();
        $j("#showRecDetails").show();
        //   CreateIvFilter();
        if ($j("#hdnListViewName").val() == "")
            Adjustwin();
    } else {
        Adjustwin();
    }
    if (gl_language == "ARABIC") {
        //   $j("#leftPanel").css("float", "right");
        $j("#dvRecPerPage").css("float", "left");
    } else {
        //  $j("#leftPanel").css("float", "left");
        $j("#dvRecPerPage").css("float", "right");
    }

    history.forward();
    if (document.title != "Save As" && document.title != "IView Picklist") {

        SetBackForwardButtonProp(enableForwardButton, enableBackButton);
    }

    try {
        AxAfterIviewLoad();
    } catch (ex) { }
    //Grouping action buttons

    $j(".ddlBtn img.flag").addClass("flagvisibility");

    $j(".ddlBtn dd ul li a").click(function () {
        var text = $j(this).html();
        $j(".ddlBtn dt a span").html(text);
        $j(".ddlBtn dd ul").hide();
    });

    $j(document).bind('click', function (e) {
        var $jclicked = $j(e.target);
        if (!$jclicked.parents().hasClass("ddlBtn"))
            $j(".ddlBtn dd ul").hide();
        if (!$(e.target).is('a') && !$(e.target).parents($("#myFilters").data("parent")).length && !$(e.target).parents().hasClass("ui-datepicker-header") && !$(e.target).parents().hasClass("pickGridData") && !$(e.target).parents().hasClass("pickListFooter") && !$(e.target).parents().hasClass("ui-datepicker")) {
            $('#Filterscollapse').collapse('hide');
        }
    });

    $j("#flagSwitcher").click(function () {
        $j(".ddlBtn img.flag").toggleClass("flagvisibility");
    });
    typeof commonReadyTasks == 'function' ? commonReadyTasks() : "";
    //TODO: code for displaying the time taken in iview
    //var edTime = new Date();
    //var diff = edTime.getTime() - stTime.getTime();
    //strTimeTaken += "DocReady-" + diff + " ";

    $j(document).delegate(".inputClass2", "keyup", function (event) {
        if (event.keyCode == 13 && pickStatus == true) {
            ParamSearchOpen($j(this)[0]);
        }
        pickStatus = true;
    });
    //function on click of picklisttxtclear to clear current text in picklist
    $j(document).delegate(".picklisttxtclear", "click", function (e) {
        $j(this).parent(".paramtd2").find("input").val("");
    });
    var ivcRef = eval(callParent('ivCmdRefresh'));
    if (ivcRef != null && ivcRef != false) {
        eval(callParent('ivCmdRefresh') + "= ''");
        showAlertDialog("success", ivcRef);
    }

    //if (parent.showingDimmer) {        
    //    ShowDimmer(false);
    //    parent.showingDimmer = false;
    //}

    if (iName == "inmemdb"){
        $('<div id="inMemChartWrapper" runat="server"></div>').insertBefore('#ivContainer');
		callCharts();
    }

});

function pickDownn() {
    var pickLength = $j("#tblPickData tr").length;
    var activatedClass = "";
    activatedClass = $j('#tblPickData tr.active').index() + 1;

    if (activatedClass == "-1") {
        activatedClass = 0;
    }

    $j("#tblPickData tr").removeClass("active");
    if (activatedClass < (pickLength)) {

        $j("#tblPickData tr:nth-child(" + (activatedClass + 1) + ")").addClass('active');
        //}
    } else {
        $j("#tblPickData tr:nth-child(1)").addClass('active');
    }
}

function pickUpp() {
    var pickLength = $j("#tblPickData tr").length;
    var activatedClass = "";
    activatedClass = $j('#tblPickData tr.active').index() + 1;
    if (activatedClass == "0") {
        activatedClass = 1;
    }

    $j("#tblPickData tr").removeClass("active");
    if (activatedClass == 1) {
        $j("#tblPickData tr:nth-last-child(1)").addClass('active');
        //}
    } else {
        $j("#tblPickData tr:nth-child(" + (activatedClass - 1) + ")").addClass('active');

    }
}

function SetLnkStyle() {
    $j("#lnkPrev").hover(function () {
        if ($j(this).hasClass("pickdis") == true)
            $j(this).css("color", "gray");
    });
    $j("#lnkNext").hover(function () {
        if ($j(this).hasClass("pickdis") == true)
            $j(this).css("color", "gray");
    });
}

function CallAssignLoadVals(result, calledFrom, actnName, NavigationURL) {
    actnName = typeof actnName != "undefined" ? actnName : "";
    if (result.indexOf(";quot") != -1) {
        result = result.replace(new RegExp(";quot", "g"), "'");
    }
    result = CheckSpCharsInFldValueIview(result);
    AssignLoadValues(result, calledFrom, actnName, NavigationURL);
}


function ScrollToTop() {
    parent.window.scrollTo(0, 0);
}

//Function to adjust the height and width of the page according to the parent window.
function Adjustwin(hgt) {

    //var browservalue = navigator.userAgent.toUpperCase();
    //var framename = window.name;
    //framename = framename.toLowerCase();
    //if ((framename.substring(0, 7) == "openpop") || (framename.substring(0, 7) == "loadpop") || (framename.substring(0, 7) == "ivewPop")) {
    //    framename = "MyPopUp";
    //}

    //var extraHgt = 0;
    //var rCnt = $j("#GridView1 tr").length;
    //extraHgt = rCnt * 15;

    //if ((framename != "MyPopUp") && (framename != "MyPopUp")) {
    //if ($j("#middle1", parent.document).length) {
    //    var nweFrm = $j("#middle1", parent.document);
    //    //TODO:It will not work in other browser . need to handle this one.
    //    if (hgt != undefined) {
    //        //if ($j(window.document.body).height() + hgt + 80 < 460 && $j("#dvCharts").css("display") == "block")
    //        //    hgt = hgt + 470;

    //        if (browservalue.indexOf("CHROME") > -1)
    //            nweFrm.height($j(window.document.body).height() + hgt + 50);
    //        else if (browservalue.indexOf("MOZILLA") > -1)
    //            //nweFrm.height($j(window.document.body).height() + hgt + 100);, On staggered load height was more hence removed
    //            nweFrm.height($j(window.document.body).height() + hgt + 50);
    //        else
    //            nweFrm.height($j(window.document.body).height() + hgt);
    //    }
    //    else {
    //        if (browservalue.indexOf("CHROME") > -1)
    //            nweFrm.height($j(window.document.body).height());
    //        else if (browservalue.indexOf("MOZILLA") > -1)
    //            // nweFrm.height($j(window.document.body).height() + 350);
    //            nweFrm.height($j(window.document.body).height());
    //        else
    //            nweFrm.height($j(window.document.body).height());
    //    }
    //}
    //}
    SetLnkStyle();
}

var xmlObj;
var ArrDep = new Array();

///Function to clear the filter fields in Listview
function pgRefresh() {
    window.location.href = window.location.href;
    Adjustwin();
}



//Function to open the email page.
function openEMail(ivn, b) {
    var rid = getRecordid();
    if (!rid) {
        showAlertDialog("warning", 3000, "client");
    } else if (rid == "n/a") { } else {
        var newWindow;
        try {
            newWindow = window.open('email.aspx?sname=' + ivn + '&stype=' + b + '&recordid=' + rid, 'MyPopUp', 'width=600,height=600');
        } catch (ex) {
            showAlertDialog("warning", eval(callParent('lcm[356]')));
        }
    }
}
// TODO:This function need to change.
function processRow() {

    var b = form1.elements.length;
    var res = "";
    var selectRowno = new Array();
    var sRecid = new Array();
    var g = 0; // no of items in selectRow
    var r = 0; // no of items in sRecid

    for (i = 0; i < b; i++) {

        if (form1.elements[i].name == "rXml") {
            res = res + form1.elements[i].value;
        }
        if (form1.elements[i].type == "checkbox") {
            if (form1.elements[i].checked) {
                selectRowno[g] = form1.elements[i].value;
                g = g + 1;
            }
        }
    }

    res = generateFullIviewXML(res);


    try // for IE
    {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) { showAlertDialog("error", e.message) }
    }
    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());
    var noofrows = s[0].childNodes.length;
    var nodlen = s[0].childNodes[0].childNodes.length; // no of columns
    for (n = 0; n < noofrows; n++) //for1
    {
        if (s[0].childNodes[n].childNodes[0].tagName.toString() == "rowno") {
            for (k = 0; k < g; k++) //for2
            {
                if (s[0].childNodes[n].childNodes[0].firstChild.nodeValue == selectRowno[k]) {
                    for (m = 0; m < nodlen; m++) // for3
                    {
                        if (s[0].childNodes[n].childNodes[m].tagName.toString() == "recordid") {
                            sRecid[r] = s[0].childNodes[n].childNodes[m].firstChild.nodeValue;

                            r = r + 1;
                        }

                    } //for3
                }
            } //for2  
        }
    } //for1

    if (g == 0) { showAlertDialog("warning", 3001, "client"); } else if (g > 1) { showAlertDialog("warning", 3002, "client"); } else {
        OpenPDFparams(sRecid[0])
    }
}
// TODO:This function need to change.
function OpenPDFparams(recid) {
    if (recid) {
        var left = (screen.width / 2) - (500 / 2);
        var top = (screen.height / 2) - (200 / 2);
        var newWindow;
        try {
            newWindow = window.open('pdfparams.aspx?sname=' + ivna + '&recid=' + recid + '&stype=' + ivtype, 'MyPopUp', 'width=500,height=200,top=' + top + ',left=' + left + '');
        } catch (ex) {
            showAlertDialog("warning", eval(callParent('lcm[356]')));
        }
    } else {
        showAlertDialog("warning", 3000, "client");
    }
}
// TODO:Function to import the csv files.
var gActionname = "";
var gConfirmmsg = "";
var IviewName = "";
var appl = "";

function callFileUploadAction(aname, iName, confirm, appl) {
    //var Title = "File Upload";
    //$j("#fileUpload").click();

    //var fileName = $j("#fileUpload").val();

    //    if (fileName != "")
    //        callActWithFile(aname, fileName, iName, confirm, appl);
    if (confirm && confirm != "") {
        var glType = eval(callParent('gllangType'));
        var isRTL = false;
        if (glType == "ar")
            isRTL = true;
        else
            isRTL = false;
        var callFileUploadActionCB = $.confirm({
            title: eval(callParent('lcm[155]')),
            onContentReady: function () {
                disableBackDrop('bind');
            },
            rtl: isRTL,
            backgroundDismiss: 'false',
            escapeKey: 'buttonB',
            content: confirm,
            buttons: {
                buttonA: {
                    text: eval(callParent('lcm[164]')),
                    btnClass: 'hotbtn',
                    acion: function () {
                        callFileUploadActionCB.close();
                        UploadFileAfterConfirm();
                    }
                },
                buttonB: {
                    text: eval(callParent('lcm[192]')),
                    btnClass: 'coldbtn',
                    action: function () {
                        disableBackDrop('destroy');
                        return;
                    }
                }
            }
        });

    }

    function UploadFileAfterConfirm() {


        var Title = "File Upload";

        gActionname = aname;
        gConfirmmsg = confirm;
        IviewName = iName;
        appl = appl;
        var na = "./fileupload.aspx?act=upload";

        createPopup(na);
    }
    if (callFileUploadActionCB === undefined) {
        UploadFileAfterConfirm();
    }
}

function CallAfterFileUpload() {
    var updFilename = $j("#cb_sactbu").val();
    callActWithFile(gActionname, updFilename, iName, gConfirmmsg, appl);
}
// TODO: Make this action as single function.
function callActWithFile(aname, fileup, iName, confirm, appl) {
    callParentNew("loadFrame()", "function");
    //window.parent.loadFrame();
    var tid = "";
    var fup = "";
    AxWaitCursor(true);
    var b = form1.elements.length;
    var res = "";
    var selectRowno = new Array();
    var g = 0; // no of items in selectRow
    if (document.getElementById("rXml") != null) {
        res = document.getElementById("rXml").value;
    }
    for (i = 0; i < b; i++) {
        if (form1.elements[i].type == "checkbox") {
            if (form1.elements[i].checked) {
                selectRowno[g] = form1.elements[i].value; // rowno  val stored in arrray
                g = g + 1;
            }
        }
    }

    res = generateFullIviewXML(res);

    try // for IE
    {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) { showAlertDialog("error", e.message) }
        AxWaitCursor(false);
    }

    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());
    var noofrows = s[0].childNodes.length;
    var selXML = "";
    if (g == 0 && appl == "" && noofrows > 0) {
        selectRowno[0] = 1;
    }

    var noofSrows = selectRowno.length;
    if (g == 0 && appl.toString().toLowerCase() == "all rows") {
        noofSrows = noofrows;
    }
    for (k = 0; k < noofSrows; k++) {
        var m = selectRowno[k];
        m = m - 1;
        if (xmlObj.getElementsByTagName("row")[m].xml == undefined) {
            nodeXml = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[m]);
            selXML = selXML + nodeXml;
        } else {
            selXML = selXML + xmlObj.getElementsByTagName("row")[m].xml;
        }

        if ((fileup != "") && (fileup != "undefined")) {

            if (fileup.substring(0, 1) == ":") {

                fup = xmlObj.getElementsByTagName(fileup)[m].firstChild.nodeValue;
            } else {

                // for fileupload value uploaded by User or 
                // for fileupload with direct path given
                fup = fileup.toString();
            }
        }
    }

    var pa = form1.paramxml.value;
    var txt = '';
    var tem = 1;
    var fileName = "";
    var trace = traceSplitStr + "Action-" + aname + traceSplitChar;
    var dummyArray = new Array();

    if (fup != "") {
        fileName = fup;
        var index = fileName.lastIndexOf('\\');
        if (index != -1) {
            fileName = fileName.substring(index + 1);
        }
    }
    txt = txt + '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  stype="iviews" sname="' + iName + '" actname="' + aname + '" __file="' + fileName + '"><params>' + pa + '</params><varlist>';
    txt = txt + selXML;
    txt = txt + '</varlist>';

    if (g == 0 && appl != "" && appl.toString().toLowerCase() != "all rows") {
        showAlertDialog("warning", 3001, "client");
        AxWaitCursor(false);
    } else {
        var ivKey = $j("#hdnKey").val();
        if (confirm != "") {
            if (confirm(confirm)) {
                ASB.WebService.CallActionWS(dummyArray, dummyArray, dummyArray, dummyArray, "", "", txt, fup, "i", "", "", ivKey, "", CActSucceededCallback);
            }
        } else {
            ASB.WebService.CallActionWS(dummyArray, dummyArray, dummyArray, dummyArray, "", "", txt, fup, "i", "", "", ivKey, "", CActSucceededCallback);
        }
    }
    closeParentFrame();
}

function CActFUSucceededCallback(result, eventArgs) {

    try {
        AssignloadValues(result, "Iview");
        AxWaitCursor(false);
    } catch (ex) {
        AxWaitCursor(false);
        ShowDimmer(false);
        showAlertDialog("error", 3003, "client");
    }
}

// TODO: Make this action as single function.
function callAction(a, x, conf, appl) {

    var tid = "";
    AxWaitCursor(true);
    var b = form1.elements.length;
    var res = "";
    var selectRowno = new Array();
    var g = 0; // no of items in selectRow
    if (document.getElementById("rXml") != null) {
        res = document.getElementById("rXml").value;
    }
    for (i = 0; i < b; i++) {
        if (form1.elements[i].type == "checkbox") {
            if (form1.elements[i].checked) {
                if (form1.elements[i].id != "chkall") {
                    selectRowno[g] = form1.elements[i].value; // rowno  val stored in arrray
                    g = g + 1;
                }
            }
        }
    }

    if (g == 0 && appl != "" && appl.toString().toLowerCase() != "all rows") {
        showAlertDialog("warning", 3001, "client");
        AxWaitCursor(false);
        return;
    }
    if (conf != "") {

        if (!confirm(conf)) {
            AxWaitCursor(false);
            return;
        }

    }


    res = generateFullIviewXML(res);
    try // for IE
    {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) { showAlertDialog("error", e.message) }
        AxWaitCursor(false);
    }

    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());
    var noofrows = s[0].childNodes.length;
    var selXML = "";
    if (g == 0 && appl == "" && noofrows > 0) {
        selectRowno[0] = 1;
    }
    var noofSrows = selectRowno.length;
    if (g == 0 && appl.toString().toLowerCase() == "all rows") {
        noofSrows = noofrows;
    }
    for (k = 0; k < noofSrows; k++) {
        if (selectRowno[k] != "" && selectRowno[k] != undefined) {
            var m = selectRowno[k];
            m = m - 1;
            var nodeXml = ""
            if (xmlObj.getElementsByTagName("row")[m].xml == undefined) {
                nodeXml = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[m]);
                selXML = selXML + nodeXml;
            } else {
                selXML = selXML + xmlObj.getElementsByTagName("row")[m].xml;
            }
        }
    }

    var ivtype = "iviews";
    var pa = form1.paramxml.value;
    var fup = "";
    var trace = traceSplitStr + "Action-" + a + traceSplitChar;

    var actXML = '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  stype="' + ivtype + '" sname="' + x + '" actname="' + a + '"><params>' + pa + '</params><varlist>' + selXML + '</varlist>';

    var dummyArray = new Array();
    try {
        var ivKey = $j("#hdnKey").val();
        ASB.WebService.CallActionWS(dummyArray, dummyArray, dummyArray, dummyArray, "", "", actXML, fup, "i", "", "", ivKey, "", CActSucceededCallback);

    } catch (ex) { showAlertDialog("error", ex.toString()); }

}

function ActButtonClick(btnId, confirmMsg, allRow, NavigationURL) {
    if (actionCallFlag == actionCallbackFlag) {

        actionCallFlag = Math.random();
        $("#icons").css({ "pointer-events": "auto" });
    } else {

        //showAlertDialog("error", 2000, "client");
        $("#icons").css({ "pointer-events": "none" });
        return;
    }

    //if (getAjaxIviewData)
    $("#" + btnId).removeAttr("href");
    ShowDimmer(true);
    var checked = false;
    var reqIsPostBack = false;
    var rows = "";
    if (allRow == "") {
        checked = true;
    }

    $j("#GridView1" + " tr").each(function () {
        //var rowIdx = $j(this).val() + 1;
        $j(this).find("input:checkbox").each(function () {
            if ($j(this).attr("name") == "chkItem" && $j(this).prop("checked") == true) {
                var rowIdx = $j(this).val();
                rows += rowIdx + "♣";
                checked = true;
            }
        });
    });

    if (iframeindex != null && iframeindex > -1 && rows == "") {
        checked = true;
        rows = "1♣";
    }

    try {
        //Hook - to call custom functions before calling action
        if (!AxCustomBeforeAction(rows, btnId)) {
            $j("#hdnIsPostBack").val("false");

            actionCallbackFlag = actionCallFlag;
            $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });
            return;
        }
    } catch (ex) {

    }
    if ($j("#hdnSRows").length)
        $j("#hdnSRows").val(rows);
    if ($j("#hdnAct").length)
        $j("#hdnAct").val(btnId);
    if (checked) {
        if (confirmMsg != "") {
            var glType = eval(callParent('gllangType'));
            var isRTL = false;
            if (glType == "ar")
                isRTL = true;
            else
                isRTL = false;
            var ActButtonClickCB = $.confirm({
                closeIcon: false,
                title: eval(callParent('lcm[155]')),
                onContentReady: function () {
                    disableBackDrop('bind');
                },
                backgroundDismiss: 'buttonB',
                escapeKey: 'buttonB',
                rtl: isRTL,
                content: confirmMsg,
                buttons: {
                    buttonA: {
                        text: eval(callParent('lcm[164]')),
                        btnClass: 'hotbtn',
                        action: function () {
                            disableBackDrop('destroy');
                            ActButtonClickCB.close();
                            if (getAjaxIviewData)
                                callRemoteDoActionWS(rows);
                            //reqIsPostBack = true;
                        }
                    },
                    buttonB: {
                        text: eval(callParent('lcm[192]')),
                        btnClass: 'coldbtn',
                        action: function () {
                            ShowDimmer(false);
                            //reqIsPostBack = false;
                            // $j("#hdnIsPostBack").val("false");
                           
                            actionCallbackFlag = actionCallFlag;
                            $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });

                            return;
                        }
                    }

                }
            });

        } else {
            reqIsPostBack = true;
            callRemoteDoActionWS(rows);
            //if (getAjaxIviewData)
            //    callRemoteDoActionWS(rows);
        }

    } else {
        reqIsPostBack = false;
        showAlertDialog("warning", 3001, "client");
        ShowDimmer(false);

        actionCallbackFlag = actionCallFlag;
        $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });
    }

    //if (!getAjaxIviewData)
    //    if ($j("#hdnIsPostBack").length) {
    //        if (reqIsPostBack) {
    //            $j("#hdnIsPostBack").val("true");
    //        } else {
    //            $j("#hdnIsPostBack").val("false");
    //            return false;
    //        }
    //    }


}

function callRemoteDoActionWS(rows) {
    ShowDimmer(true);
    var appsessionkey = callParentNew('getAppSessionKey()', 'function').substr(2);
    var username = eval(callParent("mainUserName"));
    var actName = $j("#hdnAct").val().substr(4);
    var paramxml = generateParamX();
    var trace = traceSplitStr + "trace" + traceSplitStr;
    var iXml = "";
    iXml = iXml + '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  appsessionkey="' + appsessionkey + '" stype="iviews" sname="' + iName + '" actname="' + actName + '" username="' + username + '"><params>' + paramxml + '</params><varlist>';
    iXml += jsonToXml(rows);
    iXml += '</varlist>';
    ivkey = $("#hdnKey").val();
    $.ajax({
        url: 'iview.aspx/ActionBtnClick',
        type: 'POST',
        cache: false,
        async: true,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            iName,
            ivkey,
            iXml,
            actName
        }),
        success: function (data) {
            data = data.d;
            if (data.result == "failure"){
                if (iName == "inmemdb")
                    showAlertDialog('error', appGlobalVarsObject.lcm[278]);
                else
                    showAlertDialog("error", 4041, "client");
            
            }
            else if (data.result == "Session Authentication failed...")
                parent.window.location.href = "../aspx/sess.aspx";
            else {
                if (iName == "inmemdb") {
                    removeValFromIndex = rows.substr(0, rows.length - 1).split("♣");
                    for (var i = removeValFromIndex.length - 1; i >= 0; i--)
                        ivDatas.splice(parseInt(removeValFromIndex[i]) - 1, 1);
                    ivirDataTableApi.rows().remove();
                    ivirDataTableApi.rows.add(ivDatas).draw();  //append next records to the datatable & redraw it
                    callCharts();

                    showAlertDialog('success', eval(callParent('lcm[267]')));
                }
                else {
                    if (data.ActBtnNavigation != undefined)
                        CallAssignLoadVals(data.actResponse, "Action", actName, data.ActBtnNavigation);
                    else
                        CallAssignLoadVals(data.actResponse, "Action", actName);
                }
            }
            ShowDimmer(false);

            actionCallbackFlag = actionCallFlag;
            $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });
        },
        failure: function (response) {
            ShowDimmer(false);
            showAlertDialog("error", 3028, "client");

            actionCallbackFlag = actionCallFlag;
            $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });
        },
        error: function (response) {
            ShowDimmer(false);
            showAlertDialog("error", 3028, "client");

            actionCallbackFlag = actionCallFlag;
            $("#icons,#btnSaveTst,.wizardNextPrevWrapper").css({ "pointer-events": "auto" });
        }
    });
}

/**
 * generate xml for selected row's data while calling webservice
 * @author Prashik
 * @Date   2019-04-11T11:00:20+0530
 * @param  {string}                 rows : rows seperated by ♣
 * @return {string}                 selectedRow : xml for selected rows data
 */
function jsonToXml(rows) {
    rows = rows.split("♣");
    var selectedRow = "";
    $(rows).each(function (ind, val) {
        if (val != "") {
            var doc = $.parseXML("<row/>")
            var json = ivirDataTableApi.data()[val - 1];
            var xml = doc.getElementsByTagName("row")[0];
            var key, elem;
            for (key in json) {
                var newKey = key.startsWith("@") ? key.substr(1) : key;
                if (json.hasOwnProperty(key)) {
                    elem = doc.createElement(newKey);
                    $(elem).text(json[key]);
                    xml.appendChild(elem);
                }
            }
            //console.log(xml.outerHTML);
            selectedRow += xml.outerHTML || new XMLSerializer().serializeToString(xml);
        }
    });
    return selectedRow;
}


function CActSucceededCallback(result, eventArgs) {
    if (CheckSessionTimeout(result))
        return;

    try {
        result = AfterCallActionIview(result);
    } catch (ex) { }

    try {
        AssignLoadValues(result, "Iview");
        AxWaitCursor(false);
        showAlertDialog("info", 3004, "client");
    } catch (ex) {
        AxWaitCursor(false);
        ShowDimmer(false);
        showAlertDialog("error", 3003, "client");
    }
}

/// iview column action
function callHLaction(a, x, name) {

    //The first empty row node will be deleted incase of non-directdb. So, incrementing the row no by 1 to fetch the correct row from XML.
    if ($j("#hdnIsDirectDB").val() == "false" && $j("#hdnIsPerfXml").val() == "true")
        x = x + 1;
    var selXML = "";
    var ivtype = "iviews";
    if (!getAjaxIviewData) {

        eval(callParent("loadFrame()", "function"));
        var tid = "";
        var res = "";
        var b = form1.elements.length;

        if (document.getElementById("rXml") != null) {
            res = document.getElementById("rXml").value;
        }
        if (res.indexOf("<?xml") != -1)
            res = res.substring(38, res.length); // to remove <xml version.....  
        // for IE
        try {
            xmlObj = new ActiveXObject("Microsoft.XMLDOM");
            xmlObj.loadXML(res);
        } catch (e) {
            try //Firefox, Mozilla, Opera, etc.
            {
                parser = new DOMParser();
                xmlObj = parser.parseFromString(res, "text/xml");
            } catch (e) { showAlertDialog("error", e.message) }
        }
        var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());


        if (xmlObj.getElementsByTagName("row")[x].xml == undefined) {
            nodeXml = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[x]);
            selXML = selXML + nodeXml;
        } else {
            selXML = selXML + xmlObj.getElementsByTagName("row")[x].xml;
        }

    }
    else {
        selXML = jsonToXml(x.toString());
    }

    var dummyArray = new Array();
    var trace = traceSplitStr + "CallHLAction-" + a + traceSplitChar;
    var pa = form1.paramxml.value + ConstructFldDataNodes(selXML);
    var actXML = '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  stype="' + ivtype + '" sname="' + name + '" actname="' + a + '"><params>' + pa + '</params><varlist>' + selXML + '</varlist>';
    var fup = "";
    var ivKey = $j("#hdnKey").val();
    ASB.WebService.CallActionWS(dummyArray, dummyArray, dummyArray, dummyArray, "", "", actXML, fup, "i", "", "", ivKey, "", CHlActSucceededCallback);
}

function ConstructFldDataNodes(selXML) {
    var strNodes = "";

    if ($j("#hdnIsDirectDB").val() == "false" && $j("#hdnIsPerfXml").val() == "true") {
        var selXmlObj = "";
        // for IE
        try {
            selXmlObj = new ActiveXObject("Microsoft.XMLDOM");
            selXmlObj.loadXML(selXML);
        } catch (e) {
            try //Firefox, Mozilla, Opera, etc.
            {
                parser = new DOMParser();
                selXmlObj = parser.parseFromString(selXML, "text/xml");
            } catch (e) { showAlertDialog("error", e.message) }
        }

        if (selXmlObj.childNodes[0] != undefined) {
            var xmlRowAttr = selXmlObj.childNodes[0].attributes;
            for (var i = 0; i < xmlRowAttr.length; i++) {
                var nodeName = xmlRowAttr[i].name;
                var nodeVal = xmlRowAttr[i].value;
                if (!form1.paramxml.value.toLowerCase().indexOf("<" + nodeName.toLowerCase() + ">") > -1)
                    strNodes += "<" + nodeName + ">" + nodeVal + "</" + nodeName + ">";
            }
        }
    }

    return strNodes.replace(/&/g, "&amp;");;
}

function CHlActSucceededCallback(result, eventArgs) {

    if (CheckSessionTimeout(result))
        return;
    try {
        AssignLoadValues(result, "Iview");
        AxWaitCursor(false);
        var resval = result.split("*$*");
        var strSingleLineText = resval[0].toString().replace(new RegExp("\\n", "g"), "");
        strSingleLineText = strSingleLineText.replace(new RegExp("\\t", "g"), "&#9;");
        strSingleLineText = strSingleLineText.replace(/\\/g, ";bkslh");
        var myJSONObject = $j.parseJSON(strSingleLineText);
        //var resultObj = JSON.parse(result)
        if (!myJSONObject.hasOwnProperty("error")) {
            var cmd = myJSONObject.command[0].cmd;
            if (cmd !== "opentstruct" && cmd !== "openfile")
                callParentNew("closeFrame()", "function");
            //window.parent.closeFrame();
        }
    } catch (ex) {
        AxWaitCursor(false);
        ShowDimmer(false);
        showAlertDialog("error", 3003, "client");
        callParentNew("closeFrame()", "function");
        //window.parent.closeFrame();
    }
}

///Function to toggle the cursor style.
function AxWaitCursor(act) {
    if (act) {
        $j("body").css('cursor', 'wait');
    } else {
        $j("body").css('cursor', 'arrow');
        $j("body").css('cursor', 'default');
    }
}


// Function for refreash the iframe size based on AJAX frame size changed
function EndRequestHandler(sender, args) {

    var stTime = new Date();
    AddDatePicker();
    AddTimePicker();
    if (args.get_error() == undefined) {
        parent.window.scrollTo(0, 0);
        //if ($j("#chkall").length > 0 && window.document.title == "List IView")
        //    $j("#chkall").parent().width(60);

        if (document.title == "Iview" || document.title == "List IView") {
            calledFrom = "";
            //  CreateIvFilter();
            ShowVisibleFilters();
            HighLightSelected();
            CheckMyViewFilters();
            SetParamValues($j("#hdnparamValues").val());
            evalParamExprHandler();
            //parafilterfixer(); //not required since we are displaying params window as a popup #AXP000203
        }
        try {
            AxAfterIviewLoad();
        } catch (ex) { }
        Adjustwin();
        AxWaitCursor(false);
        ShowDimmer(false);
    } else {
        if (args.get_error().name == "Sys.WebForms.PageRequestManagerTimeoutException")
            showAlertDialog("error", 3005, "client");
        document.body.style.cursor = 'default';
        // window.location.reload();
        ShowDimmer(false);
    }

    // window.parent.closeFrame();
    if (document.title != "IView Picklist") {
        //TODO:The below line is commented as in IE the self iview was getting closed and alert was displayed in the browser.
        //parent.window.close();
    }
    //TODO: code for displaying the time taken in iview
    //var edTime = new Date();
    //var diff = edTime.getTime() - stTime.getTime();
    //strTimeTaken += "EndReq-" + diff + " ";
    //$j("#lblTimeClient").text(strTimeTaken);
    if (typeof isFromClearBtn != "undefined" && isFromClearBtn == true) {
        $j("#form1").find('input:text').each(function () {
            $j(this).val("");
        });
        $j("#form1").find('select option').each(function () {
            if ($j(this).text() == '0' || $j(this).text() == '') { $j(this).attr("selected", "selected"); }
        });
        $j("#dvParamCont").find('select').each(function () {
            $j(this).val('ALL');
        });
        isFromClearBtn = false;
    }
}

function AdjustRowsperPageDiv() {
    if ($j("#showFilter").css("display") == "block") {
        if ($j("#dvRowsPerPage").is(":visible")) {
            $j("#dvRowsPerPage").css("margin-top", "-16px");
        }
    }
}

function load() {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    window.status = "";
}

function CallSaveAs(iname, ivtype) {
    if (SaveWindow && SaveWindow.open && !SaveWindow.closed) {
        SaveWindow.focus();
    } else {
        if (recordsFieldsValidForSave()) {
            var left = (screen.width / 2) - (350 / 2);
            var top = (screen.height / 2) - (150 / 2);
            var paramVal = form1.param.value;
            paramVal = CheckUrlSplChars(paramVal);
            var ivKey = "";
            // if (ivtype == "lview" || ivtype == "listview") {
            //     if ($j("#hdnlvKey").length > 0 && $j("#hdnlvKey").val() != "")
            //         ivKey = "&ivKey=" + $j("#hdnlvKey").val();
            // }
            if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
                ivKey = "&ivKey=" + $j("#hdnKey").val();

            var sw = "SaveAs.aspx?tid=" + iname + "&param=" + paramVal + "&ivtype=" + ivtype + ivKey;
            try {
                SaveWindow = window.open(sw, "MyPopUp", "width=350,height=150,resizable=yes,top=" + top + ", left=" + left + "");
            } catch (ex) {
                showAlertDialog("warning", eval(callParent('lcm[356]')));
            }
        }
    }
}

function CheckUrlSplChars(value) {
    value = value.replace(/&amp;/g, "&");
    value = value.replace(/%/g, "%25");
    value = value.replace(/&/g, "%26");
    value = value.replace(/'/g, "%27");
    value = value.replace(/"/g, "%22");
    value = value.replace(/#/g, "%23");
    //value = encodeURIComponent(value);
    return value;
}


function SaveAs(a, c) {

    var b = form1.cb_saveas.value;

    if (b == "html") { toHTML(a, c, 'false'); } else if (b == "excel") { toExcel(a, c); } else if (b == "csv") { toCSV(a, c); } else if (b == "xml") { toXML(a, c); } else if (b == "pdf") { toPDF(a, c); } else if (b == "word") { toWord(a, c); } else if (b == "-") { }
}


function recordsFieldsValidForSave() {
    var maxLimit = parseInt($("#printRowsMaxLimitField").val());
    var totalRecords = parseInt($("#hdnTotalIViewRec").val());
    if (typeof getIviewRowCount == "undefined" || getIviewRowCount) {
        if (maxLimit < totalRecords) {
            showAlertDialog("warning", 3006, "client");
            return false;
        } else if (totalRecords < 1) {
            showAlertDialog("warning", 3007, "client");
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}


function toExcel(a, c) {
    var left = (screen.width / 2) - (840 / 2);
    var top = (screen.height / 2) - (600 / 2);
    var pa = form1.param.value;
    pa = CheckUrlSplChars(pa);
    var na = "../aspx/excel.aspx?ivname=" + a + "&ivtype=" + c + "&params=" + pa;
    window.open(na, "excelWindow", "width=840,height=600,scrollbars=yes,top=" + top + ",left=" + left + "");
}

function toHTML(a, c, isprint) {

    if (recordsFieldsValidForSave()) {
        var left = (screen.width / 2) - (840 / 2);
        var top = (screen.height / 2) - (600 / 2);
        var pa = form1.param.value;
        //pa = CheckUrlSplChars(pa);
        // if (c == "lview" || c == "listview") {
        //     if ($j("#hdnlvKey").length > 0 && $j("#hdnlvKey").val() != "")
        //         ivKey = "&ivKey=" + $j("#hdnlvKey").val();
        // } else {
            if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
                ivKey = "&ivKey=" + $j("#hdnKey").val();
        // }

	SetExport("../aspx/htmliv.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=&isPrint=" + isprint, pa, a);
    }
}
function SetExport(url, params, iviewName) {
    if (iviewName != "") {
        try {
            ASB.WebService.SetExportParams(url, params, iviewName, SuccSetExportParams);
        } catch (ex) {
        }
    }
}
function SuccSetExportParams(na, eventArgs) {
    if (na != "") {
        var isHtmlIv = na.indexOf("htmliv.aspx") > -1;
        var curWin;
        try {
            if(!isHtmlIv){
                fixUnloadOnWindowSelfDownloads(window);
            }
            curWin = window.open(na + "&axpCache=true", (isHtmlIv ? "toHTML": "_self"), "width=" + $(window).width() + ",height=" + $(window).width() + ",scrollbars=yes,top=" + 0 + ",left=" + 0 + "");
        } catch (ex) {
            if(isHtmlIv){
                showAlertDialog("warning", eval(callParent('lcm[356]')));
            }
            else{
                window.onbeforeunload = BeforeWindowClose;
            }
        }
    }
}

function toCSV(a, c) {
    var left = (screen.width / 2) - (840 / 2);
    var top = (screen.height / 2) - (600 / 2);
    var pa = form1.param.value;
    //pa = CheckUrlSplChars(pa);
    if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
        ivKey = "&ivKey=" + $j("#hdnKey").val();
    SetExport("../aspx/csviview.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=", pa, a);
}

function toXML(a, c) {
    var left = (screen.width / 2) - (840 / 2);
    var top = (screen.height / 2) - (600 / 2);
    var pa = form1.param.value;
    // if (c == "lview" || c == "listview") {
    //     if ($j("#hdnlvKey").length > 0 && $j("#hdnlvKey").val() != "")
    //         ivKey = "&ivKey=" + $j("#hdnlvKey").val();
    // } else {
        if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
            ivKey = "&ivKey=" + $j("#hdnKey").val();
    // }
    SetExport("../aspx/xmliview.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=", pa, a);
}

function toPDF(a, c) {
    if (recordsFieldsValidForSave()) {
        var left = (screen.width / 2) - (840 / 2);
        var top = (screen.height / 2) - (600 / 2);
        var pa = form1.param.value;
        //pa = CheckUrlSplChars(pa);
        // if (c == "Iview") {
            if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
                ivKey = "&ivKey=" + $j("#hdnKey").val();
        // } else {
        //     if ($j("#hdnlvKey").length > 0 && $j("#hdnlvKey").val() != "")
        //         ivKey = "&ivKey=" + $j("#hdnlvKey").val();
        // }

        SetExport("../aspx/pdfiview.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=", pa, a);
    }
}

function toWord(a, c) {
    var left = (screen.width / 2) - (840 / 2);
    var top = (screen.height / 2) - (600 / 2);
    var pa = form1.param.value;
    var ivKey = "";
    if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
        ivKey = "&ivKey=" + $j("#hdnKey").val();
    SetExport("../aspx/wordview.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=", pa, a);
}

function fixUnloadOnWindowSelfDownloads(curWin) {
    curWin.onbeforeunload = function () {
        curWin.onbeforeunload = BeforeWindowClose;
    };
}

//Need to change this funciton
function getRecordid() {

    var b = form1.elements.length;
    var res = "";
    var selectRowno = new Array();
    var sRecid = new Array();
    var g = 0; // no of items in selectRow
    var r = 0; // no of items in sRecid

    for (i = 0; i < b; i++) {

        if (form1.elements[i].name == "rXml") {
            res = res + form1.elements[i].value;
        }
        if (form1.elements[i].type == "checkbox") {
            if (form1.elements[i].checked) {
                selectRowno[g] = form1.elements[i].value; // rowno  val stored in arrray
                g = g + 1;
            }
        }
    }



    res = generateFullIviewXML(res);



    try {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) { showAlertDialog("error", e.message) }
    }


    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());

    var noofrows = s[0].childNodes.length;
    var nodlen = s[0].childNodes[0].childNodes.length; // no of columns

    for (n = 0; n < noofrows; n++) //for1
    {
        if (s[0].childNodes[n].childNodes[0].tagName.toString() == "rowno") {
            for (k = 0; k < g; k++) //for2
            {
                if (s[0].childNodes[n].childNodes[0].firstChild.nodeValue == selectRowno[k]) {
                    for (m = 0; m < nodlen; m++) // for3
                    {

                        if (s[0].childNodes[n].childNodes[m].tagName.toString() == "recordid") {
                            sRecid[r] = s[0].childNodes[n].childNodes[m].firstChild.nodeValue;

                            r = r + 1;
                        }

                    } //for3
                }
            } //for2  
        }
    } //for1

    var retVal = "n/a";

    if (g == 0) {

        retVal = "0";
    } else if (g > 1) {
        showAlertDialog("warning", 3002, "client");
    } else {

        retVal = sRecid[0];
    }
    return retVal;
}


function generateFullIviewXML(res) {

    if (typeof getAjaxIviewData == "undefined" || !getAjaxIviewData) {
        if (res.indexOf("<?xml") != -1) {
            res = res.substring(38, res.length); // to remove <xml version.....
        }
    } else {
        var rows = "";
        $j("#GridView1" + " tr").each(function () {
            $j(this).find("input:checkbox").each(function () {
                if ($j(this).attr("name") == "chkItem") {
                    var rowIdx = $j(this).val();
                    rows += rowIdx + "♣";
                }
            });
        });
        res = "<table>" + jsonToXml(rows) + "</table>";
    }
    return res;
}

//=======================For Delete ===================//
//need to change 
function callDelete(x, y) {
    AxWaitCursor(true);
    var b = form1.elements.length;
    var res = "";
    var selectRowno = new Array();
    var g = 0; // no of items in selectRow
    if ($j("#rXml").length > 0) {
        res = $j("#rXml").val();
    }

    for (i = 0; i < b; i++) {
        if (form1.elements[i].type == "checkbox") {
            if (form1.elements[i].checked) {
                if (!isNaN(parseInt(form1.elements[i].value))) {
                    selectRowno[g] = form1.elements[i].value; // rowno  val stored in arrray
                    g = g + 1;
                }
            }
        }
    }
    if (g == 0) {
        showAlertDialog("warning", 3001, "client");
        AxWaitCursor(false);
        return;
    }
    res = generateFullIviewXML(res);

    try {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) {
            showAlertDialog("error", e.message);
            AxWaitCursor(false);
        }
    }

    var nodeIndex = 0;
    for (nodeIndex = 0; nodeIndex < xmlObj.childNodes.length; nodeIndex++) {
        if (xmlObj.childNodes[nodeIndex].nodeName.toLowerCase() != "xml")
            break;
    }

    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[nodeIndex].tagName.toString());
    var noofrows = s[0].childNodes.length;
    var selXML = "";
    var selRecIds = "";
    var noofSrows = selectRowno.length;
    var recidColIdx = 0;
    recidColIdx = GetRecordIdColIndex(xmlObj, selectRowno[0]);

    //If the record id column in not there in the iview result, alert will be displayed.
    if (y == "Iview" && recidColIdx == -1) {
        showAlertDialog("error", 3008, "client");
        AxWaitCursor(false);
        return;
    }

    for (k = 0; k < noofSrows; k++) {
        var m = selectRowno[k];
        if (!(isNaN(m))) {
            m = m - 1;

            //To refuse deletion of Cancelled record 
            if (xmlObj.getElementsByTagName("cancel")[m] && xmlObj.getElementsByTagName("cancel")[m].xml && xmlObj.getElementsByTagName("cancel")[m].childNodes[0].text == "T") {
                showAlertDialog("warning", 3009, "client");
                AxWaitCursor(false);
                return;
            }

            if (xmlObj.getElementsByTagName("row")[m].xml == undefined) {
                nodeXml = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[m]);
                selXML = selXML + nodeXml;
                if (recidColIdx != -1) {
                    if (selRecIds == "")
                        selRecIds = xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].textContent;
                    else
                        selRecIds = "," + xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].textContent;
                }

            } else {
                selXML = selXML + xmlObj.getElementsByTagName("row")[m].xml;
                if (recidColIdx != -1) {
                    if (selRecIds == "")
                        selRecIds = xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text;
                    else
                        selRecIds += "," + xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text;
                }
            }
        }
    }
    trace = traceSplitStr + "DeleteRow-" + x + traceSplitChar;
    var actXML = '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  stype="iviews" sname="' + x + '" actname="delete"><varlist>' + selXML + '</varlist>';
    var cutMsg = eval(callParent('lcm[5]'));
    var glType = eval(callParent('gllangType'));
    var isRTL = false;
    if (glType == "ar")
        isRTL = true;
    else
        isRTL = false;
    $.confirm({
        title: eval(callParent('lcm[155]')),
        rtl: isRTL,
        onContentReady: function () {
            disableBackDrop('bind');
            AxWaitCursor(false);
        },
        backgroundDismiss: 'false',
        escapeKey: 'buttonB',
        content: cutMsg,
        buttons: {
            buttonA: {
                text: eval(callParent('lcm[279]')),
                btnClass: 'hotbtn',
                action: function () {
                    try {
                        ASB.WebService.DeleteIviewRow(selRecIds, x, actXML, SuccessCallbackDRow);
                    } catch (ex) { AxWaitCursor(false); }
                }
            },
            buttonB: {
                text: eval(callParent('lcm[280]')),
                btnClass: 'coldbtn',
                action: function () {
                    disableBackDrop('destroy');
                }
            }
        }
    });



}

//Function to get the recordid column index from the listview columns.
function GetRecordIdColIndex(xmlObj, rowIndex) {
    var idx = -1;

    rowIndex = parseInt(rowIndex, 10) - 1;
    if (!isNaN(rowIndex)) {
        var columns = xmlObj.getElementsByTagName("row")[rowIndex].childNodes;
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].nodeName == "recordid") {
                idx = i;
                break;
            }
        }
    }
    return idx;
}

function callDeleteNew(x, y, rowNo) {

    var res = "";
    var selRecIds = "";
    var recidColIdx = 0;
    recidColIdx = GetRecordIdColIndex(xmlObj, rowNo);

    if ($j("#rXml").length > 0) {
        res = $j("#rXml").val();
    }
    res = generateFullIviewXML(res);

    try {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } catch (e) {
        try //Firefox, Mozilla, Opera, etc.
        {
            parser = new DOMParser();
            xmlObj = parser.parseFromString(res, "text/xml");
        } catch (e) { showAlertDialog("error", e.message) }
    }

    var s = xmlObj.getElementsByTagName(xmlObj.childNodes[0].tagName.toString());
    var selXML = "";

    if (xmlObj.getElementsByTagName("row")[rowNo].xml == undefined) {
        nodeXml = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[rowNo]);
        selXML = selXML + nodeXml;
        if (selRecIds == "")
            selRecIds = new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text);
        else
            selRecIds = "," + new XMLSerializer().serializeToString(xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text);

    } else {
        selXML = selXML + xmlObj.getElementsByTagName("row")[rowNo].xml;
        if (selRecIds == "")
            selRecIds = xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text;
        else
            selRecIds += "," + xmlObj.getElementsByTagName("row")[m].childNodes[recidColIdx].text;
    }

    trace = traceSplitStr + "DeleteRow-" + x + traceSplitChar;
    var actXML = '<root axpapp="' + proj + '" trace="' + trace + '" sessionid="' + sid + '"  stype="iviews" sname="' + x + '" actname="delete" ><varlist>' + selXML + '</varlist></root>';
    var cutMsg = eval(callParent('lcm[5]'));
    var glType = eval(callParent('gllangType'));
    var isRTL = false;
    if (glType == "ar")
        isRTL = true;
    else
        isRTL = false;
    $.confirm({
        title: eval(callParent('lcm[155]')),
        rtl: isRTL,
        onContentReady: function () {
            disableBackDrop('bind');
        },
        backgroundDismiss: 'false',
        escapeKey: 'buttonB',
        content: cutMsg,
        buttons: {
            buttonA: {
                text: eval(callParent('lcm[279]')),
                btnClass: 'hotbtn',
                action: function () {
                    ASB.WebService.DeleteIviewRow(selRecIds, x, actXML, SuccessCallbackDRow);
                }
            },
            buttonB: {
                text: eval(callParent('lcm[280]')),
                btnClass: 'coldbtn',
                action: function () {
                    disableBackDrop('destroy');
                }
            }
        }
    });
}

function SuccessCallbackDRow(result, eventArgs) {

    if (CheckSessionTimeout(result))
        return;

    var clrCacheKeys = result.split("*#*")[1];
    if (typeof clrCacheKeys != "undefined" && clrCacheKeys != "") {
        //console.log("Clearing cache from Delete in listview");
        ClearRedisKeys(clrCacheKeys);
    }
    if (result.indexOf("*#*") > -1)
        result = result.substring(0, result.indexOf("*#*"));

    var resval = result.split("*$*");
    for (var ind = 0; ind < resval.length; ind++) {
        var strSingleLineText = resval[ind].toString().replace(new RegExp("\\n", "g"), "");
        if (strSingleLineText == "")
            return;

        var myJSONObject = $j.parseJSON(strSingleLineText);
        if (myJSONObject.error) {
            ExecuteErrorMsg(myJSONObject.error, "Delete");
        } else if (myJSONObject.message) {
            ExecuteMessage(myJSONObject.message, "Delete");
        }
    }
    AxWaitCursor(false);
}

//Function to execute the Error message node in the json result.
function ExecuteErrorMsg(ErroMsgJsonObj, calledFrom) {

    var errMsg = ErroMsgJsonObj[0].msg;
    var errFld;
    if (ErroMsgJsonObj[0].errfld)
        errFld = ErroMsgJsonObj[0].errfld;

    var index = errMsg.indexOf("^^dq");
    while (index != -1) {
        errMsg = errMsg.replace("^^dq", '"');
        index = errMsg.indexOf("^^dq");
    }

    if (errMsg != null && errMsg != undefined && errMsg != "") {
        showAlertDialog("error", errMsg);
    }
}

//Function to execute the message node in the json result.
function ExecuteMessage(messageJsonObj, calledFrom) {

    for (var i = 0; i < messageJsonObj.length; i++) {

        var msgs = messageJsonObj[i].msg;
        msgs = msgs.split(",");
        var responsemsgFlds = msgs.length;
        var message = "";

        for (var mm = 0; mm < responsemsgFlds; mm++) {

            message += msgs[mm] + "\n";
            var stPos = message.indexOf("[");
            var endPos = message.indexOf("]");
            var errFld = message.substring(stPos + 1, endPos);
            if (errFld != "") {
                var nerr = msg.substring(stPos, endPos + 2);
                message = message.replace(nerr, "");
            }
            var index = message.indexOf("^^dq");
            while (index != -1) {
                message = message.replace("^^dq", '"');
                index = message.indexOf("^^dq");
            }

        }
        if (message != "") {
            if ((calledFrom == "Delete") && (message.indexOf("done") != -1)) {
                showAlertDialog("success", 3010, "client");
                if ($j("#btnClear").length > 0)
                    $j("#btnClear").click();
                else
                    pgRefresh();
            } else {
                showAlertDialog("error", message);
            }
        }
    }
}

function callOpenAction(a, b) {
    if (a == "opentstruct") {
        //parent.loadFrame()
        callParentNew("loadFrame()", "function");
        $j(location).attr("href", "../aspx/tstruct.aspx?transid=" + b);
        ResetNavGlobalVariables();
    }
}

function HideFindList() {
    if (IsFindBtnClicked == true) { IsFindBtnClicked = false } else {
        var dvtaskList = document.getElementById("findListPopUp");
        if (dvtaskList != null && dvtaskList.offsetWidth != undefined && dvtaskList.offsetHeight != undefined) {
            dvtaskList.style.display = "none";
        }
    }
}
//Function to hide the task list.
function HideNqIvTaskList() {

    var dvtaskList = $j("#taskListPopUp");

        if (IsNqIvTaskBtnCliked == true) {
            IsNqIvTaskBtnCliked = false;
            dvtaskList.show();
        } else {
            dvtaskList.hide();
        }
    }
// Checks if the browsers is IE or another.
// document.all will return true or false depending if its IE
// If its not IE then it adds the mouse event
if (!document.all)
    document.captureEvents(Event.MOUSEMOVE)

// On the move of the mouse, it will call the function getPosition
// These varibles will be used to store the position of the mouse
var iX = 0
var iY = 0

// This is the function that will set the position in the above varibles 
function getPosition(args) {
    // Gets IE browser position
    if (document.all) {
        iX = event.clientX + $j("body").scrollLeft
        iY = event.clientY + $j("body").scrollTop
    }

    // Gets position for other browsers
    else {
        iX = args.pageX
        iY = args.pageY
    }
}

function FindposImg() {
    var img = document.getElementById("imgFind");
    iX = findPosX(img);
    iY = findPosY(img);
}

function OnBlrLstItem(lst) {

    lst.className = "popDivBg";
}

function OnLnkFcs(ankr) {
    ankr.style.color = "White";
}

function OnLnkBlr(ankr) {
    ankr.style.color = "Black";

}

function openPrint() {
    window.print();
}

function OpenPreview() {
    var OLECMDID = 7; /* OLECMDID values:* 6 - print* 7 - print preview* 1 - open window* 4 - Save As*/
    var PROMPT = 1; // 2 DONTPROMPTUSERvar
    WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
    document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
    WebBrowser1.ExecWB(OLECMDID, PROMPT);
    WebBrowser1.outerHTML = "";
}

function LoadPopPage(poppath, pageno, isParentListView, navigationType) {
    //For popup navigation buttons will be visible
    if (!window.opener)
        window.parent.disableNavigation = false;
    else
        window.opener.parent.disableNavigation = false;
    var parFrm = $j("#axpiframe", parent.document);
    if ((navigationType == undefined || navigationType == "") && parFrm.hasClass("frameSplited"))
        navigationType = "split"
    if (navigationType === "split") {
        ShowDimmer(false);
        callParentNew(`OpenOnPropertyBase(${poppath})`, 'function');
    }
    else if (navigationType === "default") {
        ReloadIframe(poppath);

    }
    else if (navigationType === "newpage") {
        popupFullPage(poppath)
    }
    else {



        if (isParentListView && isParentListView != "" && window.ivtype == "listview") {
            window.parent.tstructPop = isParentListView;
            window.parent.listViewPage = pageno;
            if (poppath.indexOf("tstruct.aspx?") > -1)
                poppath += "&AxPop=true";
        } else {
            if (poppath.indexOf("ivtoivload.aspx?") > -1)
                poppath += "&AxIsPop=true";

            if (poppath.indexOf("tstruct.aspx?") > -1)
                poppath += "&AxPop=true";

            if (poppath.indexOf("ivtstload.aspx?") > -1) {
                poppath += "&AxPop=true";
                if ((poppath.indexOf("ivtstload.aspx?tstname=digno") > -1)) {
                    poppath = poppath.replace("ivtstload.aspx", "../dsign/DgsignSkipUser.aspx");
                }

                //else if ((poppath.indexOf("vtstload.aspx?tstname=tnaof") > -1)) {
                //    poppath = poppath.replace("ivtstload.aspx", "../Dragdrop/allocationgrid.aspx");
                //    window.location.href = poppath;
                //    return;
                //}
            }

        }
        var winSuffix = GetDateTime();
        loadPop = createPopup(poppath);
        //parent.loadFrame();
        callParentNew("loadFrame()", "function");
    }


}


function IsDependent(parItem) {
    var i = 0;
    var IsDep = false;
    for (i = 0; i < depArr.length; i++) {
        var depArrVal = depArr[i].split(",");
        for (var h = 0; h < depArrVal.length; h++) {
            if (depArrVal[h] == parItem) {
                IsDep = true;
            }
        }
    }
    return IsDep;
}

function GetAllChecked(obj) {
    var chkValue = $j("#" + obj.id).prop("checked");
    if (chkValue) {
        $j("#" + obj.id + " input[type='checkbox']").each(function () {
            $j(this).prop("checked", true);
        });
    } else {
        $j("#" + obj.id + " input[type='checkbox']").each(function () {
            $j(this).prop('checked', false);
        });
    }
}

function UncheckChkAll(obj) {
    if (obj.checked == false) {
        try {
            $j("#" + obj.id + ".chkSelectAll").prop("checked", false);
        } catch (Ex) { }
    }

}

function GetChkParamValues(pValue, ctrl) {

    var tmpVal = "";
    var paramValue = "";
    var paramDiv = $j("#" + ctrl + " input[type='checkbox']");
    $j.each(paramDiv, function () {
        if ($j(this).is(":checked")) {
            tmpVal = $j(this).prop("value");
            if (paramValue == "")
                paramValue = ctrl + "~" + tmpVal;
            else {
                paramValue = paramValue + "&grave;" + tmpVal;
            }
        }
    });

    if (pValue == "")
        pValue = paramValue;
    else
        pValue = pValue + paramValue;


    return pValue;
}

function GetParamValues(calledFrom) {
    // ShowDimmer(true);
    calledFrom = calledFrom || "";
    var i = 0;
    var pValue = "";
    ArrDep.length = 0;
    var ctrl;
    var validationMsgArr = new Array();
    if ($j("#hdnSelParamsAftrChWin").val() !== null && $j("#hdnSelParamsAftrChWin").val() !== "") {
        pValue = $j("#hdnSelParamsAftrChWin").val();
        $j("#hdnSelParamsAftrChWin").val("");
        $j("#hdnGo").val("Go");
    }
    else {
        for (i = 0; i < parentArr.length; i++) {
            var ctrl = $j("#" + parentArr[i]);
            if (ctrl.length) {
                var fldType = ctrl.prop("type");
                if (fldType == "select-one") {
                    var result = ctrl.find("option:selected").val();
                    result = typeof result == "undefined" ? "" : result;
                    if ((result == "" || result == null) && (calledFrom != "clear"))// If any select parameter is empty should not call getIview
                    {
                        if (document.readyState == "complete") {
                            showAlertDialog("warning", 3032, "client");
                        }
                        return false;
                    }
                    var isDep = IsDependent(parentArr[i]);
                    if (isDep == true) {
                        var j = 0;
                        var depVal = "";
                        $j(ctrl).find("option").each(function () {
                            if (($j(this).val() == "0") && ($j(this).val() == "")) {

                            } else {
                                depVal += ($j(this).val()) + "$$$";
                            }
                        });
                        ArrDep.push(parentArr[i] + "~" + depVal);
                        pValue += parentArr[i] + "~" + result + "¿";
                    } else {
                        pValue += parentArr[i] + "~" + result + "¿";
                    }
                } else if (fldType == "select-multiple") {
                    var cnt = 0;
                    var ft = "false";
                    var ctrl1 = document.getElementById(parentArr[i]);
                    cnt = ctrl1.options.length;
                    for (j = 0; j < cnt; j++) {
                        if (ft == "false") {
                            if (ctrl1.options[j].selected == true) {
                                ft = "true";
                                pValue += parentArr[i] + "~" + ctrl1.options[j].value;
                            }
                        } else {
                            if (ctrl1.options[j].selected == true) {
                                pValue += "&grave;" + ctrl1.options[j].value;
                            }
                        }
                    }

                    if (pValue != "") pValue += "¿";
                } else if (fldType == "checkbox") {
                    pValue = GetChkParamValues(pValue, parentArr[i]);
                    if (pValue != "") pValue += "¿";
                } else {
                    var newval = ctrl.val();
                    var vFlag = true;
                    try {
                        vFlag = AxICheckValid();
                    } catch (e) {
                        if (vFlag == undefined)
                            vFlag = true;
                    }
                    if (vFlag == undefined)
                        vFlag = true;
                    if ((paramType[i] == "Date/Time") && (dtCulture.toLowerCase() == "en-us") && newval != "")
                        newval = GetDateStr(ctrl.val(), "mm/dd/yyyy", "dd/mm/yyyy");

                    if (paramType[i] == "Date/Time" && newval == ""){
                        ctrl.val(newval = $.datepicker.formatDate(dtFormat.replace("yyyy", "yy"), new Date()));
                    }

                    if (newval == "" && ctrl.parent().prev('td').find("font").text() != "" && vFlag) {
                        validationMsgArr.push(ctrl.parent().prev('td').find("font").text());
                    }
                    pValue += parentArr[i] + "~" + newval + "¿";
                }
            }
        }
    }
    //If the parameter is not visible no need to check this validation.
    if ($j("#paramCont").css('display') != 'none') {
        for (var j = 0; j < validationMsgArr.length; j++) {
            if (validationMsgArr[j] != "") {
                ShowDimmer(false);
                //parent.window.closeFrame();
                callParentNew("closeFrame()", "function");
                showAlertDialog("warning", 3011, "client", "\"" + validationMsgArr[j] + "\"");
                break;
            }
        }
    }

    if (validationMsgArr.length == 0) {
        //below condition is if the iview is opened from a customised page then it was giving undefined error
        if (window.parent.childWindowHandler == undefined) {
            $j("#hdnGo").val("Go");
        } else {
            if (window.parent.childWindowHandler.length > 0 && window.parent.childWindowHandler[window.parent.childWindowHandler.length - 1] != undefined && !window.parent.childWindowHandler[window.parent.childWindowHandler.length - 1].closed)
                $j("#hdnGo").val("TSSave");
            else
                $j("#hdnGo").val("Go");
        }
    }

    $j("#hdnparamValues").val(pValue);
    return true;
}

function SetParamValues(pval) {
    var stTime = new Date();
    AddDatePicker();
    AddTimePicker();
    if (clearParams) {
        FillDependentsStartup(true);
        clearParams = false;
        return;
    }

    if (pval != "" && pval != undefined) {
        var strParam = pval.split("¿");
        var i = 0;
        for (i = 0; i <= strParam.length - 1; i++) {
            if (strParam[i] != "") {
                var strVal = strParam[i].split("~");
                if (strVal[1] != null && strVal[1].indexOf("quot;") != -1) {
                    strVal[1] = strVal[1].replace(/quot;/g, "'");
                }
                var pctrl = $j("#" + strVal[0]);
                if (pctrl.length) {
                    //if ALL is there in dep param just remove all and space. If no dep values exists then fill the same into that combo.
                    var pctrlreset = false;
                    var fldType = pctrl.prop("type");
                    if (fldType == "select-one") {
                        var cblen = pctrl[0].options.length;
                        //check whether "ALL" is in combobox, if so remove it and continue to load values from ArrDep
                        if (cblen == 1) {
                            if (pctrl.find("option:selected").val() == "ALL") {
                                pctrl[0].options.length = 0;
                                cblen = 0;
                                pctrlreset = true;
                            }
                        }
                        if (cblen == 0) {
                            for (var ind = 0; ind < ArrDep.length; ind++) {
                                var strArr = ArrDep[ind].split('~');
                                if (strArr[0] == strVal[0]) {
                                    var strItems = strArr[1].split('$$$');
                                    var k = 0;
                                    pctrl[0].options.length = 0;
                                    for (k = 0; k < strItems.length - 1; k++) {
                                        // Add an Option object to Drop Down/List Box
                                        $j(pctrl).each(function () {
                                            $j(this).append($j("<option></option>").attr("value", strItems[k]).text(strItems[k]));
                                        });
                                    }
                                    $j(pctrl).find("option").each(function (cbi) {
                                        if ($j(this).text() == strVal[1]) {
                                            $j(this).attr("selected", "selected");
                                            return false;
                                        }
                                    });
                                }
                            }
                            cblen = pctrl[0].options.length;
                        }
                        //If Arrdep is not available, if "ALL" is removed earlier then just add ALL in to combo.
                        if ((cblen == 0) && (pctrlreset)) {
                            $j(pctrl).each(function () {
                                $j(this).append($j("<option></option>").attr("value", "ALL").text("ALL"));
                            });
                        }

                        $j(pctrl).find("option").each(function () {
                            $j(this).removeAttr("selected");
                            if ($j(this).text() == strVal[1]) {
                                $j(this).attr("selected", "selected");
                                // return false;
                            }
                        });
                    } else if (fldType == "select-multiple") {
                        var arrSel = strVal[1].split("&grave;");
                        for (var j = 0; j < arrSel.length; j++) {
                            $j(pctrl).find("option").each(function () {
                                if ($j(this).text() == arrSel[j]) {
                                    $j(this).attr("selected", "selected");
                                    return false;
                                }
                            });
                        }
                    } else if (fldType == "checkbox") {

                        var chekedVals = strVal[1].split("&grave;");
                        var chkdCnt = 0;
                        var totalCkbCnt = $j(pctrl).parent().next('div').find("input").length;
                        $j(pctrl).parent().next('div').find("input").each(function () {
                            $j(this).prop("checked", false);
                        });
                        for (var j = 0; j < chekedVals.length; j++) {
                            $j(pctrl).parent().next('div').find("input").each(function () {
                                if ($j(this).val() == chekedVals[j]) {
                                    $j(this).prop("checked", true);
                                    //$j(this).attr("checked", "checked");
                                    chkdCnt = chkdCnt + 1;
                                    return false;
                                }
                            });
                        }
                        if (totalCkbCnt === chekedVals.length) {
                            if ($j(pctrl).length > 0) {
                                //$j(pctrl).attr("checked", "checked");
                                $j(pctrl).prop("checked", true);
                            }
                        }

                    } else {
                        if ((parentArr[i] == strVal[0]) && (paramType[i] == "Date/Time") && (dtCulture.toLowerCase() == "en-us") && (strVal[1] != ""))
                            pctrl.val(GetDateStr(strVal[1], "dd/mm/yyyy", "mm/dd/yyyy"));
                        else
                            pctrl.val(strVal[1]);
                    }

                    $j("#hdnParamChngd").val("true");
                    //Set values into the Curfldvalue array
                }
            }
        }
    }
    if (!loadPop && window.parent.isSessionCleared && document.title == "Iview")
        window.parent.isSessionCleared = false;
    $j("#hdnGo").val("");
    // HideLoadingDiv();    
    AxWaitCursor(false);
    ShowDimmer(false);

    //TODO: code for displaying the time taken in iview
    //var edTime = new Date();
    //var diff = edTime.getTime() - stTime.getTime();
    //strTimeTaken += "SetParam-" + diff + " ";
}
var clearParams = false;

function ClearParamValues() {
    isFromClearBtn = true;
    //window.parent.loadFrame();
    callParentNew("loadFrame()", "function");

    clearParams = true;
    $j("#hdnparamValues").val('');

    $j("#form1").find('input:text').each(function () {
        $j(this).val("");
    });
    $j("#form1").find('select option').each(function () {
        if ($j(this).text() == '0' || $j(this).text() == '' || $j(this).text() == aXEmptyOption) { $j(this).attr("selected", "selected"); }
    });
    var allExist = false;
    var aXEmptyOptionExist = false;
    $j("#dvParamCont").find('select').each(function () {

        $.each($j(this).find("option"), function (index, value) {
            if ($(value).text() == aXEmptyOption) {
                aXEmptyOptionExist = true;
            }
            if ($(value).text() == "ALL") {
                allExist = true;
            }
        });
        if (allExist) {
            $j(this).val('ALL');
        } else if (aXEmptyOptionExist) {
            $j(this).val('');
        } else {
            $j(this).val($j(this).find("option:selected").val());
        }
    });

    if (document.title == "Iview"){
        GetParamValues("clear");
        $j("#hdnGo").val("clear");
    }

    arrCheckedFilter.length = 0;
    selectedFilterItem = '';

}

function clearFilterValues() {
    //window.parent.loadFrame();
    callParentNew("loadFrame()", "function");
    GetParamValues();
    arrCheckedFilter.length = 0;
    selectedFilterItem = '';

}


function iviewValidatedate(obj) {
    if (obj.val() != "  /  /") {
        if (isIviewDate(obj.val()) == false) {
            obj.val("");
            obj.focus()
            return false
        }
    }
    return true
}

function isIviewDate(dtStr) {
    dtStr = DateDisplayFormat(dtStr);
    if (dtStr != "") {
        var daysInMonth = DaysArray(12)
        var pos1 = dtStr.indexOf(dtCh)
        var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
        var strDay = dtStr.substring(0, pos1)
        var strMonth = dtStr.substring(pos1 + 1, pos2)
        var strYear = dtStr.substring(pos2 + 1)
        strYr = strYear
        if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
        if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
            if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
        }
        month = parseInt(strMonth)
        day = parseInt(strDay)
        year = parseInt(strYr)
        if (pos1 == -1 || pos2 == -1) {
            showAlertDialog("warning", 3012, "client");
            return false
        }
        if (strMonth.length < 1 || month < 1 || month > 12) {
            showAlertDialog("warning", 3013, "client");
            return false
        }
        if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
            showAlertDialog("warning", 3014, "client");
            return false
        }
        if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
            showAlertDialog("warning", 3015, "client", minYear + "^♠^" + maxYear);
            return false
        }
        if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
            showAlertDialog("warning", 3016, "client");
            return false
        }
    } else {
        return true
    }
    return true
}
//iview
function Calendarvalidate(evt, val) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    var sLen = val.length;
    if (((charCode == 46) || (charCode == 47)) && (sLen > 0)) {
        charCode = 49;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;

    return true;
}

//iview
function validatecalendar(evt, val) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    var sLen = val.length;
    return true;
}


function IsInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
//iviewand detailsview
function CheckKeyPressed(evt, ctrl) {
    var ctr = document.getElementById(ctrl);
    var TAB = 9;
    var ENTER = 13;
    var keyCode;
    if ("which" in evt) { // NN4 & FF &amp; Opera
        keyCode = evt.which;
    } else if ("keyCode" in evt) { // Safari & IE4+
        keyCode = evt.keyCode;
    } else if ("keyCode" in window.event) { // IE4+
        keyCode = window.event.keyCode;
    } else if ("which" in window.event) {
        keyCode = evt.which;
    } else { showAlertDialog("error", 3017, "client"); }

}

function ChkHdrCheckbox() {

    var chkAll = $j("#chkall");

    var allChecked = true;
    $j("input[name=chkItem]:checkbox").each(function () {
        var isChked = $j(this).prop("checked");
        if (isChked == undefined || isChked == false) {
            allChecked = false;
            return false;
        }
    });
    if (allChecked)
        chkAll.prop("checked", true);
    else
        chkAll.prop("checked", false);
}


// JScript File
//Function to create browser specofic xml object.
function CreateXmlObject(res) {
    if (ie) {
        xmlObj = new ActiveXObject("Microsoft.XMLDOM");
        xmlObj.loadXML(res);
    } else {
        parser = new DOMParser();
        xmlObj = parser.parseFromString(res, "text/xml");
    }
    return xmlObj;
}

//var xmlObj = new ActiveXObject("Microsoft.XMLDOM");
var xmlObj;
var ie = document.all;

function CheckSpecialCharsInXml(str) {

    if (str == null)
        return "";
    var str = str;
    str = str.replace(/&/g, "&amp;");
    str = str.replace(/</g, "&lt;");
    str = str.replace(/>/g, "&gt;");
    // single and double quote are part of parameter , so not replaced.
    return str;
}

//Function to refresh the dependent parameters in Iviews.
function FillDependents(parentParam) {

    ValParamChanged(parentParam);
    AxWaitCursor(true);
    //ShowDimmer(true);
    ctrlID = parentParam;
    var pIndex = 0;
    var innerIPText = "";
    var i = 0;
    for (i = 0; i < parentArr.length; i++) {
        if (parentArr[i] == parentParam) {
            if (depArr[i] != "") {
                for (pIndex = 0; pIndex < parentArr.length; pIndex++) {
                    var pItem = "";
                    var pType = "";
                    pItem = parentArr[pIndex];
                    pType = typeArr[pIndex];
                    pValue = "";
                    if (pType.toLowerCase() == "accept") {
                        pValue = $j("#" + pItem).val();
                        if ((paramType[pIndex] == "Date/Time") && (dtCulture.toLowerCase() == "en-us") && pValue != "")
                            pValue = GetDateStr(pValue, "mm/dd/yyyy", "dd/mm/yyyy");
                    } else if (pType.toLowerCase() == "select") {
                        var selected = $j("#" + pItem).find('option:selected')
                        if ($j('option:selected', "#" + pItem).index() >= 0)
                            pValue = selected.text();
                    } else if (pType.toLowerCase() == "multi select") {
                        pValue = typeof $j("#" + pItem).val() == "object" ? $j("#" + pItem).val().join("~") : $j("#" + pItem).val();
                    }
                    else {
                        pValue = $j("#" + pItem).val();
                    }

                    pValue = CheckSpecialCharsInXml(pValue);
                    if (parentParam == pItem && paramType[pIndex] == "Date/Time") {
                        if (!isDtSelected && pType.toLowerCase() == "accept") return;
                        else
                            isDtSelected = false;
                    }

                    innerIPText += "<" + pItem + ">" + pValue + "</" + pItem + ">";

                }

                //Get the dependent fields
                var depValues = "";
                var isDepFldsVisible = false;
                for (var di = 0; di < depArr.length; di++) {
                    if (depArr[di] != "") {
                        //If the changed parameter is part of the dep array then do not include.
                        var depFlds = "";
                        depFlds = CheckDepParents(parentParam, depArr[di], depValues);
                        if (depFlds != "") {
                            if (depValues == "")
                                depValues = depFlds;
                            else
                                depValues += "," + depFlds;

                            var depType = $("#" + depFlds).attr("type");
                        }
                        if (depType != undefined && depType != "hidden")
                            isDepFldsVisible = true;
                    }
                }
                if (isDepFldsVisible == true)
                    ShowDimmer(true);
                var trace = traceSplitStr + "GetDependParams-" + iName + traceSplitChar;
                var txt = '<sqlresultset name="' + iName + '" axpapp="' + proj + '" transid=""  sessionid="' + sid + '" trace="' + trace + '" depparams="' + depValues + '">';
                txt = txt + innerIPText;

                try {
                    isDepsProcessed = false;
                    ASB.WebService.GetDependParamsValues(txt, $j("#hdnKey").val(), SuccGetDependents, OnParDepException);
                } catch (e) {
                    isDepsProcessed = true;
                    AxWaitCursor(false);
                    ShowDimmer(false);
                    if (formSubmited) {
                        $j("#button1").click();
                    }
                }
            } else {
                FillHiddenValues(parentParam);
                AxWaitCursor(false);
                ShowDimmer
                    (false);
            }
            break;
        }
    }
    if ($j("#" + parentParam).hasClass("onlyTime")) {
        if ($j("#" + parentParam).val() != "" && ((!timePickerSec && $j("#" + parentParam).val() != "00:00") || (timePickerSec && $j("#" + parentParam).val() != "00:00:00"))) {
            //var ValidTime = GetValidTime($j("#" + parentParam).val());
            //$j("#" + parentParam).val(ValidTime);
            $j("#" + parentParam).val(CheckValidTime($j("#" + parentParam).val()));
        } else {
            //   $j("#" + parentParam).val("00:00");
            var paramHasExpr = false;
            for (var j = 0; j < parentArr.length; j++) {
                if (parentParam == parentArr[j]) {
                    if (Expressions[j] != "") {
                        paramHasExpr = true;
                        ExprHandler(parentParam, "");
                        break;
                    }
                }
            }

            if (!paramHasExpr) {
                if (timePickerSec) {
                    $j("#" + parentParam).val("00:00:00");
                } else {
                    $j("#" + parentParam).val("00:00");
                }
            }
        }
    }
}

//Function which checks if the changed parameter is part of the dependents then it will not add to the depparams.
//If the depparam is already part of the depvalues, then it will skip adding duplicates.
function CheckDepParents(parentParam, depArr, depValues) {
    var depFlds = "";
    var strDep = depArr.split(",");
    var strFlds = depValues.split(",");

    for (var k = 0; k < strDep.length; k++) {
        if (strDep[k] == parentParam)
            continue;
        else {
            var idx = $j.inArray(strDep[k], strFlds)
            if (idx == -1) {
                if (depFlds == "")
                    depFlds += strDep[k];
                else
                    depFlds += "," + strDep[k];
            }
        }
    }

    return depFlds;
}

function OnParDepException(result) {
    AxWaitCursor(false);
    ShowDimmer(false);
    isDepsProcessed = true;
    if (formSubmited) {
        $j("#button1").click();
    }
}

function ValParamChanged(parentParam) {
    //Check for changed parameter values
    var pValue = "";
    var paramCtrl = $j("#" + parentParam);
    if (paramCtrl.length) {
        if (paramCtrl.prop("type").toLowerCase() == "accept") {
            pValue = paramCtrl.val();
        } else if (paramCtrl.prop("type").toLowerCase() == "select") {
            pValue = $j("#" + pItem).find('option:selected').text();
        } else if (paramCtrl.prop("type").toLowerCase() == "select-one") {
            if ($j('option:selected', paramCtrl).index() != -1)
                pValue = $j(paramCtrl).find('option:selected').text();
        } else {
            pValue = paramCtrl.val();
        }
    }
    var ChCnt = 0;
    var i = 0;
    for (i = 0; i < parentArr.length; i++) {
        if (parentArr[i] == parentParam) {
            currFldValue = pCurArr[i];
            ChCnt = i;
            break;
        }
    }

    var IsParamChng = "";
    IsParamChng = $j("#hdnParamChngd").val();
    if (pValue != currFldValue && IsParamChng == "true") {
        var cutMsg = eval(callParent('lcm[6]'));
        $j("#lblErrMsg").text(cutMsg);
        pCurArr[ChCnt] = pValue;
    } else {
        $j("#lblErrMsg").text("");
    }
}

function SuccGetDependents(result, eventArgs) {
    AxWaitCursor(false);
    ShowDimmer(false);
    if (CheckSessionTimeout(result))
        return;
    var rexTxt = result.substring(0, 7);
    if (rexTxt == "<error>") {
        showAlertDialog("error", result);
        this.blur();
    } else {
        var res = result;
        try {

            xmlObj = CreateXmlObject(res);

        } catch (e) {

            showAlertDialog("error", e.message)

        }
        var select = "";
        var rootNode = xmlObj.getElementsByTagName("sqlresultset");
        var childCnt = rootNode[0].childNodes.length;
        var pID = "";
        var fldname = "";
        var pType = "";
        var fldValue = "";
        var cat = "";
        var isHidden = "";
        var hdnArr = new Array();
        var dataType = "";
        var i = 0;
        for (i = 0; i < childCnt; i++) {
            fldValue = rootNode[0].childNodes[i].getAttribute("value");
            cat = rootNode[0].childNodes[i].getAttribute("cat");
            if (cat == null) { continue; }
            var childcount = rootNode[0].childNodes[i].childNodes.length;
            var resultArr = new Array();
            for (var j = 0; j < childcount; j++) {
                pID = rootNode[0].childNodes[i].childNodes[j].tagName;
                if (pID == "a0") { fldname = rootNode[0].childNodes[i].childNodes[j].firstChild.nodeValue; }
                if (pID == "a4") { dataType = rootNode[0].childNodes[i].childNodes[j].firstChild.nodeValue; }
                if (pID == "a13") { pType = rootNode[0].childNodes[i].childNodes[j].firstChild.nodeValue; }
                if (pID == "a21") { isHidden = rootNode[0].childNodes[i].childNodes[j].firstChild.nodeValue; }
                if (pID == "response") {
                    if (rootNode[0].childNodes[i].childNodes[j].childNodes.length >= 1) {
                        var rows = rootNode[0].childNodes[i].childNodes[j].childNodes;
                        var m = 0;
                        for (m = 0; m < rootNode[0].childNodes[i].childNodes[j].childNodes.length; m++) {
                            if (ie) {
                                resultArr[m] = rootNode[0].childNodes[i].childNodes[j].childNodes[m].childNodes[0].text;
                            } else {
                                resultArr[m] = rootNode[0].childNodes[i].childNodes[j].childNodes[m].childNodes[0].textContent;
                            }
                        }

                    } else { }
                }
            }
            if (fldValue == null) {
                fldValue = "";
            }
            var parentCtrl = "";
            parentCtrl = document.getElementById(fldname);
            if ((dataType == "Date/Time") && (dtCulture.toLowerCase() == "en-us") && fldValue != "")
                fldValue = GetDateStr(fldValue, "mm/dd/yyyy", "dd/mm/yyyy");
            if (pType.toLowerCase() == "select" && isHidden == "false") {
                var k = 0;
                parentCtrl.options.length = 0;
                if (resultArr.length < 1) {
                    parentCtrl.options[0] = new Option("", 0);
                    n = 1;
                } else {
                    n = 0;
                    for (k = 0; k < resultArr.length; k++) {
                        if (k == 0) {
                            parentCtrl.options[n] = new Option("", "");
                            parentCtrl.options[n].title = "";
                            n++;
                        }
                        parentCtrl.options[n] = new Option(resultArr[k], resultArr[k]);
                        if (fldValue == resultArr[k])
                            parentCtrl.options[n].selected = true;
                        parentCtrl.options[n].title = resultArr[k];
                        n++;
                    }
                }
                var cnt = resultArr.length;
                resultArr.splice(0, cnt);
            }
            if (pType.toLowerCase() == "select" && isHidden == "true") {
                parentCtrl.value = fldValue;
            }
            else if (pType.toLowerCase() == "accept" && isHidden == "false") {
                parentCtrl.value = fldValue;
            }
            else if (pType.toLowerCase() == "accept" && isHidden == "true") {
                parentCtrl.value = fldValue;
                hdnArr.push(fldname);
            } else if (pType.toLowerCase() == "multi select") {
                var k = 0;
                //parentCtrl.options.length = 0;
                $(parentCtrl).parent().next().html("");
                var chkLstStr = ``;
                for (k = 0; k < resultArr.length; k++) {
                    
                    chkLstStr += `<span><input type='checkbox' id='${fldname}' class='chkAllList chkShwSel' onclick='UncheckChkAll(this);' value='${resultArr[k]}'/>${resultArr[k]}</span></br>`;
                    
                }
                $(parentCtrl).parent().next().html(chkLstStr);
                var cnt = resultArr.length;
                resultArr.splice(0, cnt);
            }
            else if (pType.toLowerCase() == "pick list" && isHidden == "false") {
                parentCtrl.value = fldValue;
            }
        }
    }

    onLoadDep = false;
    if (isFstParamsBound) {
        document.getElementById('button1').click();
        isFstParamsBound = false;
    }
    //Updating the param html assuming the param items have changed
    $j("#hdnParamHtml").val($j("#dvParamCont").html());
    isDepsProcessed = true;
    if (formSubmited) {
        $j("#button1").click();
    }
}

function ValidateOnSubmit() {
    ShowDimmer(true);
    //parent.window.loadFrame();
    var isInValid = true;
    $j("#hdnParamHtml").val($j("#dvParamCont").html());
    if (validateParamOnGo) {
        for (j = 0; j < parentArr.length; j++) {
            paramType = hiddenArr[j];
            if (paramType != "true") {
                expr = vExpressions[j];
                fldName = parentArr[j];
                if (expr != "") {
                    var result = Evaluate(fldName, fldName.valueOf(), expr, 'vexpr', "iview");
                    if (result != "T" && result != "true") {
                        var fcharRes = result.substring(0, 1);
                        if (fcharRes == "_") {
                            result = result.substring(1);
                            showAlertDialog("error", result);
                            var fld = $j("#" + fldName);
                            if (fld.length) {
                                fld.val('');
                                try {
                                    fld.focus();
                                } catch (ex) { }
                            }
                            return;
                        } else if (!confirm(result + ". Do you want to continue?"))
                            setTimeout("document.getElementById('" + paramNm + "').focus();", 50);

                        isInValid = false;
                        break;
                    }
                }
            }
        }
    }
    if (!clearParams) {
        if (isInValid)
            isInValid = GetParamValues();
    }
    arrCheckedFilter.length = 0;
    //parent.window.closeFrame();
    if (isDepsProcessed) {
        formSubmited = false;
        if (isInValid) { 
	    totRowCount = '';
	    clearAdvancedFiltersforNewData();
            var linkParams = $j("#hdnparamValues").val();
            linkParams = linkParams.replace(/~/g, "=");
            linkParams = linkParams.replace(/¿/g, "&");
            linkParams = ReplaceUrlSpecialChars(linkParams);
            if(linkParams != ""){
                setIviewNavigationData(linkParams, iName);
                callParentNew("updateAppLinkObj(" + window.location.href.replace("iview.aspx?ivname=", "ivtoivload.aspx?ivname=") + "&AxIvNav=true,1)", "function");
            }
	    resetSmartViewVariables();
	}
        else {
            ShowDimmer(false);
        }
        return isInValid;
    } else {
        formSubmited = true;
        return false;
    }
}

function setIviewNavigationData(paramsString, iviewName) {
    if (iviewName != "") {
        try {
            ASB.WebService.SetIviewNavigationData(paramsString, iviewName, function(){});
        } catch (ex) {
        }
    }
}

function evalParamExprHandler() {
    parentArr.forEach((val, ind) => {
        if ((typeArr[ind].toLowerCase() == "accept") && (Expressions[ind] !== "" && Expressions[ind].toLowerCase() !== "date()" && !exprSuggestions[ind] && hiddenArr[ind] != "true")) {
            //if((typeArr[ind].toLowerCase() == "accept") && (Expressions[ind] !== "" && !exprSuggestions[ind] && hiddenArr[ind] != "true")){
            ExprHandler(val, "");
        }
    });
}

function FillHiddenValues(paramName) {
    var i = 0;
    var j = 0;
    var cnt = 0;
    var paramType = "";
    var expr = "";
    var fldName = "";
    var isDependent = false;
    for (i = 0; i < parentArr.length; i++) {
        if (parentArr[i] == paramName) {
            cnt = i + 1;
            for (j = cnt; j < parentArr.length; j++) {
                paramType = hiddenArr[j];
                if (paramType == "true") {
                    expr = Expressions[j];
                    fldName = parentArr[j];
                    if (expr != "") {
                        ExprHandler(fldName, '');
                    }

                    FillDependents(fldName);
                    isDependent = true;
                    break;

                } else {
                    break;
                }
            }
            if (isDependent == true)
                break;
        }
    }
}

//function to return the date in a format required  by the GetChoicesXML service.
function GetDateParamFormat(Pdate) {
    var arrdt = new Array();
    arrdt = Pdate.split("/");
    var mon = "";
    mon = arrdt[1];
    mon = eval(mon) - 1;
    var monthNames = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    mon = monthNames[mon];

    var strDate = "";
    strDate = arrdt[0] + "-" + mon + "-" + arrdt[2];

    return strDate;

}


//Functions inserted for handling Expression dependencies

//<Module>  TStruct </Module>
//<Author>  Naveen  </Author>
//<Description> Function to Evaluate expressions on onblur event </Description>
//<Return> Returns the evaluated Result </Return>
var result = "";
var expfield = "";
var expfieldname = "";

function ExprHandler(ExpfldName, objName, focus = false) {
    //field containing the expression.
    if ((focus && objName == "") || !focus) {
        expfield = ExpfldName.toString();

        for (var j = 0; j < parentArr.length; j++) {
            if (expfield == parentArr[j]) {
                if (Expressions[j] != "") {
                    //expfield = expfield;
                    result = Evaluate(expfield, ExpfldName.valueOf(), Expressions[j], "expr");
                    if ((result == "username") || (result == "usergroup")) { } else {
                        assignfldvaliv(expfield, result);
                    }
                    break;
                } //End if checking Expressions null or not
            } //End if checking whether field is present or not
        } //End For
    }
}

function assignfldvaliv(flname, fval) {

    var actFldnamesv = flname;
    var datatype = "";
    var fld = $j("#" + flname);
    if (fld.length) // if field exists
    {
        // fix for datetype field returning 0
        for (var z = 0; z < parentArr.length; z++) {
            if (parentArr[z] == flname) {
                datatype = typeArr[z];
            }
        }
        if ((fld.prop("type") == "text") || (fld.prop("type") == "hidden")) {
            if (fval == "''")
                fval = "";
            if (!fval) fval = "";
            if (datatype == "Date/Time") {
                if (fval == 0)
                    fval = "";
            }
            if (typeof fval == "string")
                fval = checkIVSpecialCharInGlobalVar(fval);
            fld.val(fval);
        } else if (fld.prop("type") == "select-one") {
            var cblen = fld.find('option').length;
            for (var cbi = 0; cbi < cblen; cbi++) {
                if (fld.val() == fval) {
                    break;
                }
            }
            fld[cbi].selectedIndex;
        }
    }
}

function checkIVSpecialCharInGlobalVar(str) {
    str = str.replace(/&amp;/g, "&");
    str = str.replace(/&lt;/g, "<");
    str = str.replace(/&gt;/g, ">");
    str = str.replace(/&apos;/g, "'");
    str = str.replace(/&quot;/g, "\\");
    return str;
}

function ValidateVexpr(paramNm, vexpr) {
    if (validateParamOnGo)
        return;
    var result = Evaluate(paramNm, paramNm.valueOf(), vexpr, 'vexpr', "iview");
    if (result != "T" && result != "true") {
        var fcharRes = result.substring(0, 1);
        if (fcharRes == "_") {
            result = result.substring(1);
            showAlertDialog("error", result);
            var fld = $j("#" + paramNm);
            if (fld.length) {
                fld.val('');
                try {
                    fld.focus();
                } catch (ex) { }
            }
            return;
        }

        var cutMsg = eval(callParent('lcm[7]'));
        cutMsg = cutMsg.replace('{0}', result);
        var glType = eval(callParent('gllangType'));
        var isRTL = false;
        if (glType == "ar")
            isRTL = true;
        else
            isRTL = false;
        var ValidateVexprCB = $.confirm({
            closeIcon: false,
            title: eval(callParent('lcm[155]')),
            rtl: isRTL,
            onContentReady: function () {
                disableBackDrop('bind');
            },
            backgroundDismiss: 'false',
            escapeKey: 'buttonB',
            content: cutMsg,
            buttons: {
                buttonA: {
                    text: eval(callParent('lcm[164]')),
                    btnClass: 'hotbtn',
                    action: function () {
                        disableBackDrop('destroy');
                        ValidateVexprCB.close();
                    }
                },
                buttonB: {
                    text: eval(callParent('lcm[192]')),
                    btnClass: 'coldbtn',
                    action: function () {
                        setTimeout("document.getElementById('" + paramNm + "').focus();", 50);
                    }
                }
            }
        });
    }
}


function SetParamxml() {
    ShowDimmer(true);
    $j("#hdnGo").val('');
}
var curPageNo = 1;

function ToggleParams() {
    var divParams;
    divParams = $j("#paramCont");
    var imgFld = document.getElementById("imgArrow");
    var imgFld = $j("#imgArrow");

    if (imgFld.attr("alt") == "Hide") {
        imgFld.attr({
            alt: 'Show',
            src: '../AxpImages/arrowup.png'
        });
        divParams.hide();
    } else if (imgFld.attr("alt") == "Show") {
        imgFld.attr({
            alt: 'Hide',
            src: '../AxpImages/arrowdown.png'
        });
        divParams.show();
    }
}
//Function to check all the row once the header row is checked.
//Calling from iview & listview.
function CheckAll() {

    var chkAll = $j("#chkall");
    var chkItems;

    if (chkAll.prop("checked") == true) {
        chkItems = $j("input[name=chkItem]:checkbox").each(function () {
            $j(this).prop("checked", true);
        });
    } else {
        chkItems = $j("input[name=chkItem]:checkbox").each(function () {
            $j(this).prop("checked", false);
        });
    }
}

function ParamSearchAdvanceOpen(txtobj) {
    var id = txtobj.id;
    var fname1 = "";
    var fname = id.split('~');
    if (id.indexOf("~") != -1)
        fname1 = fname[1].toString();
    else
        fname1 = fname[0].toString();
    var x = screen.width / 2 - 520 / 2;
    var y = screen.height / 2 - 370 / 2;
    var obj = $j("#" + fname1);
    if (obj.val() != "") {
        var nwin = "./ivpicklist.aspx?sqlsearch=" + fname1 + "&searchval=" + obj.val() + "&ivname=" + iName + "&isPlDepParBound=" + isPlDepParBound;
        try {
            open(nwin, "SaveWindow", "width=520,height=370,left=" + x + ",top=" + y + ",scrollbars=no");
        } catch (ex) {
            showAlertDialog("warning", eval(callParent('lcm[356]')));
        }
    } else {
        var cutMsg = eval(callParent('lcm[8]'));
        alert(cutMsg);
        obj.focus();
    }

}

function CheckCond(obj) {
    var value2 = $j("#filVal2");

    if (obj.value == "between") {
        value2.prop("disabled", false);
    } else {
        value2.prop("disabled", true);
    }
}

function AddCalHgtToIview(topPos) {
    //var currentfr = parent.document.getElementById("middle1");
    //if (document.title == "Iview" && window.opener != undefined)
    //    currentfr = $j("#middle1", window.opener.parent.document);
    ////Subtracting extra 30 px for considering the (gray color) footer height both in iview and tstruct
    //var temp = parseInt(currentfr.clientHeight, 10) - parseInt(topPos, 10) - 30;

    //if (temp < 185)
    //    currentfr.style.height = parseInt(currentfr.clientHeight, 10) + (220 - temp);
}
var isDtSelected = false;
function AddDatePicker(parentID = "") {
    parentID = (!parentID.startsWith("#") && parentID != "") ? ("#" + parentID + " ") : parentID;
    if (!$j(parentID + ".date").hasClass("hasDatepicker")) {
        if ($j(parentID + ".date").length > 0) {
            $j(parentID + ".date").datepicker({
                showOn: "both",
                buttonImage: "../AxpImages/icons/24x24/calendar.png",
                buttonImageOnly: true,
                buttonText: "Select Date",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+100",
                dateFormat: dtFormat.substring(0, dtFormat.length - 2),
                onSelect: function (dateText, inst) {
                    compareDates($(this));

                    //if(!($(this).prop("disabled"))){
                    this.focus();
                    isDtSelected = true;
                    //}

                }

            }).bind(
                'blur', function (e) {
                    compareDates($(this));
                });
        }
    } else {
        if ($j("#ivParamTable img.ui-datepicker-trigger[alt='Select Date']").length) {

            //  $j('#ivParamTable .paramtd2 img.ui-datepicker-trigger').not(':first').remove();;
            //$j("#ivParamTable .paramtd2 img.ui-datepicker-trigger").remove();
            $j(parentID + "#ivParamTable .paramtd2 img.ui-datepicker-trigger[alt='Select Date']").parent().children(".date").removeClass("hasDatepicker");
            $j(parentID + "#ivParamTable .paramtd2 img.ui-datepicker-trigger[alt='Select Date']").parent().children(".date").datepicker({
                showOn: "both",
                buttonImage: "../AxpImages/icons/24x24/calendar.png",
                buttonImageOnly: true,
                buttonText: "Select Date",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+100",
                dateFormat: dtFormat.substring(0, dtFormat.length - 2),
                onSelect: function (dateText, inst) {
                    compareDates($(this));

                    //if(!($(this).prop("disabled"))){
                    this.focus();
                    isDtSelected = true;
                    //}
                }
            }).bind(
                'blur', function (e) {
                    compareDates($(this));
                });
            // $j("#ivParamTable .paramtd2 img.ui-datepicker-trigger:not(:first)").remove();
            $j("#ivParamTable .paramtd2").each(function (index) {
                ($j(this).children("img.ui-datepicker-trigger[alt='Select Date']").not(":first").remove());
            });
        } else {
            $j(parentID + ".date").removeClass("hasDatepicker");
            $j(parentID + ".date").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+100",
                dateFormat: dtFormat.substring(0, dtFormat.length - 2),
                onSelect: function (dateText, inst) {
                    compareDates($(this));

                    //if(!($(this).prop("disabled"))){
                    this.focus();
                    isDtSelected = true;
                    //}
                }
            }).bind(
                'blur', function (e) {
                    compareDates($(this));
                });
        }
    }
}

function compareDates(dtObj) {
    if (iviewValidatedate(dtObj)) {
        if (dtObj.parents(".gridDataFilter").length) {
            var objID = dtObj.attr("id")
            var obj1 = $("#" + objID.substr(0, objID.length - 1) + "1");
            var obj2 = $("#" + objID.substr(0, objID.length - 1) + "2");
            var obj1Val = 0;
            var obj2Val = 0;
            try {
                obj1Val = obj1.datepicker("getDate").getTime();
            } catch (ex) { }
            try {
                obj2Val = obj2.datepicker("getDate").getTime();
            } catch (ex) { }
            if (obj1Val > obj2Val && obj1Val != 0 && obj2Val != 0) {
                dtObj.val("");
                dtObj.focus();
                showAlertDialog("warning", callParentNew('lcm')[89]);
            }
        }
    }
}

function AddTimePicker(parentID = "") {
    parentID = (!parentID.startsWith("#") && parentID != "") ? ("#" + parentID + " ") : parentID;
    if (!$j(parentID + ".onlyTime").hasClass("hasDatepicker")) {
        if ($j(parentID + ".onlyTime").length > 0) {
            $j(parentID + ".onlyTime").timepicker({
                showOn: "button",
                buttonImage: "../AxpImages/icons/24x24/clock.png",
                buttonImageOnly: true,
                buttonText: "Select Time",
                controlType: 'select',
                oneLine: true,
                timeFormat: timePickerSec == true ? 'HH:mm:ss' : 'HH:mm',
                defaultValue: timePickerSec == true ? '00:00:00' : '00:00',
                onSelect: function (dateText, inst) {
                    //if(!($(this).prop("disabled"))){
                    $j("#" + inst.inst.id).trigger("blur");
                    //}
                }
            }).timeEntry({ show24Hours: true, showSeconds: timePickerSec == true ? true : false });
        }
    } else {
        if ($j("#ivParamTable img.ui-datepicker-trigger[alt='Select Time']").length) {
            $j(parentID + "#ivParamTable .paramtd2 img.ui-datepicker-trigger[alt='Select Time']").parent().find(".onlyTime").removeClass("hasDatepicker");
            $j($j(parentID + "#ivParamTable .paramtd2 img.ui-datepicker-trigger[alt='Select Time']").parent().find(".onlyTime")).timeEntry('destroy');
            //}
            $j(parentID + "#ivParamTable .paramtd2").find(".onlyTime").timepicker({
                showOn: "button",
                buttonImage: "../AxpImages/icons/24x24/clock.png",
                buttonImageOnly: true,
                buttonText: "Select Time",
                controlType: 'select',
                oneLine: true,
                timeFormat: timePickerSec == true ? 'HH:mm:ss' : 'HH:mm',
                defaultValue: timePickerSec == true ? '00:00:00' : '00:00',
                onSelect: function (dateText, inst) {
                    //if(!($(this).prop("disabled"))){
                    $j("#" + inst.inst.id).trigger("blur");
                    //}
                }
            }).timeEntry({ show24Hours: true, showSeconds: timePickerSec == true ? true : false });
            $j("#ivParamTable .paramtd2").each(function (index) {
                ($j(this).children("img.ui-datepicker-trigger[alt='Select Time']").not(":first").remove());
            });
        } else {
            $j(parentID + ".onlyTime").removeClass("hasDatepicker");
            $j(parentID + ".onlyTime").timeEntry('destroy');
            $j(parentID + ".onlyTime").timepicker({
                showOn: "button",
                controlType: 'select',
                oneLine: true,
                timeFormat: timePickerSec == true ? 'HH:mm:ss' : 'HH:mm',
                defaultValue: timePickerSec == true ? '00:00:00' : '00:00',
                onSelect: function (dateText, inst) {
                    //if(!($(this).prop("disabled"))){
                    $j("#" + inst.inst.id).trigger("blur");
                    //}
                }
            }).timeEntry({ show24Hours: true, showSeconds: timePickerSec == true ? true : false });
        }
    }
}

function GetUserLocale() {
    for (var i = 0; i < Parameters.length; i++) {

        var parameter = Parameters[i].split("~");
        if (parameter[0] == "Culture") {
            dtCulture = parameter[1];
        }
    }

    dtCulture = eval(callParent('glCulture')) || dtCulture;

    dtFormat = GetDateFormat(dtCulture);
    dateString = dtFormat;
}

function loadParent(a, b) {
    var gov = document.getElementById("searchlist");
    var grviewpno = document.getElementById("pgno");
    var pno = parseInt(grviewpno.value)
    gov.selectedIndex = (pno * 10) + parseInt(a, 10);
    var govlistval = "searchlistval";
    SetSelectedValue(gov, govlistval, b);
}

function SetSelectedValue(listcontrol, listctrl, b) {
    if (listcontrol[listcontrol.selectedIndex].value != -1) {
        var fname = b;
        var result = listcontrol[listcontrol.selectedIndex].value;
        eval('window.opener.document.getElementById("' + fname + '").value ="' + result + '";');
        eval('window.opener.document.getElementById("' + fname + '").focus();');
        var len = eval('form1.' + listctrl + '.options.length');
        if (len > 0) {
            if (eval('form1.' + listctrl + '[' + listcontrol.selectedIndex + '].value') != -1) {
                var fldarr = eval('form1.' + listctrl + '[' + listcontrol.selectedIndex + '].value');
                fldarr = fldarr.split('~');
                for (var fill = 0; fill < fldarr.length; fill++) {
                    if (fldarr[fill] != "") {
                        var fieldnm = fldarr[fill].toString();
                        fieldnm = fieldnm.split('***');
                        if (eval("document.forms['f1']." + fieldnm[0] + cbnameExt)) {
                            if (eval("document.forms['f1']." + fieldnm[0] + cbnameExt + ".type") != "radio") {
                                if (fieldnm[1] == "*") {
                                    eval('document.f1.' + fieldnm[0] + cbnameExt + '.value=""');
                                } else {
                                    eval('document.f1.' + fieldnm[0] + cbnameExt + '.value="' + fieldnm[1] + '"');
                                }
                            }
                        }
                    }
                }
            }
        }
        window.close();
    }
}

var depParamCnt = 0;
var isFstParamsBound = false;
var onLoadDep = false;
//On load parameter ,Get all the depentent value
function FillDependentsStartup(isParamVal) {
    var stTime = new Date();
    depParamCnt = 0;
    isFstParamsBound = isParamVal;

    try {
        AxFillDepOnLoad();
    } catch (ex) {

    }

    if ((isParamVal == true && isFromClearBtn == false) || $("#hdnSelParamsAftrChWin").val() != "") {
        document.getElementById('button1').click();
        isFstParamsBound = false;
    } else if (isFromClearBtn) {
        isFstParamsBound = false;
        onLoadDep = true;
    }
    else {
        onLoadDep = true;
    }
    //TODO: code for displaying the time taken in iview
    //var edTime = new Date();
    //var diff = edTime.getTime() - stTime.getTime();
    //strTimeTaken += "FillDepStart-" + diff + " ";
}

//show the tooltip for iview dropdownlist
function showHideTooltip(objId) {

    try {
        document.getElementById(objId).title = (document.getElementById(objId).options[($("#" + objId)[0].selectedIndex)].text);
    } catch (ex) { }
}

//This function also available in the process.js
function CheckSpCharsInFldValueIview(fldValue) {

    var index = fldValue.indexOf("^^dq");
    while (index != -1) {
        fldValue = fldValue.replace("^^dq", '%22');
        index = fldValue.indexOf("^^dq");
    }
    if (fldValue.indexOf(";bkslh") != -1) {
        fldValue = fldValue.replace(new RegExp(";bkslh", "g"), "\\");
    }
    return fldValue;
}

// Show hide  on row click for list view action

function showHide(id) {

    var i = id;
    var lstCust = $j(".ListCust");
    lstCust.hide();

    $j("#div" + i).show();

    $j('#tblProp tr').each(function () {

        var cls = $j(this)[0].className


        if (cls.indexOf('selProperty') >= 0) {
            $j(this).removeClass('selProperty');
            $j(this).addClass('RowCust');
        }
    })

    $j("#" + i).removeClass('RowCust');
    $j("#" + i).addClass('selProperty');
}

//Open List view action winow
function openListActionWin() {
    $j(".ListCust").hide();
    $j("#divtr1").show();
    $j("#tr1").css('background-color', '#D9D2D4');
    //$j("#divAction").show();
    $j("#divAction").dialog({ title: "List view actions", height: 400, width: 800, position: 'center', modal: true, buttons: { "Ok": function () { ApplyCondition($j(this)); } } });
    Adjustwin();
}

function ApplyCondition(dialogObj) {
    $j("#button1").click();
}

function ToggleParamsLstProp() {
    var divParams;
    var divProp = $j("#divProperty");
    var imgFld = document.getElementById("imgArrow");
    var imgFld = $j("#imgArrow");

    if (imgFld.attr("alt") == "Hide") {
        imgFld.attr({
            alt: 'Show',
            src: '../AxpImages/arrow-up-black.gif'
        });
        divProp.hide();
    } else if (imgFld.attr("alt") == "Show") {
        imgFld.attr({
            alt: 'Hide',
            src: '../AxpImages/arrow-down-black.gif'
        });
        divProp.show();
    }
}

function client_OnTreeNodeCheckedList() {
    var obj = window.event.srcElement;
    var treeNodeFound = false;
    var checkedState;
    var currentNode;
    ValidateGridDc();

    if (griddccnt > 1) {
        var treeNode = obj;
        treeNode.checked = false;
        return;
    }
    if (obj.tagName == "INPUT" && obj.type == "checkbox") {
        var treeNode = obj;
        checkedState = treeNode.checked;
        if (checkedState == true || checkedState == false) {
            if (hdnTree == "") {
                hdnTree += treeNode.parentElement.children[1].innerHTML;
            } else {
                hdnTree += "~" + treeNode.parentElement.children[1].innerHTML;
            }
            NewTree.push(treeNode.parentElement.children[1].innerHTML);
        }

        do {
            obj = obj.parentElement;
        } while (obj.tagName != "TABLE")
        var parentTreeLevel = obj.rows[0].cells.length;
        var parentTreeNode = obj.rows[0].cells[0];
        var tables = obj.parentElement.getElementsByTagName("TABLE");
        var numTables = tables.length
        if (numTables >= 1) {
            for (i = 0; i < numTables; i++) {
                if (tables[i] == obj) {
                    treeNodeFound = true;
                    i++;
                    if (i == numTables) {
                        return;
                    }
                }
                if (treeNodeFound == true) {
                    changed = true;
                    var childTreeLevel = tables[i].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel) {
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}

var griddccnt = 0;

function ValidateGridDc() {
    var PNode;
    griddccnt = 0;
    $j('#tvList input[type=checkbox]:checked').each(function () {
        if ($j(this).parentNode != undefined)
            PNode = $j(this).parentNode;
        else
            PNode = $j(this);

        if (PNode[0].nextSibling.innerHTML.indexOf("Grid DC") != -1)
            griddccnt++;
    });

    if (griddccnt > 1)

        showAlertDialog("warning", 3018, "client");
    return;
}

var childdccnt = 0;
var childdccnt1 = 0;
var childdccnt2 = 0;

function validateChildDc() {
    var indc = 0;
    var obj = window.event.srcElement;

    var treeNode = obj;
    var TreeView = document.getElementById("tvList")
    var checkboxs = TreeView.getElementsByTagName("input")
    childdccnt1 = 0;
    childdccnt2 = 0;
    for (i = 0; i < checkboxs.length; i++) {
        if (checkboxs[i].type == "checkbox") {

            var NodeText = checkboxs[i].nextSibling.innerHTML;
            if (checkboxs[i].nextSibling.innerHTML.indexOf("Grid DC") != -1) {
                indc = 1 + indc;
            }
            if (checkboxs[i].checked && indc == 1) {
                childdccnt1 = 1;
            } else if (checkboxs[i].checked && indc == 2) {
                childdccnt2 = 2;
            }
        }
    }

    if (childdccnt1 >= 1 && childdccnt2 >= 2)

        showAlertDialog("warning", 3018, "client");
    return;
}

var fldName = "";

function checkDc() {

    var strnorepeat = $j("#hdnNoRepeat").val();
    $j('#tblNorep input[type=checkbox]').each(function () {
        var chkName = $j(this)[0].value;
        var tbl = $j(this);
        var nodecount = 0;
        $j('#tvList input[type=checkbox]:checked').each(function () {

            var node = $j(this).next('a');
            var nodeValue = '';
            var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);

            if (chkName == nodeValue)

                nodecount = 1;
        })

        if (nodecount == 0)
            tbl.parent().parent().remove();
    })

    var PNode = "";
    var parents = $j("#hdnParent").val();
    $j('#tvList input[type=checkbox]:checked').each(function () {
        PNode = $j(this);
        if (parents.indexOf(PNode[0].nextSibling.innerHTML) != -1 && (PNode[0].nextSibling.innerHTML) != "") { } else {
            fldName = 0;

            var NodeText = PNode[0].nextSibling.innerHTML;
            var node = $j(this).next('a');
            var nodeValue = '';
            var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);
            var tableRef = document.getElementById('tblNorep');

            $j('#tblNorep input[type=checkbox]').each(function () {
                var a = $j(this)[0].title;

                var value = $j(this)[0].value;

                if (value == nodeValue) {
                    fldName = 1;
                }
            })

            if (fldName == 0) {
                var len = $j('#tblNorep tr').length;
                var tableRow = tableRef.insertRow(len);

                var tableCell = tableRow.insertCell();

                var checkBoxRef = document.createElement('input');

                var labelRef = document.createElement('label');

                checkBoxRef.type = 'checkbox';
                checkBoxRef.value = nodeValue;
                checkBoxRef.title = NodeText;
                labelRef.innerHTML = NodeText;
                tableCell.title = NodeText;

                tableCell.appendChild(checkBoxRef);
                tableCell.appendChild(labelRef);

            }
        }
    })

    if (strnorepeat != "" || strnorepeat != null) {

        $j('#tblNorep input[type=checkbox]').each(function () {

            var value = $j(this)[0].value;
            if (strnorepeat.indexOf(value) != -1) {
                $j(this)[0].checked = true;
            }

        })
    }
}

var fld = "";

function bindSubtotalColmn() {

    var strsubtotlCaptn = $j("#hdnSubtotlCaptn").val();

    $j('#tblSubtotal  input[type=checkbox]').each(function () {

        var chkName = $j(this)[0].value;
        var tbl = $j(this);
        var nodecount = 0;
        $j('#tvList input[type=checkbox]:checked').each(function () {

            PNode = $j(this);
            var node = $j(this).next('a');
            var nodeValue = '';
            var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);
            if (chkName == nodeValue)

                nodecount = 1;
        })

        if (nodecount == 0)
            //  $j(this).removeNode();
            tbl.parent().parent().remove();
    })

    var PNode = "";
    var parents = $j("#hdnParent").val();
    $j('#tvList input[type=checkbox]:checked').each(function () {

        PNode = $j(this);

        if (parents.indexOf(PNode[0].nextSibling.innerHTML) != -1 && (PNode[0].nextSibling.innerHTML) != "") { } else {
            fld = 0;

            var NodeText = PNode[0].nextSibling.innerHTML;
            var tableRef = document.getElementById('tblSubtotal');

            var node = $j(this).next('a');
            var nodeValue = '';
            var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);

            $j('#tblSubtotal  input[type=checkbox]').each(function () {

                var value = $j(this)[0].value;

                if (strsubtotlCaptn.indexOf(value) != -1) {
                    $j(this)[0].checked = true;
                }
                if (value == nodeValue) {
                    fld = 1;
                }
            })

            if (fld == 0) {

                var len = $j('#tblSubtotal tr').length;
                var tableRow = tableRef.insertRow(len);
                var tableCell = tableRow.insertCell();
                var checkBoxRef = document.createElement('input');
                var labelRef = document.createElement('label');
                checkBoxRef.type = 'checkbox';
                checkBoxRef.value = nodeValue;
                checkBoxRef.title = NodeText;
                labelRef.innerHTML = NodeText + "(" + nodeValue + ")";
                labelRef.width = "20px";
                tableCell.title = NodeText;
                tableCell.appendChild(checkBoxRef);
                tableCell.appendChild(labelRef);
            }
        }
    })

    if (strsubtotlCaptn != '' || strsubtotlCaptn != null) {
        $j('#tblSubtotal  input[type=checkbox]').each(function () {

            var value = $j(this)[0].value;
            if (strsubtotlCaptn.indexOf(value) != -1) {
                $j(this)[0].checked = true;
            }
        })
    }

    $j("#tblSubtotal input[type=checkbox]").click(function () {
        var a = $j(this);
        var text = this.title;
        var val = $j(this)[0].value;

        if ($j(this).attr("checked") == "checked") {

            $j("#txtFooter").val("Total");
            $j("#txtHeader").val(val);
            $j("#ddlCaption").val(val);
            $j("#txtSubOrder").val("0");

        } else {
            if ($j("#txtHeader").val() == text) {

                $j("#txtFooter").val("");
                $j("#txtHeader").val("");
                $j("#ddlCaption option:selected").text("");
                $j("#ddlCaption option:selected").val("");
                $j("#txtSubOrder").val("");
            }
            $j('#tblSubtotlList').each(function () {
                var a = $j('#tblSubtotlList tr > td:contains(' + text + ')').parent().remove();
            })
        }
    });

    $j("#tblSubtotal").each(function () {
        $j("tr", this).addClass("clsSubtotl");
    });
}

function bindSubttlDrpdwn() {

    $j("#ddlCaption").empty();
    var parents = $j("#hdnParent").val();
    var drpdown = $j("#ddlCaption");
    var options = '<option value="' + "" + '">' + "" + '</option>';
    $j('#tvList input[type=checkbox]:checked').each(function () {
        if ($j(this).parentNode != undefined)
            PNode = $j(this).parentNode;
        else {
            PNode = $j(this);

            if (parents.indexOf(PNode[0].nextSibling.innerHTML) != -1 && (PNode[0].nextSibling.innerHTML) != "") { } else {

                var NodeText = PNode[0].nextSibling.innerHTML;
                var node = $j(this).next('a');
                var nodeValue = '';
                var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
                var nodeValues = nodePath.split("\\");
                if (nodeValues.length > 1)
                    nodeValue = nodeValues[nodeValues.length - 1];
                else
                    nodeValue = nodeValues[0].substr(1);

                options += '<option value="' + nodeValue + '">' + NodeText + '</option>';
            }
        }
    });
    drpdown.html(options);
}

//Add new condition row
function AddNewListCondition() {

    var dv = document.getElementById("tblFilter");
    var rowCount = "0";
    var rowCnt = document.getElementById("FilterRowCount");
    if (rowCnt)
        rowCount = rowCnt.value;
    var index = (parseInt(rowCount) + 1).toString();
    var newRow = dv.insertRow();
    var ocell = newRow.insertCell();
    ocell = newRow.insertCell();
    var el = document.createElement("input");
    el.type = "radio";
    el.name = "grpAndOr" + index;
    el.id = "radAnd" + index;
    el.value = "And";
    var el1 = document.createElement("input");
    el1.type = "radio";
    el1.name = "grpAndOr" + index;
    el1.id = "radOr" + index;
    el1.value = "Or";

    ocell.appendChild(el);
    var spn = document.createElement("span");
    spn.innerHTML = "And";
    ocell.appendChild(spn);

    ocell.appendChild(el1);
    spn = document.createElement("span");
    spn.innerHTML = "Or";
    ocell.appendChild(spn);
    ocell.style.align = "right";

    var rad = document.getElementById("radOr" + index);
    if (rad)
        rad.checked = true;

    var newRow = dv.insertRow();
    index = (parseInt(index) + 1).toString();
    var cell = newRow.insertCell();
    var ddlClm = document.createElement("select");
    ddlClm.id = "ddlListFilter" + index;
    ddlClm.className = "lblTxt";
    ddlClm.style.width = "120px";
    cell.appendChild(ddlClm);

    var ddlFilter = document.getElementById("ddlListFilter" + index);
    ddlFilter.length = 0;
    if (ddlFilter) {
        var parDll = document.getElementById("ddlListFilter");
        if (parDll.length > 0) {
            for (var i = 0; i < parDll.length; i++) {
                var opt = document.createElement("option");
                opt.text = parDll.options[i].text;
                opt.value = parDll.options[i].value;
                ddlFilter.options.add(opt);
            }
        }
    }

    cell = newRow.insertCell();
    var ddlOpr = document.createElement("select");
    ddlOpr.id = "ddlOpr" + index;
    ddlOpr.className = "lblTxt";
    cell.appendChild(ddlOpr);

    var ddlOperator = document.getElementById("ddlOpr" + index);
    ddlOperator.length = 0;
    if (ddlOperator) {
        var parOpr = document.getElementById("ddlListFilcond");
        if (parOpr.length > 0) {
            for (var i = 0; i < parOpr.length; i++) {
                var opt = document.createElement("option");
                opt.text = parOpr.options[i].text;
                opt.value = parOpr.options[i].value;
                ddlOperator.options.add(opt);
            }
        }
    }

    cell = newRow.insertCell();
    var inpVal1 = document.createElement("input");
    inpVal1.type = "text";
    inpVal1.id = "txtFilter" + index;
    inpVal1.className = "lblTxt";
    inpVal1.style.width = "160px";
    cell.appendChild(inpVal1);

    cell = newRow.insertCell();
    var inpVal1 = document.createElement("input");
    inpVal1.type = "text";
    inpVal1.id = "txtFillVal" + index;
    inpVal1.className = "lblTxt";
    inpVal1.style.width = "160px";
    cell.appendChild(inpVal1);

    cell = newRow.insertCell();

    //Added the anchor tag to hold the delete button since the onclick event cannot be assigned for image.
    var anc = document.createElement("a");
    anc.id = "delAnc" + index;
    anc.href = "javascript:DeleteListCondition('" + index + "');";
    anc.className = "ancLink";

    var delImg = document.createElement("span");
    //delImg.src = "../AxpImages/icons/16x16/delete.png";
    delImg.className = "curHand icon-arrows-circle-minus";
    delImg.id = "delCond" + index;
    anc.appendChild(delImg);
    cell.appendChild(anc);

    rowCnt.value = index;
}

function AddCondition() {
    var isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent);
    if (isSafari) {
        var dv = document.getElementById("tblFilter");
        var rowCount = "0";
        var rowCnt = document.getElementById("FilterRowCount");
        if (rowCnt)
            rowCount = rowCnt.value;
        var index = (parseInt(rowCount) + 1).toString();
        var newRow = dv.appendChild(document.createElement("tr"));

        var cell = newRow.insertCell();
        var anc = document.createElement("a");
        anc.id = "delAnc" + index;
        anc.title = "Remove condition";
        anc.href = "javascript:DeleteListCondition('" + index + "');";

        var delImg = document.createElement("span");
        //delImg.src = "../AxpImages/icons/16x16/delete.png";
        delImg.className = "curHand icon-arrows-circle-minus";
        delImg.id = "delCond" + index;


        anc.appendChild(delImg);
        cell.appendChild(anc);

        cell = newRow.insertCell();
        var inpVal1 = document.createElement("input");
        inpVal1.type = "text";
        inpVal1.id = "txtFillVal" + index;
        inpVal1.className = "txtfilter tem Family form-control";
        inpVal1.title = "Enter filter text";
        inpVal1.style.width = "180px";
        cell.appendChild(inpVal1);

        cell = newRow.insertCell();
        var inpVal1 = document.createElement("input");
        inpVal1.type = "text";
        inpVal1.id = "txtFilter" + index;
        inpVal1.className = "txtfilter tem Family form-control";
        inpVal1.title = "Enter filter text";
        inpVal1.style.width = "180px";
        cell.appendChild(inpVal1);

        cell = newRow.insertCell();
        var ddlOpr = document.createElement("select");
        ddlOpr.id = "ddlOpr" + index;
        ddlOpr.className = "ddlListFilter combotem Family form-control";
        ddlOpr.title = "Select filter conditions";
        ddlOpr.style.width = "145px";
        cell.appendChild(ddlOpr);

        var a = $j("ddlListFilter").width();
        var ddlOperator = document.getElementById("ddlOpr" + index);
        ddlOperator.length = 0;
        if (ddlOperator) {
            var parOpr = document.getElementById("ddlListFilcond");
            if (parOpr.length > 0) {
                for (var i = 0; i < parOpr.length; i++) {
                    var opt = document.createElement("option");
                    opt.text = parOpr.options[i].text;
                    opt.value = parOpr.options[i].value;
                    ddlOperator.options.add(opt);
                }
            }

        }

        var cell = newRow.insertCell();
        var ddlClm = document.createElement("select");
        ddlClm.id = "ddlListFilter" + index;
        ddlClm.className = "ddlListFilter combotem Family form-control";
        ddlClm.title = "Select column";
        ddlClm.style.width = "200px";
        cell.appendChild(ddlClm);

        var ddlFilter = document.getElementById("ddlListFilter" + index);
        ddlFilter.length = 0;
        if (ddlFilter) {
            var parDll = document.getElementById("ddlListFilter");
            if (parDll.length > 0) {
                for (var i = 0; i < parDll.length; i++) {
                    var opt = document.createElement("option");
                    opt.text = parDll.options[i].text;
                    opt.value = parDll.options[i].value;
                    ddlFilter.options.add(opt);
                }
            }
        }

        var ocell = newRow.insertCell();
        ocell = newRow.insertCell();

        var el = document.createElement("input");
        el.type = "radio";
        el.name = "grpAndOr" + index;
        el.id = "radAnd" + index;
        el.value = "And";
        var el1 = document.createElement("input");
        el1.type = "radio";
        el1.name = "grpAndOr" + index;
        el1.id = "radOr" + index;
        el1.value = "Or";

        ocell.className = "paramtd1";
        ocell.appendChild(el);
        var spn = document.createElement("span");
        spn.innerHTML = "And";
        ocell.appendChild(spn);

        ocell.appendChild(el1);
        spn = document.createElement("span");
        spn.innerHTML = "Or";
        ocell.appendChild(spn);
        ocell.style.align = "right";

        var rad = document.getElementById("radOr" + index);
        if (rad)
            rad.checked = true;

        rowCnt.value = index;
        var a = $j("dvNewrow").height();
        var b = $j("tblpanel").height();
    } else {
        var dv = document.getElementById("tblFilter");

        var rowCount = "0";

        var rowCnt = document.getElementById("FilterRowCount");
        if (rowCnt)
            rowCount = rowCnt.value;
        var index = (parseInt(rowCount) + 1).toString();

        var newRow = dv.insertRow();
        newRow.setAttribute("id", "tblFilterRowID" + index);
        var ocell = newRow.insertCell();
        ocell = newRow.insertCell();
        var el = document.createElement("input");
        el.type = "radio";
        el.name = "grpAndOr" + index;
        el.id = "radAnd" + index;
        el.value = "And";
        var el1 = document.createElement("input");
        el1.type = "radio";
        el1.name = "grpAndOr" + index;
        el1.id = "radOr" + index;
        el1.value = "Or";

        ocell.className = "paramtd1";
        ocell.appendChild(el);
        var spn = document.createElement("span");
        spn.innerHTML = "And";
        ocell.appendChild(spn);

        ocell.appendChild(el1);
        spn = document.createElement("span");
        spn.innerHTML = "Or";
        ocell.appendChild(spn);
        ocell.style.align = "right";

        var cell = newRow.insertCell();
        var ddlClm = document.createElement("select");
        ddlClm.id = "ddlListFilter" + index;
        ddlClm.className = "ddlListFilter combotem Family form-control";
        ddlClm.title = "Select column";
        ddlClm.style.width = "200px";
        cell.appendChild(ddlClm);

        var ddlFilter = document.getElementById("ddlListFilter" + index);
        ddlFilter.length = 0;
        if (ddlFilter) {
            var parDll = document.getElementById("ddlListFilter");
            if (parDll.length > 0) {
                for (var i = 0; i < parDll.length; i++) {
                    var opt = document.createElement("option");
                    opt.text = parDll.options[i].text;
                    opt.value = parDll.options[i].value;
                    ddlFilter.options.add(opt);
                }
            }
        }

        cell = newRow.insertCell();
        var ddlOpr = document.createElement("select");
        ddlOpr.id = "ddlOpr" + index;
        ddlOpr.className = "ddlListFilter combotem Family form-control";
        ddlOpr.title = "Select filter conditions";
        ddlOpr.style.width = "200px";
        cell.appendChild(ddlOpr);
        var a = $j("ddlListFilter").width();

        var ddlOperator = document.getElementById("ddlOpr" + index);
        ddlOperator.length = 0;
        if (ddlOperator) {
            var parOpr = document.getElementById("ddlListFilcond");
            if (parOpr.length > 0) {
                for (var i = 0; i < parOpr.length; i++) {
                    var opt = document.createElement("option");
                    opt.text = parOpr.options[i].text;
                    opt.value = parOpr.options[i].value;
                    ddlOperator.options.add(opt);
                }
            }
        }

        cell = newRow.insertCell();
        var inpVal1 = document.createElement("input");
        inpVal1.type = "text";
        inpVal1.id = "txtFilter" + index;
        inpVal1.className = "txtfilter tem Family form-control";
        inpVal1.title = "Enter filter text";
        inpVal1.style.width = "100px";
        cell.appendChild(inpVal1);

        cell = newRow.insertCell();
        var inpVal1 = document.createElement("input");
        inpVal1.type = "text";
        inpVal1.id = "txtFillVal" + index;
        inpVal1.className = "txtfilter tem Family form-control";
        inpVal1.title = "Enter filter text";
        inpVal1.style.width = "100px";
        cell.appendChild(inpVal1);

        cell = newRow.insertCell();

        var anc = document.createElement("a");
        anc.id = "delAnc" + index;
        anc.title = "Remove condition";
        anc.href = "javascript:DeleteListCondition('" + index + "');";

        var delImg = document.createElement("span");
        //delImg.src = "../AxpImages/icons/16x16/delete.png";
        delImg.className = "curHand icon-arrows-circle-minus";
        delImg.id = "delCond" + index;

        anc.appendChild(delImg);
        cell.appendChild(anc);

        var rad = document.getElementById("radOr" + index);
        if (rad)
            rad.checked = true;

        rowCnt.value = index;

        var a = $j("dvNewrow").height();
        var b = $j("tblpanel").height();
    }
}

//function DeleteListCondition(index) {
//    if (window.chrome) {
//        var dv = document.getElementById("tblFilter");
//        dv.deleteRow(index - 1);
//    }
//    else {
//        var delImg = document.getElementById("delAnc" + index);
//        var rowIndex = delImg.parentElement.parentElement.rowIndex;
//        var dv = document.getElementById("tblFilter");
//        dv.deleteRow(rowIndex);
//    }
//    var index = (parseInt(index) - 1).toString();
//    var rowCnt = document.getElementById("FilterRowCount");
//    rowCnt.value = index;

//}
function DeleteListCondition(index) {
    if (window.chrome) {
        var dv = document.getElementById("tblFilterRowID" + index);
        dv.remove();
    } else {
        var delImg = document.getElementById("delAnc" + index);
        var rowIndex = delImg.parentElement.parentElement.rowIndex;
        var dv = document.getElementById("tblFilterRowID" + index);
        dv.remove();
    }
    var index = (parseInt(index) - 1).toString();
    var rowCnt = document.getElementById("FilterRowCount");
    rowCnt.value = index;

}

function RefreshFirstListCondition() {
    $("#txtListFilter").val("");
    $("#ListfilVal2").val("");
    $("#ddlListFilcond").val($("#ddlListFilcond option:first").val());
    $("#ddlListFilter").val($("#ddlListFilter option:first").val());
}

//Enable disable textbox on filter value condition
function CheckListFilterCond(index) {

    if (index == -1) {
        var ddlCond = $j("#ddlListFilcond");
        var value2 = $j("#ListfilVal2");
        var value1 = $j("#txtListFilter");
        if (ddlCond.val() == "between") {
            value1.prop("disabled", false);
            value2.prop("disabled", false);
        } else if (ddlCond.val() == "is null") {
            value1.prop("disabled", true);
            value2.prop("disabled", true);
        } else {
            value1.prop("disabled", false);
            value2.prop("disabled", true);
        }
        value2.val("");
        value1.val("");
    }
}

//Get treeview selected columns
function GetSelectedColumns() {

    var strCols = "";
    $j('#tvList input[type=checkbox]:checked').each(function () {
        if ($j(this).parentNode == undefined) {
            if (strCols == "")
                strCols = $j(this)[0].nextSibling.innerHTML;
            else
                strCols += "," + $j(this)[0].nextSibling.innerHTML;
        }

    });

    $j("#hdnColumns").val(strCols);
}

function CheckSpecialCharsInStr(str) {

    if (str == null)
        return "";
    var str = str;
    str = str.replace(/<>/g, "");
    str = str.replace(/</g, "");
    str = str.replace(/>/g, "");
    str = str.replace(/>=/g, "");
    str = str.replace(/<=/g, "");
    str = str.replace(/'/g, "");
    str = str.replace(/"/g, "");
    str = str.replace(/,/g, "");
    str = str.replace(/./g, "");
    str = str.replace(/-/g, "");

    // single and double quote are part of parameter , so not replaced.
    return str;
}

// Save all list view property filters to hidden fields to create input xml
function saveLViewActionCond() {

    if ($j("#txtViewName").val().toLowerCase() == "new" || $j("#txtViewName").val().toLowerCase() == "") {
        showAlertDialog("warning", 3019, "client");
        return false;
    }

    if ($j("#txtViewName").val().length > 15) {
        showAlertDialog("warning", 3020, "client");
        return false;
    }

    var chkd = false;
    $j('#tvList input[type=checkbox]:checked').each(function () {

        chkd = true;
    })
    if (!chkd) {
        showAlertDialog("warning", 3021, "client");
        return false;
    }

    var strCols = "";
    var parents = $j("#hdnParent").val();

    $j('#tvList input[type=checkbox]:checked').each(function () {
        // if ($j(this).parentNode == undefined) {
        PNode = $j(this);

        if (parents.indexOf(PNode[0].nextSibling.innerHTML) != -1 && (PNode[0].nextSibling.innerHTML) != "") { } else {
            var node = $j(this).next('a');
            var nodeValue = '';
            var nodePath = node.attr('href').substring(node.attr('href').indexOf(",") + 2, node.attr('href').length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);

            if (strCols == "")
                strCols = nodeValue;
            else
                strCols += "," + nodeValue;

        }
    });

    $j("#hdnColumns").val(strCols);

    var strnorepeat = "";
    var strnorepeatCaptn = "";

    $j('#tblNorep input[type=checkbox]:checked').each(function () {

        var val = $j(this)[0].value;
        var captn = $j(this)[0].title;
        if (strnorepeat == "")
            strnorepeat = val;
        else
            strnorepeat += "," + val;

        if (strnorepeatCaptn == "")
            strnorepeatCaptn = captn;
        else
            strnorepeatCaptn += "," + captn;
    });

    var strsubtotal = "";

    var subCount = 0;
    var tblRow = $j('#tblSubtotlList tr');

    tblRow.each(function () {
        var seltdColumns = $j("#hdnColumns").val();
        var arrColumn = seltdColumns.split(',');
        var column = tblRow.parent()[0].childNodes[subCount].childNodes[0].innerHTML;
        if (arrColumn.indexOf(column) != -1) {
            strsubtotal += '<' + tblRow.parent()[0].childNodes[subCount].childNodes[0].innerHTML + 'st' + ' totalontop="' + tblRow.parent()[0].childNodes[subCount].childNodes[6].innerHTML + '">';
            strsubtotal += '<a1>' + tblRow.parent()[0].childNodes[subCount].childNodes[0].innerHTML + '</a1>'; //checkbox field name

            var header = tblRow.parent()[0].childNodes[subCount].childNodes[1].innerHTML;

            if (header.indexOf("{") == -1) {
                strsubtotal += '<a4>{' + tblRow.parent()[0].childNodes[subCount].childNodes[1].innerHTML; //header
            } else {
                strsubtotal += '<a4>' + tblRow.parent()[0].childNodes[subCount].childNodes[1].innerHTML;
            }
            if (header.indexOf("}") == -1) {
                strsubtotal += '}</a4>'; //header
            } else {
                strsubtotal += '</a4>'; //header
            }

            strsubtotal += '<a5>' + tblRow.parent()[0].childNodes[subCount].childNodes[2].innerHTML + '</a5>'; //footer
            strsubtotal += '<a12>' + tblRow.parent()[0].childNodes[subCount].childNodes[3].innerHTML + '</a12>'; //caption
            strsubtotal += '<a13>' + tblRow.parent()[0].childNodes[subCount].childNodes[4].innerHTML + '</a13>'; //subtotalorder
            strsubtotal += '<a18>' + tblRow.parent()[0].childNodes[subCount].childNodes[5].innerHTML + '</a18>'; //linespace
            strsubtotal += '</' + tblRow.parent()[0].childNodes[subCount].childNodes[0].innerHTML + 'st>';
            subCount += 1;
        }
    });

    $j("#hdnSubtotal").val(strsubtotal);

    $j("#hdnNoRepeat").val(strnorepeat);
    $j("#hdnNoRepeatCaptn").val(strnorepeatCaptn);

    var assign = $j("#ddlSortBy option:selected").val();

    $j("#hdnSort").val(assign);

    $j("#hdnSortCap").val($j("#ddlSortBy option:selected").text());

    assign = $j("#ddlGroupBy option:selected").val();

    $j("#hdnGroup").val(assign);

    $j("#hdnGroupCap").val($j("#ddlGroupBy option:selected").text());

    assign = $j("#rbtnOrder option:selected").text();
    $j("#hdnOrder").val(assign);

    assign = $j("#rbtnStatus option:selected").text();
    $j("#hdnStatus").val(assign);

    SaveListCondition();
}

// Save filter condition in list view popup
function SaveListCondition() {
    var tblFilter = document.getElementById("tblFilter");
    rowCount = tblFilter.rows.length;
    var ddlFldTypes = document.getElementById("ddlFldTypes");
    var searchXml = "";
    searchXml += "iif((";
    var displayCond = "";
    var fOpr = "";
    var fOprValue = "";
    var condTxt = "";
    var gridConditn = "";
    var xmlCond = "";

    for (var i = 0; i <= rowCount; i++) {
        var cnt = i + 1;
        if (i == 0) {
            var ddlCol = document.getElementById("ddlListFilter");
            var ddlCond = document.getElementById("ddlListFilcond");
            var txtValue1 = document.getElementById("txtListFilter");
            var txtValue2 = document.getElementById("ListfilVal2");
        } else {
            if (i > 0) {
                var ddlCol = document.getElementById("ddlListFilter" + i);
                var txtValue1 = document.getElementById("txtFilter" + i);
                var txtValue2 = document.getElementById("txtFillVal" + i);
                var ddlCond = document.getElementById("ddlOpr" + i);
            }


            if (i > 0) {
                var fOprId = "grpAndOr" + i;
                fOpr = document.getElementsByName(fOprId);
                if (fOpr) {
                    for (var j = 0; j < fOpr.length; j++) {
                        if (fOpr[j].checked) {
                            fOprValue = fOpr[j].value;
                            continue;
                        }
                    }
                }
            }
        }

        if (i == 0) {

            //if (ddlCol.selectedIndex == 0 || ddlCond.selectedIndex == 0 || txtValue1.value == "") {//Commented because if condition 'is empty' both textbox will be empty
            if (ddlCol.selectedIndex == 0 || ddlCond.selectedIndex == 0) {

                if (ddlCond.value != "is null" && txtValue1.value == "") {
                    if ($j("#lblCond").text() == "") {
                        //showAlertDialog("warning", "Please fill all the filter criteria.");
                        isExistCondtn = 0;
                        return false;
                    } else {
                        var hdn = document.getElementById("hdnSubTypeCond");
                        hdn.value = $j("#lblCond").text();
                        return true;
                    }
                }
            }
        }

        var filterCol = ddlCol.value;
        var filterColCap = ddlCol[ddlCol.selectedIndex].text;
        var filterOpr = ddlCond.value;
        var filterValue1 = txtValue1.value;
        var filterValue2 = txtValue2.value;

        var cond = ddlCond[ddlCond.selectedIndex].innerHTML;
        var condval = ddlCond[ddlCond.selectedIndex].value;

        // if (ddlCol.selectedIndex == 0 || ddlCond.selectedIndex == 0 || txtValue1.value == "") {//Commented because if condition 'is empty' both textbox will be empty
        if (ddlCol.selectedIndex == 0 || ddlCond.selectedIndex == 0) {
            continue;
        } else {
            if ((i == 0) || (i >= 0)) {
                if (searchXml == "") {
                    searchXml += ConvertToExpr(filterCol, ddlFldTypes[ddlCol.selectedIndex].Text, cond, filterValue1, filterValue2);
                    displayCond += ConvertToExprStr(filterCol, filterColCap, cond, filterValue1, filterValue2);
                    gridConditn += ConvertToDisplyStr(filterCol, filterColCap, cond, filterValue1, filterValue2);
                    xmlCond += ConvertToXml(filterCol, filterColCap, ddlFldTypes[ddlCol.selectedIndex].innerHTML, condval, filterValue1, filterValue2, i + 1, fOprValue);
                } else {
                    if (fOprValue == "Or") {
                        displayCond += " Or ";
                        searchXml += "|";
                        gridConditn += " Or ";
                    } else if (fOprValue == "And") {
                        displayCond += " And ";
                        searchXml += "&";
                        gridConditn += " And ";
                    }
                    displayCond += ConvertToExprStr(filterCol, filterColCap, cond, filterValue1, filterValue2);
                    gridConditn += ConvertToDisplyStr(filterCol, filterColCap, cond, filterValue1, filterValue2);

                    searchXml += ConvertToExpr(filterCol, ddlFldTypes[ddlCol.selectedIndex].Text, cond, filterValue1, filterValue2);

                    xmlCond += ConvertToXml(filterCol, filterColCap, ddlFldTypes[ddlCol.selectedIndex].innerHTML, condval, filterValue1, filterValue2, i, fOprValue);

                }
                if (condTxt == "")
                    condTxt = filterCol + "^" + cond + "^" + filterValue1 + "^" + filterValue2 + "^" + fOprValue;
                else
                    condTxt += "|" + filterCol + "^" + cond + "^" + filterValue1 + "^" + filterValue2 + "^" + fOprValue;
            }
        }
    }
    searchXml += "), {T},{F})";
    var hdn = document.getElementById("hdnSubTypeCond");
    hdn.value = searchXml;
    var hdnCond = document.getElementById("hdnCondTxt");
    hdnCond.value = xmlCond
    $j("#hdnDisplayCond").val(displayCond);
    $j("#hdnGridCond").val(gridConditn);
    return true;
}


function CheckSpecialCharsInXmlCond(str) {

    if (str == null)
        return "";
    var str = str;
    str = str.replace(/<>/g, "&lt;&gt;");
    str = str.replace(/</g, "&lt;");
    str = str.replace(/>/g, "&&gt;");
    str = str.replace(/>=/g, "&gt;=");
    str = str.replace(/<=/g, "&lt;=");
    // single and double quote are part of parameter , so not replaced.
    return str;
}


//function to convert the condition into expression for subtype.
function ConvertToXml(fldName, fldCaption, dataType, opr1, fldValue, fldValue1, num, fOprValue) {
    opr1 = opr1.toLowerCase();
    var colonInFldValue = false;
    if (fldValue.substring(0, 1) == ":")
        colonInFldValue = true;

    var fldtype = "";

    if (dataType.toLowerCase() == "numeric") {
        fldtype = "n";
    } else if (dataType.toLowerCase() == "date/time") {
        fldtype = "d";
    } else if (dataType.toLowerCase() == "character") {
        fldtype = "c";
    }
    var oprStr = opr1;


    var result = "";
    oprStr = CheckSpecialCharsInXml(oprStr);
    result = "<c" + num + " lopr='" + fOprValue + "'  fname='" + fldName + "' ftype='" + fldtype.toUpperCase() + "' v1='" + fldValue + "' v2='" + fldValue1 + "' op='" + oprStr + "' fcaption='" + fldCaption + "' />";
    return result;
}

function ConvertToExprStr(colName, colCaption, cond, value1, value2) {
    //(Prefix field ( pfield ) Equal to 0) And (Prefix field ( pfield ) Equal to a)
    var str = "";
    if (cond.toLowerCase() == "between")
        str = "(" + colCaption + " ( " + colName + " ) " + cond + " " + value1 + "," + value2 + ")";
    else if (cond.toLowerCase() == "is null")
        str = "(" + colCaption + "  " + cond + ")";
    else
        str = "(" + colCaption + " ( " + colName + " ) " + cond + " " + value1 + ")";
    return str;
}

function ConvertToDisplyStr(colName, colCaption, cond, value1, value2) {
    //(Prefix field ( pfield ) Equal to 0) And (Prefix field ( pfield ) Equal to a)
    var str = "";
    if (cond.toLowerCase() == "between")
        str = "(" + colCaption + " " + cond + " " + value1 + "," + value2 + ")";
    else if (cond.toLowerCase() == "is null")
        str = "(" + colCaption + "  " + cond + ")";
    else
        str = "(" + colCaption + " " + cond + " " + value1 + ")";
    return str;
}

//function to convert the condition into expression for subtype.
function ConvertToExpr(fldName, dataType, opr, fldValue, fldValue1) {
    opr = opr.toLowerCase();
    var colonInFldValue = false;
    if (fldValue.substring(0, 1) == ":")
        colonInFldValue = true;
    var result = "";

    if (opr == "less than or equal to" || opr == "greater than or equal to") {
        var oprStr = "";
        if (opr == "less than or equal to")
            oprStr = " <= ";
        else
            oprStr = " >= ";

        if (colonInFldValue) {
            fldValue = fldValue.substring(1);
            result = "(" + fldName + oprStr + fldValue + ") | (" + fldName + "=" + fldValue + ")";
        } else if (dataType == "Date/Time") {
            result = "(" + fldName + oprStr + "ctod({" + fldValue + "})) | (" + fldName + " = ctod({" + fldValue + "}))";
        } else if (dataType == "Numeric") {
            result = "(" + fldName + oprStr + fldValue + ") | (" + fldName + "=" + fldValue + ")";
        } else {
            result = "(" + fldName + oprStr + "{" + fldValue + "}) | (" + fldName + "=" + "{" + fldValue + "})";
        }
    } else if (opr == "not equal to" || opr == "equal to") {
        var oprStr = "";
        if (opr.toLowerCase() == "not equal to")
            oprStr = " # ";
        else
            oprStr = " = ";

        if (colonInFldValue) {
            fldValue = fldValue.substring(1);
            result = "(" + fldName + oprStr + fldValue + ")";
        } else if (dataType == "Date/Time") {
            result = "(" + fldName + oprStr + " ctod({" + fldValue + "}))";
        } else if (dataType == "Numeric") {
            result = "(" + fldName + oprStr + fldValue + ")";
        } else {
            result = "(" + fldName + oprStr + "{" + fldValue + "})";
        }
    } else if (opr == "less than" || opr == "greater than") {

        var oprStr = "";
        if (opr == "<")
            oprStr = " < ";
        else
            oprStr = " > ";
        if (colonInFldValue) {
            fldValue = fldValue.substring(1);
            result = "(" + fldName + oprStr + fldValue + ")";
        } else if (dataType == "Date/Time") {
            result = "(" + fldName + oprStr + "ctod({" + fldValue + "}))";
        } else if (dataType == "Numeric") {
            result = "(" + fldName + oprStr + fldValue + ")";
        } else {
            result = "(" + fldName + oprStr + "{" + fldValue + "})";
        }
    } else if (opr == "between") {
        var colonInFldVal1 = false;
        if (fldValue1.substring(0, 1) == ":")
            colonInFldVal1 = true;
        else colonInFldVal1 = false;

        if (colonInFldValue && colonInFldVal1) {
            fldValue = fldValue.substring(1);
            fldValue1 = fldValue1.substring(1);
            result = "((" + fldName + " > " + fldValue + ") | (" + fldName + "=" + fldValue + ")) &";
            result += "((" + fldName + " < " + fldValue1 + ") | (" + fldName + "=" + fldValue1 + "))";
        } else if (colonInFldValue && !colonInFldVal1) {
            fldValue = fldValue.substring(1);
            if (dataType == "Date/Time") {
                result = "((" + fldName + ">" + fldValue + ") | (" + fldName + "=" + fldValue + ")) &";
                result += "((" + fldName + "< ctod({" + fldValue1 + "})) | (" + fldName + " = ctod({" + fldValue1 + "})))";
            } else {
                result = "((" + fldName + " < " + fldValue + ") | (" + fldName + "=" + fldValue + ")) &";
                result += "((" + fldName + " < " + fldValue1 + ") | (" + fldName + "=" + fldValue1 + ")))";
            }
        } else if (!colonInFldValue && colonInFldVal1) {
            fldValue1 = fldValue1.substring(1);
            if (dataType == "Date/Time") {
                result = "((" + fldName + " > ctod({" + fldValue + "})) | (" + fldName + " = ctod({" + fldValue + "})) &";
                result += "((" + fldName + " < " + fldValue1 + ") | (" + fldName + "=" + fldValue1 + ")))";
            } else {
                result = "((" + fldName + " > " + fldValue + ") | (" + fldName + " = " + fldValue + ")) &";
                result += "((" + fldName + " < " + fldValue1 + ") | (" + fldName + " = " + fldValue1 + ")))";
            }
        } else {
            if (dataType == "Date/Time") {
                result = "((" + fldName + " > ctod({" + fldValue + "})) | (" + fldName + " = ctod({" + fldValue + "})) &";
                result += "((" + fldName + " < ctod({" + fldValue1 + "})) | (" + fldName + " = ctod({" + fldValue1 + "})))";
            } else {
                result = "((" + fldName + " > " + fldValue + ") | (" + fldName + " = " + fldValue + ")) &";
                result += "((" + fldName + " < " + fldValue1 + ") | (" + fldName + " = " + fldValue1 + ")))";
            }
        }
    }
    return result;
}

//Create table for subtotal list
function CreateSubtotal() {

    var html = '';
    var caption = $j("#ddlCaption option:selected").text();
    var value = $j("#ddlCaption option:selected")[0].value;
    $j('#tblSubtotlList').each(function () {
        $j('#tblSubtotlList tr > td:contains(' + value + ')').parent().remove();
    })

    html += '<tr><td class="tableCell">' + $j('#hdnCbValue').val() + '</td>';
    html += '<td  class="tableCell" >' + document.getElementById('txtHeader').value + '</td>';
    html += '<td  class="tableCell" >' + document.getElementById('txtFooter').value + '</td>';
    html += '<td  class="tableCell" >' + value + '</td>';
    html += '<td  class="tableCell" >' + document.getElementById('txtSubOrder').value + '</td>';
    html += '<td  class="tableCell" >' + document.getElementById('cbLineSpace').checked + '</td>';
    html += '<td  class="tableCell" >' + document.getElementById('cbTtl').checked + '</td>';
    html += '</tr>';

    $j('#tblSubtotlList').append(html);
}

//bind dummy subtotal table
function bindSubtotalList() {

    if ($j("#hdnSubtotalList").val() != "") {
        var subtotallst = $j("#hdnSubtotalList").val().split(';');
        var count = subtotallst.length;
        var html = '';
        for (var i = 0; i < count; i++) {
            var subtotl = subtotallst[i].split(',');
            var subcount = subtotl.length;

            html += '<tr><td class="tableCell">' + subtotl[0] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[1] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[2] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[3] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[4] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[5] + '</td>';
            html += '<td  class="tableCell" >' + subtotl[6] + '</td>';

            html += '</tr>';
        }

        $j('#tblSubtotlList').append(html);
    }
    return false;
}



// These varibles will be used to store the position of the mouse
var X = 0;
var Y = 0;

//Function to find the left position of the given obj on screen.
function FindListPosX(obj) {

    var curleft = 0;

    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    } else if (obj.x)
        curleft += obj.x;
    return curleft;
}

//Function to find the top position of the given object on screen.
function FindListPosY(obj) {

    var curtop = 0;

    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    } else if (obj.y)
        curtop += obj.y;
    return curtop;
}

//Function to get the left and top position of the task button image on screen.
function FindListPos() {

    var img = document.getElementById("imgView");
    X = FindListPosX(img);
    Y = FindListPosY(img);
}




//Set list view conditions to popup
function SetListCondition(condText) {
    //No of conditions
    var tblFilter = document.getElementById("tblFilter");
    rowCount = tblFilter.rows.length;
    if (rowCount > 0)
        return;
    var strWF = condText.split("|");
    for (var i = 0; i < strWF.length; i++) {
        var wfCond = strWF[i].split("^");
        var ddlCol;
        var ddlCond;
        var txtValue1;
        var txtValue2;
        var suffix = "";
        if (i == 0) {
            //The first condition will not have the operator And/Or
            if (i > 0)
                suffix = i;
            if (i == 0) {

                ddlCol = $j("#ddlListFilter");
                ddlCond = $j("#ddlListFilcond");
                txtValue1 = $j("#txtListFilter");
                txtValue2 = $j("#ListfilVal2");
            } else {

                ddlCol = $j("#ddlListFilter" + suffix);
                txtValue1 = $j("#txtFilter" + suffix);
                txtValue2 = $j("#txtFillVal" + suffix);
                ddlCond = $j("#ddlOpr" + suffix);
            }
            ddlCol.val(wfCond[0]); //            $j("#ddlCaption").val(val);
            ddlCond.find("option").each(function () {
                if ($j(this).val().toLowerCase() == wfCond[1].toLowerCase()) {
                    $j(this).attr("selected", "selected");

                    if ($j(this).text() == 'Between') {
                        txtValue2.prop("disabled", false);
                        txtValue1.prop("disabled", false);
                    } else if ($j(this).text() == 'Is empty') {
                        txtValue2.prop("disabled", true);
                        txtValue1.prop("disabled", true);
                    } else {
                        txtValue2.prop("disabled", true);
                        txtValue1.prop("disabled", false);
                    }
                }
            });
            txtValue1.val(wfCond[2]);
            txtValue2.val(wfCond[3]);
        } else {
            AddCondition();
            var OprId = "#grpAndOr" + i;
            //radOr1
            var Opr = $j("#rad" + wfCond[4] + i);
            Opr.prop("checked", true);

            // i += 1;
            suffix = i;
            ddlCol = $j("#ddlListFilter" + suffix);
            txtValue1 = $j("#txtFilter" + suffix);
            txtValue2 = $j("#txtFillVal" + suffix);
            ddlCond = $j("#ddlOpr" + suffix);

            ddlCol.val(wfCond[0]);

            ddlCond.find("option").each(function () {
                if ($j(this).val() == wfCond[1]) {
                    $j(this).attr("selected", "selected");

                    if ($j(this).text() == 'Between') {
                        document.getElementById(txtValue2[0].id).disabled = false;
                        document.getElementById(txtValue1[0].id).disabled = false;
                    } else if ($j(this).text() == 'Is empty') {
                        document.getElementById(txtValue2[0].id).disabled = true;
                        document.getElementById(txtValue1[0].id).disabled = true;
                    } else {
                        document.getElementById(txtValue2[0].id).disabled = true;
                        document.getElementById(txtValue1[0].id).disabled = false;
                    }
                }
            });
            txtValue1.val(wfCond[2]);
            txtValue2.val(wfCond[3]);

        }
    }
}

var IsexistView = true;

function checkViewName(lname) {

    var i = 0;
    var str = 0;
    var strCount = lname.length;
    var viewName = lname;
    while (IsexistView == true) {

        if (IsexistView == true) {

            str = lname.substr(lname.length - 1);
            i = parseInt(str) + 1;
            lname = lname.substr(0, lname.length - 1) + i;
            NameEnd = lname.substr(strCount - 1, lname.length - 2);

            if (NameEnd.toLowerCase() == "nan") {
                lname = viewName + "01";
            }

            IsexistView = false;
            checkddName(lname);
        } else {
            IsexistView = false;
            $j("#ddlViewList").val(lname);
        }

    }
    $j("#ddlViewList").val(lname);
    $j("#txtViewName").val(lname);
}



function checkddName(lname) {

    var views = $j("#hdnViewNames").val();
    var viewarr = views.split(',');

    for (var i = 0; i < viewarr.length; i++) {

        if (viewarr[i] == lname) {
            IsexistView = true;
        }
    }
}

//Check wether view name already exist 
var isExistCondtn = 1;
var views = "";
var lname = "";

function isExistViewName() {

    if ($j("#txtViewName").val().toLowerCase() == "new" || $j("#txtViewName").val().toLowerCase() == "" || $j("#txtViewName").val().toLowerCase() == "default") {
        showAlertDialog("warning", 3019, "client");
        return false;
    }
    if ($j("#txtViewName").val().toLowerCase() == $j("#hdnTransId").val().toLowerCase()) {
        showAlertDialog("warning", 3022, "client");
        return false;
    }

    if ($j("#txtViewName").val().length > 15) {
        showAlertDialog("warning", 3023, "client");
        return false;
    }

    var chkd = false;
    $j('#tvList input[type=checkbox]:checked').each(function () {

        chkd = true;
    })
    if (!chkd) {
        showAlertDialog("warning", 3021, "client");
        return false;
    }


    $j("#hdnCondTxt").val('');
    $j("#hdnSubtotal").val('');
    $j("#hdnSubtotal").val('');
    var mode = $j("#hdnSaveMode").val();
    isExistCondtn = 1;
    var exist = false;
    views = $j("#hdnViewNames").val();

    lname = $j("#txtViewName").val();

    var viewarr = views.split(',');

    for (var i = 0; i < viewarr.length; i++) {

        if (viewarr[i].toLowerCase() == lname.toLowerCase()) {
            exist = true;
        }
    }

    if (exist == true && mode != "Edit") {

        showAlertDialog("warning", 3022, "client");
        return false;
    } else {
        var status = $j("#hdnStatus").val();

        if (status == "ON") {

            SaveListCondition();
            if (isExistCondtn == 0) {
                return false;
            }
        }

        saveLViewActionCond();


        document.getElementById('btnSav').click();
        $j("#hdnViewNames").val(views + "," + lname);
        return true;

    }

}

//Remove changes from listview popup
function discardChanges() {

    $j("#hdnSubtotal").val('');
    $j("#hdnNoRepeat").val('');
    $j("#ddlSortBy option:selected").val('');
    $j("#hdnSort").val('');
    $j("#ddlGroupBy option:selected").val('');
    $j("#hdnGroup").val('');
    $j("#rbtnOrder option:selected").text('');
    $j("#hdnOrder").val('');
    $j("#rbtnStatus option:selected").text('');
    $j("#hdnStatus").val('');
    $j("#hdnColumns").val('');
    $j("#hdnSubtotlCaptn").val('');
    $j("#hdnCondTxt").val('');


    $j("#ddlSortBy").val('');
    $j("#ddlGroupBy").val('');

    $j("#rbtnOrder option:selected").text("Asc");
    $j("#rbtnStatus option:selected").text("OFF");


    $j("#tblFilter").empty();
    $j("#ddlListFilter").val("");
    $j("#ddlListFilcond").val("");

    $j("#txtListFilter").val('');
    $j("#ListfilVal2").val('');

    $j("#txtHeader").val('');
    $j("#txtFooter").val('');
    $j("#ddlCaption").val('');


    $j("#cbLineSpace").prop('checked', false);

    $j("#cbTtl").prop('checked', false);

    $j("#txtSubOrder").val('');


    $j('#tblSubtotal  input[type=checkbox]').each(function () {

        $j(this)[0].checked = false;
    })
    $j('#tblNorep  input[type=checkbox]').each(function () {

        $j(this)[0].checked = false;
    })

    $j("#tblSubtotlList").empty();

}

//Function to get the left and top position of the task button image on screen.
function FindViewPos() {

    var img = document.getElementById("imgView");
    X = FindPosX(img);
    Y = FindPosY(img);
}


var IsViewBtnCliked = false;
//Function to show the task list on click of the task button in the toolbar.
function ShowViewList() {

    //var div;
    //////////////////IsViewBtnCliked = true;
    //////////////////IsNqViewBtnCliked = true;
    // div = $j("#listViewPopUp");
    // if (div.css("display") != "block") {
    //     div.show();
    // }
    // // Off-sets the X position by 15px
    //// X = X - 78;
    // Y = Y + 28;


    // // Sets the position of the DIV
    // div.css("left", X + 'px');
    // div.css("top", Y + 'px');
    // $j(document).height($j("#listViewPopUp").height() + $j(document).height());
    // $j(".listViewDc").height($j("#listViewPopUp").height() + $j(document).height());
    // var t1 = $j("#listViewPopUp").height();
    // var t2 = $j(document).height();
    // var t3 = $j(window).height();
    // $j("#divListView").height($j("#divListView").height() + 800);
}


// These varibles will be used to store the position of the mouse
var X = 0;
var Y = 0;

//Function to find the left position of the given obj on screen.
function FindPosX(obj) {

    var curleft = 0;

    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    } else if (obj.x)
        curleft += obj.x;
    return curleft;
}

//Function to find the top position of the given object on screen.
function FindPosY(obj) {

    var curtop = 0;

    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    } else if (obj.y)
        curtop += obj.y;
    return curtop;
}


//Function to hide the view list.
function HideNqIvViewList() {

    var dvviewList = $j("#listViewPopUp");

    if (IsNqViewBtnCliked == true) {
        IsNqViewBtnCliked = false;
        dvviewList.show();
    } else {
        dvviewList.hide();
    }
}

function ConfirmDelete() {

    var cutMsg = eval(callParent('lcm[9]'));
    var glType = eval(callParent('gllangType'));
    var isRTL = false;
    if (glType == "ar")
        isRTL = true;
    else
        isRTL = false;
    var ConfirmDeleteCB = $.confirm({
        title: eval(callParent('lcm[155]')),
        onContentReady: function () {
            disableBackDrop('bind');
        },
        backgroundDismiss: 'false',
        rtl: isRTL,
        escapeKey: 'buttonB',
        content: cutMsg,
        buttons: {
            buttonA: {
                text: eval(callParent('lcm[164]')),
                btnClass: 'hotbtn',
                action: function () {
                    ConfirmDeleteCB.close();
                    var e = document.getElementById("ddlViewList");
                    var fname = e.options[e.selectedIndex].innerHTML;

                    $j('#hdnViewDelt').val(fname);

                    DeleteView(fname);
                }
            },
            buttonB: {
                text: eval(callParent('lcm[192]')),
                btnClass: 'coldbtn',
                action: function () {
                    disableBackDrop('destroy');
                    return false;
                }
            }
        }
    });

}

function DeleteView(fname) {
    var a = $j('#btnRemv').click();

    return false;
}


function EditCondPopUp() {
    if ($j("#hdnCondTxt").val() != "") {
        SetListCondition($j("#hdnCondTxt").val());
    }
    return false;
}

function NewViewMode() {


    $j('#hdnNoRepeat').val('');
    $j('#hdnNoRepeatCaptn').val('');
    $j('#hdnSubtotlCaptn').val('');
    $j('#hdnSubtotalList').val('');
    $j('#hdnGridCond').val('');
    $j('#hdnGroup').val('');
    $j('#hdnSort').val('');
    $j('#hdnStatus').val('OFF');
    $j('#hdnColumns').val('');
    $j('#hdnOrder').val("Asc");

    return true;
}

function OpenopupWithCond() {

    $j('#lblViewName').show();
    $j('#txtViewName').show();
    $j('#lblviewtext').hide();
    $j('#ddlViewList').hide();
    $j('#btnRemove').hide();

    $j(divtr1).show();
    tr1.className = "tvRow selProperty";
    $j(divtr2).hide();
    $j(divtr3).hide();
    $j(divtr4).hide();
    $j(divtr5).hide();

    //Calculate Page width and height
    var pageWidth = window.innerWidth;
    var pageHeight = window.innerHeight;
    if (typeof pageWidth != "number") {
        if (document.compatMode == "CSS1Compat") {
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
    }
    //Make the background div tag visible...
    var divbg = document.getElementById('bg');
    $j(divbg).show();
    var divobj = document.getElementById('divAction');
    $j(divobj).show();

    if (navigator.appName == "Microsoft Internet Explorer")
        computedStyle = divobj.currentStyle;
    else computedStyle = document.defaultView.getComputedStyle(divobj, null);
    //Get Div width and height from StyleSheet
    var divWidth = computedStyle.width.replace('px', '');
    var divHeight = computedStyle.height.replace('px', '');
    var divLeft = (pageWidth - divWidth) / 2;
    var divTop = (pageHeight - divHeight) / 2;
    //Set Left and top coordinates for the div tag


    $j(divobj).css("left", divLeft + "px");
    $j(divobj).css("top", divTop + "px");
    $j(divobj).css("top", "40px");


    //Adjust popup size when list view view is empty or less number of row
    var listPnl = document.getElementById('Panel2');

    var docHight = $j(document).height();

    var height = 0;
    var PopupHeight = 500;
    var top = 40;
    if (PopupHeight > docHight)
        height = PopupHeight - docHight;

    var winHeight = docHight + top + height;
    Adjustwin(winHeight);
    showAlertDialog("warning", 3024, "client");
}

function OpenPopup(id, Mode) {


    var a = Mode;
    var transId = $j('#hdnTransId').val()
    var view = $j('#txtViewName').val();
    if (view.toLowerCase() == "default")
        Mode = "New";

    if (Mode == 'New') {


        $j('#lblViewName').show();
        $j('#txtViewName').show();
        $j('#lblviewtext').hide();
        $j('#ddlViewList').hide();
        $j('#btnRemove').hide();


        checkViewName($j('#hdnViewName').val());
        NewViewMode();
        $j('#hdnSaveMode').val("New");
        $j('#tblFilter').empty();
        $j("#ddlSortBy option:selected").val('');
        $j("#ddlGroupBy option:selected").val('');
        $j('#ddlGroupBy')[0].selectedIndex = 0;
        $j('#ddlSortBy')[0].selectedIndex = 0;
        $j("#hdnSubtotlCaptn").val('');
        $j("#tblSubtotlList").empty();
        $j('#tblSubtotal  input[type=checkbox]').each(function () {
            $j(this)[0].checked = false;
        })

    } else if (Mode == 'Edit') {

        $j('#hdnSaveMode').val("Edit");

        $j('#lblViewName').hide();
        $j('#txtViewName').hide();
        $j('#lblviewtext').show();
        $j('#ddlViewList').show();
        $j('#btnRemove').show();

    } else if (Mode == 'EditNew') {

        $j('#lblViewName').show();
        $j('#txtViewName').show();
        $j('#lblviewtext').show();
        $j('#ddlViewList').show();
        $j('#btnRemove').hide();


        $j('#hdnSaveMode').val("New");
        NewViewMode();
    }

    var divtr1 = document.getElementById('divtr1');
    var divtr2 = document.getElementById('divtr2');
    var divtr3 = document.getElementById('divtr3');
    var divtr4 = document.getElementById('divtr4');
    var divtr5 = document.getElementById('divtr5');
    var viewname = $j('#hdnViewName').val();

    $j('#txtViewName').innerHTML = viewname;

    $j("#ddlSortBy option:selected").val($j('#hdnSort').val());

    $j("#ddlGroupBy option:selected").val($j('#hdnGroup').val());


    $j("#ddlListFilcond").val("");
    $j("#ddlListFilter").val("");
    $j("#txtListFilter").val("");
    $j("#ListfilVal2").val("");


    $j("#FilterRowCount").val("0");

    $j("#txtHeader").val("");
    $j("#txtFooter").val("");

    $j("#txtSubOrder").val("");



    $j("#cbLineSpace").prop('checked', false);

    $j("#cbTtl").prop('checked', false);

    var order = $j('#hdnOrder').val();

    var Status = $j('#hdnStatus').val();


    $j('input:radio[name="rbtnOrder"]').filter('[value="' + order + '"]').attr('checked', true);
    $j('input:radio[name="rbtnStatus"]').filter('[value="' + Status.toUpperCase() + '"]').attr('checked', true);


    $j(divtr1).show();
    tr1.className = "tvRow selProperty";
    tr2.className = "tvRow RowCust";
    tr3.className = "tvRow RowCust";
    tr4.className = "tvRow RowCust";
    tr5.className = "tvRow RowCust";

    $j(divtr2).hide();
    $j(divtr3).hide();
    $j(divtr4).hide();
    $j(divtr5).hide();

    //Calculate Page width and height
    var pageWidth = window.innerWidth;
    var pageHeight = window.innerHeight;
    if (typeof pageWidth != "number") {
        if (document.compatMode == "CSS1Compat") {
            pageWidth = document.documentElement.clientWidth;
            pageHeight = document.documentElement.clientHeight;
        } else {
            pageWidth = document.body.clientWidth;
            pageHeight = document.body.clientHeight;
        }
    }
    //Make the background div tag visible...
    var divbg = document.getElementById('bg');
    $j(divbg).show();
    var divobj = document.getElementById(id);
    $j(divobj).show();

    if (navigator.appName == "Microsoft Internet Explorer")
        computedStyle = divobj.currentStyle;
    else computedStyle = document.defaultView.getComputedStyle(divobj, null);
    //Get Div width and height from StyleSheet
    var divWidth = computedStyle.width.replace('px', '');
    var divHeight = computedStyle.height.replace('px', '');
    var divLeft = (pageWidth - divWidth) / 2;
    var divTop = (pageHeight - divHeight) / 2;
    //Set Left and top coordinates for the div tag


    $j(divobj).css("left", divLeft + "px");
    $j(divobj).css("top", divTop + "px");
    $j(divobj).css("top", "40px");


    //Adjust popup size when list view view is empty or less number of row
    var listPnl = document.getElementById('Panel2');

    var docHight = $j(document).height();

    var height = 0;
    var PopupHeight = 500;
    var top = 40;
    if (PopupHeight > docHight)
        height = PopupHeight - docHight;

    var winHeight = docHight + top + height;
    Adjustwin(winHeight);
    $('#txtViewName').focus();
    jQuery(".popup_bg").bind("click.modalPopupKD", function (event) {
        CloseListActionWin();
    })
    jQuery(document).bind("keydown.modalPopupKD", function (event) {

        //var elemntsToCheck = 'button[tabindex!="-1"],a[tabindex!="-1"],input[tabindex!="-1"],select[tabindex!="-1"],textarea[tabindex!="-1"]';
        if (!event.shiftKey && event.keyCode == 9) {
            //tab key
            if ($(document.activeElement)[0].id == $("#btnSave")[0].id) {
                event.preventDefault();
                $('#closeList').focus();
            } else if ($(document.activeElement)[0].id == $("#closeList")[0].id) {
                event.preventDefault();
                $('#txtViewName').focus();
            }
        } else if (event.shiftKey && event.keyCode == 9) {
            if ($(document.activeElement)[0].id == $("#closeList")[0].id) {
                event.preventDefault();
                $('#btnSave').focus();
            }
        } else if (event.keyCode == 27) {
            CloseListActionWin();
        }


    })

    //var winh = $j(document).height();

    //var high = winh + 40;

    //parent.document.getElementById("middle1").style.cssText = "height:" + high + "px;";


}

function CloseListActionWin() {
    $j('#bg').hide();
    $j('#divAction').hide();
    var listPnl = document.getElementById('Panel2');

    var docHight = $j(document).height();
    var height = 0;
    var PopupHeight = 500;
    var top = 40;
    if (PopupHeight > docHight)
        height = PopupHeight - docHight;

    var winHeight = docHight + top + height;
    Adjustwin(winHeight);
    jQuery(document).unbind("keydown.modalPopupKD");
    jQuery(document).unbind("click.modalPopupKD");

    return false;
}


function EditView(context) {
    context.onchange = "";

    var e = document.getElementById("ddlViewList");
    var fname = e.options[e.selectedIndex].innerHTML;

    if (fname == "New view") {

        $j('#lblViewName').show();
        $j('#txtViewName').show();
        $j('#lblviewtext').show();
        $j('#ddlViewList').show();
        $j('#btnRemove').hide();

        checkViewName($j('#hdnViewName').val());
        discardChanges();
        $j("#ddlSortBy option:selected").val($j('#hdnSort').val());

        $j("#ddlGroupBy option:selected").val($j('#hdnGroup').val());

        $j('#hdnSaveMode').val("New");
    } else {
        $j('#hdnViewName').val(fname);
        $j('#txtViewName').val(fname);
        $j('#hdnSaveMode').val("Edit");
        discardChanges();
        BindXml(fname);
    }


    return true;
}

function BindXml(fxml) {
    $j('#txtViewName').val(fxml);
    $j('#hdnListViewName').val(fxml);
    $j('#btnListView').click();
    return true;
}

function BindGrid(viewName) {
    $j('#hdnSelectedView').val(viewName);

    $j('#btnShowGrid').click(); //hdnSelectedView

    return true;
}

///Function to click the new button from iview ,display the tstruct page. 
function pgReload() {
    ResetNavGlobalVariables();
    $j(location).attr("href", "tstruct.aspx?transid=" + tst);
    try {
        if (!window.parent.isSessionCleared && window.document.title == "List IView") {
            ASB.WebService.ClearNavigationSession();
            window.parent.isSessionCleared = true;
        }
    } catch (ex) { }
}

function submitIt() {

    var empCode = $j("#EmployeeCode").val();
    if (empCode == "") {
        var dob = $j("#dob").val();
        if (dob == "") {
            ShowDimmer(false);
            showAlertDialog("warning", 3025, "client");
            return false;
        } else {
            var empName = $j("#EmployeeName").val();
            var deptname = $j("#dept").val();
            var ddo = $j("#dddo").val();
            var doa = $j("#doa").val();
            if ((empCode == "") && (empName == "") && (deptname == "--") && (ddo == "") && (doa == "")) {
                ShowDimmer(false);
                showAlertDialog("warning", 3026, "client");
                return false;
            }
        }
    }
    return true;
}
//to set clicked rowIndex,column name and parent as IView
function SetColumnName(columnName, dataRowIndex, isParentIview) {
    ShowDimmer(true);
    window.parent.dataRowIndex = dataRowIndex;
    window.parent.clickedColumn = columnName;
    if (isParentIview)
        window.parent.isParentIview = isParentIview;
}

function ShowCharts(calledFrom, action) {
    if ($j("#dvCharts").is(':visible'))
        return;
    if (!isChartsAvailable) {
        showAlertDialog("error", 3027, "client");
    } else {
        var chartSize = $j("#hdnChartSize").val();
        if (chartSize == "" || chartSize == "half") {


            $j("#dvCharts").show();
            Adjustwin();
            SetContentPanelWidth("half");
        } else {
            ShowChartPopUp(calledFrom, action);
        }
    }
}

function CloseChart(calledFrom) {
    var chartSize = $j("#hdnChartSize").val();
    var dvChart = $j("#dvCharts");
    dvChart.hide();
    var width = "";
    if (chartSize == "" || chartSize == "half") {
        ShowFilter();

    } else {
        $j("#bg").hide();
        dvChart.removeClass("dvChartsPop");
        dvChart.addClass("dvChart");
        $j("#hdnChartSize").val("");
        $j("#lnkChart").text("View Full Screen");
    }

    SetContentPanelWidth(width);

    if (chartSize == "full") {
        if (gl_language == "ARABIC") {
            $j("#divcontainer").css("margin-top", "0%");
        }
        Adjustwin();
    }

}

function ShowChartPopUp(calledFrom, action) {
    var lnk = $j("#lnkChart");
    var dvChart = $j("#dvCharts");
    var width = "";
    if (lnk.text() == "View Full Screen") {
        $j("#bg").show();

        if (isFilterSortEnabled || isParamVisible.toLowerCase() == "true")
            width = "half";
        else
            width = "full";
        dvChart.removeClass("dvChart");
        dvChart.addClass("dvChartsPop");
        $j("#hdnChartSize").val("full");
        if (action == undefined) {
            if ($j("#ddlChartCol1").val() != "" && $j("#ddlChartCol2").val() != "")
                $j("#btnGetChart1").click();
        }
        dvChart.show();
        //dvChart.css("left", "200px");
        dvChart.css("top", "20px");
        $j("#lnkChart").text("Close Full Screen");
        Adjustwin(100);
        if (gl_language == "ARABIC") {
            $j("#dvCharts").css("margin-top", "1%");
        }
    } else {

        width = "half";
        $j("#bg").hide();
        dvChart.removeClass("dvChartsPop");
        dvChart.addClass("dvChart");
        $j("#hdnChartSize").val("half");
        $j("#lnkChart").text("View Full Screen");

        dvChart.show();
        if (action == undefined) {
            if ($j("#ddlChartCol1").val() != "" && $j("#ddlChartCol2").val() != "")
                $j("#btnGetChart1").click();
        }
        if (gl_language == "ARABIC") {
            $j("#dvCharts").css("margin-top", "-13%");
        }
    }
    SetContentPanelWidth(width);
}

//#Region Filter Iview and ListView

var visibleColumns = new Array();
var visibleColumnIds = new Array();
var isFilterSortEnabled = false;
//Construction of filter using Json Object and Grid Header


var selectedFilterItem = '';
//To add sorting on the column name click.
function CallSorting(obj) {
    // ShowDimmer(true);
    //window.parent.loadFrame();
    callParentNew("loadFrame()", "function");
    var colName = obj.innerHTML.replace("<br>", "~");
    selectedFilterItem = obj.id;
    $j("#hdnSortColName").val(colName);
    $j("#SortGrid").click();

}
//To add filter feature on the basis of Item selected to filter
function CallFilter(colName, filterVal, item) {
    if (colName) {
        //window.parent.loadFrame();
        callParentNew("loadFrame()", "function");
        // ShowDimmer(true);
        arrCheckedFilter.push(item);
        if ($j("#cb" + item).attr('checked'))
            $j("#hdnFilterChecked").val("checked");
        else
            $j("#hdnFilterChecked").val("unchecked");
        selectedFilterItem = item;

        $j("#hdnFilterBy").val(filterVal);
        $j("#hdnFilterColName").val(colName);
        $j("#FilterGrid").click();
        $j("#showRecDetails").hide();
    }
}

//To make filter hidden on hide filter click
function HideFilter(src) {
    document.getElementById("hdnIsParaVisible").value = "0";
    if (isFilterSortEnabled || isParamVisible.toLowerCase() == "true") {
        $j("#leftPanel").hide("slide", { percent: 0 }, 500);
        $j("#showFilter").css('display', 'block');
        $j("#hideFilter").css('display', 'none');
        $j(".dvContent").css('height', 'auto');

        if (navigator.appName != "Microsoft Internet Explorer")
            $j('.vertical').css('margin-left', '25px');
        $j(".hdIvFilter").hide();
        if (src == undefined) {
            if (!($j("#dvCharts").is(":visible"))) {
                $j("#divcontainer").css("width", "100%");
                SetContentPanelWidth("full");
            } else {
                if ($j("#leftPanel").is(":visible"))

                    //halfchart
                    SetContentPanelWidth("half");
                else
                    SetContentPanelWidth("halfchart");
            }
        }
    }
    if (gl_language == "ARABIC") {
        $j("#divcontainer").css("margin-top", "0px");
        $j("#divcontainer").css("margin-right", "0px");
    } else {
        $j("#divcontainer").css("margin-left", "0px");
    }
    setGridviewWidth();
}

function SetContentPanelWidth(strSize) {


    var contentPanel;
    if (document.title == "List IView")
        contentPanel = $j("#contentPanelGrid");
    else
        contentPanel = $j("#contentPanelGridIview");

    if (strSize == "full") {
        contentPanel.css("width", "100%");
        contentPanel.css("margin-left", "0px");
        SetDynamicScrollWdith("full");
    } else if (strSize == "half") {
        contentPanel.css("width", "50%");
        contentPanel.css("margin-left", "10px");
        SetDynamicScrollWdith("half");
    } else if (strSize == "halfchart") {
        contentPanel.css("width", "60%");
        contentPanel.css("margin-left", "10px");
        SetDynamicScrollWdith("halfchart");
    } else {
        if (gl_language == "ARABIC" && screen.width > 1024)
            $j("#divcontainer").css("width", "75%");
        else if (gl_language == "ENGLISH" && screen.width > 1024)
            $j("#divcontainer").css("width", "75%");
        else
            $j("#divcontainer").css("width", "75%");
        $j("#contentPanelGrid").css("width", "75%");
        contentPanel.css("width", "100%");
        contentPanel.css("margin-left", "10px");
        SetDynamicScrollWdith("");
    }

    if (window.opener != undefined) {
        if (strSize == "") {
            contentPanel.css("width", "65%");
            contentPanel.css("margin-left", "10px");
        }
    }
}

function SetDynamicScrollWdith(action) {

    var DivHR = document.getElementById('DivHeaderRow');
    var DivMC = document.getElementById('DivMainContent');
    var width = screen.width - 50;

    if (DivHR && DivMC) {
        if (action == "full") {
            DivHR.style.width = (parseInt(width) - 16) + 'px';
            DivMC.style.width = width + 'px';
        } else if (action == "half") {
            var newWid = width / 2;
            DivHR.style.width = (parseInt(newWid) - 16) + 'px';
            DivMC.style.width = newWid + 'px';
        } else if (action == "halfchart") {
            var newWid = width / 2;
            DivHR.style.width = (parseInt(newWid) - 16) + 'px';
            DivMC.style.width = newWid + 'px';
        } else {

            var newWid = (width * 75 / 100) + 50;
            if (screen.width == 1024)
                newWid = (width * 75 / 100);
            DivHR.style.width = (parseInt(newWid) - 16) + 'px';
            DivMC.style.width = newWid + 'px';
        }
    }
}

//To make filter visible on show filter click
function ShowFilter() {
    // $j("#dvShowFilter").show();
    $j("#dvHideFilter").css("display", "block");
    $j(".dvContent").css("height", "60vh");
    document.getElementById("hdnIsParaVisible").value = "1";
    //added the condition if show filter is called hide the graph
    if ($j("#dvCharts").is(":visible"))
        $j("#dvCharts").hide();
    if (isFilterSortEnabled || isParamVisible.toLowerCase() == "true") {
        if (gl_language == "ARABIC") {
            $j("#divcontainer").css("float", "right");
            $j("#leftPanel").css("float", "right");
        } else {
            $j("#leftPanel").css("float", "left");
            $j("#leftPanel").css("clear", "both");
            $j("#divcontainer").css("margin-top", "1%");
            if (screen.width == 1024)
                $j("#divcontainer").css("margin-left", "0%");
            else
                $j("#divcontainer").css("margin-left", "0%");

            //$j("#contentPanelGridIview").css("position", "none !important");   

        }
        $j("#leftPanel").show("slide", { percent: 0 }, 500);
        $j("#showFilter").css('display', 'none');
        $j('#dvUpdateFilter').css('margin-top', '-16px');
        $j(".hdIvFilter").show();

        if (!($j("#dvCharts").is(":visible"))) {
            if (document.title == "List IView")
                $j("#contentPanelGrid").css("width", "75%");
            else
                SetContentPanelWidth("");
        } else {
            SetContentPanelWidth("half");
        }
    }
    // Calling adjust window for ajusting "leftpanel" height on show filter
    //Adjustwin();
    setGridviewWidth();
}

//In List View This feature is to hide or show column on the basis of checked column
function CallColumnHide(obj) {
    // ShowDimmer(true);
    //window.parent.loadFrame();
    callParentNew("loadFrame()", "function");
    var colId = obj.id;
    var colName = obj.parentElement.textContent;
    if (colId && colName) {
        $j("#hdnHideColName").val(obj.value);
        $j("#HideColumn").click();
    }
}
//check the filter columns
function CheckFilterColumns() {

    for (var j = 0; j < arrCheckedFilter.length; j++) {
        if ($j("#cb" + arrCheckedFilter[j]).attr('checked'))
            $j("#cb" + arrCheckedFilter[j]).prop('checked', false);
        else
            $j("#cb" + arrCheckedFilter[j]).prop('checked', 'checked');

    }
}
//To select the checkboxes which are not visible
function CheckHiddenColumns(visibleColumns, visibleColumnIds) {

    var arrayHiddenCol = new Array();
    var gridColumn;
    if (document.title == "Iview")
        gridColumn = '#GridView1 tr:first-child th'
    else
        gridColumn = '#GridView1 tr:first-child th a'
    $j(gridColumn).each(function (i) {
        if (this.innerHTML && $j(this).text()) {
            arrayHiddenCol.push(this.innerHTML.replace(/<br>/g, ""));
        }
    });
    for (var k = 0; k < visibleColumns.length; k++) {
        var colName = visibleColumns[k];
        colName = colName.replace(/~/g, "");
        var id = colName.replace(/[^\w\s]/gi, '').replace(/\s/g, '');
        if ($j.inArray(colName, arrayHiddenCol) == -1)
            $j("#cb" + id).prop('checked', 'checked');
        else
            $j("#cb" + id).prop('checked', false);
    }
}
var calledFrom = "";

//To update global array AxIvFilterMinHeader, whenever a filter pannel is minimized or maximized 
function UpdateActiveArray(obj) {
    if (calledFrom == "") {
        var id = $j(obj).attr('id');
        if ($j(obj).hasClass("ui-state-active") && window.parent.AxIvFilterMinHeader.indexOf(id) == -1)
            window.parent.AxIvFilterMinHeader.push(id);
        else if (window.parent.AxIvFilterMinHeader.indexOf(id) > -1)
            window.parent.AxIvFilterMinHeader.splice($j.inArray(id, window.parent.AxIvFilterMinHeader), 1);
    }
    if ($j("#filterIvParam").attr("aria-selected") == "true") {
        $j("#leftPanel").css("margin-top", "-1%");
    } else {
        $j("#leftPanel").css("margin-top", "-2%");
    }
}

//To maintain the Filter Pannel state for the end user
function ShowVisibleFilters() {
    for (var k = 0; k < window.parent.AxIvFilterMinHeader.length; k++) {
        calledFrom = "clicked";
        $j("#" + window.parent.AxIvFilterMinHeader[k]).click();
    }
    calledFrom = "";
}

//To highlight selected Filter Item
function HighLightSelected() {
    if (selectedFilterItem && $j("#" + selectedFilterItem).length > 0)
        $j("#" + selectedFilterItem).addClass("selectedFilterItem");
    else
        selectedFilterItem = '';
}

//#End Region Filter Iview and List View

function toExcelWeb(a, c) {
    if (recordsFieldsValidForSave()) {
        var left = (screen.width / 2) - (840 / 2);
        var top = (screen.height / 2) - (600 / 2);
        var pa = "";
        var ivKey;
        pa = form1.param.value;
        //pa = pa.replace("#", "%23");
        //pa = pa.replace("&", "%26");

        // if (c == "Iview") {
            if ($j("#hdnKey").length > 0 && $j("#hdnKey").val() != "")
                ivKey = "&ivKey=" + $j("#hdnKey").val();
        // } else {
        //     if ($j("#hdnlvKey").length > 0 && $j("#hdnlvKey").val() != "")
        //         ivKey = "&ivKey=" + $j("#hdnlvKey").val();
        // }
        SetExport("../aspx/excelweb.aspx?ivname=" + a + "&ivtype=" + c + ivKey + "&params=", pa, a);
    }
}

function MakeStaticHeader(gridId, height, width, headerHeight, isFooter) {
    var tbl = document.getElementById(gridId);
    if (tbl) {
        var DivHR = document.getElementById('DivHeaderRow');
        var DivMC = document.getElementById('DivMainContent');
        //var DivFR = document.getElementById('DivFooterRow');
        headerHeight = $j('#GridView1').find("tr:first").height();
        width = screen.width - 50;
        // width = $j('#GridView1').find("tr:first").width();
        //*** Set divheaderRow Properties ****
        DivHR.style.height = headerHeight + 'px';
        DivHR.style.width = (parseInt(width) - 16) + 'px';
        DivHR.style.position = 'relative';
        DivHR.style.top = '0px';
        DivHR.style.zIndex = '10';
        DivHR.style.verticalAlign = 'top';

        //*** Set divMainContent Properties ****
        DivMC.style.width = width + 'px';
        DivMC.style.height = height + 'px';
        DivMC.style.position = 'relative';
        DivMC.style.top = -headerHeight + 'px';
        DivMC.style.zIndex = '1';
        DivHR.appendChild(tbl.cloneNode(true));

    }
}

function OnScrollDiv(Scrollablediv) {
    document.getElementById('DivHeaderRow').scrollLeft = Scrollablediv.scrollLeft;
    //document.getElementById('DivFooterRow').scrollLeft = Scrollablediv.scrollLeft;
}
$j(window).on("load", function () {
    setGridviewWidth();
});

function setGridviewWidth() {
    if ($j("#DivMainContent").width() > $j("#GridView1").width())
        $j("#DivMainContent").width($j("#GridView1").width() + 17);
}

function ShowMessageBox(msg, objId) {

    if (objId != undefined) {
        var fld = document.getElementById(objId);
        var posY = FindPosY(fld);
        $j.msgBox({ content: msg, type: "info" }, posY);
    } else
        $j.msgBox({ content: msg, type: "info" }, 100);
}

function ddToggle(e) {
    e.preventDefault();
    $j(".ddlBtn dd ul").toggle();
}


function ShowViewPopup() {
    $j("#divFilterView").css('display', 'block');
}

function CloseViewPopup() {
    $j("#divFilterView").css('display', 'none');
}

function FilterMyView(id) {
    //ShowDimmer(true);
    //window.parent.loadFrame();
    callParentNew("loadFrame()", "function");
    $j("#hdnMyViewName").val(id);

}


function DeleteMyView(id) {

    var result = confirm("Are you sure you want to delete this item?");
    if (result) {
        $j("#hdnMyViewName").val(id);
        $j("#btnDeleteMyView").click();
    }

}

function CheckMyViewFilters() {


    var strMyviewcol = $j("#hdnMyViewFilterCol").val();
    if (strMyviewcol != "" && typeof strMyviewcol !== "undefined") {
        var arrmyViewcolValues = new Array();
        //colname:value1,value2^colname:value,value
        //for(var j=0; j<)
        var arrColDtls = strMyviewcol.split("^");
        for (var j = 0; j < arrColDtls.length; j++) {
            var filColNm = arrColDtls[j].substring(0, arrColDtls[j].indexOf(":"));
            var filColDt = arrColDtls[j].substring(arrColDtls[j].indexOf(":") + 1);
            arrmyViewcolValues = filColDt.split(",");
            for (var i = 0; i < arrmyViewcolValues.length; i++) {
                $j("#leftPanel:input[value=" + arrmyViewcolValues[i] + "]").attr("checked", "true");

                //leftPanel filterUL,,,id=LevelcolValue
                var chkId = "#" + filColNm + "colValue";
                $j(chkId + " input").each(function () {
                    if ($j(this).val() == arrmyViewcolValues[i].toString())
                        $j(this).prop("checked", true);
                    //$j(this).attr("checked", "checked");
                });
            }
        }

        if ($j("#hdnparamValues").val() != "")
            SetParamValues($j("#hdnparamValues").val());
    }
    $j("#hdnMyViewFilterCol").val("");
}



function OpenIviewFromIv(srcUrl, params, ivname, navigationType) {
    if (navigationType != undefined)
        configNavProp = navigationType;
    else
        configNavProp = "";

    if (srcUrl != "") {
        closeParentFrame();
        srcUrl = srcUrl + "&AxOpenIvAction=true";
        try {

            ASB.WebService.SetActionIvParams(srcUrl, params, ivname, SuccOpenIvAction);
        } catch (ex) {
            closeParentFrame();
        }
    }
}


function SuccOpenIvAction(result, eventArgs) {
    if (result != "") {
        callParentNew("updateAppLinkObj(" + result + ",1)", "function")
        //todo : need to remove
        var parFrm = $j("#axpiframe", parent.document);

        if (configNavProp == "" && parFrm.hasClass("frameSplited"))
            configNavProp = "split";

        if (configNavProp === "split") {
            ShowDimmer(false);
            callParentNew(`OpenOnPropertyBase(${result})`, 'function');

        }
        else if (configNavProp === "popup") {
            LoadPopPage(result)
        }
        else if (configNavProp === "default" || configNavProp === "") {
           UpdatePrevMid1FrameUrl(result);
            document.location.href = result;
            
        }
        else if (configNavProp === "newpage") {
            popupFullPage(result)
        }
    }
}

function AdjustIviewWinff() {
    //if ($j.browser.mozilla)
    //    adjustwin($j(window.document.body).height(), window);
}

function adjustwin(ht, iframeWindow, height) {
    var framename = iframeWindow.name;
    framename = framename.toLowerCase();
    if ((framename.substring(0, 7) == "openpop") || (framename.substring(0, 7) == "loadpop") || (framename.substring(0, 7) == "ivewPop")) {
        framename = "MyPopUp";
    }

    //if ((iframeWindow.name != "MyPopUp") && (framename != "MyPopUp")) {
    //    var FFextraHeight = 0;
    //    if ($j("#middle1", parent.document)) {
    //        var nweFrm = $j("#middle1", parent.document);
    //        if (height != undefined && height > ht)
    //            nweFrm.height(height);
    //        else {
    //            var custToolFld = $j("#ax_customtoolbar000F1");
    //            if (custToolFld.length > 0)
    //                nweFrm.height(ht + 100);
    //            else
    //                nweFrm.height(ht);
    //        }
    //    }
    //}
}
//global variable for pick list 
var initialSrchVal = "";
$j(document).keydown(function (e) {
    if (e.which == 13 && $j('#dvPickList').is(':visible') && $j('#tblPickData tr.active').length > 0) {
        //check for the value
        if (initialSrchVal == $j("#" + $j("#tblPickData").attr("data-pick")).val()) {
            e.preventDefault();
            e.stopPropagation();
            var paramString = $j("#tblPickData tr.active td").attr("onclick");
            paramString = paramString.split(';')[0];
            paramString = paramString.substring(23, paramString.length - 2);
            SetPickVal(paramString);
            FillDependents(paramString.split('^')[0]);
            pickStatus = false;
            selectedRow = 0;
            e.which = -1;
            //clear the value
            initialSrchVal = "";
        } else {
            ParamSearchOpen($j("#" + $j("#tblPickData").attr("data-pick"))[0]);
        }
    }
    if (e.which == 40 && $j('#dvPickList').is(':visible') && $j('#tblPickData tr.active').length > 0) {
        e.preventDefault();
        e.stopPropagation();
        pickDownn();
        if ($("#dvPickHead .active").offset().top - $("#dvPickHead").offset().top + $("#dvPickHead .active").outerHeight() >= $("#dvPickHead").height() || $("#dvPickHead .active").offset().top - $("#dvPickHead").offset().top < 0) {
            $("#dvPickHead").animate({
                scrollTop: ($("#dvPickHead .active").offset().top - $("#dvPickHead").offset().top) + $("#dvPickHead .active").height()
            }, 100);
        }
        return false;
    }
    if (e.which == 38 && $j('#dvPickList').is(':visible') && $j('#tblPickData tr.active').length > 0) {
        e.preventDefault();
        e.stopPropagation();
        pickUpp();
        //   if ($("#dvPickHead .active").offset().top - $("#dvPickHead").offset().top < $("#dvPickHead").height()) {
        $("#dvPickHead").animate({
            scrollTop: ($("#dvPickHead .active").offset().top - $("#dvPickHead").offset().top) - $("#dvPickHead .active").height()
        }, 100);
        //    }
        return false;
    }

});

function SetFileToDownload(rid, tid, rowid, filename) {
    if (rid != undefined && tid != undefined && filename != undefined) {
        $j.ajax({
            type: "POST",
            url: "../WebService.asmx/GetFilePathForIviewAttachment",
            contentType: "application/json;charset=utf-8",
            data: '{proj: "' + rid + '",tid: "' + tid + '",rowid: "' + rowid + '",filename: "' + filename + '"}',
            dataType: "json",
            success: function (data) {
                InvokeDownloadButton(data.d);
            },
            failure: function (response) {
                showAlertDialog("error", 3028, "client");
            },
            error: function (response) {
                showAlertDialog("error", 3028, "client");
            }
        });
    }
}

function InvokeDownloadButton(link) {
    for (var i = 0; i < link.split('♠').length; i++) {
        var fileUrl = link.split('♠')[i];
        if (fileUrl.indexOf('Error:') > -1) {
            fileUrl = fileUrl.replace('Error:', '')
            showAlertDialog("error", fileUrl);
            continue;
        }
        var a = document.getElementById('hdnButtonInvokeDownload');
        a.href = fileUrl;
        a.click();
    }
}

var ivAttachRid = [];
var ivAttachTransid = [];
var ivAttachRowNo = [];
var ivFileNames = [];

function DownloadSelectedFiles() {
    $j('#GridView1').find('input[type="checkbox"]:checked').each(function (index) {
        var rowIdx = $j('.downloadLink' + (parseInt($j(this).val()) - 1));
        rowIdx.each(function (i) {
            var link = $j(this).attr("href");
            if (link != undefined && link != null && link != "") {
                link = link.replace("javascript:SetFileToDownload(", "");
                link = link.replace(");", "");
                link = link.replace(/'/g, "");
                var tempArray = link.split(',');
                GetFileValues(tempArray);
            }
        })
    });
    GetDownloadLink();
    ivAttachRid = [];
    ivAttachTransid = [];
    ivAttachRowNo = [];
    ivFileNames = [];
}


function GetDownloadLink() {
    try {
        if (ivAttachRid.length > 0) {
            ASB.WebService.GetZipPathForIviewAttachment(ivAttachRid, ivAttachTransid, ivAttachRowNo, ivFileNames, InvokeDownloadButton, OnException);
        } else
            showAlertDialog("warning", 3029, "client");
    } catch (exp) { }
}

function OnException(response) {

}

function GetFileValues(arr) {
    ivAttachRid.push(arr[0]);
    ivAttachTransid.push(arr[1]);
    ivAttachRowNo.push(arr[2]);
    ivFileNames.push(arr.slice(3).join(","));
}

function setWidth() {

    HideFilter();
    $j("#showFilter").css("display", "block");
    $j("#dvHideFilter").css("display", "none");
    $j("#contentPanelGridIview").css("margin-top", "2%");


}


function parafilterfixer() {
    //if (parafilterfix.myFilters == false || parafilterfix.myFilters == null) {
    if (parafilterfix.myFilters == false) {
        $j("#myFilters").attr("aria-expanded", "false");
        if ($j("#Filterscollapse").hasClass("in")) {
            iviewTableWrapperHeightFix();
        }
        $j("#Filterscollapse").removeClass("in");


        $j("#Filterscollapse").attr("aria-expanded", "false");
    } else {
        $j("#myFilters").attr("aria-expanded", "true");
        if (!$j("#Filterscollapse").hasClass("in")) {
            iviewTableWrapperHeightFix();
        }
        $j("#Filterscollapse").addClass("in");
        $j("#Filterscollapse").attr("aria-expanded", "true");
    }
}

function RefreshParams() {
    $j('#hdnGo').val("refreshparams");
    $j('#btnRefreshParams').click();
}

function UpdateGetParamCache() {
    $j('#hdnGo').val("updateCache");
    clearAdvancedFiltersforNewData();
    capturedButton1Submit = true;
}

function GetStagRecords(checkLink) {

    //parent.window.loadFrame();
    callParentNew("loadFrame()", "function");

    $j("#hdnIViewShow").val(checkLink);

    var key = $j("#hdnKey").val();

    var tableno = $j("#hdnStagTableNo").val();
    var totalRecords = $j("#hdnTotalIViewRec").val();
    if (totalRecords > 20) {
        if (checkLink == 'More') {
            try {
                ASB.WebService.GetNextStagPage(key, tableno, SuccessGetStag);
            } catch (ex) { AxWaitCursor(false); }
        } else if (checkLink == 'All') {
            try {
                ASB.WebService.GetAllPages(key, tableno, SuccessGetStag);
            } catch (ex) { AxWaitCursor(false); }
        }
    } else {
        showMore = false;
        DisableStagLoad();
        //parent.window.closeFrame();
        callParentNew("closeFrame()", "function");
    }

}

function FailureGetStag(result, eventArgs) {
    closeParentFrame();
}

function SuccessGetStag(result, eventArgs) {
    if (CheckSessionTimeout(result))
        return;
    var xmlDoc = $j.parseXML(result);
    var xml = $j(xmlDoc);
    var tableno = $j("#hdnStagTableNo").val();
    tableno = parseInt(tableno, 10) + 1;
    $j("#hdnStagTableNo").val(tableno);

    var iViewShowCat = $j("#hdnIViewShow").val();
    var customers;
    if (iViewShowCat == 'More') {
        customers = xml.find("Table_" + tableno);
    } else {
        customers = $j(xml).find("Table1");
    }

    customers.each(function () {
        var customer = $j(this);
        var row = $j("[id$=GridView1] tr").eq(1).clone(true);
        var rowHtml = "";
        if (customer[0].childNodes) {
            var idx = 0;
            for (var j = 0; j < customer[0].childNodes.length; j++) {
                var colText = customer[0].childNodes[j].textContent;
                if (colText != "\n    ") {

                    if (j == 0 || j == 1) {
                        var htmlString = row.find("td").eq(idx).html();
                        if (htmlString.indexOf("checkbox") > -1) {
                            row.find("td").eq(idx).html(htmlString.replace('value="1"', 'value="' + colText + '"'));
                        } else
                            row.find("td").eq(idx).html(colText);
                    } else {
                        if (colText == "Grand Total")
                            row.find("td").eq(idx - 1).html("");
                        row.find("td").eq(idx).html(colText);
                    }
                    idx++;
                }
            }
        } else {
            for (var j = 0; j < customer[0].children.length; j++) {
                var colText = customer[0].children[j].textContent;
                if (j == 0) {
                    var htmlString = row.find("td").eq(j).html();
                    if (htmlString.indexOf("checkbox") > -1) {
                        row.find("td").eq(j).html(row[0].children[0].innerHTML.replace('value="1"', 'value="' + colText + '"'));
                    } else {
                        row.find("td").eq(j).html(colText);
                    }
                } else {
                    row.find("td").eq(j).html(colText);
                }
            }
        }
        $j("[id$=GridView1]").append(row);


    });
    //Adjustwin();

    var totalGridRows = $j("#GridView1 tr").length - 1;

    if ($j("#GridView1 tr:last td:eq(0)").text() == "Grand Total" || $j("#GridView1 tr:last td:eq(1)").text() == "Grand Total" || $j("#GridView1 tr:first td:eq(1)").text() == "Grand Total") {
        if ($j("#GridView1 tr:last td:eq(0)").text() == "Grand Total") {
            $j("#GridView1 tr:last td:eq(1)").text("Grand Total");
            $j("#GridView1 tr:last td:eq(1)").css("color", "DarkGreen");
            $j("#GridView1 tr:last td:eq(1)").css("font-weight", "bold");
            $j("#GridView1 tr:last td:eq(0)").text(" ")
        } else if ($j("#GridView1 tr:last td:eq(1)").text() == "Grand Total") {
            $j("#GridView1 tr:last td:eq(2)").text("Grand Total");
            $j("#GridView1 tr:last td:eq(2)").css("color", "DarkGreen");
            $j("#GridView1 tr:last td:eq(2)").css("font-weight", "bold");
            $j("#GridView1 tr:last td:eq(1)").text(" ");
        }
        totalGridRows = totalGridRows - 1;
    }
    var totalRecords = $j("#hdnTotalIViewRec").val();
    if ($j("#nextPrevBtns").is(':visible')) {

        $j("#lblNoOfRecs").text("Total no of Rows :  " + (totalGridRows) + " of " + totalRecords);
    } else {
        if ($j("#dvFilteredRowCount").is(':visible')) {
            var cutMsg = eval(callParent('lcm[12]'));
            cutMsg = cutMsg.replace('{0}', totalGridRows).replace('{1}', totalRecords)
            $j("#lblFilteredRowCount").text(cutMsg);
        }
    }
    if (iViewShowCat == 'All' || totalGridRows == totalRecords) {
        showMore = false;
        DisableStagLoad();
    }
    //parent.window.closeFrame();
    callParentNew("closeFrame()", "function");
}

function DisableStagLoad() {
    var lnkShow = $j("#dvStagLoad").find('a');
    lnkShow.removeAttr('onclick');
    lnkShow.removeClass('handCursor');
    lnkShow.attr("disabled", "disabled");
    lnkShow.css("color", "GREY");
}


function CheckValidTime(fldValue) {
    var finalFldValue = "";
    var values = fldValue.split(':');
    if (timePickerSec && values.length == 2) {
        values.push("");
    } else if (timePickerSec && values.length == 1) {
        values.push("");
        values.push("");
    } else if (!timePickerSec && values.length == 1) {
        values.push("");
    }
    values[0] = values[0].replace(/[^0-9]/g, "0");
    values[1] = values[1].replace(/[^0-9]/g, "0");
    if (timePickerSec) {
        values[2] = values[2].replace(/[^0-9]/g, "0");
    }
    if (!(parseInt(values[0]) <= 23)) {
        values[0] = (values[0].charAt(1) == ":" ? "0" : values[0].charAt(1)) + (values[0].charAt(0) == ":" ? "0" : values[0].charAt(0));
        if (!(parseInt(values[0]) <= 23))
            values[0] = "00";
    }
    if (!(parseInt(values[1]) <= 59)) {
        values[1] = (values[1].charAt(1) == ":" ? "0" : values[1].charAt(1)) + (values[1].charAt(0) == ":" ? "0" : values[1].charAt(0));
        if (!(parseInt(values[1]) <= 59))
            values[1] = "00";
    }
    if (timePickerSec && !(parseInt(values[2]) <= 59)) {
        values[2] = (values[2].charAt(1) == ":" ? "0" : values[2].charAt(1)) + (values[2].charAt(0) == ":" ? "0" : values[2].charAt(0));
        if (!(parseInt(values[2]) <= 59))
            values[2] = "00";
    }
    if (timePickerSec) {
        finalFldValue = values[0] + ":" + values[1] + ":" + values[2];
    } else {
        finalFldValue = values[0] + ":" + values[1];
    }

    return finalFldValue;
}
var plname;
var plvalue;
var plpageNo;
var plpagesize;
var objid;

function ParamSearchOpen(txtobj) {
    curPageNo = 1;
    var id = txtobj.id;
    var fname1 = "";
    var fname = id.split('~');
    if (id.indexOf("~") != -1)
        fname1 = fname[1].toString();
    else
        fname1 = fname[0].toString();

    var obj = $j("#" + fname1);
    var value = obj.val();
    //if (obj.val() != "") {
    plname = fname1;
    plvalue = value;
    plpageNo = 1;
    plpagesize = 10;
    objid = id;
    CallPicklistService(plname, plvalue, plpageNo, plpagesize, objid);
    //}
    //else {
    //    alert("Enter search parameter");
    //    obj.focus();
    //}
}

var fallback;
var totalCount;

function CallPicklistService(fldname, value, pageNo, pagesize, id) {
    ShowDimmer(true);
    initialSrchVal = value;
    GetParamValues("clear");//To get all the parameters name & value for picklist dependency.  //"clear" param is passed to skip the alert msg.
    $j.ajax({
        type: "POST",
        url: "../aspx/iview.aspx/GetIviewPickListData",
        contentType: "application/json;charset=utf-8",
        data: '{iviewName: "' + iName + '",pageNo: "' + pageNo +
            '",pageSize: "' + pagesize + '",fieldName :"' + fldname + '",fieldValue :"' +
            value + '",depParamVal:"' + $j("#hdnparamValues").val() + '"}',
        dataType: "json",
        success: function (data) {
            var resultString = "";
            if (CheckSessionTimeout(data.d)) {
                ShowDimmer(false);
                return;
            }
            if (data.d.toLowerCase().indexOf("error") > -1) {
                var cutMsg = eval(callParent('lcm[10]'));
                alert(cutMsg);
                ShowDimmer(false);
            } else {
                var json = JSON.parse(data.d.replace(/@/g, ''));
                if (json.sqlresultset.response != null) {
                    //alert('No values found');
                    //ShowDimmer(false);
                    //AxWaitCursor(false);
                    //return;

                    var count = -1;

                    if (!fallback) {
                        count = parseInt(json.sqlresultset.response.totalrows);
                        totalCount = count;
                    } else {
                        count = totalCount;
                        fallback = false;
                    }
                    resultString = count + '♣';
                    var dfparam = eval('dfval' + fldname);
                    if (count == 1) {
                        var temp = json.sqlresultset.response.row;
                        resultString += temp[Object.keys(temp)[0]] + '¿';
                    } else
                        $j.each(json.sqlresultset.response.row, function (i, item) {
                            var temp = json.sqlresultset.response.row[i];

                            if (typeof temp != "object") {
                                if (dfparam != "" && temp.indexOf("~") != -1)
                                    resultString += temp[dfparam.split('|')[0].split('~')[0].toUpperCase()] + '¿';
                                else
                                    resultString += temp[Object.keys(temp)[0]] + '¿';
                            } else {
                                resultString += temp[Object.keys(temp)[0]] + '¿';
                            }
                        })


                    //for (var i = 0; i < pagesize; i++) {
                    //    resultString += 
                    //json.sqlresultset.response.row[i].NAME + '¿';
                    //}
                }
                AssignPLValues(resultString, pagesize, id, fldname);
            }
        },
        failure: function (response) {
            var cutMsg = eval(callParent('lcm[10]'));
            alert(cutMsg);
            ShowDimmer(false);
            AxWaitCursor(false);
        },
        error: function (response) {
            var cutMsg = eval(callParent('lcm[10]'));
            alert(cutMsg);
            ShowDimmer(false);
            AxWaitCursor(false);
        }
    });
}

function AssignPLValues(result, pageSize, id, field) {
    var resultArr;
    if (result != "" && result.substring(0, 7) != "<error>") {
        TogglePrevNextLink("inline");
        var tableStr = "";
        if (result.indexOf("♣") != -1) {
            var totRowStr = result.split("♣");
            totalPLRows = parseInt(totRowStr[0], 10);
            if ((totalPLRows / pageSize) % 1 > 0)
                noOfPLPages = Math.floor(totalPLRows / pageSize) + 1;
            else
                noOfPLPages = totalPLRows / pageSize;
            resultArr = totRowStr[1].split("¿");
        } else {
            resultArr = result.split("¿");
        }

        var dv = $j("#dvPickHead");

        if (resultArr != undefined && resultArr != "") {
            tableStr = "<table style='width:100%;height:auto;' id='tblPickData' data-pick='" + field + "' class='pickGridData'>";
            //tableStr += "<tr><td style='background-color:gray';><div style='float:right;'><img class='curHand' src='../AxpImages/icons/close-button.png' alt='Close' onclick=\"javascript:HidePLDiv(true,'" + id + "');\"/></div></td></tr>";

            for (var i = 0; i < resultArr.length; i++) {
                var tdId = "axPickTd00" + (i + 1) + 'F';
                var pickValue = CheckSpecialCharsInHTML(resultArr[i]);
                if (resultArr[i].toString() != "") {
                    if (resultArr[i].toString().indexOf("^") != -1) {
                        var displayText = resultArr[i].toString().split('^')[1];
                        tableStr += "<tr><td id =" + tdId + " onclick='javascript:SetPickVal(\"" + field + "^" + pickValue + "\");FillDependents(\"" + field + "\");' class='handCur'><a>" + displayText + "</a></td></tr>";
                    } else {
                        displayText = resultArr[i].toString();
                        if (pickValue.indexOf("\\") != -1) {
                            pickValue = pickValue.replace("\\", "\\\\");
                            tableStr += "<tr><td id =" + tdId + " onclick='javascript:SetPickVal(\"" + field + "^" + pickValue + "\");FillDependents(\"" + field + "\");' class='handCur' ><a>" + displayText + "</a></td></tr>";
                        } else {
                            tableStr += "<tr><td id =" + tdId + " onclick='javascript:SetPickVal(\"" + field + "^" + pickValue + "\");FillDependents(\"" + field + "\");' class='handCur'><a>" + displayText + "</a></td></tr>";
                        }
                    }
                }
            }
            tableStr += "</table>";

            if (dv.length > 0)
                dv.html(tableStr);
            if ($j("#tblPickData tr").length > 0) {
                $j("#tblPickData tr:nth-child(1)").addClass('active');
            }
        } else {
            if (dv.length > 0) {
                var cutMsg = eval(callParent('lcm[11]'));
                dv.html("<label>" + cutMsg + "</label>");
            }
        }
        SetPrevNextLinks();
        document.getElementById('advancebtn').style.visibility = 'visible';
    } else {
        tableStr = "<table style='width:100%;height:auto;' id='' cellpadding='1' cellspacing='1'>";
        tableStr += "";
        tableStr += "</table>";
        document.getElementById('advancebtn').style.visibility = 'hidden';
        var cutMsg = eval(callParent('lcm[11]'));
        tableStr += "<label>" + cutMsg + "</label>";
        TogglePrevNextLink("none");
        var dv = $j("#dvPickHead");
        if (dv.length > 0)
            dv.html(tableStr);
    }
    var rowsCount = $j("#tblPickData tbody tr").length;
    if (rowsCount > 1) {
        $j(".inputClass2").keydown(function (e) {

            if (e.which == 40) { // down arrow
                typeof selectedRow == "undefined" ? selectedRow = 0 : "";
                if (selectedRow < rowsCount) {
                    if ($j("#tblPickData tr:nth-child(" + selectedRow++ + ")").length == 0)
                        selectedRow = 2;

                    $j("#tblPickData tr").removeClass("pickbg");
                    $j("#tblPickData tr:nth-child(" + selectedRow + ")").addClass("pickbg");

                    var totalHgt = 0;
                    var rowTotHgt = 0;
                    for (var i = 0; i < selectedRow; i++) {
                        rowTotHgt += $j("#tblPickData tr:nth-child(" + i + ")").height();
                    }
                    var dvHgt = 0;
                    dvHgt = $j('#dvPickHead').height();
                    if ((rowTotHgt + 20) > dvHgt) {
                        //$j('#dvPickHead').scrollTop((rowTotHgt - dvHgt) + 20);
                        $j('#dvPickHead').scrollTop($j('#dvPickHead')[0].scrollHeight);
                    }
                }
            } else if (e.which == 38) { // up arrow
                if (selectedRow > 2) {
                    if ($j("#tblPickData tr:nth-child(" + selectedRow-- + ")").length == 0)
                        selectedRow++;

                    $j("#tblPickData tr").removeClass("pickbg");
                    $j("#tblPickData tr:nth-child(" + selectedRow + ")").addClass("pickbg");

                    if (selectedRow == 1) {
                        e.preventDefault();
                    }
                    var totalHgt = 0;
                    var rowTotHgt = 0;
                    for (var i = rowsCount; i >= selectedRow; i--) {
                        rowTotHgt += $j("#tblPickData tr:nth-child(" + i + ")").height();
                    }
                    var dvHgt = 0;
                    dvHgt = $j('#dvPickHead').height();
                    if ((rowTotHgt + 10) > dvHgt) {
                        var tmpRowNo = selectedRow - 1;
                        $j('#dvPickHead').scrollTop($j("#tblPickData tr:nth-child(" + tmpRowNo + ")").height());

                    }
                }
            }
            // for next,Prev,close and advanced
            else if (e.which == 39) { // right arrow
                if ($j('#nextPick').attr("onclick") != undefined && $j('#nextPick').attr("onclick") != "") {
                    GetData('next');
                    $j('#dvPickHead').scrollTop(0);
                    selectedRow = 1;
                }
            } else if (e.which == 37) { // left arrow
                if ($j('#prevPick').attr("onclick") != undefined && $j('#prevPick').attr("onclick") != "") {
                    GetData('prev');
                    selectedRow = 1;
                    $j('#dvPickHead').scrollTop(0);
                }
            } else if (e.which == 27 || e.which == 9) { // for Close the picklist
                //if ($j("#dvPickList").is(":visible"))
                HidePLDiv(false, id);
                selectedRow = 0;
            }
            e.which = -1;
        });
    }

    $j(document).keypress(function (e) {

        if (e.keyCode == -1)
            return;
        if (e.keyCode == 13) {
            var fldId = GetActivePickListId();
            var fieldDcNo = GetFieldsDcNo(fldId);
            var fieldRowNo = GetFieldsRowNo(fldId);
            AxActiveRowNo = GetDbRowNo(fieldRowNo, fieldDcNo);

            if ($j("#dvPickList").is(':visible')) {
                var tdId = "#axPickTd00" + --selectedRow + 'F' + GetTdFrameNo();
                if ($j(tdId).length > 0) {
                    var selcteditm = $j(tdId).find('a')[0].innerText;
                    if (selcteditm == undefined)
                        selcteditm = $j(tdId).find('a')[0].textContent;
                    SetPickVal(field + "^" + selcteditm);
                    FillDependents(field);
                    pickStatus = false;
                }
                selectedRow = 0;
            }
        }
        e.which = -1;
    });
    $j(document).click(function (e) {
        if (e.target.id != 'nextPick' && e.target.id != 'prevPick' && e.target.className != 'curHand' && e.target.className != 'hdrRow') {
            HidePLDiv(false, id);
            selectedRow = 0;
        }
    });
    ShowPickList(id);
    ShowDimmer(false);
    AxWaitCursor(false);
    $j("#" + id).focus();
}

function HidePLDiv(setFocus, id) {



    var dv = $j("#dvPickList");
    if (dv.length > 0)
        dv.hide();
    dv.addClass("hide");
    // adjustwin('700', window);
    if (setFocus) {
        $j("#" + id).focus();
    }
    // event.preventDefault();
}

function ShowPickList(id) {

    $j("#pickDimmer").css("display", "none");
    var objId = id;
    var fldProps = objId.split("~");
    var i;
    if (fldProps.length == 2)
        i = 1;
    else
        i = 0;
    var pickFld = document.getElementById(fldProps[i]);

    FindCtrlPos(pickFld);
    var pickFld = $j("#" + fldProps[i]);
    var wdth = pickFld.width() + 19;
    var plHgt = pickFld.height();

    var dv = $j("#dvPickList");
    var divFrameNo = GetFieldsDcNo(fldProps[i]);
    // Sets the position of the DIVn
    //dv.height("242");
    //dv.width("200");
    dv.width(wdth);

    //If it's a scroll div,set the position of the left scroll value
    var scrollleft = $j("#contentScroll" + divFrameNo).scrollLeft();
    var scrollTop = $j("#contentScroll" + divFrameNo).scrollTop();
    if (scrollTop > 0) {
        var tmpHgt = (iY - plHgt - scrollTop + 18);
        var tmpTop = parseInt((tmpHgt - 0), 10);
        if (tmpTop > 0)
            dv.css("top", tmpTop + 'px');
        else
            dv.css("top", (iY + 5) + 'px');
    } else
        dv.css("top", (iY + 5) + 'px');

    if (scrollleft > 0) {
        var slValue = (iX - wdth - scrollleft);
        var clientWidth = $j("#divDc" + divFrameNo).width() + 20;
        var scrollWidth = 0;
        var popupScrollWidth = (slValue + 200);
        if (clientWidth < popupScrollWidth)
            scrollWidth = popupScrollWidth - clientWidth;
        var leftV = parseInt((slValue - scrollWidth), 10);
        if (leftV > 0)
            dv.css("left", leftV + 'px');
        else
            dv.css("left", '0px');
    } else
        dv.css("left", (iX - wdth) + 'px');


    dv.show();
    dv.removeClass("hide");
    CheckHeight(iY + 5, 205);
}

function CheckHeight(top, dvHgt) {
    var frm = $j("#middle1", parent.document);
    var docHgt = document.body.offsetHeight;
    var totHgt = top + dvHgt;
    if (totHgt > docHgt) {
        docHgt = docHgt + (totHgt - docHgt);
        frm.height(docHgt + 100);
    }
}

function TogglePrevNextLink(status) {
    var prev = $j("#prevPick");
    var next = $j("#nextPick");

    if (status == "block" || status == "inline") {
        prev.show();
        next.show();
    } else {
        prev.hide();
        next.hide();
    }
}

//Function which decides to display the next and prev button in the picklist div.
function SetPrevNextLinks() {
    var prev = $j("#prevPick");
    var next = $j("#nextPick");
    var tblPick = $j("#tblPickData");

    if (curPageNo == 1) {
        prev.attr("disabled", "disabled");
        prev.css("display", "none");
        prev.removeAttr("onclick");
    } else {
        prev.attr("disabled", false);
        prev.css("color", "black");
        next.css("color", "black");
        prev.attr("onclick", "javascript:GetData('prev')");
    }

    if (curPageNo < noOfPLPages) {
        next.attr("disabled", false);
        next.css("color", "black");
        next.attr("onclick", "javascript:GetData('next')");
    } else if (curPageNo >= noOfPLPages) {
        next.attr("disabled", "disabled");
        next.css("display", "none");
        next.removeAttr("onclick");
    } else {
        tblPick.attr("disabled", "disabled");
        tblPick.removeAttr("onclick");

    }

}

//Function to get the next or prev page records in picklist dropdown.
function GetData(str) {

    if (str == "prev" && curPageNo > 1) {
        curPageNo = curPageNo - 1;
    } else if (str == "next" && curPageNo < noOfPLPages) {
        curPageNo = curPageNo + 1;
    }
    fallback = true;
    CallPicklistService(plname, plvalue, curPageNo, plpagesize, objid);
}


function FindCtrlPos(fldName) {

    if (typeof (fldName) == "string") {
        fldName = document.getElementById(fldName);
    }
    var fld = fldName;
    var textbox = jQuery(fldName);
    var offset = textbox.offset();
    iX = findPosX(fld) + fld.clientWidth + 1;
    iY = findPosY(fld) + fld.clientHeight + 1;
}

function findPosX(obj) {
    var curleft = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    } else if (obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    } else if (obj.y)
        curtop += obj.y;
    return curtop;
}

function SetPickVal(vall) {
    vall = RepSpecialCharsInHTML(vall);
    vall = vall.split('^');
    var elem = vall[0],
        value = vall[1];
    value = value.replace(/&#94;/g, "^");
    value = value.replace(/&#64;/g, "@");
    document.getElementById(elem).value = value;
    //event.preventDefault();
    //pickStatus = false;
    HidePLDiv(true, elem);
}

function CallSearchOpen() {
    if ($j("#hdnparamValues").val() != "")
        isPlDepParBound = true;
    ParamSearchAdvanceOpen($j("#" + $j("#tblPickData").attr("data-pick"))[0]);
    HidePLDiv(false, $j("#" + $j("#tblPickData").attr("data-pick"))[0].id);
    isPlDepParBound = false;
}

function GetActivePickListId() {
    var hdn = $j("#hdnPickFldId");
    var objId = hdn.val();
    var fldProps = objId.split("~");
    var i;
    if (fldProps.length > 1)
        i = 1;
    else
        i = 0;

    var fldName = fldProps[i];
    return fldName;
}

/**
 * Initialize objects and html for smartviews
 * @author Prashik
 * @Date   2019-04-11T10:46:29+0530
 * @param  {[type]}                 jsonString : json string for initializing smartview's object and html
 */
function createIvir(jsonString) {
    $("td[data-order^=Grand]").attr("data-order", "");
    ivirColumnTypeObj = {};
    var jsonObject = $.parseJSON(jsonString);
    //var jsonObject = jsonString;
    if (!getAjaxIviewData) {


        ColumnType = $.parseJSON(jsonObject.ColumnType);
        HeaderText = $.parseJSON(jsonObject.HeaderText);
        FieldName = $.parseJSON(jsonObject.FieldName);
        HideColumn = $.parseJSON(jsonObject.HideColumn);
        if ((ind = FieldName.indexOf("axrowtype")) > -1) {
            ColumnType.splice(ind, 1);
            HeaderText.splice(ind, 1);
            FieldName.splice(ind, 1);
            HideColumn.splice(ind, 1);
        }
        if ((ind2 = FieldName.indexOf("axp__font")) > -1) {
            ColumnType.splice(ind2, 1);
            HeaderText.splice(ind2, 1);
            FieldName.splice(ind2, 1);
            HideColumn.splice(ind2, 1);
        }
        //HideColumnLength = HideColumn.length;
        custBtnIVIR = $.parseJSON(jsonObject.customObjIV);
        isChkBox = jsonObject.HasCKB;
    } else {
        if (jsonObject.data) {
            ivComps = jsonObject.data.comps;
            ivHeadRows = jsonObject.data.headrow;
            ivDatas = processRowData(jsonObject.data);
            if (jsonObject.data.actions) {
                ivActions = jsonObject.data.actions;
            }
            if (jsonObject.data.configurations && jsonObject.data.configurations.config) {
                ivConfigurations = jsonObject.data.configurations.config.length == undefined ? [jsonObject.data.configurations.config] : jsonObject.data.configurations.config;

                if (ivConfigurations) {
                    try {
                        trimIviewData = getCaseInSensitiveJsonProperty(ivConfigurations.filter((val, ind) => {
                            var thisVal = getCaseInSensitiveJsonProperty(val, "PROPS");
                            return thisVal && thisVal.toString() && thisVal.toString().toLowerCase() == "trim iview data"
                        })[0], ["PROPSVAL"]).toString().toLowerCase() == "true";
                    } catch (ex) { }
                }
            }
        }

        //ivDatas = jsonObject.data.row || jsonObject.data.rowdata || [];


        FieldName = [];
        HeaderText = [];
        ColumnType = [];
        HideColumn = [];
        custBtnIVIR = [];//
        isChkBox = "true";
        $.each(ivHeadRows, (key, value) => {
            if (!key.startsWith("@") && key != "axp__font" && key != "axrowtype" && key != "pivotghead") {
                FieldName.push(key);
                HeaderText.push(value["#text"] || "");
                ColumnType.push(value["@type"] || "c");
                HideColumn.push(value["@hide"].toString() || "true");
                if (key == "rowno") {
                    isChkBox = (value["@hide"].toString() == "false").toString();
                }
            }

            //console.log(key);
        });
    }
    for (var i = 0; i < HeaderText.length; i++) {
        if (HeaderText[i] !== "") {
            ivirColumnTypeObj[FieldName[i]] = ColumnType[i];
        }
    }
    rowTypeExist = FieldName.filter(function (a) { return a == "axrowtype" }).length > 0;
    filteredColumns = FieldName.filter(function (a, b, c) {
        return HideColumn[b] == "false" && (FieldName[b] != "rowno" && FieldName[b] != "axrowtype")
        //return FieldName[b] != "rowno" && FieldName[b] != "axrowtype"
    });
    if (getAjaxIviewData) {
        var pivotHeaderHtml = ``;
        if (ivHeadRows.pivotghead !== undefined) {
            var pivotCreated = true;
            pivotHeaderHtml += `<tr style="height:25px;">`;
            //pivotHeaderHtml += (isChkBox == "true" ? `<th align="center" colspan="1" rowspan="1" class="pivotHeaderStyle"></th>` : ``);
            pivotHeaderHtml += `<th style="width:${minCellWidth}px;" align="center" colspan="1" rowspan="1" class="pivotHeaderStyle"></th>`;
            if (ivHeadRows.pivotghead) {
                ivHeadRows.pivotghead.head.forEach(function (a, b, c) {
                    var colSpan = parseInt(a.en) - parseInt(a.sn);
                    a.ghead = a.ghead || "";
                    pivotHeaderHtml += `<th align="center" colspan="${colSpan}" rowspan="1" class="pivotHeaderStyle">${a.ghead.replace(/~/g, "<br />")}</th>`;
                });
            } else if (jsonObject.data.PivotAndMerge && Object.keys(jsonObject.data.PivotAndMerge).filter((key, ind) => { return jsonObject.data.PivotAndMerge[key]["#text"] !== "" }).length) {
                //delete jsonObject.data.PivotAndMerge[Object.keys(jsonObject.data.PivotAndMerge)[0]];
                Object.keys(jsonObject.data.PivotAndMerge).forEach((key, ind) => {
                    var a = jsonObject.data.PivotAndMerge[key];
                    var colSpan = parseInt(a["@e"]) - parseInt(a["@s"]);
                    a["#text"] = a["#text"] || "";
                    pivotHeaderHtml += `<th align="center" colspan="${colSpan}" rowspan="1" class="pivotHeaderStyle">${a["#text"].replace(/~/g, "<br />")}</th>`;
                });
            } else {
                pivotCreated = false;
            }
            pivotHeaderHtml += `</tr>`;
            if (!pivotCreated) {
                pivotHeaderHtml = ``;
            }
        }
        var thMenuTemplete = `<button title="Actions Menu" class="noBg pull-right rightClickMenuIcn" type="button"><span class="fa fa-ellipsis-v " aria-hidden="true"></span></button>`;
        var footerTdTemplate = `<td></td>`;
        var footerHtml = ``;
        //var GridView2THtml = `<table class="gridData" rules="all" border="1" id="GridView1" style="width:559px;" data-row>` +
        tableWidth = 0;
        var GridView2THtml = `<table class="gridData" rules="all" border="1" id="GridView1" style="" data-row>` +
            `<thead>` +
            pivotHeaderHtml +
            `<tr style="height:25px;">`;
        //`<th scope="col" style="width:130px;">empid</th>`+
        //GridView2THtml += (isChkBox == "true" ? `<th scope="col" style="width:10px;"><input name="chkall" id="chkall" onclick="javascript:CheckAll();" style="width:10px; height:12px;" type="checkbox"></th>` : ``);

        GridView2THtml += `<th id="GridView1_ctl01_rowno" scope="col" style="width:10px;"><input name="chkall" id="chkall" onclick="javascript:CheckAll();" style="width:${findGetParameter("tstcaption") == null ? minCellWidth : listViewCheckBoxSize}px; height:12px;" type="checkbox">${thMenuTemplete}</th>`;
        footerHtml += footerTdTemplate;
        tableWidth += (findGetParameter("tstcaption") == null ? minCellWidth : listViewCheckBoxSize);
        HeaderText.forEach(function (a, b, c) {
            //if((FieldName[b] != "rowno" && FieldName[b] != "axrowtype")){
            if ((FieldName[b] != "rowno")) {
                var width = ivHeadRows[FieldName[b]]["@width"] || minCellWidth;
                (ivHeadRows[FieldName[b]]["@hide"].toString() || "true") == "false" ? tableWidth += parseInt(width, 10) : "";
                //tableWidth += parseInt(width, 10);
                GridView2THtml += `<th id="GridView1_ctl01_${FieldName[b]}" scope="col" style="width:${width}px;">${a.replace(/~/g, "<br />")}${thMenuTemplete}</th>`;
                footerHtml += footerTdTemplate;
            }

        });
        footerHtml = `<tfoot><tr>${footerHtml}</tr></tfoot>`
        GridView2THtml += `</tr>` +
            `</thead>${footerHtml}</table>`;


        //$("#GridView2Wrapper").html($(`<table class="gridData" rules="all" border="1" id="GridView1" style="width: ${tableWidth}px;" data-row>${GridView2THtml}</table>`));
        $("#GridView2Wrapper").html($(GridView2THtml).css({ "width": `${tableWidth}px` }));

    }

    hiddenColumnIndex = FieldName.map(function (a, b, c) {
        return FieldName[b] == "rowno" || FieldName[b] == "axrowtype" ? "" : (HideColumn[b] == "true" ? b - (rowTypeExist ? 1 : 0) : "")
    }).filter(function (a) { return a !== "" });

    if (isChkBox == "true") {
        filteredColumns = ["", ...filteredColumns];
    } else {
        hiddenColumnIndex = [0, ...hiddenColumnIndex];
    }
    if (getAjaxIviewData) {

        //Expand/Collapse TreeMethod
        HeaderText.forEach((val, ind) => {
            if (val.toString().toLowerCase() == "root_class") {
                cellHeaderConf.root_class_index = FieldName[ind];
            }
            if (val.toString().toLowerCase() == "account name" || val.toString().toLowerCase() == "particulars" || val.toString().toLowerCase() == "+/-") {
                cellHeaderConf.root_account_index = FieldName[ind];
            }
            if (val.toString().toLowerCase() == "root_type") {
                cellHeaderConf.root_atype_index = FieldName[ind];
            }
        });



        if (bulkDownloadEnabled) {
            HeaderText = HeaderText.map((val, ind) => {
                if (val.indexOf("axp_attachfilename") > -1) {
                    if (!cellHeaderConf.FilesIndexes) {
                        cellHeaderConf.FilesIndexes = [];
                    }
                    cellHeaderConf.FilesIndexes.push(FieldName[ind]);
                    val.replace("axp_attachfilename", "");
                    //if(ivHeadRows[FieldName[ind]]["@align"]){
                    ivHeadRows[FieldName[ind]]["@align"] = "Center";
                    //}
                }
                return val;
            });
        }




        if (cellHeaderConf.FilesIndexes && bulkDownloadEnabled) {
            $("#btnDownloadAll").show();
        } else {
            $("#btnDownloadAll").hide();
        }
    }
}

//changes for HMS000158,AGI001702
$(document).on("keydown", "input[type='text'],input[type='search'],input[type='radio'],input[type='checkbox']", function (e) {

    if (e.keyCode == 13) {
        e.preventDefault();
    }
    if (e.keyCode == 13 && $(e.target).parents("#Filterscollapse").length !== 0 && $(e.target).parents(".paramtd2.picklist").length == 0) {
        if ($(e.target).is("input:text")) {
            $(e.target).blur();
        }
        $("#button1").click();
    }
});

function CheckDisabled(elemID) {
    var elem = $("#" + elemID);
    clearAdvancedFiltersforNewData();
    if (elem.length && !elem.hasClass("disabled")) {
        elem.addClass("disabled");
        if ($("#waitDiv").is(":hidden"))
            ShowDimmer(true);
    } else {
        return false;
    }
    return true;
}

/**
 * remove advanced filters and its objects for required scenarioes
 * @author Prashik
 * @Date   2019-04-11T10:49:17+0530
 * @return {[type]}                 [description]
 */
function clearAdvancedFiltersforNewData() {
    $("#divModalAdvancedFilters .body-cont").remove();
    advFiltersObjectToApply = {};
}

/**
 * reset smartview variables for required scenarioes
 * @author Prashik
 * @Date   2019-07-05T10:49:17+0530
 * @return {[type]}                 [description]
 */
function resetSmartViewVariables() {
    nxtScrollSize = 100;
    defaultRecsPerPage = 100;
    checkNextDBRowsExist = true;
    dtPageNo = 1;
    dtTotalRecords = 0;
    dtDbTotalRecords = 0;
    autoAppendRecords = false;
    pageScrollToEnd = false;
    scrollTopPosition = 0;
}

function valdDecPts(obj, actValue) {
    var newVal = parseFloat(obj.value).toFixed(actValue);
    obj.value = newVal;
}

function setRefreshParent(val) {
    eval(callParent('isRefreshParentOnClose') + "=" + val.toLowerCase() + "");
}

function assignclassnames() {

    //$('.hiderow').hide();
    //var totalwidth = "";

    ////Custom Iframe
    //var data = $('#GridView1 > thead > tr > th');

    //for (var s = 0; s < data.length; s++) {
    //    var name = data[s].textContent;
    //    if (name.toLocaleLowerCase() == "link url") {
    //        iframeindex = data[s].cellIndex;
    //        eval(callParent('IsContentAxpIframe') + "= true");
    //    }
    //}

    //var parFrm = $j("#axpiframe", parent.document);
    //var axpFrm = parFrm[0];
    //var parFrm1 = $j("#middle1", parent.document);
    //var midFrm = parFrm1[0];
    //if (frames.frameElement.id == "middle1") {
    //if (iframeindex > -1 && $j("#iframeresizable").length == 0) {
    //    var axpsrc = $('#GridView1 tbody tr').first().find('a').first().attr('href');
    //    if (axpsrc != undefined) {

    //        if (parent.currAxpFrmUrl != "") {
    //            axpsrc = parent.currAxpFrmUrl;
    //            parent.currAxpFrmUrl = "";
    //        }
    //        parent.LoadIframeOnline(axpsrc);
    //        var frmWidtParam = $j("#axp_iframewidth");
    //        if (frmWidtParam.length > 0)
    //        {
    //            var frmWidt = frmWidtParam.val().split(":");

    //            midFrm.style.width = frmWidt[0] + "%";                
    //            axpFrm.style.width = frmWidt[1] + "%";
    //        }
    //        else {               
    //            midFrm.style.width = "40%";                
    //            axpFrm.style.width = "60%";
    //        }
    //        axpFrm.style.cssFloat = "right";
    //    }
    //}
    //else {
    //    eval(callParent('IsContentAxpIframe') + "= false");
    //    midFrm.style.width = "100%";        
    //    axpFrm.style.display = "none";
    //    axpFrm.src = "";
    //}
    //}

    //var data = $('#GridView1 > tbody > tr');
    //for (var s = 0; s < data.length; s++) {
    //    data[s].className = data[s].className.replace("showrow", "hiderow");

    //}


    ////Row Expand All
    //$('#btn_ExpandAll').removeAttr("onclick");
    //$('#btn_ExpandAll').removeAttr("href");

    //$('#btn_ExpandAll').click(function () {

    //    $('.hiderow').removeClass('hiderow').addClass('showrow');
    //    $('.icon-arrows-plus').removeClass('icon-arrows-plus').addClass('icon-arrows-minus');
    //    $('.showrow').show();
    //    //alert(129);

    //});

    ////Row Collapse All
    //$('#btn_CollapseAll').removeAttr("onclick");
    //$('#btn_CollapseAll').removeAttr("href");

    //$('#btn_CollapseAll').click(function () {

    //    $('.showrow').removeClass('showrow').addClass('hiderow');
    //    $('.showchildrow').removeClass('showchildrow').addClass('hiderow');
    //    $('.icon-arrows-minus').removeClass('icon-arrows-minus').addClass('icon-arrows-plus');

    //    $('.hiderow').hide();
    //    $('.rootrow').show();

    //});


    ////Column Merge
    //$('.mergecol').each(function () {
    //    //alert(1);
    //    var cells = $('td', this);
    //    for (var i = 0; i < cells.length; i++) {
    //        $(this).find('a').remove();
    //        if ($(cells[i]).text() !== "\u00a0" && $(cells[i]).html() !== '' &&
    //        ($(cells[i + 1]).text() == "\u00a0" || $(cells[i + 1]).html() == '')) {
    //            var colSpan = parseInt($(cells[i]).attr('colspan'), 10) || 1;
    //            //  alert(colSpan);
    //            $(cells[i]).attr('colspan', ++colSpan);
    //            $(cells[i + 1]).remove();
    //            cells = $('td', this);
    //            i -= 1;
    //        }
    //    }
    //});


    ////On click expend & collapse
    //$('#GridView1 > tbody > tr').click(function () {

    //    if ($(this)[0].className.indexOf('rootrow') > -1 || $(this)[0].className.indexOf('rootchildrow') > -1) {

    //        var rowindex = $(this).closest('tr').index();

    //        if ($(this)[0].className.indexOf('showrow') > 0) {
    //            $(this).removeClass('showrow');
    //            $(this).addClass('hiderow');
    //            $(this)[0].cells[0].firstElementChild.className = 'icon-arrows-plus';
    //        }
    //        else {
    //            $(this).removeClass('hiderow');
    //            $(this).removeClass('showchildrow');
    //            $(this).addClass('showrow')
    //            $(this)[0].cells[0].firstElementChild.className = 'icon-arrows-minus';

    //        }

    //        var clickedclass = $(this).attr('class');
    //        var lastvalue = clickedclass.split(' ');
    //        var len = lastvalue.length;


    //        var prevvalue = lastvalue[len - 2];
    //        lastvalue = lastvalue[len - 1];



    //        //Current row
    //        if ($(this)[0].className.indexOf('hiderow') > 0) {
    //            $('.' + prevvalue).hide();
    //        }
    //        else {
    //            $('.' + prevvalue).show();
    //        }


    //        var trcollection = $('#GridView1 > tbody > tr');
    //        for (var c = 0; c < trcollection.length; c++) {
    //            var classarray = trcollection[c].className.split(' ');

    //            if (classarray.indexOf('showchildrow') > 0) {
    //                trcollection[c].classList.remove('showchildrow');
    //                trcollection[c].className = trcollection[c].className + ' hiderow';

    //            }

    //            if (classarray[len - 2] == prevvalue && classarray.length == (len + 1) && lastvalue == 'showrow') {
    //                trcollection[c].classList.remove('showrow');
    //                trcollection[c].classList.remove('hiderow');
    //                trcollection[c].classList.remove('showchildrow');
    //                trcollection[c].className = trcollection[c].className + ' showchildrow';
    //                trcollection[c].cells[0].firstElementChild.className = 'icon-arrows-plus';
    //                // alert(classarray[len - 1]);
    //                $('.' + classarray[len - 1]).hide();
    //            }

    //            if (classarray[len - 2] == prevvalue && classarray.length == (len + 1) && lastvalue == 'hiderow') {
    //                trcollection[c].classList.remove('showrow');
    //                trcollection[c].classList.remove('hiderow');
    //                trcollection[c].classList.remove('showchildrow');
    //                trcollection[c].className = trcollection[c].className + ' hiderow';
    //                // alert(classarray[len - 1]);
    //                //  $('.' + classarray[len - 1]).hide();
    //            }

    //        }

    //        //    $('.hidechildrow').hide();
    //        $('.showchildrow').show();
    //        if ($(this)[0].className.indexOf('hiderow') > 0 || $(this)[0].className.indexOf('showchildrow')) {
    //            $(this).show();
    //            $(this)[0].classList.remove('showchildrow');
    //        }
    //    }

    //});
}
/**
 * Generate Iview Data in proper format to be consumed by smartviews
 * @author Prashik
 * @Date   2019-04-11T10:50:54+0530
 * @param  {object}                 processData : json object including full get iview data and additional response
 * @return {object}                 returnData : json object in proper format to be consumed by smartviews
 */
function processRowData(processData) {
    processParamBadge();
    var returnData = [];
    if (processData["@perfxml"] && processData["@perfxml"] == "true") {
        if (processData.rowdata.row.length == undefined) {
            returnData = [processData.rowdata.row];
        } else {
            returnData = processData.rowdata.row;
        }
        //if (firstTime) {
        returnData.splice(0, 1);
        //}
        if (returnData.length > 0) {
            if (Object.keys(returnData[0]).indexOf("@ROWNO") < 0) {
                returnData = returnData.map((dataa, indexx) => {
                    return { "@ROWNO": (indexx + 1).toString(), ...dataa };
                });
            }
        }
        isPerf = true;
    } else {
        if (processData.row.length == undefined) {
            returnData = [processData.row];
        } else {
            returnData = processData.row;
        }
    }

    //remove single dummy empty row response from WebService
    returnData.length == 1 && Object.keys(returnData[0]).filter(function(data){
        return returnData[0][data] != "" && returnData[0][data] != null && (typeof returnData[0][getPropertyAccess("recordid")] == "undefined" || returnData[0][getPropertyAccess("recordid")] != "0") && getColumnName(data) != "rowno";
    }).length == 0 && (returnData = []);

    return returnData;
}

/**
 * Generate Parameters badge to selected filters and its count on hover of badge wrapper available next to parameters show/hide button
 * @author Prashik
 * @Date   2019-04-11T10:54:15+0530
 */
function processParamBadge() {
    var paramValuesArray = getParamsValueArray();
    if ($("#FilterValues").length) {
        if (Object.keys(paramValuesArray).length) {
            var filterValuesRowsString = ``;
            var paramsRowsCount = 0;
            //paramValuesArray.forEach((val, ind)=>{
            Object.keys(paramValuesArray).forEach((val, ind) => {
                if ($(`.paramtd2 #${val}:not([type=hidden])`).length && paramValuesArray[val] != "") {
                    paramsRowsCount++;
                    filterValuesRowsString += "<tr><td><b>" + val + "</b> </td><td>: " + paramValuesArray[val] + "</td></tr>";
                }
            });
            var filterValuesHTML = "<span class=\"filtertooltip\" style='text-decoration: none;'>" + paramsRowsCount + "<span class=\"tooltiptext\"><table>" + filterValuesRowsString + "</table></span></span>"
            $("#FilterValues").html(filterValuesHTML);
        } else {
            $("#FilterValues").html("");
        }
        dvToolBarFix();
    }
}

function UpdatePrevMid1FrameUrl(src) {
    const presentFrame = $(window.frameElement);
    if (presentFrame.attr("id") === "middle1")
        parent.prevMid1FrameUrl = src;
}
function LoadTstFrmIview(srcUrl, ivname, navigationType) {
    var parFrm = $j("#axpiframe", parent.document);
    if ((navigationType == undefined || navigationType == "") && parFrm.hasClass("frameSplited"))
        navigationType = "split"

    try {
        srcUrl = loadTstUrlReWrite(srcUrl) || srcUrl;
    } catch (ex) { }

    if (navigationType === "split") {
        ShowDimmer(false);
        callParentNew(`OpenOnPropertyBase(${srcUrl + params})`, 'function');

    }
    else if (navigationType === "popup") {
        LoadPopPage(srcUrl + params)
    }
    else if (navigationType === "default" || navigationType == "") {
        ReloadIframe(srcUrl + params);
    }
    else if (navigationType === "newpage") {
        popupFullPage(srcUrl + params)
    }


}

function ReloadIframe(NavigationURL) {
    var parFrm = $(window.frameElement);
    try {
        parFrm = reloadingFrameSwitch() || parFrm;
    } catch (ex) { }
    parFrm.attr("src", NavigationURL);

}
function popupFullPage(NavigationURL) {
    callParentNew("splitfull()", 'function');
    var frm = $j("#middle1", parent.document);
    frm.attr("src", NavigationURL);
}

//to get all iview records - displays an option only if pagination exists
function getAllRecords() {
    var cutMsg = eval(callParent('lcm[390]'));
    var glType = eval(callParent('gllangType'));
    var isRTL = false;
    if (glType == "ar")
        isRTL = true;
    else
        isRTL = false;
    var ConfirmDeleteCB = $.confirm({
        title: eval(callParent('lcm[164]')),
        onContentReady: function () {
            disableBackDrop('bind');
        },
        backgroundDismiss: 'false',
        rtl: isRTL,
        escapeKey: 'buttonB',
        content: cutMsg,
        buttons: {
            buttonA: {
                text: eval(callParent('lcm[282]')),
                btnClass: 'hotbtn',
                action: function () {
                    ConfirmDeleteCB.close();
                    ShowDimmer(true);
                    setTimeout(function () {
                        getNextDtRecords(0);
                    }, 100);
                }
            },
            buttonB: {
                text: eval(callParent('lcm[192]')),
                btnClass: 'coldbtn',
                action: function () {
                    disableBackDrop('destroy');
                    ShowDimmer(false);
                    return;
                }
            }
        }
    });
}

function requestNextRecords() {
    setTimeout(function () {
        getNextDtRecords(dtPageNo + 1);
    }, 100);
}

//get next page records based on fetch size & page no
function getNextDtRecords(pageNo) {
    showDataTableLoading();
    //console.log(new Date() + ': scroll datatable ajax call started');
    var ivKey = $j("#hdnKey").val();
    var url = "iview.aspx/GetiViewData";
    var paramX = generateParamX();
    var data = '{ivKey: "' + ivKey + '", recsPerPage: "' + defaultRecsPerPage + '", pageno: "' + pageNo + '", paramX: "' + paramX + '"}'
    $j.ajax({
        url,
        type: 'POST',
        cache: false,
        async: false,
        data,
        dataType: 'json',
        contentType: "application/json",
        success: function (data) {
            if (data && data.d != "") {
                if (data.d.status == "success") {
                    var parsedData = JSON.parse(data.d.result);
                    if (pageNo == 0) {//to get all records
                        ivDatas = processRowData(parsedData.data);
                        dtDbTotalRecords = getDtRecordCount(); //update total record count on every page change
                        checkNextDBRowsExist = false;
                        ivirDataTableApi.rows().remove();
                        ivirDataTableApi.rows.add(ivDatas).draw();  //append next records to the datatable & redraw it
                        $("#lnkShowAll, #requestNextRecords").remove();
                    }
                    else {
                        dtPageNo++
                        var currRowSize = ivDatas.length;
                        var processedData = processRowData(parsedData.data);
                        if (processedData.length > 0) { //if records exists
                            ivDatas = ivDatas.concat(processedData);
                            dtDbTotalRecords = getDtRecordCount(); //update total record count on every page change

                            var currScrollSize = nxtScrollSize > ivDatas.length ? ivDatas.length : nxtScrollSize;
                            nxtScrollSize = ivDatas.length;

                            checkIfNextDBRowsExist();

                            pageScrollToEnd = true;
                            ivirDataTableApi.rows.add(ivDatas.slice(currRowSize, ivDatas.length)).draw(false);  //append next records to the datatable & redraw it

                            $(".dataTables_scrollBody").scrollTop(scrollTopPosition);
                            pageScrollToEnd = false;
                        }
                        else {
                            //when checking/getting for the next records if no records came from webservice then update record count
                            checkNextDBRowsExist = false;
                            $("#lnkShowAll").remove();
                            $("#lblCurPage").text('Rows: 1-' + dtDbTotalRecords + ' of ');
                            $("#lblNoOfRecs").text(dtDbTotalRecords);
                        }
                    }
                    clearAdvancedFiltersforNewData();
                    ShowDimmer(false);
                }
                else {
                    showAlertDialog("warning", data.d.result);
                    if (data.d.result == "Session Authentication failed...")
                        parent.window.location.href = "../aspx/sess.aspx";
                    ShowDimmer(false);
                }
            }
            AxWaitCursor(false);
            //console.log(new Date() + ': scroll datatable ajax call completed');
        },
        failure: function (response) {
            AxWaitCursor(false);
        },
        error: function (response) {
            AxWaitCursor(false);
        }
    });
}


/**
 * Get no of valid rows available in iview data response object
 * @author Prashik
 * @Date   2019-04-11T10:56:51+0530
 * @return {int}                 : Count of valid iview rows
 */
function getDtRecordCount() {
    return ivDatas.reduce((sum, val, ind) => {
        if (isPerf || (typeof val["axrowtype"] != "undefined" && (val["axrowtype"] == null || val["axrowtype"] == ""))) {
            return sum + 1;
        } else {
            return sum + 0;
        }
    }, 0);
}

/**
* get no of having infomation like grand total/sub total/sub headings
* @author Abhishek
* @Date   2019-04-23 10:56:51+0530
* @return {int}                 : Count of special rows
*/
function getSpecialRowCount() {
    return ivDatas.length - dtDbTotalRecords;
}

/**
* check if next db records exist or not
* @author Abhishek
* @Date   2019-04-23 10:56:51+0530
*/
function checkIfNextDBRowsExist(onPageLoad) {
    dtDbTotalRecords = getDtRecordCount();
    var axpertPageSize, allRecsCached = false;

    if (onPageLoad) { //first time when page load
        axpertPageSize = JSON.parse($("#hdnIViewData").val()).data.headrow["@pagesize"];
        if (axpertPageSize != undefined) {
            axpertPageSize = parseInt(axpertPageSize);
            if (axpertPageSize == 0) { //if pagesize is defined as <=0 then it will get total records
                defaultRecsPerPage = iviewDataWSRows = dtDbTotalRecords;
                allRecsCached = true;
            }
            else
                defaultRecsPerPage = iviewDataWSRows = axpertPageSize;
        }
        else
            defaultRecsPerPage = iviewDataWSRows;
    }
    /*
        Disable checkNextDBRowsExist if any of the below condition true
            1. no records exists
            2. all records came 
            3. fetch size is defined from Adv settings & pagesize is defined from Axpert --  dtDbTotalRecords < axpertPageSize
            4. fetch size > db records
            5. last record came
    */
    if (dtDbTotalRecords == 0 || allRecsCached || (dtDbTotalRecords % iviewDataWSRows != 0) && (axpertPageSize == undefined ? true : dtDbTotalRecords < axpertPageSize)) {
        checkNextDBRowsExist = false;
        $("#lnkShowAll").hide();
    }
    else {
        defaultRecsPerPage = iviewDataWSRows = dtTotalRecords;
        checkNextDBRowsExist = true;
        $("#lnkShowAll").show();
    }
}
//inmemdb 
function pieChart1(datats) {
    try {
        $('#inMemChartWrapper').highcharts({
            colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5'],
            credits: {
                enabled: false
            },
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                },
                   height:250  
            },
            exporting: { enabled: false },
            legend: {
                layout: 'vertical',
                backgroundColor: '#FFFFFF',
                floating: true,
                align: 'right',
                x: 90,
                y: 0,
                itemStyle: {
                    color: '#FFFFF'
                },
                align: 'right',
                verticalAlign: 'middle'
            },
            title: {
                style: {
                    fontWeight: 'bold',
                    fontSize: '15px'
                },
                text: eval(callParent('lcm[291]'))
            },
            tooltip: {
                shared: true, //headerFormat: '<span style="font-size: 25px;" >{point.key}</span><br/>',
                style: {
                    fontWeight: 'bold',
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    size: 180,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                colorByPoint: true,
                name: "Memory details in MB",
                data: JSON.parse(JSON.stringify(JSON.parse(datats)))
            }]
        });
    }
    catch (e) {
        showAlertDialog('warning', eval(callParent('lcm[361]')));
    }
}

// region get memory represnention for redis
function callCharts() {
    ShowDimmer(true);
    $.ajax({
        type: "POST",
        url: "../aspx/iview.aspx/MemoryDetails",
        contentType: "application/json;charset=utf-8",
        data: '{}',
        dataType: "json",
        success: function (data) {
            ShowDimmer(false);
            pieChart1(data.d);
            isPostback = false;
        },
        error: function (response) {
            showAlertDialog('warning', eval(callParent('lcm[273]')) + eval(callParent('lcm[274]')));
            ShowDimmer(false);
        }
    });
}

function loadAxTstruct(urlNavigationPath) {
    var parFrm = $j("#axpiframe", parent.document);
    var parFrmmiddle = $j("#middle1", parent.document);
    if (parFrm.hasClass("frameSplited")) {
        if (urlNavigationPath.toLowerCase().indexOf("tstruct.aspx?") > -1) {
            var tstRecId = "", tstQureystr = "";
            if (urlNavigationPath != "") {
                urlNavigationPath.split('&').forEach(function (paramType) {
                    if (paramType.indexOf("recordid=") > -1)
                        tstRecId = paramType.split("=")[1];
                    else
                        tstQureystr += paramType + "♠";
                });
            }
            if (tstRecId != "" && tstRecId != "0")
                $j("#axpiframe", parent.document)[0].contentWindow.GetLoadData(tstRecId, tstQureystr);
            else
                $j("#axpiframe", parent.document)[0].contentWindow.GetFormLoadData(tstQureystr);
            AxWaitCursor(false);
            ShowDimmer(false);
        }
        else
            parFrm.attr("src", urlNavigationPath);
    }
    else
        parFrmmiddle.attr("src", urlNavigationPath);
}

/**
 * @description: push recordID for visible records in session for tstruct navigation
 * @author Prashik
 * @date 2019-10-01
 */
function setListViewDictionary(viewNavigationDataTemp, viewRecordKey) {
    ASB.WebService.SetListViewDictionary(viewNavigationDataTemp, parseInt(viewRecordKey, 10), iName, function(e, t){}, function(e){});
}
