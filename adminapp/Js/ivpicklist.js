﻿$j(document).ready(function () {
    // ChangeTheme(window, true);
    srchFld = $j("#srchFld").val();
    GetParams();
    checkSuccessAxpertMsg();
});

function GetParams() {
    if (window.opener != undefined) {

        var i = 0;
        var pValue = "";
        var parentArr = window.opener.parentArr;
        var paramType = window.opener.paramType;
        var ctrl;
        for (i = 0; i < parentArr.length; i++) {
            if (parentArr[i] == srchFld)
                break;

            var ctrl = $j("#" + parentArr[i], window.opener.document);
            if (ctrl.length) {
                var fldType = ctrl.prop("type");
                if (fldType == "select-one") {
                    var result = ctrl.find("option:selected").val();
                    pValue += parentArr[i] + "¿" + result + "###";
                }
                else if (fldType == "select-multiple") {
                    var cnt = 0;
                    var ft = "false";
                    var ctrl1 = window.opener.document.getElementById(parentArr[i]);
                    cnt = ctrl1.options.length;
                    for (j = 0; j < cnt; j++) {
                        if (ft == "false") {
                            if (ctrl1.options[j].selected == true) {
                                ft = "true";
                                pValue += parentArr[i] + "¿" + ctrl1.options[j].value + "###";
                            }
                        }
                    }
                }
                else {
                    var newval = ctrl.val();
                    if ((paramType[i] == "Date/Time") && (dtCulture.toLowerCase() == "en-us") && newval != "")
                        newval = GetDateStr(ctrl.val(), "mm/dd/yyyy", "dd/mm/yyyy");
                    pValue += parentArr[i] + "¿" + newval + "###";
                }
            }
        }

        $j("#paramXml").val(pValue);
        if (pValue != "" || (parentArr.length == 1 && parentArr[0] == srchFld))
            document.getElementById("btnTemp").click();

    }
}

//Function to show the dimmer on the background.
function ShowDimmer(status) {

    DimmerCalled = true;
    var dv = $j("#waitDiv");

    if (dv.length > 0 && dv != undefined) {
        if (status == true) {

            var currentfr = $j("#middle1", parent.document);
            if (currentfr) {
                dv.height(currentfr.height());
                dv.width(currentfr.width());
            }
            dv.show();
            document.onkeypress = function EatKeyPress() { return false; }
        }
        else {
            dv.hide();
            document.onkeypress = function EatKeyPress() { if (DimmerCalled == true) { return true; } }
        }
    }
    else {
        if (window.opener != undefined) {

            dv = $j("#waitDiv", window.opener.document);
            if (dv.length > 0) {
                if (status == true)
                    dv.show();
                else
                    dv.hide();
            }
        }
    }
    DimmerCalled = false;
}

function ChangeTheme(iframewindow, fromDraft) {
    if (iframewindow != undefined) {
        var framename = iframewindow.name;
        framename = framename.toLowerCase();
        if ((framename.substring(0, 7) == "openpop") || (framename.substring(0, 7) == "loadpop") || (framename.substring(0, 7) == "ivewPop") || (framename == "savewindow") || (framename.substring(0, 7) == "mypopup")) {
            framename = "MyPopUp";
        }
        if ((iframewindow.name != "MyPopUp") && (framename != "MyPopUp")) {

            var theme = eval(callParent('currentThemeColor'));
            $j("#themecss").attr('href', "../App_Themes/" + theme + "/Stylesheet.min.css?v=23");
            if (theme == "") {
                if (fromDraft) {
                    theme = $j("#themecss", window.opener.document).attr("href");
                    $j("#themecss").attr("href", theme);
                }
                else {

                    if (window.parent.document)
                        theme = $j("#themecss", window.parent.document).attr("href");
                    else
                        theme = $j("#themecss", window.opener.document).attr("href");
                    $j("#themecss").attr("href", theme);
                }
            }

        }
        else {

            var themeref = "";
            try {
                themeref = $j("#themecss", window.opener.document).attr("href");
            }
            catch (ex) {
                themeref = $j("#themecss", window.parent.document).attr("href");
            }

            if (themeref != "") {
                $j("#themecss").attr("href", themeref);
            }
        }
    }
}
