<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Complaint.aspx.cs" Inherits="aspx_Complaint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Complaint Box</title>
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/Complaint.min.css" rel="stylesheet" />
    <link href="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css" rel="stylesheet" />
    <link href="../ThirdParty/DataTables-1.10.13/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
</head>
    
<body dir="<%=direction %>" class="btextDir-<%=direction%>">
    <div class="wrap-contact100">
			<form id="form1" runat="server" class="contact100-form validate-form">
				<span class="contact100-form-title">
					Send us a message
				</span>

				<div class="wrap-input100">
					<input id="subject" class="input100" type="text" name="email" placeholder="Subject"/>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100">
					<textarea id="content" class="input100" name="message" placeholder="How Can We Help?"></textarea>
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<button type="button" class="contact100-form-btn">
						Send Message
					</button>
				</div>
                <asp:HiddenField ID="CloudAPI" runat="server" />
                 <asp:HiddenField ID="UserName" runat="server" />
			</form>
		</div>
    <script type="text/javascript" src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <script src="../Js/noConflict.min.js?v=1"></script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <script type="text/javascript" src="../Js/Complaint.min.js?v=1"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js"></script>
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js"></script>
    <script src="../Js/alerts.min.js?v=24"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/moment.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/media/js/jquery.dataTables.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/datetime-moment.js"></script>
</body>
</html>
