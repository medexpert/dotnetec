<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Forgotpassword.aspx.cs" Inherits="aspx_Forgotpassword" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="Forgot Password">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title><%=appTitle%></title>

    <% if (EnableOldTheme == "true")
        { %>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" />
    <% }
        else
        { %>

    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <%} %>
    <link href="../Css/login.min.css?v=3" rel="stylesheet" type="text/css" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" type="text/css" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" type="text/css" />
    <script src="../Js/alerts.min.js?v=23" type="text/javascript"></script>
    <%--custom alerts end--%>
    <script type="text/javascript" src="../Js/login.min.js?v=32"></script>
    <script src="../Js/ForgotPassword.min.js?v=4" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />
    <link href="../Css/agileui.min.css?v=8" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" type="text/css" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <script>
        var gllangType = '<%=langType%>';
    </script>
</head>

<body id="main_body" runat="server">
    <video id="bgvid" runat="server" playsinline="" autoplay="" muted="" loop="">
        <source src="" type="video/mp4" id="bgvidsource" runat="server">
    </video>
    <noscript>
        <div style="background-color: #FFFF00">
            <asp:Label ID="lblscript" runat="server" meta:resourcekey="lblscript">JavaScript is turned off in your web browser. Turn it on to take full advantage of this site, then refresh the page.</asp:Label>
        </div>
    </noscript>
    <div id="login-container">


        <div id="container-arrow">
            <div id="login-box">
                <h2 class="form-title" style="font-size: 1.5em; font-weight: normal; color: #111; margin: 10px 0 10px;">
                    <img src="../images/favicon.ico" style="vertical-align: middle;">
                    <asp:Label ID="lblfrgotpwd" runat="server" meta:resourcekey="lblfrgotpwd">Forgot password</asp:Label>
                </h2>
                <div>
                    <div class="toplbl" id="projlbl" runat="server">
                    </div>
                    <div class="control-group fields" id="selectProj1" runat="server">
                        <asp:Label ID="lblproj" runat="server" meta:resourcekey="lblproj" class="control-label visible-ie8 visible-ie9">
                            Select Project</asp:Label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-desktop"></i>
                                <input type='text' value='' id='axSelectProj' name="axSelectProj" runat="server" tabindex='2' placeholder='Select Project' class='m-wrap placeholder-no-fix new-search-input search-input' required />
                                <i class="icon-cross" runat="server" title="Clear" onclick="$('#axSelectProj').val('').focus();"></i>
                                <i class="icon-chevron-down autoClickddl" runat="server" title="Select Project" onclick="$('#axSelectProj').autocomplete('search','').focus();"></i>
                            </div>
                        </div>
                    </div>
                    <div class=" control-group fields">
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-user"></i>
                                <input type="text" id="txtName" name="uname" runat="server" value="" tabindex="2"
                                    size="15" autocomplete="off" class="loginContr" placeholder="Username" required />
                            </div>
                        </div>
                    </div>
                    <div class=" control-group fields">
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-envelope"></i>
                                <input type="text" id="txtMl" name="email" runat="server" value="" tabindex="2"
                                    class="loginContr" placeholder="Email" required />
                            </div>
                        </div>
                    </div>
                    <div class="divbutton">
                        <input type="submit" title="Send password" id="btnSubmit" value="Send password" class="hotbtn btn" tabindex="3"
                            onclick="javascript: ForgotPwd();" />
                    </div>
                    <div style="padding-top: 15px; width: 100%;">
                        <a onclick="OpenSigninPage();" href="javascript:void(0)">
                            <asp:Label ID="lbllogin" runat="server" meta:resourcekey="lbllogin">Go back to login</asp:Label></a>
                    </div>
                </div>

                <label runat="server" id="lblMessage">
                </label>
                <asp:Label runat="server" Text="" ID="Label2" CssClass="lblMsg"></asp:Label>
            </div>
        </div>
    </div>
    <form id="Form1" runat="server">
        <input type="hidden" id="hdnLangs" runat="server" />
        <div style="display: none">
            <asp:Button ID="btnReset" class="hotbtn btn" runat="server" OnClick="btnReset_Click" />
        </div>
        <asp:HiddenField ID="hdnUsrName" runat="server" />
        <asp:HiddenField ID="hdnMl" runat="server" />
        <asp:HiddenField ID="hdnAxProjs" runat="server" />
        <asp:HiddenField ID="hdnProj" runat="server" />
        <label runat="server" id="Label1">
        </label>
    </form>    
</body>
</html>
