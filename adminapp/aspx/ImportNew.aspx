<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportNew.aspx.cs" Inherits="aspx_ImportNew" EnableEventValidation="false" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Import">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Import Data</title>
    <link href="../Css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Css/wizardComp.min.css?v=4">
    <link href="../Css/import.min.css?v=19" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../App_Themes/Gray/Stylesheet.min.css?v=23" rel="stylesheet" />
    <link id="themecss" type="text/css" href="" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
</head>
<body dir='<%=direction%>' class="btextDir-<%=direction%>">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="600">
            <Services>
                <asp:ServiceReference Path="../WebService.asmx" />
            </Services>
        </asp:ScriptManager>

        <!-- Widget Work flow - begins -->
        <div id="wizardWrappper">
            <!-- Widget Header menus - begins -->
            <div id="wizardHeader">
            </div>
            <!-- Widget Header menus - end -->

            <div id="wizardBodyContent">
                <!-- Widget Data Search - begins -->
                <div class="wizardContainer wizardValidation animated fadeIn" id="imWizardDataSearch">
                    <section class="form-group col-md-12">
                        <asp:Label runat="server" AssociatedControlID="ddlImpTbl" class="selectalign">
                            <asp:Label ID="lblImptbl" runat="server" meta:resourcekey="lblImptbl" Text="Select Form"></asp:Label><span class="allowempty">*</span>
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl1" data-content="Select a TStruct from which the data needs to be imported." data-placement="right" style="cursor: pointer;"></i>
                        <asp:DropDownList runat="server" ID="ddlImpTbl" CssClass="form-control forValidation selectpicker" data-live-search="true" data-size="6" AutoPostBack="true" OnSelectedIndexChanged="ddlImpTbl_SelectedIndexChanged">
                        </asp:DropDownList>
                    </section>
                    <section class="form-group col-md-12">
                        <label class="selectalignn" for="txtExFields" onclick="$('.ms-selectable ul.ms-list').focus();">
                            <asp:Label ID="lbltxtim" runat="server" meta:resourcekey="lbltxtim" Text="Fields"></asp:Label><span class="allowempty">*</span>
                        </label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl2" data-content="Select the field(s) to be included in the import template." data-placement="right" style="cursor: pointer;"></i>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 45%">
                                    <select id="mSelectLeft" name="from[]" class="multiselect form-control" size="8" multiple="multiple" data-right="#mSelectRight" data-right-all="#right_All_1" data-right-selected="#right_Selected_1" data-left-all="#left_All_1" data-left-selected="#left_Selected_1">
                                        <asp:Repeater ID="rptSelectFields" runat="server">
                                            <ItemTemplate>
                                                <option value='<%# Container.DataItem.ToString().Substring(0,Container.DataItem.ToString().IndexOf("&&")) %>'><%# Container.DataItem.ToString().Substring(Container.DataItem.ToString().IndexOf("&&")+2)%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </td>
                                <td style="width: 10%; text-align: center;">
                                    <%--button icons are updating based on the lang selection <, >, <<, >>--%>
                                    <div><a href="javascript:void(0)" id="right_All_1" title="Select All" class="fa colorButton select-button"></a></div>
                                    <div><a href="javascript:void(0)" id="right_Selected_1" title="Select" class="fa colorButton select-button"></a></div>
                                    <div><a href="javascript:void(0)" id="left_Selected_1" title="Unselect" class="fa colorButton select-button"></a></div>
                                    <div><a href="javascript:void(0)" id="left_All_1" title="Unselect All" class="fa colorButton select-button"></a></div>
                                </td>
                                <td style="width: 45%">
                                    <select name="to[]" id="mSelectRight" class="multiselect form-control" size="8" multiple="multiple"></select>
                                </td>
                            </tr>
                        </table>
                    </section>

                    <asp:UpdatePanel runat="server" ID="plnUpdate1">
                        <ContentTemplate>
                            <asp:Button Text="text" runat="server" ID="btnCreateTemplate" OnClick="btnCreateTemplate_Click" Style="display: none" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="hdnMandatoryColCount" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnMandatoryFields" runat="server" Value="" />
                    <asp:HiddenField ID="hdnSelectedColumnCount" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnColValues" runat="server" Value="" />
                    <asp:HiddenField ID="hdnColNames" runat="server" Value="" />
                </div>
                <!-- Widget Data Search - end -->

                <!-- Widget Data Filter - begins -->
                <div class="wizardContainer animated fadeIn" id="imWizardUpload">
                    <span class="customMessage fileUploadErrorMessage"></span>
                    <section class="col-md-12  upload-section">
                        <asp:HyperLink ID="lnkExpTemp" runat="server" class="hotbtn btn" title="Download Data Template">
                            <span runat="server" class=" glyphicon glyphicon-download-alt icon-basic-download" id="ico"></span>
                            <asp:Label ID="lbldwnldtmp" runat="server" meta:resourcekey="lbldwnldtmp">Download Data Template</asp:Label>
                        </asp:HyperLink>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl3" data-content="Template should be available in .CSV format" data-placement="right" style="cursor: pointer;"></i>
                        <br />
                        <asp:Label ID="lblTemplateNtAvalble" runat="server" Text="" class="lblleft" ForeColor="#cf4444" Visible="false"></asp:Label>
                    </section>
                    <section class="col-md-12">
                        <asp:UpdatePanel ID="updatePnl2" runat="server" UpdateMode="conditional">
                            <ContentTemplate>
                                <div class="col-sm-6">
                                    <span class="file-upload">
                                        <span class="file-select" tabindex="0" id="spnFileSelect">
                                            <span class="file-select-button" id="fileName">
                                                <asp:Label ID="lblfilename" runat="server" meta:resourcekey="lblfilename">Choose File</asp:Label></span>
                                            <span class="file-select-name" id="noFile">
                                                <asp:Label ID="lblnofile" runat="server" meta:resourcekey="lblnofile">No file chosen...</asp:Label></span>
                                            <asp:FileUpload runat="server" ID="fileToUpload" Style="margin-top: -1px;" TabIndex="-1" />
                                        </span>
                                    </span>
                                    <asp:Label ID="fileuploadsts" Text="" runat="server" ForeColor="#DB2222"></asp:Label>
                                    <input type="button" name="name" value="Upload" class="hotbtn btn" id="btnFileUpload" style="display: none" />
                                    <asp:Button runat="server" ID="UploadButton" class="hotbtn btn" Text="Upload" OnClick="UploadButton_Click" Style="display: none" />
                                </div>
                                <div class="col-sm-6 customclscol">
                                    <asp:Label runat="server" AssociatedControlID="ChkColNameInfile" Style="float: left;">
                                        <asp:CheckBox runat="server" ID="ChkColNameInfile" onchange="ColNameInfileChanged()" Checked="true" meta:resourcekey="lblFileHeaders" />
                                    </asp:Label>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </section>
                    <section class="col-xs-12">
                        <div class="col-xs-12 customclscol">
                            <div class="form-group upload-progress">
                                <div id="divProgress" class="progress" style="width: 100%; color: white; display: none">
                                    <div id="divProgressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <div class="form-group col-md-12">
                        <div class="col-sm-6">
                            <asp:Label runat="server" ID="lblseparator" meta:resourcekey="lblseparator" AssociatedControlID="ddlSeparator">
                            </asp:Label>
                            <i tabindex="0" data-trigger="focus" class="icon-arrows-question helptxt" data-toggle="popover" id="icocl6" data-content="Select character used for separating columns in the data file." data-placement="right" style="cursor: pointer; margin-left: 5px"></i>
                            <asp:DropDownList ID="ddlSeparator" CssClass="form-control" runat="server">
                                <asp:ListItem Value="," Selected="True">Comma [ , ]</asp:ListItem>
                                <asp:ListItem Value=";">Semicolon [ ; ]</asp:ListItem>
                                <asp:ListItem Value="|">Pipe [ | ]</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-sm-6 customclscol">
                            <asp:Label runat="server" ID="lblimgroupby" meta:resourcekey="lblimgroupby" AssociatedControlID="ddlGroupBy">
                            </asp:Label>
                            <i tabindex="0" data-trigger="focus" class="icon-arrows-question helptxt" data-toggle="popover" id="icocl7" data-content="Form contains data grid, please select column with unique values." data-placement="right" style="cursor: pointer; margin-left: 5px; float: left;" data-original-title="" title=""></i>
                            <asp:DropDownList ID="ddlGroupBy" CssClass="form-control" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <!-- Widget Data Filter - end -->

                <!-- Widget Data Filter - begins -->
                <div class="wizardContainer animated fadeIn" id="imWizardEdit">
                    <div class="col-sm-12">
                        <table>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkForIgnoreErr" runat="server" checked="checked" /><asp:Label ID="lblForIgnoreErr" AssociatedControlID="chkForIgnoreErr" meta:resourcekey="lblForIgnoreErr" runat="server"> Ignore errors</asp:Label>
                                    <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl4" data-content="Check this to ignore errors in the rows during file upload." data-placement="right" style="cursor: pointer;"></i>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkForAllowUpdate" runat="server" onclick="javascript: ChkAllowUpdate();" />
                                    <asp:Label ID="lblAllowUpdate" AssociatedControlID="chkForAllowUpdate" runat="server">
                                        <asp:Label ID="lblAllowUpdte" runat="server" meta:resourcekey="lblAllowUpdte">Allow Update</asp:Label></asp:Label>
                                    <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl5" data-content="If allow update is checked,the rows will be updated using the primary key." data-placement="right" style="cursor: pointer;"></i>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblprimarycolmn" runat="server" meta:resourcekey="lblprimarycolmn">Primary Column </asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlPrimaryKey">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hdnPrimaryKey" runat="server" Value="" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <section class="col-sm-12">
                        <%--<asp:Button ID="RefreshGridButton" runat="server" OnClick="GridRefresh" CssClass="hide" />--%>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div style="float: right">
                                    <asp:Label ID="lblrecords" runat="server" meta:resourcekey="lblrecords">Top 5 records</asp:Label>
                                </div>
                                <div style="width: 100%; max-height: 200px; min-height:190px; overflow: auto">
                                    <asp:GridView CellSpacing="-1" ID="gridImpData" runat="server" Visible="true" Style="margin: auto;" OnRowDataBound="gridImpData_RowDataBound" CssClass="" Width="100%"></asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </section>
                    <section class="col-sm-12" style="text-align: left; float: left;">
                        <asp:HiddenField ID="hdnIgnoredColumns" runat="server" Value="" />
                        <asp:HiddenField ID="colheader" runat="server" Value="" />
                    </section>
                </div>
                <!-- Widget Data Filter - end -->

                <!-- Widget Data Export - begins -->
                <div class="wizardContainer animated fadeIn" id="imWizardSummary" style="text-align: center">
                    <asp:Label runat="server" ID="lblPleaseWait" Visible="false"></asp:Label>
                    <asp:UpdatePanel ID="updatePln3" runat="server">
                        <ContentTemplate>
                            <textarea name="text" id="summaryText" runat="server" rows="11" class="resize:none hidden" cols="70"></textarea>
                            <div class="form-group col-md-12">
                                <h3 style="text-align: left">
                                    <asp:Label ID="lblimportsum" runat="server" meta:resourcekey="lblimportsum">Import Summary</asp:Label></h3>
                                <asp:Label runat="server" ID="lblTest"></asp:Label>
                                <br />
                                <a id="SummaryDwnld" runat="server" class="hotbtn btn" tabindex="0" visible="false" title="Download Summary"><span class=" glyphicon glyphicon-download-alt icon-basic-download"></span>
                                    <asp:Label ID="lbldwnldsum" runat="server" meta:resourcekey="lbldwnldsum">Download Summary</asp:Label></a>
                            </div>
                            <asp:HiddenField ID="hdnIgnoredColCount" runat="server" Value="0" />
                            <asp:HiddenField ID="uploadFileName" runat="server" Value="" />
                            <asp:HiddenField ID="uploadIviewName" runat="server" Value="" />
                            <asp:HiddenField ID="upFileName" runat="server" Value="" />
                            <asp:HiddenField ID="IsFileUploaded" runat="server" Value="" />
                            <asp:HiddenField ID="fileUploadComplete" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnTemplateName" runat="server" Value="" />
                            <asp:HiddenField ID="hdnUploadFileWarnings" runat="server" Value="" />
                            <asp:HiddenField ID="hdnGroupBy" runat="server" Value="NA" />
                            <asp:HiddenField ID="hdnGroupByColName" runat="server" Value="" />
                            <asp:HiddenField ID="hdnGroupByColVal" runat="server" Value="" />
                            <asp:Button ID="btnImport" runat="server" Text="Import" OnClick="btnImport_Click" CssClass="cloudButton -hot btn-info-full hide next-step" />
                            <br />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnImport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <!-- Widget Data Export - end -->


            </div>
        </div>
        <div id="wizardFooter" class="wizard-footer">
            <div class="pull-left">
                <button onclick="impWizardObj.checkClick(this,'prev')" type="button" title="Previous" id="wizardPrevbtn" style="display: none;" class="btn hotbtn prev-step">&lt; Prev</button>
            </div>
            <div class="pull-right">
                <button id="wizardCancelbtn" onclick="closeWindow()" type="button" title="Cancel" class="coldbtn btn btn-info-full ">Cancel</button>
                <button onclick="impWizardObj.checkClick(this,'next')" data-validator="" title="Next" id="wizardNextbtn" type="button" style="display: none;" class="hotbtn btn ">Next &gt;</button>
                <button type="button" style="display: none;" id="wizardCompbtn" class="hotbtn btn  " title="Done">Done</button>
            </div>
        </div>
        <!-- Widget work flow - end -->

        <div id='waitDiv' style='display: none;'>
            <div id='backgroundDiv' style='background: url(../Axpimages/loadingBars.gif) center center no-repeat rgba(255, 255, 255, 0.843137); background-size: 135px;'>
            </div>
        </div>

        <script src="../Js/Jquery-2.2.2.min.js" type="text/javascript"></script>
        <script src="../AssetsNew/js/bootstrap.min.js"></script>
        <script src="../Js/wizard.min.js?v=10"></script>
        <script src="../Js/common.min.js?v=62"></script>
        <script src="../Js/wizardComp.min.js?v=9" type="text/javascript"></script>

        
        <script src="../Js/alerts.min.js?v=24"></script>
        <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2"></script>
        <link href="../newPopups/Remodal/remodal-default-theme.min.css?v=2" rel="stylesheet" />
        <link href="../newPopups/Remodal/remodal.min.css?v=2" rel="stylesheet" />
        <script src="../newPopups/Remodal/remodal.min.js"></script>
        <script src="../newPopups/axpertPopup.min.js?v=28"></script>
        <script src="../Js/import.min.js?v=25"></script>
        <script src="../Js/multiselect.min.js" type="text/javascript"></script>
        <script src="../Js/bootstrap-select.min.js" type="text/javascript"></script>

        <script>
            var proj = '<%=proj%>';
            var sid = '<%=sid%>';

            var $j = jQuery.noConflict();
            var $ = jQuery.noConflict();
              var AxwizardType = '<%=Session["AxWizardType"]%>';
        </script>

    </form>
</body>

</html>
