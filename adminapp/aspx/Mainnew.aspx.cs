﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Xml;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Security.Principal;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using Axpert_Object;
using System.Web.UI;
using System.Linq;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Configuration;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Globalization;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

public partial class aspx_Mainnew : System.Web.UI.Page
{

    Util.Util util;
    public string appName;
    public string appTitle;
    public string themeColor = string.Empty;
    int alertsTimeout = 3000;
    int errorTimeout = 0;
    bool errorEnable = false;
    public string scriptsPath = string.Empty;
    public string RestDllPath = string.Empty;
    LogFile.Log logobj = new LogFile.Log();
    string signinProj = string.Empty;
    public string thmProj = string.Empty;
    public string thmUser = string.Empty;
    public string thmSid = string.Empty;
    public bool manage = false;
    public bool userAccess = false;
    public bool workflow = false;
    public string signOutPath = string.Empty;
    public string errorlog = "false";
    string strGlobalVar = string.Empty;
    string loginPath = "";
    public string Navigationpage = "";
    public string regthmdata = string.Empty;
    string _xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
    public string user = string.Empty;
    public string language = string.Empty;
    public string appsessionKey = string.Empty;
    public string hdnpsid = string.Empty;
    public string PeriodicRCKey = string.Empty;
    public string direction = "ltr";
    public string langType = "en";
    public string glCulture = string.Empty;
    public string strLang = string.Empty;
    public string MenuHTML = "<ul class=\"mainParent\">";

    protected bool axChangePwd = false;
    protected bool axChangePwdReDir = false;

    string proj = string.Empty;
    public string AxRole = string.Empty;
    string sid = string.Empty;
    string axApps = string.Empty;
    string axProps = string.Empty;
    string axRegId = string.Empty;
    string axSchema = string.Empty;
    public string printInterval = string.Empty;
    public string draftScript = string.Empty;
    public string traceStatus = "F";
    public bool traceValue = false;
    string enableDraftSave = "false";
    public string[] gloApp_Split = null;
    string globalUsr_vars = null;
    string global_vars = null;
    ArrayList globalVariables = new ArrayList();
    ArrayList userVariables = new ArrayList();
    string forcePwdChange = string.Empty;
    Boolean useCulture = false;
    public bool build = false;
    public bool export = false;
    public bool import = false;
    public bool history = false;
    bool isResultXml = false;
    bool isLockOnRead = false;
    public int pwdExpDays = 0;
    bool isCloud = false;
    public bool isCloudApp = false;
    public bool IsLogoAvail = false;
    public string fromSSO = "false";
    public string copyRightTxt = "";
    public string HelpIview = string.Empty;
    public string IviewNews = string.Empty;
    public string IviewLive = string.Empty;
    public string menuStr = string.Empty;
    public string menuStrLo = string.Empty;
    public bool isLoggedIn = false;
    public string isNewMenu = "true";
    public string menuStyle = "modern";
    public string menuXmlData = string.Empty;
    public string utilNode = string.Empty;
    public string Utl = string.Empty;
    public string redisUtl = string.Empty;
    string loginTrace = "false";
    public bool IsCustomLogoAvail = false;
    public bool isRapidLoad = false;
    public string axUtlGlobalVars = String.Empty;
    public string axUtlUserVars = String.Empty;
    public string axUtlApps = String.Empty;
    public string axMainPageReload = String.Empty;
    public string axMenuReSize = String.Empty;
    public string axProjectTile = String.Empty;
    ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
    ASB.WebService asbWebService = new ASB.WebService();
    // static RedisServer objRS = RedisServer.Instance;
    public StringBuilder strParamsRF = new StringBuilder();
    public string PageBuilderAccess = string.Empty;
    StringBuilder strMenuHtml1 = new StringBuilder();
    static string paramTransid = string.Empty;
    string disableSplit = "false";
    FDW fdwObj = FDW.Instance;
    string rnd_key = string.Empty;
    string pwd = string.Empty;
    string cacKey = string.Empty;
    public string templateText = string.Empty;
    Dictionary<string, string> UserOptions = new Dictionary<string, string>();
    public string axUserOptions = string.Empty;
    public bool compressedMode = false;

    protected override void InitializeCulture()
    {
        if (Request.Form["language"] != null || Session["language"] != null)
        {
            string langProj = Request.Form["language"] == null ? Session["language"].ToString() : Request.Form["language"];
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(langProj.ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
                Application["LangSess"] = langProj;
            }
            FileInfo filcustom = new FileInfo(HttpContext.Current.Server.MapPath("~/Js/lang/content-" + langType + ".js"));
            if (!(filcustom.Exists))
            {
                langType = "en";
                direction = "ltr-en";
            }
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        util = new Util.Util();
        util.IsValidSession();
        if (!IsPostBack)
        {
            if ((Request.UserAgent.IndexOf("AppleWebKit") > 0) || (Request.UserAgent.IndexOf("Unknown") > 0) || (Request.UserAgent.IndexOf("Chrome") > 0))
            {
                Request.Browser.Adapters.Clear();
            }
            Page.ClientTarget = "uplevel";
        }
        if (util.fBrowserIsMobile())
        {
            Session["MobileView"] = true;

        }
        else
            Session["MobileView"] = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if ((Util.Util.CheckCrossScriptingInString(Request.Form["hdnAxProjs"]) || Util.Util.CheckCrossScriptingInString(Request.Form["rn"]) || Util.Util.CheckCrossScriptingInString(Request.Form["hash"]) || Util.Util.CheckCrossScriptingInString(Request.Form["rndh"]) || Util.Util.CheckCrossScriptingInString(Request.Form["language"]) || Util.Util.CheckCrossScriptingInString(Request.Form["key"])))
            {
                Response.Redirect(Constants.LOGINERR);
            }
        }
        catch (Exception ex)
        {
            Response.Redirect(Constants.LOGINERR);
        }
        if (Request.Form["hdnAxProjs"] != null && !string.IsNullOrEmpty(Request.Form["hdnAxProjs"].ToString()))
            HttpContext.Current.Session["Project"] = Request.Form["hdnAxProjs"].ToString();
        if (Request.Form["hdnCloudDb"] != null && !string.IsNullOrEmpty(Request.Form["hdnCloudDb"].ToString()))
            HttpContext.Current.Session["AxCloudDB"] = Request.Form["hdnCloudDb"].ToString();
        CheckCloudRequest();
        GetLoginDtlsFromCache();
        LoadAppConfiguration();

        if (HttpContext.Current.Session["Project"] == null || Convert.ToString(HttpContext.Current.Session["Project"]) == string.Empty)
        {
            SessExpires();
            return;
        }
        else
        {
            GetSessionVars();
            SetCloudSignoutPath();

            axProps = Application["axProps"].ToString();
            axChangePwd = Convert.ToBoolean(Application["axChangePwd"]);
            axChangePwdReDir = Convert.ToBoolean(Application["axChangePwdReDir"]);

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            InitSessExpiryAlert();

            if (!IsPostBack)
            {
                if (Request.UserAgent.IndexOf("AppleWebKit") > 0 | Request.UserAgent.IndexOf("Unknown") > 0 | Request.UserAgent.IndexOf("Chrome") > 0)
                {
                    Request.Browser.Adapters.Clear();
                }

                logobj.CreateLog("Loading main.aspx", Session.SessionID.ToString(), "login", "new");
                Session["backForwBtnPressed"] = false;

                if (Session["validated"] == null || Session["validated"].ToString() == "")
                {
                    logobj.CreateLog("Calling ValidatePage", Session.SessionID.ToString(), "login", "");
                    if (!isCloud)
                    {
                        if (CheckIsUserLoggedIn())
                            return;
                    }
                }
                else
                {
                    SetAccessFromSession();
                }

                GetGlobalVariables();
                createTopLinks();
                SetPrintInterval();
            }
            else
            {
                if (Session["project"] != null)
                {
                    traceValue = Convert.ToBoolean(Session["AxTrace"]);
                    traceStatus = (string)Session["traceStatus"];
                    SetGlobalVariables();
                    SetTheme();
                }
            }
            AddCustomLinks();
            GetMenuData();
            if (Session["project"] != null)
            {
                IsLogoAvail = util.IsLogoExists();
                SetLandingPage();
                GetConfigs();

                if (Session["projTitle"] != null)
                    appName = Session["projTitle"].ToString();
                if (Session["AxAppTitle"] != null && Session["AxAppTitle"].ToString() != "")
                    appName = Session["AxAppTitle"].ToString();
                appTitle = appName;
                var axApp = UserOptions.Where(x => x.Key == "axAppName").ToList();
                if (axApp.Count > 0)
                    UserOptions["axAppName"] = "\"appName\":\"" + appName + "\"♠\"onclick\":\"loadHome();\"";
                else
                    UserOptions.Add("axAppName", "\"appName\":\"" + appName + "\"♠\"onclick\":\"loadHome();\"");

                string dynamicScript = "isLockOnRead = " + isLockOnRead.ToString().ToLower() + ";";
                if (pwdExpDays > 0)
                    dynamicScript += "$j('#dvMessage').show();";

                //If the global vars tstruct is there and the AxShowHideGloVars is false, then show the tstruct on page load

                string isGloVarTrue = string.Empty;
                if (Session["AxGloHideShow"] != null)
                    isGloVarTrue = Session["AxGloHideShow"].ToString();
                string url = "ParamsTstruct.aspx?transid=" + paramTransid + "";


                if (!IsPostBack)
                {
                    if (isGloVarTrue != string.Empty && isGloVarTrue != "")
                    {
                        if (isGloVarTrue == "F")
                        {
                            string AxGloRecId = string.Empty;
                            if (Session["AxGloRecId"] != null)
                                AxGloRecId = Session["AxGloRecId"].ToString();
                            if (AxGloRecId != "")
                            {
                                url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "&recordid=" + AxGloRecId + "";
                            }
                            else
                            {
                                url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "";
                            }
                            Navigationpage = url;
                            hdHomeUrl.Value = Navigationpage;
                        }
                    }
                    else
                    {
                        editAxGlo2.Visible = false;
                        UserOptions.Add("GlobalParams", "\"display\":\"block\"♠\"title\":\"Global Parameters\"");
                    }
                }

                DoUtlEncode();

                try
                {
                    //hook for AJSSO
                    Custom cust = Custom.Instance;
                    cust.AxCustomPageLoad(Page, this.GetType());
                }
                catch (Exception ex)
                {
                }

                if (Session["AxDisableSplit"] != null)
                {
                    disableSplit = Session["AxDisableSplit"].ToString();
                }
                Page.ClientScript.RegisterStartupScript(GetType(), "set global var to unlock tstruct records", "<script>" + dynamicScript + "var navigatePage='" + Navigationpage + "';" + "var isCloudApp = '" + isCloudApp.ToString() + "'; " + "var userResp = '" + Session["AxResponsibilities"] + "';" + "var sesslanguage='" + language + "';" + "var mainRestDllPath='" + RestDllPath + "';" + "var mainProject='" + proj + "';" + "var mainSessionId='" + sid + "';" + "var mainUserName='" + Session["user"].ToString() + "';" + "var AxHelpIview='" + HelpIview + "';" + " var axUtlGlobalVars='" + axUtlGlobalVars + "'; var axUtlUserVars='" + axUtlUserVars + "'; var axUtlApps='" + axUtlApps + "';var alertsTimeout='" + alertsTimeout.ToString() + "';var errorTimeout='" + errorTimeout.ToString() + "';var errorEnable='" + errorEnable.ToString().ToLower() + "';var appDiableSplit='" + disableSplit + "'" + "</script>");
            }
        }

        EncryptConnDtls();
        SetPageLoadVars();
        CheckConfigLock();
        if (Session["AxCloudDB"] != null)
        {
            complaintLI.Visible = true;
        }
        else
        {
            complaintLI.Visible = false;
        }
        GetUserOptions();
    }

    private void GetUserOptions()
    {
        if (Session["axUserOptions"] == null)
        {
            UserOptions.Add("About", "\"onclick\":\"showAbout()\"♠\"title\":\"About\"");
            UserOptions.Add("InMemoryDB", "\"onclick\":\"DoUtilitiesEvent('InMemoryDB')\"♠\"title\":\"In-Memory DB\"");
            UserOptions.Add("AppConfig", "\"onclick\":\"LoadIframe('tstruct.aspx?transid=axstc')\"♠\"title\":\"Advanced Settings\"");
            if (Session["MobileView"] != null && Session["MobileView"].ToString() == "True")
                UserOptions.Add("qrCodeBtnWrapper", "\"display\":\"none\"♠\"onclick\":\"createMobileQRcode()\"♠\"title\":\"Mobile QR\"");
            else
                UserOptions.Add("qrCodeBtnWrapper", "\"display\":\"block\"♠\"onclick\":\"createMobileQRcode()\"♠\"title\":\"Mobile QR\"");

            if (Session["MobileView"] != null && Session["MobileView"].ToString() == "True")
                UserOptions.Add("splitIcon", "\"display\":\"none\"");
            else if (Session["MobileView"] != null && Session["MobileView"].ToString() == "False")
            {
                if (Session["AxDisableSplit"] != null)
                {
                    disableSplit = Session["AxDisableSplit"].ToString();
                    if (disableSplit == "true")
                        UserOptions.Add("splitIcon", "\"display\":\"none\"");
                }
            }
            else
                UserOptions.Add("splitIcon", "\"display\":\"block\"");

            UserOptions.Add("Dashboard", "\"onclick\":\"ToogleLeftMenu('dashboardPanel', 'true','Dashboard','')\"♠\"title\":\"Dashboard\"");
            UserOptions.Add("pageBuilderBtn", "\"onclick\":\"LoadIframe('PageDesigner.aspx')\"♠\"title\":\"Page Builder\"");
            UserOptions.Add("traceValue", "\"traceVal\":\"" + traceValue + "\"♠\"onclick\":\"ToggleShowLog(true)\"");
            try
            {
                StringBuilder usOprions = new StringBuilder();
                foreach (var option in UserOptions)
                {
                    usOprions.Append("\"" + option.Key + "\":{" + option.Value.Replace("♠", ",") + "},");
                }
                axUserOptions = "{" + usOprions.ToString().Substring(0, usOprions.Length - 1) + "}";
                Session["axUserOptions"] = axUserOptions;
            }
            catch (Exception ex)
            { }
        }
        else
            axUserOptions = Session["axUserOptions"].ToString();
    }

    private static string pushHeader(String fileName)
    {
        string results = string.Empty;

        try
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(fileName);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            results = sr.ReadToEnd();
            sr.Close();
        }
        catch (Exception ex)
        {
            //results = ex.Message;
            LogFile.Log logObj = new LogFile.Log();
            logObj.CreateLog("MainPage Templete - \n\tURL- " + fileName + "\n\tError - " + ex.Message, HttpContext.Current.Session.SessionID, "MainPage Templete", "");
        }
        return results;
    }

    private void AddCustomLinks()
    {
        Custom cusObj = Custom.Instance;
        string projName = HttpContext.Current.Session["Project"].ToString();
        for (int i = 0; i < cusObj.jsMainFiles.Count; i++)
        {
            string fileName = cusObj.jsMainFiles[i].ToString();
            HtmlGenericControl js = new HtmlGenericControl("script");
            js.Attributes["type"] = "text/javascript";
            string path = "../" + projName + "/" + fileName;
            js.Attributes["src"] = path;
            ScriptManager1.Controls.Add(js);
        }

        for (int j = 0; j < cusObj.cssMainFiles.Count; j++)
        {
            string fileName = cusObj.cssMainFiles[j].ToString();
            HtmlGenericControl css = new HtmlGenericControl("link");
            css.Attributes["type"] = "text/css";
            css.Attributes["rel"] = "stylesheet";
            string path = "../" + projName + "/" + fileName;
            css.Attributes["href"] = path;
            ScriptManager1.Controls.Add(css);
        }
    }

    private void CheckConfigLock()
    {
        if (!IsPostBack)
        {
            string schemaName = string.Empty;
            if (HttpContext.Current.Session["dbuser"] != null)
                schemaName = HttpContext.Current.Session["dbuser"].ToString();

            string lockedBy = String.Empty;

            FDR fdrObj = (FDR)HttpContext.Current.Session["FDR"];
            lockedBy = fdrObj.StringFromRedis(Constants.CONFIG_LOCK_KEY);
            string[] lockDetails = lockedBy.Split('♦');
            bool clearLock = false;

            if (lockDetails.Length == 4)
            {
                DateTime lockedOn = Convert.ToDateTime(lockDetails[1]);
                bool lockExpired = lockedOn.AddMinutes(10) < DateTime.Now;
                if (lockDetails[0] != user)
                {
                    if (lockExpired)
                        clearLock = true; //Different user + lock expired -> Clear Lock
                }
                else
                {
                    if (HttpContext.Current.Session["lictype"] != null && HttpContext.Current.Session["lictype"].ToString() == "unlimited")
                    {
                        if (lockExpired)
                            clearLock = true; //Same user + un-limited user licence + lock expired -> Clear Lock
                    }
                    else
                        clearLock = true; //Same user + limited user licence -> Clear Lock
                }
            }

            if (clearLock)
                fdwObj.ClearRedisServerDataByKey(Constants.CONFIG_LOCK_KEY, Constants.CONFIG_LOCK_KEY, false, schemaName);
        }
    }

    private void SetPageLoadVars()
    {
        if (ConfigurationManager.AppSettings["NodeAPI"] != null)
            utilNode = ConfigurationManager.AppSettings["NodeAPI"].ToString();
        if (Convert.ToString(Session["Build"]) == "F")
        {
            //configApp.Style.Add("display", "none");
            configApp1.Style.Add("display", "none");
            UserOptions.Add("globalsettings", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('ConfigApp')\"♠\"title\":\"Global Settings\"");
        }
        else
            UserOptions.Add("globalsettings", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('ConfigApp')\"♠\"title\":\"Global Settings\"");

        if (Convert.ToString(Session["workflowEnabled"]) == "False" || Convert.ToString(Session["workflowEnabled"]) == "false")
        {
            li_WorkFlow1.Style.Add("display", "none");
            UserOptions.Add("WorkFlow", "\"display\":\"none\"♠\"onclick\":\"LoadIframe('workflownew.aspx')\"♠\"title\":\"WorkFlow\"");
        }
        else
            UserOptions.Add("WorkFlow", "\"display\":\"block\"♠\"onclick\":\"LoadIframe('workflownew.aspx')\"♠\"title\":\"WorkFlow\"");

        if (ConfigurationManager.AppSettings["AxpertWebLogs"] != null && ConfigurationManager.AppSettings["AxpertWebLogs"].ToString() == "true" && Session["user"].ToString() == "admin")
        {
            axpertLogs1.Style.Add("display", "block");
        }

        FileInfo filcustom = new FileInfo(HttpContext.Current.Server.MapPath("~/images/Custom/logo.png"));
        if (filcustom.Exists)
        {

            IsCustomLogoAvail = true;
            UserOptions.Add("AxAppLogo", "\"imgSrc\":\"../images/Custom/logo.png\"");
        }
        else
            UserOptions.Add("AxAppLogo", "\"imgSrc\":\"../images/agileFlight.png\"");
        if (Session["AppSessionKey"] != null)
        {
            if (Session["lictype"] != null && Session["lictype"].ToString() == "limited")
                appsessionKey = "l~";
            else
                appsessionKey = "u~";
            appsessionKey += Session["AppSessionKey"].ToString();
        }
        if (Session["nsessionid"] != null)
        {
            hdnpsid = Session["nsessionid"].ToString();
        }
        if (Session["project"] != null)
        {
            string projName = Session["project"].ToString();
            if (Application[projName + "-PeriodicRCKey"] != null)
                PeriodicRCKey = "true";
            else
            {
                Application[projName + "-PeriodicRCKey"] = "true";
                PeriodicRCKey = "false";
            }
        }
    }

    private void EncryptConnDtls()
    {
        if (!(string.IsNullOrEmpty(Session["axdb"].ToString()) && string.IsNullOrEmpty(Session["dbuser"].ToString()) && string.IsNullOrEmpty(Session["axconstr"].ToString())))
        {
            string strDB = string.Empty;
            if (Session["axdb"] != null && Session["axdb"].ToString() != String.Empty)
                strDB = Session["axdb"].ToString();

            Utl = Session["dbuser"].ToString() + "," + strDB + ",";
            string conStr = Session["axconstr"].ToString();
            string conId = string.Empty;
            string conpwd = string.Empty;

            if (strDB.ToLower() == "mysql" || strDB.ToLower() == "mariadb")
            {
                for (int i = 0; i < conStr.Split(';').Length; i++)
                {
                    string temp = conStr.Split(';')[i];
                    if (temp.Contains("Server"))
                        conId = temp.Split('=')[1];
                    if (temp.Contains("Pwd"))
                        conpwd = temp.Split('=')[1];
                }
            }
            else if (strDB.ToLower() == "ms sql" || strDB.ToLower() == "mssql")
            {
                for (int i = 0; i < conStr.Split(';').Length; i++)
                {
                    string temp = conStr.Split(';')[i];
                    if (temp.Contains("Server"))
                        conId = temp.Split('=')[1];
                    if (temp.Contains("Password"))
                        conpwd = temp.Split('=')[1];
                }
            }
            else if (strDB.ToLower() == "postgresql" || strDB.ToLower() == "postgre")
            {
                for (int i = 0; i < conStr.Split(';').Length; i++)
                {
                    string temp = conStr.Split(';')[i];
                    if (temp.Contains("Server"))
                        conId = temp.Split('=')[1];
                    if (temp.Contains("Pwd"))
                        conpwd = temp.Split('=')[1];
                    if (temp.Contains("Database"))
                    {
                        Utl = Session["dbuser"].ToString() + "\\" + temp.Split('=')[1] + "," + strDB + ",";
                    }
                }
            }

            else
            {
                for (int i = 0; i < conStr.Split(';').Length; i++)
                {
                    string temp = conStr.Split(';')[i];
                    if (temp.Contains("Data Source"))
                        conId = temp.Split('=')[1];
                    if (temp.Contains("Password"))
                        conpwd = temp.Split('=')[1];
                }
            }

            Utl += conId + "," + conpwd + "^~)";

            if (ConfigurationManager.AppSettings["EncryptionKey"] != null && ConfigurationManager.AppSettings["EncryptionIV"] != null && ConfigurationManager.AppSettings["EncryptionKey"] != string.Empty && ConfigurationManager.AppSettings["EncryptionIV"] != string.Empty)
            {
                string[] keyStr = ConfigurationManager.AppSettings["EncryptionKey"].ToString().Split(',');
                byte[] keyBytes = keyStr.Select(Byte.Parse).ToArray();
                string[] ivStr = ConfigurationManager.AppSettings["EncryptionIV"].ToString().Split(',');
                byte[] ivBytes = ivStr.Select(Byte.Parse).ToArray();
                byte[] encryptedUtl = util.EncryptStringToBytes_Aes(Utl, keyBytes, ivBytes);
                Utl = BitConverter.ToString(encryptedUtl).Replace("-", string.Empty);
                Session["utl"] = Utl;
            }
        }
        if (ConfigurationManager.AppSettings["redisIP"] != null && ConfigurationManager.AppSettings["redisIP"].ToString() != String.Empty && ConfigurationManager.AppSettings["redisPass"] != null)
        {
            redisUtl = ConfigurationManager.AppSettings["redisIP"].ToString() + "," + ConfigurationManager.AppSettings["redisPass"].ToString() + "^~)";

            if (ConfigurationManager.AppSettings["EncryptionKey"] != null && ConfigurationManager.AppSettings["EncryptionIV"] != null && ConfigurationManager.AppSettings["EncryptionKey"] != string.Empty && ConfigurationManager.AppSettings["EncryptionIV"] != string.Empty)
            {
                string[] keyStr = ConfigurationManager.AppSettings["EncryptionKey"].ToString().Split(',');
                byte[] keyBytes = keyStr.Select(Byte.Parse).ToArray();
                string[] ivStr = ConfigurationManager.AppSettings["EncryptionIV"].ToString().Split(',');
                byte[] ivBytes = ivStr.Select(Byte.Parse).ToArray();
                byte[] encryptedUtl = util.EncryptStringToBytes_Aes(redisUtl, keyBytes, ivBytes);
                redisUtl = BitConverter.ToString(encryptedUtl).Replace("-", string.Empty);
            }
        }
    }
    private void GetConfigs()
    {
        string LandingPage = util.GetAdvConfigs("Change Password");
        if (LandingPage != "")
        {
            if (LandingPage == "disable")
            {
                changepassword.Visible = false;
                UserOptions.Add("ChangePassword", "\"display\":\"none\"♠\"onclick\":\"showChangePasswordDialog()\"♠\"title\":\"Change Password\"");
            }
            else if (LandingPage == "enable")
            {
                changepassword.Visible = true;
                UserOptions.Add("ChangePassword", "\"display\":\"block\"♠\"onclick\":\"showChangePasswordDialog()\"♠\"title\":\"Change Password\"");
            }
        }
        else
            UserOptions.Add("ChangePassword", "\"display\":\"block\"♠\"onclick\":\"showChangePasswordDialog()\"♠\"title\":\"Change Password\"");
        string mainReload = util.GetAdvConfigs("Main Page Reload");
        if (mainReload != "")
        {
            axMainPageReload = mainReload;
            Session["AxReloadCloud"] = mainReload;
        }

        string MenuSize = util.GetAdvConfigs("Menu Size");
        if (MenuSize != "")
            axMenuReSize = MenuSize;
        string ProjectTile = util.GetAdvConfigs("Project Title");
        if (ProjectTile != "")
            axProjectTile = appName;
        string DateFormat = util.GetAdvConfigs("Date Format");
        if (DateFormat != "")
        {
            DateFormat = DateFormat.ToLower();
            Session["ClientLocale"] = DateFormat;
            glCulture = DateFormat;
        }
    }
    private void SetLandingPage()
    {
        string approle = "";
        approle = AxRole;
        //TODO:add condition for cloudapp
        if (ConfigurationManager.AppSettings["isCloudApp"] == null || ConfigurationManager.AppSettings["isCloudApp"].ToString().ToLower() == "false")
        {
            if (approle[approle.Length - 1] == ',' || approle[approle.Length - 1] == ';')
                approle = approle.Substring(0, approle.Length - 1);
        }

        //If there is a role specific default page then that will have higher priority, hence no todo check if approle is given
        string todo = "false";
        if (ViewState["todo"] != null)
            todo = Convert.ToString(ViewState["todo"]);

        #region new code for role check
        if (!string.IsNullOrEmpty(approle))
        {
            foreach (var role in approle.Split(','))
            {
                if (Application[role] != null)
                {
                    Navigationpage = Application[role].ToString();
                    break;
                }
            }
        }
        if (string.IsNullOrEmpty(Navigationpage))
        {
            if (todo.ToLower() == "true")
                Navigationpage = "../aspx/iview.aspx?ivname=todo";
            else if (Session["AxLandingPage"] != null)
                Navigationpage = Session["AxLandingPage"].ToString();
        }
        #endregion
        #region old code for role check
        //if (Application[approle] == null)
        //{
        //    if (todo.ToLower() == "true")
        //        Navigationpage = "../aspx/iview.aspx?ivname=todo";
        //    else if (Session["AxLandingPage"] != null)
        //        Navigationpage = Session["AxLandingPage"].ToString();
        //}
        //else
        //{
        //    Navigationpage = Application[approle].ToString();
        //}
        #endregion



        compressedMode = util.GetAdvConfigs("ApplicationCompressedMode") == "true";

        string LandingPage = util.GetAdvConfigs("Landing Structure");// util.GetAdvConfigs("Landing Structure");

        string tempProj = "";

        string headerPath = util.GetAdvConfigs("ApplicationTemplate");

        if (headerPath == "" || (headerPath.ToLower().IndexOf(".html") == -1))
        {
            headerPath = "mainPageTemplate.html" + "?v=" + Guid.NewGuid().ToString();
        }
        else
        {
            headerPath += "?v=" + Guid.NewGuid().ToString();
        }

        string headerPathFull = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("aspx/")) + tempProj + "/aspx/" + headerPath;
        templateText = pushHeader(headerPathFull);
        templateStage.Text = templateText;

        if (LandingPage != "")
        {
            string[] landingValue = LandingPage.Split('♦');
            if (landingValue[0].ToLower() == "tstruct")
            {
                Navigationpage = "../aspx/tstruct.aspx?transid=" + landingValue[1];
                if (Session["AxCloudDB"] != null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "", "<script>displayBootstrapModalDialog('Company SetUp', 'lg', '510px', true, '" + Navigationpage + "', true, cloudSetupInit)</script>");
                    Navigationpage = "";
                }
            }
            else if (landingValue[0].ToLower() == "iview")
            {
                Navigationpage = "../aspx/iview.aspx?ivname=" + landingValue[1];
                if (Session["AxCloudDB"] != null)
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "", "<script>displayBootstrapModalDialog('Company SetUp', 'lg', '510px', true, '" + Navigationpage + "', true, cloudSetupInit)</script>");
                    Navigationpage = "";
                }
            }
            else if (landingValue[0].ToLower() == "general")
            {
                Navigationpage = "../" + proj + "/aspx/" + landingValue[2];
                hdHomeUrl.Value = Navigationpage;
            }
        }
        else
        {
            Navigationpage = "../aspx/Page.aspx";
            hdHomeUrl.Value = Navigationpage;
        }
    }

    private void SetAccessFromSession()
    {
        if (Session["IsSSO"] != null)
            fromSSO = Session["IsSSO"].ToString();

        if (Convert.ToString(Session["Build"]) == "T")
            build = true;

        if (Convert.ToString(Session["ImportAccess"]).ToLower() == "true")
            import = true;

        if (Convert.ToString(Session["ExportAccess"]).ToLower() == "true")
            export = true;

        if (Convert.ToString(Session["HistoryAccess"]).ToLower() == "true")
            history = true;

        if (Session["forcePwdChange"] != null)
            forcePwdChange = Session["forcePwdChange"].ToString();

        if (Session["draftScript"] != null)
            draftScript = Session["draftScript"].ToString();
    }

    private void InitSessExpiryAlert()
    {
        //Session Alert start
        if (Session["AxSessionExtend"] != null && Session["AxSessionExtend"].ToString() == "true")
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);

        }
        //Session Alert end
    }

    private void SetCloudSignoutPath()
    {
        if (Session["AxAxCLOUD"] != null)
            isCloud = Convert.ToBoolean(Session["AxAxCLOUD"].ToString());

        if (isCloud)
        {
            signOutPath = "../" + Session["domainName"];
            UserOptions.Add("Logout", "\"onclick\":\"signout('" + signOutPath + "')\"");
        }
        else
        {
            signOutPath = util.SIGNOUTPATH;
            UserOptions.Add("Logout", "\"onclick\":\"signout('" + signOutPath + "')\"");
        }
    }

    private void GetSessionVars()
    {
        if (ConfigurationManager.AppSettings["LoginTrace"] != null)
            loginTrace = ConfigurationManager.AppSettings["LoginTrace"].ToString();
        if (Session["AxMenuStyle"] != null && Session["AxMenuStyle"].ToString() == "classic")
        {
            isNewMenu = "false";
            menuStyle = "classic";
        }
        if (Session["AxHomeBuildAccess"] != null)
            PageBuilderAccess = Session["AxHomeBuildAccess"].ToString();
        else
            PageBuilderAccess = "default";

        if (HttpContext.Current.Session["AxIsPerfCode"] != null && HttpContext.Current.Session["AxIsPerfCode"].ToString().ToLower() == "true")
            isRapidLoad = true;

        if (Session["themeColor"] != null)
            themeColor = Session["themeColor"].ToString();

        try
        {
            alertsTimeout = HttpContext.Current.Session["AxAlertTimeout"] != null ? (Convert.ToInt32(HttpContext.Current.Session["AxAlertTimeout"]) * 1000) : 3000;
        }
        catch (Exception ex)
        {
            alertsTimeout = 3000;
        }
        //Added by Souvik
        //to show/hide error/failure alert messages based on config settings
        try
        {
            var session = HttpContext.Current.Session;
            if (session["AxErrorMsg"] != null)
            {
                errorEnable = Convert.ToBoolean(session["AxErrorMsg"]);
                errorTimeout = session["AxErrorMsgTimeout"] != null ? (Convert.ToInt32(session["AxErrorMsgTimeout"]) * 1000) : 0;
            }
            else
            {
                errorTimeout = 0;
                errorEnable = false;
            }
        }
        catch (Exception ex)
        {
            errorTimeout = 0;
            errorEnable = false;
        }
        if (ConfigurationManager.AppSettings["isCloudApp"] != null)
            isCloudApp = Convert.ToBoolean(ConfigurationManager.AppSettings["isCloudApp"].ToString());

        scriptsPath = HttpContext.Current.Application["ScriptsPath"].ToString();
        if (ConfigurationManager.AppSettings["RestDllPath"] != null)
            RestDllPath = ConfigurationManager.AppSettings["RestDllPath"].ToString();
        if (HttpContext.Current.Session["AxEnableDraftSave"] != null)
            enableDraftSave = HttpContext.Current.Session["AxEnableDraftSave"].ToString();

        if (Session["AxCopyRightText"] != null)
        {
            copyRightTxt = Session["AxCopyRightText"].ToString();
            copyRightTxt = copyRightTxt.Replace("#br#", "<br/>");
        }
        if (Session["AxTrace"] == null || Session["AxTrace"].ToString() == "")
            Session["AxTrace"] = "false";
        if (Session["AxHelpIview"] != null)
            HelpIview = Session["AxHelpIview"].ToString();

        traceValue = Convert.ToBoolean(Session["AxTrace"]);
        loginPath = Application["LoginPath"].ToString();

    }

    private void CheckCloudRequest()
    {
        if (Request.QueryString["id"] != null)
        {
            string result = string.Empty;
            string appDetails = string.Empty;
            Session["Thmcolor"] = "Blue";

            string guid = Request.QueryString["ID"].ToString();

            result = util.GetAppAndUserInfo(guid, Request.UserHostAddress);

            if (result.ToLower().Contains("error"))
            {
                Response.Redirect(util.ACERRPATH + "1_Error_In_Connection");
            }
            else if (result != string.Empty)
            {
                string[] usrDetails = result.Split('~');
                foreach (string usrStr in usrDetails)
                {
                    int idx = usrStr.IndexOf("=");
                    if (idx != -1 && usrStr.Substring(0, idx).ToLower() != "appdb" && usrStr.Substring(0, idx).ToLower() != "dbtype")
                    {
                        Session[usrStr.Substring(0, idx)] = usrStr.Substring(idx + 1);
                    }
                    else if (usrStr.Substring(0, idx).ToLower() == "appdb")
                    {
                        appDetails = usrStr.Substring(idx + 1);
                        appDetails = appDetails.Replace("[fs]", "/").Replace("[bs]", "\\");

                    }
                }

                result = util.GetCloudGlobalVariables(appDetails);
                if (result.ToLower().Contains("error"))
                {
                    Response.Redirect(util.ACERRPATH + "2");
                }
                else
                {
                    ParseGlobalVars(result);
                    LogFile.Log objLog = new LogFile.Log();
                    objLog.CreateLog(result, "", "ParseGlobalVars()", "new");
                }
            }
        }
        if (Session["IscloudRelogin"] != null && Session["IscloudRelogin"].ToString() == "true")
        {
            Session["IscloudRelogin"] = null;
            string result = util.GetCloudGlobalVariables();
            if (result.ToLower().Contains("error"))
            {
                Response.Redirect(util.ACERRPATH + "2");
            }
            else
            {
                ParseGlobalVars(result);
                LogFile.Log objLog = new LogFile.Log();
                objLog.CreateLog(result, "", "ParseGlobalVars()", "new");
            }
        }
    }

    //This function is referred in the cient side
    private void checkDashboard()
    {
        if (HttpContext.Current.Session["user"] != null)
        {
            DataSet dsdasboard = new DataSet();
            DBContext obj = new DBContext();
            dsdasboard = obj.GetMainPageDBInline("main");
            if (dsdasboard.Tables.Count > 0)
            {
                if (dsdasboard.Tables[0].Rows.Count != 0)
                {
                    if (dsdasboard.Tables[0].Rows[0]["rcount"].ToString() != "0" && !string.IsNullOrEmpty(dsdasboard.Tables[0].Rows[0]["rcount"].ToString()))
                    {
                        //hdndashBoardIcon.Value = "t";
                    }
                }
            }
        }
        else
        {
            SessExpiresStatic();
        }
    }

    [WebMethod]
    public static string getGlobalSearchData(string keyword, string cond)
    {
        DateTime startTime = DateTime.Now;
        DataSet dsSearchData = new DataSet();
        string json = string.Empty; int limit = 10000;
        if (keyword.Contains("'")) keyword = keyword.Replace("'", "''");
        string srchKey = "'%" + keyword + "%'";
        if (cond == "StartsWith") srchKey = srchKey.Replace("'%", "'");
        int SessCount = 0; string SessKeyword = string.Empty;
        bool pickFromCache = false; bool isKeyChanged = false;
        int newFilterCount = 0; bool callService = false;
        string sessCond = "Contains";
        LogFile.Log logObj = new LogFile.Log();
        DataTable filteredData = null;
        try
        {
            logObj.CreateLog("Timetaken in milliseconds details for keyword-" + keyword + ",Cond=" + cond, HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "new");
            if (HttpContext.Current.Session["user"] != null)
            {
                logObj.CreateLog("Start to search with keyword-" + keyword, HttpContext.Current.Session.SessionID, "GlobalSearch", "new");
                if (HttpContext.Current.Session["AxGlobalSrchLimit"] != null)
                {
                    if (HttpContext.Current.Session["AxGlobalSrchLimit"].ToString() != string.Empty)
                        limit = Convert.ToInt32(HttpContext.Current.Session["AxGlobalSrchLimit"].ToString());
                }

                //If data is available in session set varaibles from session 
                if (HttpContext.Current.Session["Axp_App_Srch"] != null)
                {
                    DateTime st1 = DateTime.Now;
                    dsSearchData = (DataSet)HttpContext.Current.Session["Axp_App_Srch"];
                    if (dsSearchData.Tables.Count > 0)
                        SessCount = dsSearchData.Tables[0].Rows.Count;
                    SessKeyword = HttpContext.Current.Session["Axp_App_SrchKey"].ToString();
                    sessCond = HttpContext.Current.Session["Axp_App_SrchCond"].ToString();
                    logObj.CreateLog("Getting data from session-" + st1.Subtract(DateTime.Now).TotalMilliseconds.ToString(), HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "");
                }
                else
                    callService = true;

                if (SessKeyword == keyword)
                {
                    pickFromCache = true;
                    if (dsSearchData.Tables.Count > 0)
                        filteredData = dsSearchData.Tables[0];
                }
                else if (SessKeyword != "" && keyword.StartsWith(SessKeyword))
                {
                    pickFromCache = true;
                    isKeyChanged = true;
                }
                else
                {
                    pickFromCache = false;
                    callService = true;
                    HttpContext.Current.Session.Remove("Axp_App_SrchKey");
                    HttpContext.Current.Session.Remove("Axp_App_Srch");
                }

                //If the condition modified from contains to starts with or vice-versa the service should be called
                if (sessCond != cond)
                {
                    pickFromCache = false;
                    callService = true;
                }

                if (pickFromCache)
                {
                    //First If - Key is changed and session has data                    
                    if (isKeyChanged && dsSearchData != null)
                    {
                        try
                        {
                            DateTime st2 = DateTime.Now;
                            filteredData = FilterSearchTable(dsSearchData.Tables[0], keyword);
                            newFilterCount = filteredData.Rows.Count;
                            logObj.CreateLog("Filtering data from session-" + st2.Subtract(DateTime.Now).TotalMilliseconds.ToString(), HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "");
                        }
                        catch (Exception ex)
                        {
                            logObj.CreateLog("Exception while filtering data from session-" + ex.StackTrace, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
                        }
                    }

                    if (SessCount == limit && newFilterCount == 0 && isKeyChanged)
                    {
                        HttpContext.Current.Session.Remove("Axp_App_SrchKey");
                        HttpContext.Current.Session.Remove("Axp_App_Srch");
                        callService = true;
                    }
                    else if (SessCount == limit && isKeyChanged)
                    {
                        HttpContext.Current.Session.Remove("Axp_App_SrchKey");
                        HttpContext.Current.Session.Remove("Axp_App_Srch");
                        callService = true;
                    }
                    else if (SessCount < limit && isKeyChanged && filteredData != null && newFilterCount == 0)
                    {
                        callService = true;
                    }
                    //else if (SessCount < limit && newFilterCount != 0 && isKeyChanged && filteredData != null)
                    else if (SessCount < limit && isKeyChanged && filteredData != null)
                    {
                        dsSearchData = new DataSet();
                        dsSearchData.Tables.Add("Table");
                        dsSearchData.Tables["Table"].Merge(filteredData);
                        callService = false;
                        logObj.CreateLog("Getting from cache-" + keyword + "-with count =" + newFilterCount, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
                    }
                    else if (filteredData == null)
                    {
                        callService = true;
                    }
                    //If the sessCount is less than the limit then it will pick from cache                  
                }
                if (callService)
                {
                    DateTime st3 = DateTime.Now;
                    DBContext obj = new DBContext();
                    string query = Constants.GET_SEARCH_DATA.Replace("$KEYWORD$", srchKey.ToLower());
                    logObj.CreateLog("Getting from DB-" + keyword + "-with query =" + query, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
                    dsSearchData = obj.GetSearchData(query, limit);
                    logObj.CreateLog("Getting data from DB-" + st3.Subtract(DateTime.Now).TotalMilliseconds.ToString(), HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "");

                    //If view is not present or data is not returned from view- get the data filtered on menu
                    if (dsSearchData == null || dsSearchData.Tables.Count == 0 || (dsSearchData.Tables.Count > 0 && dsSearchData.Tables[0].Rows.Count == 0))
                    {
                        DataTable dt = new DataTable();
                        dt = GetMenu();
                        if (dt.Rows.Count > 0)
                        {
                            dsSearchData = new DataSet();
                            dsSearchData.Tables.Add("Table");
                            dsSearchData.Tables[0].Merge(FilterSearchTable(dt, keyword, cond));
                        }
                        else
                            dsSearchData = new DataSet();
                        logObj.CreateLog("Getting from Menu-" + keyword, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
                    }
                }

                //If the records in DB is

                HttpContext.Current.Session["Axp_App_Srch"] = dsSearchData;
                HttpContext.Current.Session["Axp_App_SrchKey"] = keyword;
                HttpContext.Current.Session["Axp_App_SrchCond"] = cond;

                json = GetPageWiseSearchData(0, 100, dsSearchData.Tables[0]);
                logObj.CreateLog("Result =" + json, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
            }
            else
            {
                json = "Session Expired";
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Getting exception in getGlobalSearchData-" + ex.StackTrace + "-with keyword =" + keyword, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
            return "getting exception in code";
        }

        logObj.CreateLog("Total GetGlobalSearchServerTime-" + startTime.Subtract(DateTime.Now).TotalMilliseconds.ToString(), HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "");
        return json;
    }


    private static DataTable FilterSearchTable(DataTable dt, string keyword, string cond = "")
    {
        DataTable filteredData = dt;
        if (cond == "StartsWith")
        {
            var data = dt.AsEnumerable().Where(x => x.Field<string>("SEARCHTEXT").ToLower().StartsWith(keyword)).Select(x => new { SEARCHTEXT = x.Field<string>("SEARCHTEXT"), HLTYPE = x.Field<string>("HLTYPE"), STRUCTNAME = x.Field<string>("STRUCTNAME"), PARAMS = x.Field<string>("PARAMS") }).ToList();
            filteredData = data.ToDataTable();
        }
        else
        {
            var data = dt.AsEnumerable().Where(x => x.Field<string>("SEARCHTEXT").ToLower().Contains(keyword)).Select(x => new { SEARCHTEXT = x.Field<string>("SEARCHTEXT"), HLTYPE = x.Field<string>("HLTYPE"), STRUCTNAME = x.Field<string>("STRUCTNAME"), PARAMS = x.Field<string>("PARAMS") }).ToList();
            filteredData = data.ToDataTable();
        }
        return filteredData;
    }

    [WebMethod]
    public static string GetMoreSearchData(string pageIndex, string pageSize)
    {
        DateTime st = DateTime.Now;
        string result = string.Empty;
        LogFile.Log logObj = new LogFile.Log();
        if (HttpContext.Current.Session["Axp_App_Srch"] != null)
        {
            DataSet ds = (DataSet)HttpContext.Current.Session["Axp_App_Srch"];
            try
            {
                int nextRec = int.Parse(pageIndex) * int.Parse(pageSize);
                result = GetPageWiseSearchData(int.Parse(pageIndex), int.Parse(pageSize), ds.Tables[0]);
            }
            catch (Exception ex)
            {
                logObj.CreateLog("Exception while bringing more records on scroll with page Index=" + pageIndex + "and Exception details=" + ex.Message, HttpContext.Current.Session.SessionID, "GlobalSearch", "");
                result = JsonConvert.SerializeObject(ds.Tables[0], Newtonsoft.Json.Formatting.Indented);
            }
        }
        logObj.CreateLog("Total timetaken on scroll-pageno=" + pageIndex + st.Subtract(DateTime.Now).TotalMilliseconds.ToString(), HttpContext.Current.Session.SessionID, "GlobalSearch-Time", "");

        return result;
    }

    public static string GetPageWiseSearchData(int pageIndex, int pageSize, DataTable dtData)
    {
        string result = string.Empty;
        int nextRec = pageIndex * pageSize;
        int gsRowCount = 0;
        if (dtData.Rows.Count > 0)
        {
            IEnumerable<DataRow> MyDataPage = dtData.AsEnumerable().Skip(nextRec).Take(pageSize);
            DataTable NewDT = MyDataPage.CopyToDataTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add("Table");
            ds1.Tables["Table"].Merge(NewDT);
            result = JsonConvert.SerializeObject(ds1, Newtonsoft.Json.Formatting.Indented);
            if (ds1.Tables.Count > 0 && pageIndex == 0)
                gsRowCount = dtData.Rows.Count;
            else
                gsRowCount = ds1.Tables[0].Rows.Count;
            result = gsRowCount + "*♦*" + result;
        }
        return result;
    }

    #region global search
    public static DataTable GetMenu()
    {
        string iXml = string.Empty;
        string errlog = string.Empty;
        string fileName = string.Empty;
        string searchtext = string.Empty;
        string hltype = string.Empty;
        string structname = string.Empty;
        string strparams = string.Empty;
        string result = string.Empty;
        if (HttpContext.Current.Session["MenuData"] != null)
            result = HttpContext.Current.Session["MenuData"].ToString();
        if ((result.Length == 0) || (result.Substring(1, 7) == Constants.ERROR))
        {
            return null;
        }
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(result);

        DataTable dt = new DataTable();

        dt.Columns.Add("SEARCHTEXT");
        dt.Columns.Add("HLTYPE");
        dt.Columns.Add("STRUCTNAME");
        dt.Columns.Add("PARAMS");

        string max = "0";
        XmlNodeList RootNodes = default(XmlNodeList);
        RootNodes = xmlDoc.SelectNodes("/root");
        // To get the maximum depth value at root node     
        foreach (XmlNode RootNode in RootNodes)
        {
            max = RootNode.Attributes["max"].Value;
        }
        XmlNodeList parentNodes = default(XmlNodeList);
        try
        {
            parentNodes = RootNodes[0].ChildNodes;
            foreach (XmlNode menuchldNode in parentNodes)
            {
                //Parent Node
                searchtext = menuchldNode.Attributes["name"].Value;
                hltype = menuchldNode.Attributes["target"].Value;
                if (!string.IsNullOrEmpty(hltype))
                {
                    structname = hltype.Split('=')[1];
                    hltype = hltype.Split('.')[0];
                    dt = AddRow(dt, searchtext, hltype, structname, strparams);
                }
                XmlNodeList zeroChldNode = default(XmlNodeList);
                zeroChldNode = menuchldNode.ChildNodes;
                foreach (XmlNode firstChildNodes in zeroChldNode)
                {
                    //First child Node
                    searchtext = firstChildNodes.Attributes["name"].Value;
                    hltype = firstChildNodes.Attributes["target"].Value;
                    if (!string.IsNullOrEmpty(hltype))
                    {
                        structname = hltype.Split('=')[1];
                        hltype = hltype.Split('.')[0];
                        dt = AddRow(dt, searchtext, hltype, structname, strparams);
                    }
                    foreach (XmlNode secondChldNode in firstChildNodes)
                    {
                        //Second child Node
                        searchtext = secondChldNode.Attributes["name"].Value;
                        hltype = secondChldNode.Attributes["target"].Value;
                        if (!string.IsNullOrEmpty(hltype))
                        {
                            structname = hltype.Split('=')[1];
                            hltype = hltype.Split('.')[0];
                            dt = AddRow(dt, searchtext, hltype, structname, strparams);
                        }
                        foreach (XmlNode thirdChildNodes in secondChldNode)
                        {
                            //Third child Node
                            searchtext = thirdChildNodes.Attributes["name"].Value;
                            hltype = thirdChildNodes.Attributes["target"].Value;
                            if (!string.IsNullOrEmpty(hltype))
                            {
                                structname = hltype.Split('=')[1];
                                hltype = hltype.Split('.')[0];
                                dt = AddRow(dt, searchtext, hltype, structname, strparams);
                            }
                        }
                    }
                }
            }
        }
        catch
        {

        }
        return dt;
    }

    private static DataTable AddRow(DataTable dt, string parent, string child, string target, string level)
    {
        DataRow dr = dt.NewRow();
        dr["SEARCHTEXT"] = parent;
        dr["HLTYPE"] = child;
        dr["STRUCTNAME"] = target;
        dr["PARAMS"] = level;
        dt.Rows.Add(dr);
        return dt;
    }
    #endregion

    private static void SessExpiresStatic()
    {
        string url = Convert.ToString(HttpContext.Current.Application["SessExpiryPath"]);
        HttpContext.Current.Response.Write("<script>" + Constants.vbCrLf);
        HttpContext.Current.Response.Write("parent.parent.location.href='" + url + "';");
        HttpContext.Current.Response.Write(Constants.vbCrLf + "</script>");
    }

    private void ParseGlobalVars(string result)
    {
        StringBuilder globalVarToShow = new StringBuilder();
        Dictionary<string, string> vars = new Dictionary<string, string>();
        string[] aryGloVar = result.Split('~');
        Session["isowner"] = false;
        Dictionary<string, string> NewglobalVariables = new Dictionary<string, string>();
        foreach (var gloVar in aryGloVar)
        {
            string[] tempVar = gloVar.Split('=');
            if (tempVar[0].ToString() == "axglo_recordid")
                Session["AxGloRecId"] = tempVar[1].ToString();
            else if (tempVar[0].ToString() == "AXGLO_HIDE")
                Session["AxGloHideShow"] = tempVar[1].ToString();
            else if (tempVar[0].ToString() == "AXP_DISPLAYTEXT")
            {
                string sbDspText = string.Empty;
                if (Session["appname"] != null)
                {
                    //sbDspText = "<span class='globalProjname'>" + Session["appname"].ToString() + "</span>";
                }

                //if (tempVar[1].ToString() != "")
                globalVarToShow.Append(sbDspText + FormatGlobalVariables(tempVar[1].ToString()));
            }
            else if (tempVar[0].ToString() == "responsibilies")
            {
                Session["AxResponsibilities"] = tempVar[1].ToString();
                if (tempVar[1] != null && tempVar[1].ToString().Contains("default"))
                {
                    Session["isowner"] = true;
                }
            }
            if (tempVar[0] != "Done#")
                NewglobalVariables.Add(tempVar[0], tempVar[1]);
        }
        string golVarNode = "<globalvars>";

        ArrayList UpglobalVariables = (ArrayList)Session["globalVarsArray"];
        if (UpglobalVariables == null)
            UpglobalVariables = new ArrayList();
        StringBuilder paramlist = new StringBuilder();
        int i = 0;
        int paramCnt = 0;
        foreach (string item in UpglobalVariables)
        {
            int valueIndex = item.IndexOf("=");
            string key = item.Substring(0, valueIndex);
            string val = string.Empty;
            if (NewglobalVariables.Count > 0)
                val = NewglobalVariables.AsEnumerable().Where(x => x.Key == key).Select(y => y.Value).FirstOrDefault();
            if (val == null)
                val = item.Substring(valueIndex + 1);
            golVarNode += "<" + key + ">" + val + "</" + key + ">";
            val = CheckSpecialChars(val);
            paramlist.Append("Parameters[" + i + "] = " + "\"" + key + "~" + val + "\";  ");
            if (key == "project" && val.Contains(','))
                Session[key] = val.Split(',')[1];
            else
                Session[key] = val;
            if (isRapidLoad)
                Session["axGloVars_" + key] = val;
            paramCnt = paramCnt + 1;
            i++;
        }
        golVarNode += "</globalvars>";
        Session["axGlobalVars"] = golVarNode;
        DisplayGloVars(globalVarToShow.ToString());

        string dtCulture = Request.UserLanguages[0];
        //If useCulture flag is false , we are not checking the browser culture.
        //If it's true ,we are setting the culture based on the client browser language.
        if (Session["AxUSCulture"] != null)
            useCulture = Convert.ToBoolean(Session["AxUSCulture"].ToString());
        if (!useCulture)
            dtCulture = "en-gb";
        else
        {
            if (dtCulture.ToLower() == "en" || dtCulture.ToLower() == "en-us")
                dtCulture = "en-us";
        }
        paramlist.Append("Parameters[" + paramCnt + "] = " + "\"username~" + user + "\";  ");
        paramlist.Append("Parameters[" + (paramCnt + 1) + "] = " + "\"Culture~" + dtCulture + "\";  ");
        Session["ClientLocale"] = dtCulture;
        glCulture = dtCulture;
        if (paramlist.ToString() != string.Empty)
        {
            Session["globalvarstring"] = paramlist.ToString();
        }
    }

    private string FormatGlobalVariables(string gloVarStr)
    {
        string result = string.Empty;
        if (gloVarStr == "")
        {
            return "";
        }

        try
        {
            editAxGlo2.Visible = true;
            var AxGlo = UserOptions.Where(x => x.Key == "GlobalParams").ToList();
            if (AxGlo.Count > 0)
                UserOptions["GlobalParams"] = "\"display\":\"block\"♠\"title\":\"Global Parameters\"";
            else
                UserOptions.Add("GlobalParams", "\"display\":\"block\"♠\"title\":\"Global Parameters\"");
            bool isEditbtnVsbl = false;
            StringBuilder sbDspText = new StringBuilder("");
            string[] displayText = gloVarStr.Split(',');
            if (isNewMenu != "true")
            {
                foreach (var txtStr in displayText)
                {
                    string[] innerGloVar = txtStr.Split(':');
                    if (innerGloVar.Length > 1)
                    {
                        if (innerGloVar[1].ToString() != "")
                        {
                            sbDspText.Append(innerGloVar[1].ToString() + ", ");
                            isEditbtnVsbl = true;
                        }
                    }
                }
                if (sbDspText.ToString() != "")
                    result = sbDspText.ToString().Remove(sbDspText.ToString().LastIndexOf(','));// + "."
            }
            else
            {
                sbDspText.Append("<table class=\"poplitxt\">");
                foreach (var txtStr in displayText)
                {
                    string[] innerGloVar = txtStr.Split(':');
                    if (innerGloVar.Length > 1)
                    {
                        sbDspText.Append("<tr>");
                        if (innerGloVar[0].ToString() != "")
                            sbDspText.Append("<td>" + innerGloVar[0].ToString() + ":</td>");
                        if (innerGloVar[1].ToString() != "")
                            sbDspText.Append("<td>" + innerGloVar[1].ToString() + "</td>");
                        sbDspText.Append("</tr>");
                    }
                }
                sbDspText.Append("</table>");
            }
            if (Session["MobileView"] != null && Session["MobileView"].ToString() == "True")
                myPopover.Visible = false;
            else
                myPopover.InnerHtml = sbDspText.ToString();
            result = string.Empty;
        }
        catch (Exception ex)
        {
            result = "";
        }
        return result;
    }

    private void toggleEditGloButton(bool result)
    {
        if (result)
        {
            editAxGlo2.Visible = true;
            var AxGlo = UserOptions.Where(x => x.Key == "GlobalParams").ToList();
            if (AxGlo.Count > 0)
                UserOptions["GlobalParams"] = "\"display\":\"block\"♠\"title\":\"Global Parameters\"";
            else
                UserOptions.Add("GlobalParams", "\"display\":\"block\"♠\"title\":\"Global Parameters\"");
        }
        else
        {
            editAxGlo2.Visible = false;
            var AxGlo = UserOptions.Where(x => x.Key == "GlobalParams").ToList();
            if (AxGlo.Count > 0)
                UserOptions["GlobalParams"] = "\"display\":\"none\"♠\"title\":\"Global Parameters\"";
            else
                UserOptions.Add("GlobalParams", "\"display\":\"none\"♠\"title\":\"Global Parameters\"");
            Session["AxGloHideShow"] = "F";
        }
    }

    private void DisplayGloVars(string globalVarToShow)
    {
        // Below is used to display the saved Global Variables.
        string url = string.Empty;
        string AxGloHideShow = string.Empty;
        if (Session["AxGloHideShow"] != null && Session["AxGloHideShow"].ToString() != "")
            AxGloHideShow = Session["AxGloHideShow"].ToString();
        string AxGloRecId = string.Empty;
        if (Session["AxGloRecId"] != null && Session["AxGloHideShow"].ToString() != "")
            AxGloRecId = Session["AxGloRecId"].ToString();
        if (AxGloRecId != string.Empty)
            url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "&recordid=" + AxGloRecId + "";
        else
            url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "";
        editAxGlo2.Attributes.Add("onclick", "LoadIframe('" + url + "');");
        var AxGlo = UserOptions.Where(x => x.Key == "GlobalParamsonclick").ToList();
        if (AxGlo.Count > 0)
            UserOptions["GlobalParamsonclick"] = "\"onclick\":\"LoadIframe('" + url + "')\"";
        else
            UserOptions.Add("GlobalParamsonclick", "\"onclick\":\"LoadIframe('" + url + "')\"");
        if (globalVarToShow.Length > 100)
        {
            string globalValuesShown = string.Empty;
            globalValuesShown = globalVarToShow.ToString();
            globalVarToShow.Remove(100, ((globalValuesShown.Length - 1) - 100));
        }
        hdParamsValues.Value = globalVarToShow.ToString();
    }

    private void SetPrintInterval()
    {
        string prInterval = ConfigurationManager.AppSettings["PrintUpdateInterval"];
        if (prInterval == null)
            prInterval = "-1";
        else
            prInterval = prInterval.ToString();

        printInterval = "<script language=\"javascript\" type=\"text/javascript\" >var prInterval='" + prInterval + "';</script>";
    }

    private void GetMenuData()
    {
        if (Session["MenuData"] == null || Session["MenuData"].ToString() == "")
        {
            string sXml = string.Empty;
            string result1 = string.Empty;
            string err = string.Empty;
            string lang_at = "";
            if (Session["language"].ToString().ToUpper() != "ENGLISH")
                lang_at = " lang=\"" + language + "\"";
            string key = user;
            string menulang = Session["language"].ToString();

            string errlog = logobj.CreateLog("Getting Menu", sid, "GetMultiMenu", "new");
            if (loginTrace.ToLower() == "true")
                errlog = logobj.CreateLog("Getting Menu", sid, "GetMultiMenu", "new", "true");

            sXml = sXml + "<root axpapp='" + proj + "' sessionid='" + sid + "' trace='" + errlog + "' mname =\"\" " + lang_at + " appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "'";
            sXml = sXml + "> ";
            sXml = sXml + Session["axApps"].ToString() + axProps + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString();
            sXml = sXml + "</root>";
            result1 = objWebServiceExt.CallGetMultiLevelMenuWS("main", sXml);
            result1 = Regex.Replace(result1, ";fwdslh", "/");
            result1 = Regex.Replace(result1, ";hpn", "-");
            result1 = Regex.Replace(result1, ";bkslh", "\\");
            result1 = Regex.Replace(result1, ";eql", "=");
            result1 = Regex.Replace(result1, ";qmrk", "?");

            //' handling edit menu 
            Session["MenuData"] = result1;

            string errMsg = util.ParseXmlErrorNode(result1);

            if (errMsg != string.Empty)
            {
                if (errMsg == Constants.SESSIONERROR || errMsg == Constants.SESSIONEXPMSG)
                {
                    SessExpires();
                    if (isCloudApp)
                        Response.Redirect(util.ACERRPATH + "14");
                    else
                        Response.Redirect(util.SESSEXPIRYPATH);
                }
                else
                {
                    if (isCloudApp)
                        Response.Redirect(util.ACERRPATH + "15");
                    else
                        Response.Redirect(util.SESSEXPIRYPATH);
                }
            }
        }

        string MenuHtml = string.Empty;
        menuXmlData = Session["MenuData"].ToString();
        menuXmlData = Regex.Replace(menuXmlData, "'", "&apos;");
        menuXmlData = Regex.Replace(menuXmlData, "&quot;", " ");
        if (isNewMenu == "true")
            MenuHTML = "";
        else
            MenuHtml = GetClassicMenu();
        menuStr = MenuHtml;
    }

    private string GetClassicMenu()
    {
        // StringBuilder strMenu = new StringBuilder();
        // XmlDocument xmldoc = new XmlDocument();
        // if (Session["MenuData"] != null)
        // {
        //     if (string.IsNullOrEmpty(Session["MenuData"].ToString()))
        //     {
        //         return "";
        //     }
        //     xmldoc.LoadXml(Session["MenuData"].ToString());
        // }

        // XmlNode rootNode = xmldoc.SelectSingleNode("root");
        // int MenuLength = rootNode.ChildNodes.Count;
        // strMenuHtml1.Append("<div class=\"leftPartAC\">");
        // strMenuHtml1.Append("<div class=\"leftPartACHeader\">");
        // strMenuHtml1.Append("<div class=\"wrapperForLeftMenuIcons\">");
        // strMenuHtml1.Append("<span class=\"glyphicon-chevron-left glyphicon closeSidePanel pull-right icon-arrows-circle-right\" id=\"leftMenuToggleBtn\" style=\"cursor: pointer;top: 1px;color:black\" title=\"Collapse\"></span>");
        // strMenuHtml1.Append("<span title = \"Home\" id=\"homeIcon\" onclick=\"resetLeftMenu();\" class=\"glyphicon glyphicon-home icon-basic-home\" style=\"cursor: pointer;\" > ");
        // strMenuHtml1.Append(" </span>");
        // strMenuHtml1.Append("<span title = \"Dashboard\" onclick='ToogleLeftMenu(\"dashboardPanel\", true, \"Dashboard\", \"Dashboard.aspx\");' id=\"dashBoardIcon\" class='icon-ecommerce-graph-increase' style=\"cursor: pointer;width:initial;height:initial;\"></span>");
        // strMenuHtml1.Append("<span title = \"Messages\" id=\"messagesIcon\" class='glyphicon glyphicon-comment icon-basic-message' style='cursor: pointer;margin-left:7px;display:none;'></span>");
        // strMenuHtml1.Append("</div>");
        // strMenuHtml1.Append("<div id='wrapperForSearch' style='display:none;'>");
        // strMenuHtml1.Append("<div class='search-form'>");
        //// string strSearch = lblsearch.Text;
        // strMenuHtml1.Append("<input type='text' value='' id='globalSearchinp' placeholder='" + strSearch + "' class='search-input'/>");
        // strMenuHtml1.Append("<span title=\"Clear\" id='GSclearBtn' class='close glyphicon glyphicon-remove icon-arrows-remove' style='font-size: 21px;right: 1px;top: 5px;display:none;color:black;opacity:1!important;' tabindex='0'></span>");
        // strMenuHtml1.Append("<button type='button' style='font-size: 15px;color: black;cursor:default;' id='globalSearchBtn' class='search-button'>");
        // strMenuHtml1.Append("<i class='glyphicon glyphicon-search icon-basic-magnifier'></i>");
        // strMenuHtml1.Append("</button>");
        // strMenuHtml1.Append("<span id='GSclearBtn' class='close glyphicon glyphicon-remove icon-basic-remove' style='font-size: inherit;right: 1px;top: 8px;display:none; '></span>");
        // strMenuHtml1.Append("</div>");
        // strMenuHtml1.Append("</div>");
        // strMenuHtml1.Append(" <div class=\"dropdown hideInClose\" style=\"display:block;\"><button id=\"moduleSelector\" class=\"btn dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\"></button><ul class=\"dropdown-menu\" style=\"width: 259px;left: 5px;top: 100 %;\">");

        // for (int i = 0; i < MenuLength; i++)
        //     CreateMenuSellecterDropbox(rootNode.ChildNodes[i]);

        // strMenuHtml1.Append("</ul></div><div class=\"clear\"></div></div>");

        // for (int i = 0; i < MenuLength; i++)
        //     CreatePanelsWrapper(rootNode.ChildNodes[i]);

        // strMenuHtml1.Append("</div></div>");
        // strMenuHtml1.Append("</div>");
        // return strMenuHtml1.ToString();
        return string.Empty;
    }

    private string GetModernMenu()
    {
        // StringBuilder strMenu = new StringBuilder();
        // XmlDocument xmldoc = new XmlDocument();
        // if (Session["MenuData"] != null)
        // {
        //     if (string.IsNullOrEmpty(Session["MenuData"].ToString()))
        //     {
        //         return "";
        //     }
        //     xmldoc.LoadXml(Session["MenuData"].ToString());
        // }

        // XmlNode rootNode = xmldoc.SelectSingleNode("root");
        // int MenuLength = rootNode.ChildNodes.Count;
        // strMenuHtml1.Append("<div id=\"mainMenuWrapper\">" +
        //     "<div id=\"mainMobileData\">" +
        //                       "<div class=\"hamburger col-md-1 col-sm-2 col-xs-2\" id=\"hamburgerMenuIcon\">" +
        //                        "<button title=\"Menu\" class=\"noBg\" type=\"button\" tabindex=\"2\">" +
        //                        "<span class=\"line\"></span>" +
        //                        "<span class=\"line\"></span>" +
        //                        "<span class=\"line\"></span></button></div></div>" +
        //                        " <div id=\"mobileSidenav\" class=\"sidenav firstClick\"></div>" +
        //                        "<div id=\"menuContentWrapper\" class=\"firstClick\" style=\"display: none;\">" +
        //                        "<div id=\"hirarchyContainer\" style=\"display: none; \" class=\"hirarchyContainer\">" +
        //                        "<span id=\"hirarchyHome\"  title=\"Home\" style=\"display: none;\" onclick=\"changeHirarchy('home')\" class=\"hirarchy icon-basic-home\"></span>" +
        //                        "</div>" +
        //                        "<div id=\"menuContainer\"></div>");

        // strMenuHtml1.Append("<div class=\"clear\"></div>" +
        //         "<div class=\"text-right\" id=\"menuContentFooter\">" +
        //             "<button title=\"Previous\" type=\"button\" class=\"noBg menuFooter menuFooterPrev\"><span class=\"icon-arrows-left\"></span></button>" +
        //             "<button title=\"Next\" type=\"button\" class=\"noBg menuFooter menuFooterNext\"><span class=\"icon-arrows-right\"></span></button>" +
        //         "</div></div></div>");

        // //below data is for global Search
        // strMenuHtml1.Append("<div classs='newMenuSearchWrapper' id='wrapperForSearch'>");
        // strMenuHtml1.Append("<div id=\"newMenuSearch\" class='search-form col-md-10 col-sm-10 col-xs-10'>");
        /// string strSearch = lblsearch.Text;
        // strMenuHtml1.Append("<input type='text' value='' id='globalSearchinp' tabindex='3' placeholder='" + strSearch + "' class='new-search-input search-input'/>");
        // strMenuHtml1.Append("<span title=\"Clear\" id='GSclearBtn' class='close glyphicon glyphicon-remove icon-arrows-remove' style='font-size: 21px;right: 1px;top: 5px;display:none;color:black;opacity:1!important;' tabindex='0'></span>");
        // strMenuHtml1.Append("<button type='button' style='font-size: 15px;color: black;cursor:default;' id='globalSearchBtn' class='search-button'>");
        // strMenuHtml1.Append("<i class='glyphicon glyphicon-search icon-basic-magnifier'></i>");
        // strMenuHtml1.Append("</button>");
        // strMenuHtml1.Append("<span id='GSclearBtn' class='close glyphicon glyphicon-remove icon-basic-remove' style='font-size: inherit;right: 1px;top: 8px;display:none; '></span>");
        // strMenuHtml1.Append("</div>");
        // strMenuHtml1.Append("</div>");
        // return strMenuHtml1.ToString();
        return string.Empty;
    }

    private void CreateMenuSellecterDropbox(XmlNode item)
    {
        if (item.HasChildNodes)
        {
            strMenuHtml1.Append("<li onclick=\"menuSellecter('" + item.Attributes["oname"].Value + "')\"><a id=\"" + item.Attributes["oname"].Value + "\" class=\"newMenuDropdown\" href=\"javascript:void(0)\">" + item.Attributes["name"].Value + "</a></li>");
        }
    }
    private void CreatePanelsWrapper(XmlNode item)
    {
        string hreflink = string.Empty;
        if (item.HasChildNodes)
        {
            strMenuHtml1.Append("<div class=\"panelsWrapper hideInClose " + item.Attributes["oname"].Value + "\" style=\"display: none;\">");
            for (int i = 0; i < item.ChildNodes.Count; i++)
            {
                if (item.ChildNodes[i].HasChildNodes)
                {
                    strMenuHtml1.Append("<div class=\"transactionsPanel\"><div class=\"panelHeading\">" + item.ChildNodes[i].Attributes["name"].Value + "</div><ul class=\"listUl\">");
                    for (int j = 0; j < item.ChildNodes[i].ChildNodes.Count; j++)
                    {
                        var target = item.ChildNodes[i].ChildNodes[j].Attributes["target"].Value;
                        if (target.Contains("iviewInteractive"))
                            target = target.Replace("iviewInteractive", "iview");
                        if (target.Contains("iview.aspx") || target.Contains("tstruct.aspx"))
                            hreflink = getSmallMenuName(target);
                        if (hreflink.Length > 0)
                        {
                            strMenuHtml1.Append("<li><a href=\"#" + hreflink + "\" onclick='LoadIframe(\"" + target + "\")'>" + item.ChildNodes[i].ChildNodes[j].Attributes["name"].Value + "</a></li>");
                        }
                        else
                        {
                            strMenuHtml1.Append("<li><a href=\"javascript:void(0)\" onclick='LoadIframe(\"" + target + "\")'>" + item.ChildNodes[i].ChildNodes[j].Attributes["name"].Value + "</a></li>");
                        }

                    }
                    strMenuHtml1.Append("</ul></div>");
                }
            }
            strMenuHtml1.Append("</div>");
        }
    }

    private void SessExpires()
    {
        string url = util.SESSEXPIRYPATH;
        Response.Write("<script>" + Constants.vbCrLf);
        Response.Write("parent.parent.location.href='" + url + "';");
        Response.Write(Constants.vbCrLf + "</script>");
    }

    private void GetGlobalVariables()
    {
        if (Session["traceStatus"] != null && Session["traceStatus"].ToString() != string.Empty)
            traceStatus = (string)Session["traceStatus"];
        if (Session["workflowEnabled"] != null)
            workflow = Convert.ToBoolean(Session["workflowEnabled"].ToString());
        if (Session["userAccessEnabled"] != null)
            userAccess = Convert.ToBoolean(Session["userAccessEnabled"].ToString());
        if (Session["AxAppTitle"] != null)
            appName = Session["AxAppTitle"].ToString();
        appTitle = appName;
        var axApp = UserOptions.Where(x => x.Key == "axAppName").ToList();
        if (axApp.Count > 0)
            UserOptions["axAppName"] = "\"appName\":\"" + appName + "\"♠\"onclick\":\"loadHome();\"";
        else
            UserOptions.Add("axAppName", "\"appName\":\"" + appName + "\"♠\"onclick\":\"loadHome();\"");

        Session.Add("SelectedLang", "");
        AxRole = Session["AxRole"].ToString();
        AxRole = CheckSpecialChars(AxRole);
        ViewState["AxRole"] = AxRole;
        proj = Session["project"].ToString();
        proj = CheckSpecialChars(proj);
        ViewState["proj"] = proj;
        sid = Session["nsessionid"].ToString();
        sid = CheckSpecialChars(sid);
        ViewState["sid"] = sid;
        user = Session["user"].ToString();
        user = CheckSpecialChars(user);
        ViewState["user"] = user;
        language = Session["language"].ToString();
        ViewState["language"] = language;

        if (Session["globalvarstring"] != null)
            strGlobalVar = Session["globalvarstring"].ToString();
    }
    private void SetGlobalVariables()
    {
        AxRole = ViewState["AxRole"].ToString();
        proj = ViewState["proj"].ToString();
        sid = ViewState["sid"].ToString();
        user = ViewState["user"].ToString();
        language = ViewState["language"].ToString();
    }
    private string CheckSpecialChars(string str)
    {
        str = Regex.Replace(str, "&", "&amp;");
        str = Regex.Replace(str, "<", "&lt;");
        str = Regex.Replace(str, ">", "&gt;");
        str = Regex.Replace(str, "'", "&apos;");
        str = Regex.Replace(str, "\"", "&quot;");
        string delimited = @"\\";
        str = Regex.Replace(str, delimited, ";bkslh");
        return str;
    }

    private void SetTheme()
    {
        if (Session["Thmcolor"] == null)
            Session["Thmcolor"] = "Blue";
        themeColor = Session["themeColor"].ToString();
        //Themes are written as script for direct access in the javascript
        regthmdata = "<script language=\"javascript\" type=\"text/javascript\" >";
        regthmdata += "var thmProj = '" + proj + "';var thmUser = '" + user + "';var thmRole = '" + AxRole + "';var thmSid = '" + sid + "';var thmSelected = '" + Session["Thmcolor"].ToString() + "';var g_lang='" + Session["language"].ToString() + "';";
        regthmdata += "</script>";
    }

    protected void ValidatePage()
    {
        string IsSSo = string.Empty;
        if (Session["AxSSO"] != null)
            IsSSo = Session["AxSSO"].ToString();

        string sXml = string.Empty;
        string ires = string.Empty;
        sid = Session.SessionID;
        bool IsProjSelected = false;

        //loginTrace
        string errlog = string.Empty;

        if (loginTrace.ToLower() == "true")
            errlog = logobj.CreateLog("Call to Login Web Service", sid, "login", "", "true");
        else
            errlog = logobj.CreateLog("Call to Login Web Service", sid, "login", "");

        proj = string.Empty;

        if (HttpContext.Current.Session["Project"] != null)
            proj = HttpContext.Current.Session["Project"].ToString();

        if (HttpContext.Current.Session["Project"] == null)
        {
            if (ConfigurationManager.AppSettings["proj"] != null && ConfigurationManager.AppSettings["proj"].ToString() != "")
                proj = ConfigurationManager.AppSettings["proj"].ToString();
            else
                proj = signinProj;
            Session["project"] = proj;
        }
        else if (!string.IsNullOrEmpty(proj) && Application["axApps"] != null)
        {
            Session["axApps"] = Application["axApps"];
            Application["axApps"] = null;
        }

        if (Session["axApps"] == null)
        {
            util.GetAxApps(proj);
            IsProjSelected = true;
        }
        axApps = Session["axApps"].ToString();
        Application[proj + "-axApps"] = Session["axApps"].ToString();
        //TO Get client information 
        string userDetails = GetBrowserDetails();
        string AxCloudDb = string.Empty;
        if (HttpContext.Current.Session["AxCloudDB"] != null)
            AxCloudDb = Session["AxCloudDB"].ToString();
        if (IsSSo == "true")
        {
            IIdentity winID = default(IIdentity);
            winID = HttpContext.Current.User.Identity;
            user = winID.Name;
            int indx = 0;
            indx = user.IndexOf("\\") + 1;
            user = user.Substring(indx, user.Length - indx);
            sXml = "<login clouddb='" + AxCloudDb + "' axpapp='" + proj + "' sessionid='" + sid + "' username='" + user + "' password='' sso='" + IsSSo + "' language='" + language + "' url='' direct='t'  trace='" + errlog + "'>";
            sXml = sXml + axApps + axProps + "</login>";
        }
        else
        {
            bool isMDHashValid = util.IsAlphaNum(pwd);
            if (!isMDHashValid)
                Response.Redirect(Constants.LOGINERR);

            if (user == null) Response.Redirect(Constants.LOGINERR);
            bool isUserValid = util.IsUserNameValid(user);
            if (!isUserValid)
                Response.Redirect(Constants.LOGINERR);

            bool isHashValid = util.IsHashValid(rnd_key);
            if (!isHashValid)
                Response.Redirect(Constants.LOGINERR);

            // language = Request.Form["language"];
            if (language != null)
                language = Regex.Replace(language, @"[^0-9a-zA-Z]+", "");
            if (string.IsNullOrEmpty(language))
                language = "ENGLISH";
            else
            {
                bool isLangValid = util.IsChar(language);
                if (!isLangValid)
                    Response.Redirect(Constants.LOGINERR);
            }

            //Adding the macid and licfile
            string licDetails = string.Empty;
            string macId = string.Empty;
            string licfile = string.Empty;

            if (Application["AxpString1"] != null)
            {
                string str1 = Application["AxpString1"].ToString();
                if (str1.StartsWith("<e>"))
                {
                    str1 = str1.Replace("<e>", "");
                    str1 = str1.Replace("</e>", "");
                    Response.Redirect(util.ACERRPATH + "17");
                }
                else
                {
                    macId = Application["AxpString1"].ToString();
                }
            }

            if (Application["AxpString2"] != null)
                licfile = Application["AxpString2"].ToString();

            licDetails = " macid='" + macId + "' licfile='" + licfile + "'";

            if (!string.IsNullOrEmpty(pwd))
            {
                string ipaddress = "";
                try
                {
                    ipaddress = util.GetIpAddress();
                }
                catch (Exception ex)
                {
                    ipaddress = "";
                    logobj.CreateLog("Fetching client IP address", sid, "login", "true", ex.Message);
                }

                string lang_attr = string.Empty;
                if (language.ToUpper() != "ENGLISH")
                    lang_attr = " lang=\"" + language + "\"";
                sXml = "<login clouddb='" + AxCloudDb + "' ip='" + ipaddress + "' other='" + userDetails + "'  seed='" + rnd_key + "'  axpapp='" + proj + "' sessionid='" + sid + "' username='" + user + "' password='" + pwd + "' url='' direct='t' trace='" + errlog + "' " + lang_attr + licDetails + ">";
                sXml = sXml + axApps + axProps + "</login>";
            }
        }

        string result = string.Empty;
        try
        {
            result = objWebServiceExt.CallLoginWS("main", sXml);
        }
        catch (Exception ex)
        {
            string strErrMsg = ex.Message;
            if (strErrMsg.ToLower().Contains("ora-"))
            {
                strErrMsg = "Error occurred(2). Please try again or contact administrator.";
            }
            else if (strErrMsg.Length > 50)
            {
                strErrMsg = strErrMsg.Substring(0, 50);
                strErrMsg += "...";
            }
            else
            {
                strErrMsg = ex.Message;
            }
            Response.Redirect(loginPath + "?msg=" + strErrMsg.Trim());
        }
        finally
        {
            if (cacKey != null && HttpContext.Current.Cache[cacKey] != null)
                HttpContext.Current.Cache.Remove(cacKey);
        }

        Request.Cookies.Remove("project");
        Response.Cookies["project"].Value = proj;
        Response.Cookies["project"].Expires = DateTime.Now.AddHours(1);

        if (result.StartsWith(Constants.ERROR) || result.Contains(Constants.ERROR))
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(result);
            string msg = string.Empty;
            XmlNode errorNode = xmldoc.SelectSingleNode("/error");
            if (result.Contains("\n"))
                result = result.Replace("\n", "");

            foreach (XmlNode msgNode in errorNode)
            {
                if (msgNode.Name == "msg")
                {
                    msg = msgNode.InnerText;
                    break;
                }
            }

            if (msg == string.Empty && errorNode.InnerText != string.Empty)
                msg = errorNode.InnerText;
            if (msg != string.Empty && msg.Contains("\n"))
                msg = msg.Replace("\n", "");
            //Unique Constraint Violation error
            if (msg.Contains("Duplicate entry") || msg.Contains("Violation of PRIMARY KEY constraint"))
            {
                Session["project"] = proj;
                Session["nsessionid"] = sid;
                Response.Redirect(HttpContext.Current.Application["SessExpiryPath"] + "?msg=" + msg);
            }
            else if (msg.ToLower().Contains("ora-"))
            {
                msg = "Error occurred(2). Please try again or contact administrator.";
                Response.Redirect(loginPath + "?msg=" + msg);
            }
            else
                Response.Redirect(loginPath + "?msg=" + msg);
        }
        else
        {
            Session["project"] = proj;
            Session["user"] = user;
            Session["username"] = user;
            thmProj = proj;
            thmUser = user;
            thmSid = sid;
            Session["pwd"] = pwd;
            Session["nsessionid"] = sid;
            Session["validated"] = "True";
            Session["language"] = language;
            Session["axp_language"] = language.ToLower();

            try
            {
                if (Session["FDR"] == null)
                {
                    FDR fObj = new FDR();
                    fObj.schemaNameKey = Session["dbuser"].ToString();
                    Session["FDR"] = fObj;
                }
            }
            catch (Exception ex)
            {

            }

            if (!string.IsNullOrEmpty(language))
                Session["language"] = language;
            else
                Session["language"] = string.Empty;
            CheckResultFormat(result);

            if (isResultXml)
            {

                ParseLoginResult(result);

                var loggedUserList = new List<string>();
                loggedUserList = util.GetUserList(proj);
                string newUser = string.Empty;
                if (Session["transidlist"] != null)
                {
                    string licType = string.Empty, transidlist = string.Empty;
                    if (Session["transidlist"].ToString().Contains('~'))
                        transidlist = Session["transidlist"].ToString().Split('~')[3];
                    if (transidlist == "9867")
                    {
                        licType = "unlimited";
                        Session["lictype"] = "unlimited";
                    }
                    else
                    {
                        licType = "limited";
                        Session["lictype"] = "limited";
                    }
                    newUser = user + "♦" + sid + "♣" + licType;
                }
                else
                    newUser = user + "♦" + sid + "♣";
                loggedUserList.Add(newUser);
                util.SetUserList(proj, loggedUserList);

                if (manage == true || Session["AxRole"].ToString() == "default_")
                {
                    traceStatus = "T";
                    Session["traceStatus"] = traceStatus;
                    util.sysErrorlog = true;
                    logobj.errorlog = "true";
                    errlog = "true";
                }
                else
                {
                    Session["AxTrace"] = "false";
                }

                if ((forcePwdChange == "1" && Session["AxCPWDOnLogin"] != null && Session["AxCPWDOnLogin"].ToString() == "true") || forcePwdChange == "2" || forcePwdChange == "3")
                {
                    Session["validated"] = "";
                    Response.Redirect("../aspx/cpwd.aspx?remark=" + forcePwdChange);
                }
            }
            else
            {
                ParseStringResult(result);

            }

            // user selected application from app dropdown
            if (IsProjSelected)
            {
                // onApp event datatables will be created in redis for the selected(dropdown value) application                 
                fdwObj.Initialize(proj);
                //This function will create datasets for Login event
                CreateFDOnLogin(fdwObj);
            }
            else
            {
                //This function will create datasets for Login event     
                CreateFDOnLogin(fdwObj);
            }

            Session["CSRFToken"] = "";
            string cookieName = "CSRFToken";
            Response.Cookies.Add(new System.Web.HttpCookie(cookieName, ""));
            if (Request.Cookies[cookieName] != null)
            {
                Response.Cookies[cookieName].Value = string.Empty;
                Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-20);
            }
        }
    }

    protected void loginFormValues()
    {
        for (int i = 0; i < Request.QueryString.Count; i++)
        {
            strParamsRF.Append("<input type=hidden name=" + Request.QueryString.Keys[i].ToString() + " value=" + Request.QueryString[i].ToString() + ">");
        }
    }

    protected void loginFormValuesDelete()
    {
        strParamsRF.Append("");
    }

    protected bool CheckIsUserLoggedIn()
    {
        string PrevProj = string.Empty;
        if (HttpContext.Current.Session["Project"] != null)
            PrevProj = HttpContext.Current.Session["Project"].ToString();

        if (HttpContext.Current.Session["Project"] == null)
        {
            if (ConfigurationManager.AppSettings["proj"] != null && ConfigurationManager.AppSettings["proj"].ToString() != "")
                PrevProj = ConfigurationManager.AppSettings["proj"].ToString();
            else
                PrevProj = signinProj;
        }
        string IsSSo = string.Empty;
        string existSid = string.Empty, existUser = string.Empty, existLic = string.Empty;
        if (Session["AxSSO"] != null)
            IsSSo = Session["AxSSO"].ToString();
        if (IsSSo == "true")
        {
            IIdentity winID = default(IIdentity);
            winID = HttpContext.Current.User.Identity;
            existUser = winID.Name;
            int indx = 0;
            indx = existUser.IndexOf("\\") + 1;
            existUser = existUser.Substring(indx, existUser.Length - indx);
        }
        else
        {
            if (Request.QueryString["rn"] != null)
                existUser = Request.QueryString["rn"].ToString();
            else
                existUser = user;
        }
        string checkValue = string.Empty;
        var loggedUserList = new List<string>();
        loggedUserList = util.GetUserList(PrevProj);
        if (loggedUserList != null)
            checkValue = string.Join(",", loggedUserList.Select(x => x.ToString()).ToList());
        if (loggedUserList != null && checkValue.IndexOf(existUser + "♦") != -1 && Session["loggedUserDetails"] == null)
        {
            var prevValue = loggedUserList.AsEnumerable().Where(x => x.Contains(existUser + "♦")).ToList();
            Session["loggedUserDetails"] = prevValue[0];
            loginFormValues();
            ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:CheckIsUserLogged();", true);
            Session["AxInternalRefresh"] = "true";
            return true;
        }
        else if (Session["loggedUserDetails"] != null)
        {
            if (Session["axApps"] == null)
            {
                util.GetAxApps(PrevProj);
            }
            existSid = Session["loggedUserDetails"].ToString().Split('♦')[1].Split('♣')[0];
            existLic = Session["loggedUserDetails"].ToString().Split('♣')[1];
            if (existLic == "limited")
            {
                string errorLog = logobj.CreateLog("Calling Signout ws", existSid, "Signout", "new");
                string iXml = string.Empty;
                iXml = "<root axpapp='" + PrevProj + "' sessionid='" + existSid + "' trace='" + errorLog + "'>";
                if (Session["axApps"] != null)
                    iXml += Session["axApps"].ToString() + HttpContext.Current.Application["axProps"].ToString();
                iXml += "</root>";
                string result = string.Empty;
                ASBExt.WebServiceExt asbExt = new ASBExt.WebServiceExt();
                result = asbExt.CallLogoutNewWS("Signout", iXml);

                string scriptsPath = HttpContext.Current.Application["ScriptsPath"].ToString();
                scriptsPath = scriptsPath + "Axpert\\" + existSid;
                if (Directory.Exists(scriptsPath) && existSid != "")
                {
                    try
                    {
                        Directory.Delete(scriptsPath, true);
                    }
                    catch (Exception Ex)
                    {
                        //Do nothing
                    }
                }
            }
            loggedUserList.Remove(existUser + "♦" + existSid + "♣" + existLic);
            util.SetUserList(PrevProj, loggedUserList);
            Session["loggedUserDetails"] = null;
            ValidatePage();
            loginFormValuesDelete();
            return false;
        }
        else
        {
            ValidatePage();
            return false;
        }
    }

    private void GetLoginDtlsFromCache()
    {
        bool isLoginKeyValid = false;
        string key = Request.Form["key"];
        logobj.CreateLog("GetLoginDtlsFromCache-key- " + key + ",Sessionid=" + sid, sid, "GetLoginDtlsFromCache-key", "new", "true");
        cacKey = key;
        try
        {
            if (key == null && Session["AxInternalRefresh"] != null && Session["AxInternalRefresh"].ToString() == "true")
            {
                logobj.CreateLog("Internal Refresh-" + Session["AxInternalRefresh"], sid, "GetLoginDtlsFromCache-key", "", "true");
                isLoginKeyValid = true;
                signinProj = Session["Project"].ToString();
                user = Session["username"].ToString();
                pwd = Session["pwd"].ToString();
                rnd_key = Session["rnd_key"].ToString();
                language = Session["language"].ToString();
                Session["AxInternalRefresh"] = null;
            }
            else if (key != "")
            {
                logobj.CreateLog("First time login with key- " + key, sid, "GetLoginDtlsFromCache-key", "", "true");
                string loginDtls = HttpContext.Current.Cache.Get(key).ToString();
                logobj.CreateLog("Cache details for key- " + loginDtls, sid, "GetLoginDtlsFromCache-key", "", "true");
                if (loginDtls != string.Empty)
                {
                    if (HttpContext.Current.Cache[key] != null)
                        HttpContext.Current.Cache.Remove(key);
                    isLoginKeyValid = true;
                    string[] dtls = loginDtls.Split('~');
                    signinProj = dtls[0].ToString();
                    user = dtls[1].ToString();
                    pwd = dtls[2].ToString();
                    rnd_key = dtls[3].ToString();
                    language = dtls[4].ToString();
                    logobj.CreateLog("Cache details Session start- " + isLoginKeyValid, sid, "GetLoginDtlsFromCache-key", "", "true");
                    Session["user"] = user;
                    Session["username"] = user;
                    Session["pwd"] = pwd;
                    Session["nsessionid"] = sid == string.Empty ? Session.SessionID : sid;
                    Session["language"] = language;
                    Session["rnd_key"] = rnd_key;
                    logobj.CreateLog("Cache details Session end- " + isLoginKeyValid, sid, "GetLoginDtlsFromCache-key", "", "true");
                }
            }
        }
        catch (Exception ex)
        {
            logobj.CreateLog("Exception while getting session details from cache- " + ex.Message, sid, "GetLoginDtlsFromCache-exp", "new", "true");
        }
        finally
        {
            if (!isLoginKeyValid && !string.IsNullOrEmpty(key))
            {
                string lpage = string.IsNullOrEmpty(Application["LoginPath"].ToString()) ? "../aspx/signin.aspx" : Application["LoginPath"].ToString();
                Response.Redirect(lpage + "?msg=Error while login, please try again.");
            }
            else if (!isLoginKeyValid)
                Response.Redirect(util.SESSEXPIRYPATH);
        }
    }

    public void CreateFDOnLogin(FDW fdwObj)
    {
        string schemaName = string.Empty;
        if (HttpContext.Current.Session["dbuser"] != null)
            schemaName = HttpContext.Current.Session["dbuser"].ToString();
        if (Session["username"] != null && Session["username"].ToString().ToLower() == "admin")
            fdwObj.GetAxRelations(schemaName);//If anything updated in AxRelations table either directly or indirectly needs to login admin once in web.
    }

    private void ParseStringResult(string result)
    {
        string[] themeres = result.Split(',');
        if (themeres.Length > 1)
        {
            themeColor = themeres[0].ToString();
            Session["themeColor"] = themeres[0].ToString();
            //Themes are written as script for direct access in the javascript
            regthmdata = "<script language=\"javascript\" type=\"text/javascript\" >";
            regthmdata += "var thmProj = '" + proj + "';var thmUser = '" + user + "';var thmRole = '" + AxRole + "';var thmSid = '" + sid + "';var thmSelected = '" + themeres[0].ToString() + "';var g_lang='" + Session["language"].ToString() + "';";
            regthmdata += "</script>";
            result = themeres[1].ToString();
        }
        else
        {
            Session["themeColor"] = "Gray";
            Session["themeColor"] = "Gray";
            regthmdata = "<script language=\"javascript\" type=\"text/javascript\" >";
            regthmdata += "var thmProj = '" + proj + "';var thmUser = '" + user + "';var thmRole = '" + AxRole + "';var thmSid = '" + sid + "';var thmSelected = '" + "Blue" + "';var g_lang='" + Session["language"].ToString() + "';";
            regthmdata += "</script>";
            result = themeres[0].ToString();
        }

        string[] splitout = result.Split('~');
        Session["AxRole"] = splitout[1].ToString().Substring(0, splitout[1].ToString().Length - 1);
        string[] reg_gv = result.Split('#');
        if (reg_gv.Length > 0)
        {
            global_vars = reg_gv[1].ToString();
            string[] glo_Split = global_vars.Split('^');
            string globalApp_vars = glo_Split[0].ToString();
            globalUsr_vars = null;
            if (glo_Split.Length > 1)
            {
                globalUsr_vars = glo_Split[1].ToString();
            }
            else
            {
                globalUsr_vars = "";
            }
            Session["global_vars"] = global_vars;
            Session["globalUsr_vars"] = globalUsr_vars;
            CreateGlobalVars(globalApp_vars);
        }


        //Build, firstTimeLogin,Manage,Workflow

        Session["Build"] = splitout[0].ToString().Substring(0, 1);
        if (Convert.ToString(Session["Build"]) == "T")
            build = true;

        if (splitout[0].Substring(2, 1) == "T")
        {
            manage = true;
        }

        if (splitout[0].Substring(2, 1) == "T")
        {
            userAccess = true;
            Session["userAccessEnabled"] = true;
        }

        if (splitout[0].Substring(3, 1) == "T")
        {
            workflow = true;
            Session["workflowEnabled"] = workflow;
        }

        if (splitout[0].Length > 4)
        {
            if (splitout[0].Substring(4, 1) == "T")
            {
                import = true;
                Session["ImportAccess"] = import;
            }
            if (splitout[0].Substring(5, 1) == "T")
            {
                export = true;
                Session["ExportAccess"] = import;
            }

            if (import)
            {
                history = true;
                Session["HistoryAccess"] = history;
            }
        }


        if (manage == true || Session["AxRole"].ToString() == "default_")
        {
            traceStatus = "T";
            Session["traceStatus"] = traceStatus;
            util.sysErrorlog = true;
            logobj.errorlog = "true";
        }
        else
        {
            Session["AxTrace"] = "false";
        }

        if (splitout[0].Substring(1, 1) == "T" && axChangePwdReDir == true)
        {
            Session["validated"] = "";
            Response.Redirect("../aspx/cpwd.aspx?remark=firsttime");
        }

    }

    private void CreateGlobalVars(string globalApp_vars)
    {
        Session["globalvarstring"] = string.Empty;
        StringBuilder paramlist = new StringBuilder();
        int paramno = 0;

        gloApp_Split = globalApp_vars.Split('~');
        Session["paramstring"] = global_vars;
        if (gloApp_Split.Length > 0)
        {
            int glo_inx = 0;
            for (glo_inx = 1; glo_inx <= gloApp_Split.Length - 1; glo_inx++)
            {
                int indx = gloApp_Split[glo_inx].ToString().IndexOf("=");
                string leftreg_glovar = gloApp_Split[glo_inx].ToString().Substring(0, indx);
                string rightreg_glovar = gloApp_Split[glo_inx].Substring(indx + 1);
                paramlist.Append("Parameters[" + paramno + "] = " + "\"" + leftreg_glovar + "~" + CheckSpecialChars(rightreg_glovar) + "\";  ");
                paramno = paramno + 1;
                string nSessname = leftreg_glovar.ToString();
                string nSessvalue = rightreg_glovar.ToString();
                Session[nSessname] = nSessvalue;
            }
        }


        string[] gloUsr_Split = globalUsr_vars.Split('~');
        if (gloUsr_Split.Length > 0)
        {
            int glo_inx1 = 0;
            for (glo_inx1 = 1; glo_inx1 <= gloUsr_Split.Length - 1; glo_inx1++)
            {
                int indx1 = gloUsr_Split[glo_inx1].ToString().IndexOf("=");
                string leftreg_glovar1 = gloUsr_Split[glo_inx1].ToString().Substring(0, indx1);
                string rightreg_glovar1 = gloUsr_Split[glo_inx1].ToString().Substring(indx1 + 1);
                paramlist.Append("Parameters[" + paramno + "] = " + "\"" + leftreg_glovar1 + "~" + rightreg_glovar1 + "\";  ");
                paramno = paramno + 1;
                string nSessname1 = leftreg_glovar1.ToString();
                string nSessvalue1 = rightreg_glovar1.ToString();
                Session[nSessname1] = nSessvalue1;
            }
        }

        string dtCulture = Request.UserLanguages[0];

        //If useCulture flag is false , we are not checking the browser culture.
        //If it's true ,we are setting the culture based on the client browser language.
        if (Session["AxUSCulture"] != null)
            useCulture = Convert.ToBoolean(Session["AxUSCulture"].ToString());
        if (!useCulture)
            dtCulture = "en-gb";
        else
        {
            if (dtCulture.ToLower() == "en" || dtCulture.ToLower() == "en-us")
                dtCulture = "en-us";
        }
        paramlist.Append("Parameters[" + paramno + "] = " + "\"username~" + user + "\";  ");
        paramlist.Append("Parameters[" + (paramno + 1) + "] = " + "\"Culture~" + dtCulture + "\";  ");

        Session["ClientLocale"] = dtCulture;
        glCulture = dtCulture;
        if (paramlist.ToString() != string.Empty)
        {
            Session["globalvarstring"] = paramlist.ToString();
        }
    }

    private void CheckResultFormat(string result)
    {
        if (result.TrimStart().StartsWith("<") && result.TrimEnd().EndsWith(">"))
            isResultXml = true;
        else
            isResultXml = false;
    }

    private string GetBrowserDetails()
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        string userDetails = browser.Type + "¿" + browser.Browser + "¿"
            + browser.Version + "¿" + browser.MajorVersion + "¿"
            + browser.MinorVersion + "¿" + browser.Platform + "¿"
            + Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"];

        if (userDetails.Length > 200)
            userDetails = userDetails.Substring(0, 200);

        return userDetails;
    }

    /// <summary>
    /// Function to parse the login result xml and set the access properties and global variables
    /// </summary>
    /// <param name="result"></param>
    private void ParseLoginResult(string result)
    {
        StringBuilder globalVarToShow = new StringBuilder();
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(result);

        XmlNode resultNode = xmlDoc.SelectSingleNode("/result");
        if (resultNode.Attributes["theme"] != null)
            ApplyTheme(resultNode.Attributes["theme"].Value);
        if (resultNode.Attributes["nickname"] != null)
            Session["AxNickName"] = resultNode.Attributes["nickname"].Value;
        if (resultNode.Attributes["email_id"] != null)
            UserOptions.Add("AxUserEmail", "\"display\":\"block\"♠\"email\":\"" + resultNode.Attributes["email_id"].Value + "\"");
        else
            UserOptions.Add("AxUserEmail", "\"display\":\"block\"♠\"email\":\"\"");

        //displaying an icon(next to setting menu) - to display license related warning messages onclick 
        if (resultNode.Attributes["licmsg"] != null)
        {
            string licInfo = resultNode.Attributes["licmsg"].Value;
            liLicInfo.Visible = true;
            aLicinfo.Attributes.Add("onclick", "showAlertDialog('info','" + licInfo + "')");
            UserOptions.Add("LicInfo", "\"display\":\"block\"♠\"onclick\":\"showAlertDialog('info', '" + licInfo + "')\"♠\"title\":\"License Infomation\"");
        }
        else
            UserOptions.Add("LicInfo", "\"display\":\"none\"♠\"onclick\":\"showAlertDialog('info', '')\"♠\"title\":\"License Infomation\"");

        //Below code checks if there is a global variable for 'nickname', if defined then the display name will be nickname
        if (Session["AxNickName"] == null || Session["AxNickName"].ToString() == string.Empty)
            Session["AxNickName"] = Session["user"];

        if (Session["AxNickName"] != null)
            UserOptions.Add("AxNickName", "\"NickName\":\"" + Session["AxNickName"].ToString() + "\"");

        if (resultNode.Attributes["axglo"] != null)
        {
            string axGlovalue = resultNode.Attributes["axglo"].Value;
            if (axGlovalue.IndexOf("~") != -1)
            {
                string[] AxGloFld = axGlovalue.Split('~');
                Session["AxGloFldCount"] = AxGloFld[0];
                paramTransid = AxGloFld[1];
            }
            else
            {
                Session["AxGloFldCount"] = axGlovalue;
                paramTransid = "axglo";
            }
        }

        foreach (XmlNode childNode in resultNode.ChildNodes)
        {
            if (childNode.Name == "globalvars")
            {
                //storing config app global fields in axGlobalVars
                string axGridAttachPath = string.Empty, axAttachmentpath = string.Empty;
                if (Session["AxGridAttachPath"] != null && Session["AxGridAttachPath"].ToString() != "")
                {
                    if (childNode.SelectSingleNode("AXPIMAGEPATH") != null)
                    {
                        childNode.SelectSingleNode("AXPIMAGEPATH").InnerText = Session["AxGridAttachPath"].ToString();
                        axGridAttachPath = "";
                    }
                    else
                    {
                        axGridAttachPath = Session["AxGridAttachPath"].ToString();
                        axGridAttachPath = "<AXPIMAGEPATH>" + axGridAttachPath + "</AXPIMAGEPATH>";
                    }
                }
                if (Session["AxAttachFilePath"] != null && Session["AxAttachFilePath"].ToString() != "")
                {
                    if (childNode.SelectSingleNode("AXPATTACHMENTPATH") != null)
                    {
                        childNode.SelectSingleNode("AXPATTACHMENTPATH").InnerText = Session["AxAttachFilePath"].ToString();
                        axAttachmentpath = "";
                    }
                    else
                    {
                        axAttachmentpath = Session["AxAttachFilePath"].ToString();
                        axAttachmentpath = "<AXPATTACHMENTPATH>" + axAttachmentpath + "</AXPATTACHMENTPATH>";
                    }
                }
                Session["axGlobalVars"] = "<globalvars>" + childNode.InnerXml + axGridAttachPath + axAttachmentpath + "</globalvars>";
                foreach (XmlNode xmlNode in childNode.ChildNodes)
                {
                    if (xmlNode.Name.ToLower() == "axp_lockonread" && (xmlNode.InnerText.ToLower() == "t" || xmlNode.InnerText.ToLower() == "true"))
                        isLockOnRead = true;
                    globalVariables.Add(xmlNode.Name + "=" + xmlNode.InnerText);
                    if (xmlNode.Name.ToLower() == "axglo_hide")
                        Session["AxGloHideShow"] = xmlNode.InnerText;
                    if (xmlNode.Name.ToLower() == "axglo_recordid")
                        Session["AxGloRecId"] = xmlNode.InnerText;
                    if (xmlNode.Name.ToLower() == "axp_displaytext")
                    {
                        string sbDspText = string.Empty;
                        if (Session["appname"] != null)
                        {
                            sbDspText = "<span class='globalProjname'>" + Session["appname"].ToString() + "</span> ";
                        }
                        sbDspText += FormatGlobalVariables(xmlNode.InnerText);
                        if (sbDspText != "")
                            globalVarToShow.Append(sbDspText.ToString());
                    }
                    if (xmlNode.Name.ToLower() == "rolename")
                        Session["AxRole"] = xmlNode.InnerText;
                    if (xmlNode.Name.ToLower() == "responsibilies")
                        Session["AxResponsibilities"] = xmlNode.InnerText;

                    if (xmlNode.Name.ToLower() == "axpimagepath")
                        Session["AxpImagePathGbl"] = xmlNode.InnerText;
                    if (xmlNode.Name.ToLower() == "axpimageserver")
                        Session["AxpImageServerGbl"] = xmlNode.InnerText;
                    if (xmlNode.Name.ToLower() == "axpattachmentpath")
                        Session["AxpAttachmentPathGbl"] = xmlNode.InnerText;
                }
            }
            else if (childNode.Name == "uservars")
            {
                Session["axUserVars"] = "<uservars>" + childNode.InnerXml + "</uservars>";
                foreach (XmlNode xmlNode in childNode.ChildNodes)
                    userVariables.Add(xmlNode.Name + "=" + xmlNode.InnerText);
            }
            else if (childNode.Name == "access")
            {
                SetUserAccess(childNode);
            }
            // for getting application title from Webservice Result.
            else if (childNode.Name == "name")
            {
                Session["projTitle"] = childNode.InnerText;
                if (Session["AxAppTitle"] != null && Session["AxAppTitle"].ToString() == "") Session["AxAppTitle"] = childNode.InnerText;
            }
            else if (childNode.Name == "pwdconf")
            {
                foreach (XmlNode xmlNode in childNode.ChildNodes)
                {
                    if (xmlNode.Name == "minpwdchars")
                    {
                        Session["minPwdChars"] = xmlNode.InnerText;
                    }
                    else if (xmlNode.Name == "pwdalphanumeric")
                    {
                        Session["IsPwdAlphaNumeric"] = xmlNode.InnerText;
                    }
                }
            }

        }

        if (globalVariables.Count > 0 || userVariables.Count > 0)
        {
            CreateParamaterArray();
        }
        // Below is used to display the saved Global Variables.
        string url = string.Empty;
        string AxGloHideShow = string.Empty;
        if (Session["AxGloHideShow"] != null && Session["AxGloHideShow"].ToString() != "")
            AxGloHideShow = Session["AxGloHideShow"].ToString();
        string AxGloRecId = string.Empty;
        if (Session["AxGloRecId"] != null && Session["AxGloHideShow"] != "")
            AxGloRecId = Session["AxGloRecId"].ToString();
        if (AxGloRecId != string.Empty)
            url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "&recordid=" + AxGloRecId + "";
        else
            url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "";

        editAxGlo2.Attributes.Add("onclick", "LoadIframe('" + url + "');");
        var AxGlo = UserOptions.Where(x => x.Key == "GlobalParamsonclick").ToList();
        if (AxGlo.Count > 0)
            UserOptions["GlobalParamsonclick"] = "\"onclick\":\"LoadIframe('" + url + "')\"";
        else
            UserOptions.Add("GlobalParamsonclick", "\"onclick\":\"LoadIframe('" + url + "')\"");
        if (globalVarToShow.Length > 100)
        {
            string globalValuesShown = string.Empty;
            globalValuesShown = globalVarToShow.ToString();
            globalVarToShow.Remove(100, ((globalValuesShown.Length - 1) - 100));
            globalVarToShow.Append("<a  onclick='javascript:ShowMore(\"" + globalValuesShown + "\");'> More</a>");
        }
        hdParamsValues.Value = globalVarToShow.ToString();
    }

    /// <summary>
    /// The user access properties will be set for managing the application.
    /// </summary>
    /// <param name="childNode"></param>
    private void SetUserAccess(XmlNode childNode)
    {
        Session["Build"] = "F";
        Session["forcePwdChange"] = "0";
        Session["userAccessEnabled"] = false;
        Session["workflowEnabled"] = false;
        Session["ImportAccess"] = false;
        Session["ExportAccess"] = false;
        Session["pwdExpiresIn"] = "0";

        foreach (XmlNode xmlNode in childNode.ChildNodes)
        {
            if (xmlNode.Name == "build" && xmlNode.InnerText == "T")
            {
                Session["Build"] = xmlNode.InnerText;
                build = true;
                manage = true;
            }
            else if (xmlNode.Name == "forcepwdchange")
            {
                forcePwdChange = xmlNode.InnerText;
                Session["forcePwdChange"] = xmlNode.InnerText;
            }
            else if (xmlNode.Name == "users" && xmlNode.InnerText == "T")
            {
                userAccess = true;
                Session["userAccessEnabled"] = true;
            }
            else if (xmlNode.Name == "workflow" && xmlNode.InnerText == "T")
            {
                workflow = true;
                Session["workflowEnabled"] = workflow;
            }
            else if (xmlNode.Name == "import" && xmlNode.InnerText == "T")
            {
                import = true;
                Session["ImportAccess"] = import;
            }
            else if (xmlNode.Name == "export" && xmlNode.InnerText == "T")
            {
                export = true;
                Session["ExportAccess"] = import;
            }
            else if (xmlNode.Name == "sessionexists")
            {
                // not in use - To display a message to the user that the previous session is expired.
            }
            else if (xmlNode.Name == "pwdexpiresin")
            {
                Session["pwdExpiresIn"] = xmlNode.InnerText;
                pwdExpDays = Convert.ToInt32(Session["pwdExpiresIn"]);
            }
        }
        if (import || export)
        {
            history = true;
            Session["HistoryAccess"] = history;
        }
    }

    /// <summary>
    /// Function to apply the theme from the login result for the logged in user, Default theme will be blue.
    /// </summary>
    /// <param name="theme"></param>
    private void ApplyTheme(string theme)
    {
        if (theme != string.Empty)
        {
            themeColor = theme;
            Session["themeColor"] = theme;
            regthmdata = "<script language=\"javascript\" type=\"text/javascript\" >";
            regthmdata += "var thmProj = '" + proj + "';var thmUser = '" + user + "';var thmRole = '" + AxRole + "';var thmSid = '" + sid + "';var thmSelected = '" + theme + "';var g_lang='" + Session["language"].ToString() + "';";
            regthmdata += "</script>";

        }
        else
        {
            themeColor = "Default";
            Session["themeColor"] = "Gray";
            regthmdata = "<script language=\"javascript\" type=\"text/javascript\" >";
            regthmdata += "var thmProj = '" + proj + "';var thmUser = '" + user + "';var thmRole = '" + AxRole + "';var thmSid = '" + sid + "';var thmSelected = '" + "Blue" + "';var g_lang='" + Session["language"].ToString() + "';";
            regthmdata += "</script>";
        }
    }

    /// <summary>
    /// Function to create the Parameter array scripts for javascript and stores in the session variable Session["globalvarstring"]
    /// </summary>
    private void CreateParamaterArray()
    {
        StringBuilder paramlist = new StringBuilder();
        string value = string.Empty;
        int paramCnt = 0;

        for (int i = 0; i < globalVariables.Count; i++)
        {
            string[] strVar = globalVariables[i].ToString().Split('=');
            value = CheckSpecialChars(strVar[1].ToString());
            paramlist.Append("Parameters[" + i + "] = " + "\"" + strVar[0] + "~" + value + "\";  ");
            if (strVar[0].ToString() == "project" && value.Contains(','))
                Session[strVar[0].ToString()] = value.Split(',')[1];
            else
                Session[strVar[0].ToString()] = value;
            if (isRapidLoad)
                Session["axGloVars_" + strVar[0].ToString()] = value;
        }
        for (int j = 0; j < userVariables.Count; j++)
        {
            string[] strVar = userVariables[j].ToString().Split('=');
            value = CheckSpecialChars(strVar[1].ToString());
            paramlist.Append("Parameters[" + j + "] = " + "\"" + strVar[0] + "~" + value + "\";  ");
            Session[strVar[0].ToString()] = value;
            if (isRapidLoad)
                Session["axGloVars_" + strVar[0].ToString()] = value;
        }

        Session["globalVarsArray"] = globalVariables;
        Session["userVarsArray"] = userVariables;

        string dtCulture = Request.UserLanguages[0];
        //If useCulture flag is false , we are not checking the browser culture.
        //If it's true ,we are setting the culture based on the client browser language.
        if (Session["AxUSCulture"] != null)
            useCulture = Convert.ToBoolean(Session["AxUSCulture"].ToString());
        if (!useCulture)
            dtCulture = "en-gb";
        else
        {
            if (dtCulture.ToLower() == "en" || dtCulture.ToLower() == "en-us")
                dtCulture = "en-us";
        }
        paramCnt = globalVariables.Count + userVariables.Count;
        paramlist.Append("Parameters[" + paramCnt + "] = " + "\"username~" + user + "\";  ");
        paramlist.Append("Parameters[" + (paramCnt + 1) + "] = " + "\"Culture~" + dtCulture + "\";  ");

        Session["ClientLocale"] = dtCulture;
        glCulture = dtCulture;
        if (paramlist.ToString() != string.Empty)
        {
            Session["globalvarstring"] = paramlist.ToString();
        }
    }

    /// <summary>
    /// Function to return the param value from the global variables array.
    /// </summary>
    /// <param name="paramName"></param>
    /// <returns></returns>

    public void createTopLinks()
    {

        if (userAccess || workflow || export || import)
        {
            Boolean useWorkflowScript = false;
            if (ConfigurationManager.AppSettings["WorkflowScript"] != null)
                useWorkflowScript = Convert.ToBoolean(ConfigurationManager.AppSettings["WorkflowScript"].ToString());
        }
        if (build)
        {
            li_Trace1.Visible = true;
            UserOptions.Add("showLog", "\"display\":\"block\"♠\"onclick\":\"showTraceFileDialog()\"♠\"title\":\"Show Logs\"");
            UserOptions.Add("auditReport", "\"display\":\"block\"♠\"onclick\":\"LoadIframe('iview.aspx?ivname=auditlog')\"♠\"title\":\"Audit Report\"");

        }
        else
        {
            UserOptions.Add("showLog", "\"display\":\"none\"♠\"onclick\":\"showTraceFileDialog()\"♠\"title\":\"Show Logs\"");
            UserOptions.Add("auditReport", "\"display\":\"none\"♠\"onclick\":\"LoadIframe('iview.aspx?ivname=auditlog')\"♠\"title\":\"Audit Report\"");
        }
        if (AxRole.Contains("default"))
        {
            li_WidgetBuilder1.Visible = true;
            UserOptions.Add("WidgetBuilder", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('WidgetBuilder')\"♠\"title\":\"Widget Builder\"");
            li_Responsibilities1.Visible = true;
            UserOptions.Add("Responsibilities", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('Responsibilities')\"♠\"title\":\"Responsiblity\"");
        }
        else
        {
            UserOptions.Add("WidgetBuilder", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('WidgetBuilder')\"♠\"title\":\"Widget Builder\"");
            UserOptions.Add("Responsibilities", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('Responsibilities')\"♠\"title\":\"Responsiblity\"");
        }

        if (export)
        {
            li_ExportData1.Visible = true;
            UserOptions.Add("ExportData", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('ExportData')\"♠\"title\":\"Export Data\"");
        }
        else
        {
            li_ExportData1.Visible = false;
            UserOptions.Add("ExportData", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('ExportData')\"♠\"title\":\"Export Data\"");
        }

        if (import)
        {
            li_ImportData1.Visible = li_ImportHistory1.Visible = true;
            UserOptions.Add("ImportData", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('ImportData')\"♠\"title\":\"Import Data\"");
            UserOptions.Add("ImportHistory", "\"display\":\"block\"♠\"onclick\":\"DoUtilitiesEvent('ImportHistory')\"♠\"title\":\"Import History\"");
        }
        else
        {
            li_ImportData1.Visible = li_ImportHistory1.Visible = false;
            UserOptions.Add("ImportData", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('ImportData')\"♠\"title\":\"Import Data\"");
            UserOptions.Add("ImportHistory", "\"display\":\"none\"♠\"onclick\":\"DoUtilitiesEvent('ImportHistory')\"♠\"title\":\"Import History\"");
        }
    }

    //Functions for Global Variable
    protected void btnSetParams_Click(object sender, EventArgs e)
    {
        string showHideAxGlo = string.Empty;
        string paramValue = hdParamsValues.Value.ToString();
        string decodedParam = HttpContext.Current.Server.UrlDecode(paramValue);

        string encodedDispTxt = hdnDisplayTxt.Value.ToString();
        string formatedDispTxt = HttpContext.Current.Server.UrlDecode(encodedDispTxt);
        hdnDisplayTxt.Value = CheckSpecialChars(formatedDispTxt);
        string tstGloVal = decodedParam;
        string[] arrTstGlo = tstGloVal.Split('¿');    //Array of modified values
        string sbDspText = string.Empty;
        string[] strGloVar = new string[arrTstGlo.Length];
        string value = string.Empty;
        string[] myValues = new string[arrTstGlo.Length];
        StringBuilder strglobalValues = new StringBuilder();
        ArrayList userVar = new ArrayList();
        globalVariables = (ArrayList)Session["globalVarsArray"];
        userVariables = (ArrayList)Session["userVarsArray"];
        //isLoggedIn = true; // This is for forcing the user to save Global vars if he doesnot have.
        Session["AxGloHideShow"] = "T";
        if (globalVariables == null)
            globalVariables = new ArrayList();
        if (userVariables == null)
            userVariables = new ArrayList();
        for (int i = 0; i < arrTstGlo.Length; i++)
        {
            string[] myStrVar = arrTstGlo[i].ToString().Split('♣');

            if (myStrVar.Length > 1)
            {
                myValues[i] = CheckSpecialChars(myStrVar[1].ToString());
            }
            strGloVar[i] = myStrVar[0];
            userVar.Add(strGloVar[i]);
            for (int j = 0; j < globalVariables.Count; j++)
            {
                bool flag = false;
                string[] strVar = globalVariables[j].ToString().Split('=');
                value = CheckSpecialChars(strVar[1].ToString());
                if (strVar[0].ToLower().Contains(strGloVar[i].ToLower()))
                {
                    string test = string.Empty;
                    flag = true;
                    globalVariables[j] = strGloVar[i] + "=" + myValues[i];
                    test = strglobalValues.ToString();
                    if (test.Contains(globalVariables[j].ToString()))
                        continue;
                    else
                        strglobalValues.Append(globalVariables[j].ToString() + ", ");
                }
                if (strVar[0].ToString().ToLower() == "axp_displaytext")
                {
                    globalVariables[j] = globalVariables[j].ToString().Split('=')[0] + "=" + hdnDisplayTxt.Value;
                }
                if (flag)
                    break;

            }
        }
        string globalValuesShown = strglobalValues.ToString();
        if (hdnDisplayTxt.Value.Length > 100)
        {
            strglobalValues.Remove(100, ((globalValuesShown.Length - 1) - 100));
            strglobalValues.Append("<a  onclick='javascript:ShowMore(\"" + globalValuesShown + "\");'> More</a>");

            if (isCloudApp)
            {
                //globalVariableValues.InnerHtml = sbDspText + FormatGlobalVariables(formatedDispTxt);//
            }
            else
            {
                FormatGlobalVariables(formatedDispTxt);
            }

        }
        else
        {
            if (isCloudApp)
            {
                //  globalVariableValues.InnerHtml = sbDspText + FormatGlobalVariables(formatedDispTxt);//
            }
            else
            {
                FormatGlobalVariables(formatedDispTxt);

            }
        }
        string url = string.Empty;
        if (hdnAxGloRecId.Value != "")
            url = "../aspx/ParamsTstruct.aspx?transid=" + paramTransid + "&recordid=" + hdnAxGloRecId.Value + "";
        editAxGlo2.Attributes.Add("onclick", "LoadIframe('" + url + "');");
        var AxGlo = UserOptions.Where(x => x.Key == "GlobalParamsonclick").ToList();
        if (AxGlo.Count > 0)
            UserOptions["GlobalParamsonclick"] = "\"onclick\":\"LoadIframe('" + url + "')\"";
        else
            UserOptions.Add("GlobalParamsonclick", "\"onclick\":\"LoadIframe('" + url + "')\"");
        Session["globalVarsArray"] = globalVariables;

        string golVarNode = "<globalvars>";
        foreach (string item in globalVariables)
        {
            int valueIndex = item.IndexOf("=");
            string key = item.Substring(0, valueIndex);
            if (userVar.Contains(key))
                key = key.ToUpper();
            string val = item.Substring(valueIndex + 1);
            golVarNode += "<" + key + ">" + val + "</" + key + ">";
        }
        golVarNode += "</globalvars>";
        Session["axGlobalVars"] = golVarNode;
        CreateParamaterArray();
    }

    private void DoUtlEncode()
    {
        try
        {
            JObject objConfig = null;
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(Session["axGlobalVars"].ToString());
            string axGlobalVars = JsonConvert.SerializeXmlNode(xmlDoc);
            objConfig = JObject.Parse(axGlobalVars);
            axGlobalVars = objConfig["globalvars"].ToString().Replace("\r", "").Replace("\n", "");
            axUtlGlobalVars = util.UtlEncode(axGlobalVars);

            xmlDoc.LoadXml(Session["axUserVars"].ToString());
            string axUserVars = JsonConvert.SerializeXmlNode(xmlDoc);
            objConfig = JObject.Parse(axUserVars);
            axUserVars = objConfig["uservars"].ToString().Replace("\r", "").Replace("\n", "");
            axUtlUserVars = util.UtlEncode(axUserVars);

            xmlDoc.LoadXml(Session["axApps"].ToString());
            string axApps = JsonConvert.SerializeXmlNode(xmlDoc);
            axUtlApps = util.UtlEncode(axApps);
        }
        catch (Exception Ex)
        {
        }
    }
    public string getSmallMenuName(string fullname)
    {
        string nametosend = fullname;
        if (nametosend.Length > 0)
        {
            if (nametosend.Contains("tstruct.aspx"))
            {
                nametosend = "t" + fullname.Substring(fullname.IndexOf("transid=") + 8);
            }
            else
            {
                nametosend = "i" + fullname.Substring(fullname.IndexOf("ivname=") + 7);
            }
        }
        return nametosend;
    }





    #region Application Configuration
    /// <summary>
    /// Loading Project's configuration details
    /// </summary>
    public void LoadAppConfiguration()
    {
        string strProj = string.Empty;

        if (HttpContext.Current.Session["Project"] != null)
            strProj = HttpContext.Current.Session["Project"].ToString();
        else
        {
            if (ConfigurationManager.AppSettings["proj"] != null && ConfigurationManager.AppSettings["proj"].ToString() != "")
                strProj = ConfigurationManager.AppSettings["proj"].ToString();
            else
                strProj = signinProj;
            Session["project"] = strProj;
        }
        if (strProj != string.Empty)
        {
            if (strProj == null) strProj = "";
            string configStr = util.GetConfigAppJSON(strProj);
            util.UpdateConfiginSession(configStr);      // Updating the session with configuration variables
        }
        if (HttpContext.Current.Session["AxLandingPage"] != null)
            Application["-dPage-"] = "../aspx/Page.aspx";
        Session["AxEnableOldTheme"] = "false";
        Session["AxLogging"] = "false";

    }
}
#endregion
