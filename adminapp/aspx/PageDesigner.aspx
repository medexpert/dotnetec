<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageDesigner.aspx.cs" Inherits="aspx_PageDesigner" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Page Designer</title>
    <link href="../ThirdParty/materialize/css/materialize.min.css?v=1" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../ThirdParty/bgrins-spectrum/spectrum.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../ThirdParty/fancytree-master/dist/skin-lion/ui.fancytree.min.css" rel="stylesheet" />
    <link href="../Css/builder.min.css?v=29" rel="stylesheet" />
    <link href="" id="homeBuilderLink" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script>
        var userroles = '<%=Session["AxRole"] %>';
        userroles = userroles.split(",");
        var imageBase = '<%=homeBuliderImageaPath%>';
        var myTaskString = '<%=myTaskString%>';
        var totalWidgetCanAdd = '<%=Session["AxMaxNumOfWidgets"]%>' || 20;
        totalWidgetCanAdd = parseInt(totalWidgetCanAdd);
        var buildModeAccess = '<%=homeBulider%>';
        var globalVars = '<%=globalVars%>';
        buildModeAccess = buildModeAccess.split(',');
        var appsessionKey = '<%=appsessionKey%>';
        var dbType = '<%=Session["axdb"]%>';
        var isPageBuilder = true;//important flag to differantiate from homebuilder
        var presBuiildMode = "homeBuild";
    </script>
</head>
<body class="menu-<%=menuStyle%> btextDir-<%=direction%>" dir="<%=direction%>">
    <div class="row" style="margin: 0px;">
        <!--   <div class="HPBmainTitle">
            <h5 class="left">Home Page Designer</h5>
            
        </div> -->
        <div id="HPBmainWrapper">
            <div class="col s2" id="HPBToolBox">
                <div class="hpbWrapper">
                    <div class="hpbHeaderTitle ">
                        <span class="icon-pencil-ruler"></span>
                        <span class="title">Object Browser</span>
                    </div>
                    <div class="toolBxPanelWrapper">
                        <div class="toolBxPanel">
                            <ul class="collection with-header">
                                <li class="collection-header">
                                    <span class="toolBxPanelSrchSpn">
                                        <input placeholder="Tstruct" id="HMTsSrch" class="toolBxPanelSrch">
                                        <span class="searchIcon icon-magnifier"></span>
                                    </span>
                                </li>
                                <ul id="toolBarLsttstruct" class="collectionListUl">
                                    <li class="collection-item noDataLi">Loading...</li>
                                    <!-- <li data-target="tr1" data-type="tstruct" class="collection-item"></li> -->
                                </ul>
                            </ul>
                        </div>
                        <div class="toolBxPanel">
                            <ul class="collection with-header">
                                <li class="collection-header">
                                    <span class="toolBxPanelSrchSpn">
                                        <input placeholder="Iview" id="HBIVSrch" class="toolBxPanelSrch">
                                        <span class="searchIcon icon-magnifier"></span>
                                    </span>
                                </li>
                                <ul id="toolBarLstiview" class="collectionListUl">
                                    <li class="collection-item noDataLi">Loading...</li>
                                    <!-- <li data-target="city" data-type="iview" class="collection-item">Purchase Order</li> -->
                                </ul>
                            </ul>
                        </div>
                        <div class="toolBxPanel">
                            <ul class="collection with-header">
                                <li class="collection-header">
                                    <span class="toolBxPanelSrchSpn">
                                        <input placeholder="Widgets" id="HBWSrch" class="toolBxPanelSrch">
                                        <span class="searchIcon icon-magnifier"></span>
                                    </span>
                                </li>
                                <ul id="toolBarLstwidget" class="collectionListUl">
                                    <li class="collection-item noDataLi">Loading...</li>
                                </ul>
                            </ul>
                        </div>
                        <div class="toolBxPanel">
                            <ul class="collection with-header">
                                <li class="collection-header">
                                    <span class="toolBxPanelSrchSpn">
                                        <input placeholder="Custom Widget" id="HBCWSrch" class="toolBxPanelSrch">
                                        <span class="searchIcon icon-magnifier"></span>
                                    </span>
                                </li>
                                <ul class="collectionListUl customWidgetsUl">
                                    <li title="Image" data-type="Custom__img" data-target="C__img1" class="collection-item">Image</li>
                                    <li title="HTML" data-type="Custom__html" data-target="C__html1" class="collection-item">HTML</li>
                                    <li title="Static Text" data-type="Custom__txt" data-target="C__txt1" class="collection-item">Static Text</li>
                                    <li title="Sql Query" data-type="Custom__sql" data-target="C__sql1" class="collection-item">SQL Query</li>
                                    <li title="RSS Feed" data-type="Custom__rss" data-target="C__rss1" class="collection-item">RSS Feed</li>
                                    <li title="My Tasks" data-expand="myTsk" data-multiselect="false" data-type="Custom__mytsk" data-target="C__mytsk" class="collection-item">My Tasks</li>
                                    <li title="My Tasks" data-expand="dynamicWidget" data-multiselect="false" data-type="Custom__dynamic" data-target="C__dynamic" class="collection-item">Dynamic</li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s10" id="HPBdesignerCanvas">
                <div class="hpbWrapper">
                    <div class="hpbHeaderTitle ">
                        <div style="float: left;">
                            <span class="icon-window"></span>
                            <!-- <i class="material-icons">border_all</i> -->
                            <span class="title">Designer Canvas</span>
                        </div>
                        <div id="searchPageElm">
                            <input type="text" style="width: 80%; padding: 0px; margin: 0px 0px 5px 50px; height: 28px;" id="searchPage" autocomplete="off" class="searchPage" placeholder="Type to search page" />
                        </div>
                        <div>
                            <span class="right upDwnBtnWrapper">
                                <button title="Create New Tab" id="createNewPageModalBtn" data-target="createNewPageModal" class="waves-effect waves-light btn-flat"><span class="icon-plus"></span></button>
                                <button title="Save" id="saveDesign" onclick="ajaxCallObj.saveJsonData()" class="waves-effect waves-teal btn-flat"><span class="icon-check"></span></button>
                                <button title="Publish" id="publishDesign" onclick="ajaxCallObj.publishJsonData()" class="waves-effect waves-teal btn-flat"><span class="icon-upload2"></span></button>
                            </span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <ul class="tabs" id="HPBtabsHaeder" style="display: none;">
                        <!-- <li class="tab col s2"><a class="active" href="#test1">Tab 1</a></li> -->
                        <!-- <li class="tab col s2">
                            
                        </li> -->
                    </ul>
                    <!-- Modal Structure -->
                    <div id="createNewPageModal" class="modal modal-fixed-footer">
                        <div class="modal-content">
                            <!-- <h4>Modal Header</h4> -->
                            <div id="hpbPageRespSelector" class="ht100Per">
                                <h5>Create new page</h5>
                                <div class="ht100Per">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="newTabName" type="text">
                                            <label for="newTabName">Name</label>
                                        </div>

                                    </div>
                                    <div class="col s12">
                                        <span>
                                            <label for="defaultHomePage">Default HomePage : </label>
                                        </span>
                                        <span class="switch">
                                            <label>
                                                No
                                                <input id="defaultHomePage" type="checkbox">
                                                <span class="lever"></span>Yes
                                            </label>
                                        </span>
                                    </div>
                                    <br>
                                    <div class="row" id="pageCreationTreeWrapper">
                                        <div id="treeWrapper"></div>
                                    </div>

                                </div>
                            </div>
                            <div style="display: none;" id="hpbPageTemplateSelector">
                                <h5>Please select a responsive template</h5>
                                <div class="templateWrapper">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button title="Ok" onclick="addNewHomePage()" type="button" class="waves-effect btn themeButton cutsomColorbtn">Ok</button>
                            <button type="button" title="Cancel" style="margin-right: 10px;" class="cancelButton waves-effect btn modal-action modal-close">Cancel</button>
                        </div>
                    </div>
                    <!-- Modal Structure - END -->
                    <div id="hpbDsgnrcnvsWrapper">
                        <div id="sortable" class="row mainWidgetAddedWrapper">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="col s3 card hoverable scale-transition hide scale-out" id="propertySheet">
                <div class="hpbHeaderTitle themeDarkenBackground">
                    <span class="icon-paint-roller"></span>
                    <span class="title">Property Sheet</span>
                    <button title="Close" type="button" id="propertySrchCls" onclick="closeProprtySht();" class="btn-flat waves-effect btn-floating right"><i class="icon-cross2"></i></button>
                </div>
                <div id="propertySheetDataWrapper">
                    <div id="propertySearch">
                        <input placeholder="Search..." type="text" id="propertySearchFld" class="normalFld searchFld">
                        <span class="srchIcn icon-magnifier"></span>
                    </div>
                    <div class="posr" id="propTableContent">
                        <table class="bordered">
                            <tr>
                                <td class="subHeading themeTextColor" colspan="2"><span class="td-general">General</span> <span data-target="general" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                            </tr>
                            <tr data-group="general">
                                <td id="ppsLcmp">Component</td>
                                <td id="prpShtComponent">Tstruct</td>
                            </tr>
                            <tr class="prpShtTitleRelated" data-group="general">
                                <td id="ppsLttl">Title</td>
                                <td>
                                    <input type="text" maxlength="50" onkeyup="homeJsonObj.updateValueOnKeyUp(this,'tl')" value="" class="normalInpFld themeMaterialInp" id="prpShtTitle">
                                </td>
                            </tr>
                            <tr id="prpShtHeadingSrc">
                                <td class="subHeading themeTextColor" colspan="2"><span class="td-src">Source</span> <span data-target="src" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                            </tr>
                            <tr id="prpShtTargetTypeWrapper" data-group="src">
                                <td>Target Type</td>
                                <td>
                                    <select onchange="prpShtChangeTrgtType(this)" name="" id="prpShtTargetType">
                                        <option value="none">None</option>
                                        <option value="url">URL</option>
                                        <option value="tstruct">Tstruct</option>
                                        <option value="iview">Iview</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="prpShtRssFeedTime" class="commonCloseOnOpen" data-group="src">
                                <td>Refresh Time</td>
                                <td>
                                    <select onchange='homeJsonObj.updateDataInJson($(this).parents("#propertySheet").data("target"),"rsst",$(this).val())' name="">
                                        <option value="">None</option>
                                        <option value="2">2 Mins</option>
                                        <option value="5">5 Mins</option>
                                        <option value="8">8 Mins</option>
                                        <option value="10">10 Mins</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="prpShtCustomTargetWrapper" data-group="src">
                                <td>Target</td>
                                <td>
                                    <select name="" id="prpShtCustomTarget"></select>
                                </td>
                            </tr>
                            <tr id="prpShtTargetWrapper" data-group="src">
                                <td>Target</td>
                                <td>
                                    <input type="text" value="" class="normalInpFld themeMaterialInp" id="prpShtTarget" autocomplete="off">
                                </td>
                            </tr>
                            <tr id="prpShtTxtBxWrapper" data-group="src">
                                <td>Description</td>
                                <td>
                                    <input type="text" value="" data-key="dc" onfocus="createCustomTxtBx(this)" class="normalInpFld themeMaterialInp" id="prpShtTxtBx">
                                </td>
                            </tr>

                            <tr id="prpShtDrLoadWrapper" class="commonCloseOnOpen" data-group="src">
                                <td>Direct Load</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            No
                                            <input id="prpShtDirectLoad" onchange="prpShtToggleSwtch('directLoad',this)" type="checkbox">
                                            <span class="lever"></span>Yes
                                        </label>
                                    </div>
                                </td>
                            </tr>

                            <!--  <tr id="prpShtHtmlWrapper">
                                <td>HTML</td>
                                <td>
                                    <input type="text" value="" class="normalInpFld themeMaterialInp" id="prpShtHtml">
                                </td>
                            </tr> -->
                            <tr id="prpShtImgWrapper" data-group="src">
                                <td>Image</td>
                                <td>
                                    <div class="file-field input-field">
                                        <div class="btn fileupbtn themeButton">
                                            <span>...</span>
                                            <form id="frmUploader" enctype="multipart/form-data" action="" method="post">
                                                <input type="hidden" id="imageSessionId" name="session_id" value="123">
                                                <input type="hidden" id="imageUtl" name="utl" value="123">
                                                <input type="hidden" name="imagename" value="mani.jpg">
                                                <input type="hidden" name="username" id="imgUsername" />
                                                <input type="hidden" name="authorization" id="imgauthorization" />
                                                <input type="hidden" name="appSKey" id="imgappSKey" />
                                                <input type="file" name="imgUploader" id="imgUploader" />
                                            </form>
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                            <div class="progress" id="prpShtImgUpload">
                                                <div class="determinate" style="width: 0%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr data-group="src" class="commonCloseOnOpen" id="prpShtWidgetRefresherTr">
                                <td>Cache</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            No
                                            <input id="phpShtWidgetRefresher" onchange="prpShtToggleSwtch('cacheWidget',this)" type="checkbox">
                                            <span class="lever"></span>Yes
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr data-group="src" class="commonCloseOnOpen" id="prpShtWidgetRefresheTimerTr">
                                <td>Auto Refresh (Mins)</td>
                                <td>
                                    <input onfocusout="homeJsonObj.updateValueOnKeyUp(this,'cei')" type="number" max="999" value="" class="normalInpFld themeMaterialInp" id="prpShtWidgetRefresheTimer" />
                                </td>
                            </tr>
                            <tr id="prpShtDepTr">
                                <td class="subHeading themeTextColor" colspan="2">
                                    <span class="td-dep">Dependency</span> <span data-target="dep" class="propShtDataToggleIcon icon-chevron-up"></span>
                                </td>
                            </tr>
                            <tr data-group="dep" id="prpShtDepMappingTr">
                                <td>Mapping
                                </td>
                                <td>
                                    <button id="parameterMappingBtn" onclick="createDependencyPopup()" class="waves-effect themeButton waves-light btn">...</button>
                                </td>
                            </tr>

                            <tr id="prpShtPropsTr" class="commonCloseOnOpen">
                                <td class="subHeading themeTextColor" colspan="2">
                                    <span class="td-props">Properties</span> <span data-target="props" class="propShtDataToggleIcon icon-chevron-up"></span>
                                </td>
                            </tr>

                            <tr id="prpShtColPropsTr" class="commonCloseOnOpen" data-group="props">
                                <td>Column
                                </td>
                                <td>
                                    <button id="parameterMappingBtn" onclick="openSQLcolumnProps()" class="waves-effect themeButton waves-light btn">...</button>
                                </td>
                            </tr>

                            <tr>
                                <td class="subHeading themeTextColor" colspan="2"><span class="td-appr">Appearance</span> <span data-target="appr" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                            </tr>
                            <!-- <tr>
                                <td>Font Size:</td>
                                <td>
                                    <input type="text" value="" class="normalInpFld themeMaterialInp" id="prpShtFont">
                                </td>
                            </tr> -->
                            <tr data-group="appr" id="prpShtTtlRgnTr">
                                <td>Title Region</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            Off
                                            <input id="prpShtTargetRgn" onchange="prpShtToggleSwtch('ttlRgn',this)" type="checkbox">
                                            <span class="lever"></span>On
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr data-group="appr" class="commonCloseOnOpen" id="prpShtImgRespTr">
                                <td>Image Responsive</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            Off
                                            <input id="prpShtRespimg" onchange="prpShtToggleSwtch('respImg',this)" type="checkbox">
                                            <span class="lever"></span>On
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <!-- <tr data-group="appr">
                                <td>Strech</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            Off
                                            <input onchange="prpShtToggleSwtch('strch',this)" type="checkbox">
                                            <span class="lever"></span> On
                                        </label>
                                    </div>
                                </td>
                            </tr> -->
                            <tr id="prpShtPageTemplateTr" class="commonCloseOnOpen" data-group="appr">
                                <td>Template</td>
                                <td>
                                    <!-- Dropdown Trigger -->
                                    <a class='dropdown-button dropDownBtnAsSelect' href='javascript:void(0)' data-activates='prpShtTmpFropDwn'>Drop Me! <span class="caret">▼</span></a>
                                    <!-- Dropdown Structure -->
                                    <ul id='prpShtTmpFropDwn' class='dropdown-content'>
                                    </ul>
                                </td>
                            </tr>
                            <tr id="prpShtTitlePickerTr" class="prpShtTitleRelated" data-group="appr">
                                <td>Icon</td>
                                <td>
                                    <input type="text" value="" class="normalInpFld themeMaterialInp iconInpFld" id="prpShtIcon">
                                    <button type="button" onclick="toggleIcons()" class="iconSelector themeTextColor btn-flat waves-effect btn-floating right"><span id="iconSelectorSpn" class="icon-home"></span></button>
                                    <div id="iconWrapper">
                                    </div>
                                </td>
                            </tr>
                            <tr class="prpShtTitleRelated" id="prpShtIcnClrTr" data-group="appr">
                                <td>Icon Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtIcnColor">
                                        <input type="text" value="" data-type="icc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr class="prpShtTitleRelated" id="prpShtTtlClrTr" data-group="appr">
                                <td>Title Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtTtlColor">
                                        <input type="text" value="" data-type="tc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr class="prpShtTitleRelated" id="prpShtTtlBgClrTr" data-group="appr">
                                <td>Title Bg Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtTtlBgColor">
                                        <input type="text" value="" data-type="tbc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>

                            <tr data-group="appr" id="prpShtFltngBtnTr">
                                <td>Floating Button</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            Off
                                            <input id="prpShtFltngBtn" onchange="prpShtToggleSwtch('floatingBtn',this)" type="checkbox">
                                            <span class="lever"></span>On
                                        </label>
                                    </div>
                                </td>
                            </tr>

                            <tr class="prpShtfloatingBtnGrp" id="prpShtFmBgClrTr" data-group="appr">
                                <td>Floating Menu Bg Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtBtnBgColor">
                                        <input type="text" value="" data-type="fbc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr class="prpShtfloatingBtnGrp" id="prpShtFmTcTr" data-group="appr">
                                <td>Floating Menu Text Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtBtnColor">
                                        <input type="text" value="" data-type="fc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr id="bdTxtClrTr" data-group="appr">
                                <td>Body text Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtBodyTxtColor">
                                        <input type="text" value="" data-type="bc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr id="bdBgClrTr" data-group="appr">
                                <td>Body Bg Color</td>
                                <td>
                                    <div class="colorPickerWrapper">
                                        <input type="text" value="" class="normalInpFld themeMaterialInp colorPickerInp" id="prpShtBodyColor">
                                        <input type="text" value="" data-type="bbc" class="normalInpFld themeMaterialInp colorPicker" id="">
                                    </div>
                                </td>
                            </tr>
                            <tr data-group="appr" id="gradientPickerWrapper" class="commonCloseOnOpen">
                                <td>Gradient Color</td>
                                <td>
                                    <div id="gradientPicker" class="colorMe blue center-align">
                                        <a class="colorMeA white-text" data-color='blue' onclick="gradientClick('colorClick')" href="javascript:void(0)">blue  
                                        </a>
                                    </div>
                                    <div class="gradientPalletWrapper" style="display: none;">
                                        <div class="pallet">
                                            <div class="colorMe blue palletIns">
                                                <a title="Blue" onclick="gradientClick('colorPick','blue')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe red palletIns">
                                                <a title="Red" onclick="gradientClick('colorPick','red')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe yellow palletIns">
                                                <a title="Yellow" onclick="gradientClick('colorPick','yellow')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe green palletIns">
                                                <a title="Green" onclick="gradientClick('colorPick','green')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe purple palletIns">
                                                <a title="Purple" onclick="gradientClick('colorPick','purple')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe multiBlue palletIns">
                                                <a title="Multiple Blue" onclick="gradientClick('colorPick','multiBlue')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                        <div class="pallet">
                                            <div class="colorMe multiLime palletIns">
                                                <a title="Multiple Lime" onclick="gradientClick('colorPick','multiLime')" class="colorMeA" href="javascript:void(0)"></a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="prpShtAuthrzatn" class="commonCloseOnOpen">
                                <td class="subHeading themeTextColor" colspan="2"><span class="td-auth">Authorization</span> <span data-target="auth" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                            </tr>
                            <tr data-group="auth" class="commonCloseOnOpen">
                                <td>Default HomePage</td>
                                <td>
                                    <div class="switch">
                                        <label>
                                            No
                                            <input id="prpShtIsDefault" onchange="prpShtToggleSwtch('isDefault',this)" type="checkbox">
                                            <span class="lever"></span>Yes
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr data-group="auth" id="prpShtRepTr" class="commonCloseOnOpen">
                                <td>Responsibility</td>
                                <td id="prpShtRolesWrappperTd">
                                    <div class="input-field col s12">
                                        <select multiple onchange='homeJsonObj.updateDataInJson($(this).parents("#propertySheet").data("target"),"rl",$(this).val())' id="prpShtRoles">
                                            <!-- <option selected value="0">All</option> -->
                                            <option disabled value="0">Please wait...</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="subHeading themeTextColor" colspan="2"><span class="td-log">Activity Log</span> <span data-target="log" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                            </tr>
                            <tr data-group="log" id="prpShtMdByTr">
                                <td>Modified By</td>
                                <td id="prpShtModifiedBy"></td>
                            </tr>
                            <tr data-group="log" id="prpShtMdyOnTr">
                                <td>Modified On</td>
                                <td id="prpShtModifiedOn"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="customTxtAreaWrapperBg" style="display: none;">
        <div id="customTxtAreaWrapper">
            <!-- <button id="customTxtAreaWrapperCls" type="button" class="btn-flat waves-effect btn-floating right"><i class="icon-cross2"></i></button> -->
            <textarea tabindex="-1" name="" id="" cols="30" rows="10"></textarea>
            <div id="customTxtAreaFooter" style="display: none;" class="right-align">
                <button title="save" class="cutsomColorbtn themeButton save waves-effect waves-light btn"><span class="icon-check"></span></button>
                <button title="cancel" class="cancel waves-effect waves-light btn"><span class="icon-cross2"></span></button>
            </div>
        </div>
    </div>
    <!-- extraModel Component -->
    <div id="extraModelComp">
    </div>


    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <script src="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.js"></script>
    <script src="../ThirdParty/materialize/js/materialize.min.js?v=11"></script>
    <script src="../ThirdParty/bgrins-spectrum/spectrum.js"></script>
    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js"></script>
    <script src="../Js/alerts.min.js?v=24"></script>
    <script src="../ThirdParty/ajaxForm.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/moment.min.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-3d.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-more.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-exporting.js"></script>
    <script src="../Js/high-charts-functions.min.js?v=17"></script>
    <script src="../Js/multiselect.min.js"></script>
    <script src="../assets/js/loadingoverlay.min.js?v=3"></script>
    <script src="../Js/templateParser.min.js?v=8"></script>
    <script src="../Js/commonBuilder.min.js?v=42"></script>
    <script src="../Js/xmlToJson.min.js?v=2"></script>
    <script src="../ThirdParty/fancytree-master/dist/jquery.fancytree-all-deps.min.js"></script>
    <script src="../Js/treecreator.min.js?v=2"></script>
    <script src="../Js/commonHome.min.js?v=20"></script>
    <script src="../Js/builder.min.js?v=58"></script>
</body>
</html>
