﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReleaseProcess.aspx.cs" Inherits="aspx_ReleaseProcess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .dv {
            padding:2px;
            
        }
        .lbl {
            margin-right:10px;
        }
    </style>
</head>
<body dir="<%=direction%>">
    <form id="form1" runat="server">
    <div class="dv">
        <label class="lbl">Modified JS or CSS file name</label> <asp:TextBox ID="txtFilename" runat="server"></asp:TextBox><label> (If all files have to be checked leave it blank else give only filename. for eg. "tstuct.js" or "tstruct.min.js")</label>
        </div>
        <div class="dv">
       <label class="lbl">Old Version No</label> <asp:TextBox ID="txtOldVersion" runat="server"></asp:TextBox><label> If the file name is "tstruct.min.js?v=9.8", then enter "9.8"</label>
             </div>
        <div class="dv">
       <label class="lbl">New Version No</label> <asp:TextBox ID="txtNewVersion" runat="server"></asp:TextBox><label> If the file has to be renamed to "9.8.1", then enter "9.8.1"(The new version no. will be replaced for all files matching the old verion number)</label> </div>
        <div class="dv">
    <asp:Button id="btnVersion" Text="Version Minified Files" runat="server" OnClick="btnVersion_Click"/>
    </div>
    </form>
</body>
</html>
