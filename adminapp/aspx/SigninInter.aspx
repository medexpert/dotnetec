﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SigninInter.aspx.cs" Inherits="aspx_postpage" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Signin intermidiate page">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">

    <title>SigninInter</title>
</head>
<body onload="window.document.form1.submit();">
    <form name="form1" method="post" action="mainnew.aspx">
        <div>
            <%=strParams.ToString() %>
        </div>
        <%if (Request.Browser.Browser == "Firefox")
            {%>
        <script type="text/javascript">        window.document.form1.submit(); </script>
        <%}%>
    </form>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript" src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <script src="../assets/js/loadingoverlay.min.js?v=3" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
             $.LoadingOverlay("show", { "fixedBackgroundSize": "135px" });
        });
    </script>
</body>
</html>
