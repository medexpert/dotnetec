﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections;

public partial class aspx_postpage : System.Web.UI.Page
{
    public StringBuilder strParams = new StringBuilder();
    public string target = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if ((Util.Util.CheckCrossScriptingInString(Request.Form["hdnAxProjs"]) || Util.Util.CheckCrossScriptingInString(Request.Form["rn"]) || Util.Util.CheckCrossScriptingInString(Request.Form["hash"]) || Util.Util.CheckCrossScriptingInString(Request.Form["rndh"]) || Util.Util.CheckCrossScriptingInString(Request.Form["language"]) || Util.Util.CheckCrossScriptingInString(Request.Form["key"])))
            {
                Response.Redirect(Constants.LOGINERR);
            }
        }
        catch (Exception ex)
        {
            Response.Redirect(Constants.LOGINERR);
        }
        if (Request.Form["hdnAxProjs"] != null && !string.IsNullOrEmpty(Request.Form["hdnAxProjs"].ToString()))
        {
            ASB.WebService wsObj = new ASB.WebService();
            wsObj.CreateLoginKey(Request.Form["hdnAxProjs"], Request.Form["rn"], Request.Form["hash"], Request.Form["rndh"], Request.Form["language"], Request.Form["key"]);
        }
        if (!Request.Browser.Type.ToLower().Contains("safari"))
        {
            //create new sessionID        
            SessionIDManager manager = new SessionIDManager();
            manager.RemoveSessionID(System.Web.HttpContext.Current);
            var newId = manager.CreateSessionID(System.Web.HttpContext.Current);
            var isRedirected = true;
            var isAdded = true;
            manager.SaveSessionID(System.Web.HttpContext.Current, newId, out isRedirected, out isAdded);
        }
        if (Request.Form.Count == 0)
            Response.Redirect(Application["LoginPath"].ToString());
        //Util.Util utilObj = new Util.Util();
        // for (int i = 0; i < Request.Form.Count; i++)
        //{
        //if (Request.Form.Keys[i].ToString() == "rn")
        //{
        //    bool isUserValid = utilObj.IsUserNameValid(Request.Form[i].ToString());
        //    ViewState["User"] = Request.Form[i].ToString();
        //    if (!isUserValid)
        //        Response.Redirect(Constants.LOGINERR);

        //}
        //else if (Request.Form.Keys[i].ToString() == "sw")
        //{
        //    if (Request.Form[i].ToString() == null || Request.Form[i].ToString() == "")
        //        continue;
        //    else
        //        Response.Redirect(Constants.LOGINERR);
        //}
        //else if (Request.Form.Keys[i].ToString() == "rndh")
        //{
        //    bool isrndHashValid = utilObj.IsHashValid(Request.Form[i].ToString());
        //    if (!isrndHashValid)
        //        Response.Redirect(Constants.LOGINERR);
        //}
        //else if (Request.Form.Keys[i].ToString() == "hdnflag")
        //{
        //    bool isflagValid = isHdnFlagValid(Request.Form[i].ToString());
        //    if (!isflagValid)
        //        Response.Redirect(Constants.LOGINERR);

        //    strParams.Append("<input type=hidden name=rn value=" + ViewState["User"].ToString() + ">");
        //}
        //else
        //{
        //    try
        //    {
        //        if (Session["CSRFToken"] != null && Session["CSRFToken"].ToString() != Request.Cookies["CSRFToken"].Value)
        //        {
        //            Response.Redirect("../cuserror/axcustomerror.aspx");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Redirect("../err.aspx?errmsg=" + ex.Message);
        //    }
        //    string cookieName = "CSRFToken";
        //    Response.Cookies.Add(new System.Web.HttpCookie(cookieName, ""));
        //    if (Request.Cookies[cookieName] != null)
        //    {
        //        Response.Cookies[cookieName].Value = string.Empty;
        //        Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-20);
        //    }
        //    bool isFldValid = utilObj.IsAlphaNumUnd(Request.Form[i].ToString());
        //    if (!isFldValid)
        //        Response.Redirect(Constants.LOGINERR);
        //}
        // }

        string cookieName = "CSRFToken";
        Response.Cookies.Add(new System.Web.HttpCookie(cookieName, ""));
        if (Request.Cookies[cookieName] != null)
        {
            Response.Cookies[cookieName].Value = string.Empty;
            Response.Cookies[cookieName].Expires = DateTime.Now.AddMonths(-20);
        }

        //bool isFldValid = utilObj.IsAlphaNumUnd(Request.Form[i].ToString());
        //if (!isFldValid)
        //    Response.Redirect(Constants.LOGINERR);
        //}

        if (Request.Form["key"] != null)
            strParams.Append("<input type=hidden name=key value=" + Request.Form["key"].ToString() + ">");
		
		
        if (Request.Form["language"].ToString() != "")
            strParams.Append("<input type=hidden name=language value=" + Request.Form["language"].ToString() + ">");
		
		if (Request.Form["hdnAxProjs"] != null)
            strParams.Append("<input type=hidden name=hdnAxProjs value=" + Request.Form["hdnAxProjs"].ToString() + ">");
		
		
        if (Request.Form["hdnCloudDb"] != null)
            strParams.Append("<input type=hidden name=hdnCloudDb value=" + Request.Form["hdnCloudDb"].ToString() + ">");
    }
    public bool isHdnFlagValid(string Str)
    {
        if (Str == null) return false;
        Regex regexItem = new Regex("^[a-zA-Z0-9-_.!@#$^]*$");
        if (regexItem.IsMatch(Str))
            return true;
        else
            return false;
    }


}
