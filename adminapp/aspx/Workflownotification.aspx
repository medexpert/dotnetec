<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Workflownotification.aspx.cs" Inherits="aspx_Workflownotification" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Messages">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title></title>
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/Workflownotification.min.css?v=1" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <%--custom alerts start--%>
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <script src="../Js/workflowNotification.js?v=1"></script>
    <script type="text/javascript" src="../Js/md5.min.js"></script>
    <%--custom alerts end--%>
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <script src="../assets/js/loadingoverlay.min.js?v=3"></script>

    <script type="text/javascript">
            var alertsTimeout = 3000;
    </script>
</head>
<body>
    <div id="header">
        <div class="left" style="float: left;">
            <img class="axpimg" src="../assets/img/logo.png" height="40" width="50">
        </div>
    </div>
    <div class="section-title center" style="padding-top: 73px;">
        <h2>Workflow <strong>Approval</strong></h2>
    </div>

    <div id="main">
        <div class="container">
            <div class="row">
                <div class="col-sm-10  col-sm-offset-1 col-md-10  col-md-offset-1 col-xs-10  col-xs-offset-1 custrow">
                    <div class="row feeddesgin">
                        <div class="col-md-8 col-sm-offset-2">
                            <form runat="server">
                                <div class="lpanel" style="display: block">
                                    <asp:ScriptManager runat="server">
                                    </asp:ScriptManager>
                                    <h1>Login</h1>
                                    <div style="display: block" class="inputcls">
                                        <input id="userName" runat="server" placeholder="Username" required="" class="commoncls">
                                        <input id="password" runat="server" placeholder="Password" type="password" required="" class="commoncls">
                                        <asp:UpdatePanel ID="updSubmit" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="submit" class="workflowbtn" OnClick="submit_Click" Text="Submit" runat="server" Style="padding: 6px !important;" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>                                        
                                        <asp:HiddenField ID="hdnPwd" runat="server" />
                                    </div>
                                </div>
                                <div id="workflowComments" class="modal" data-backdrop="static" data-keyboard="false" data-easein="expandIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-target="#consumergoods2">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Comment</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="5" id="comment"></textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <asp:Button ID="commentSave" class="btn btn-primary hotbtndynamic" OnClick="comment_Click" Text="Save" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="isAuthorized" runat="server" />
                                <asp:HiddenField ID="hdnComment" runat="server" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>