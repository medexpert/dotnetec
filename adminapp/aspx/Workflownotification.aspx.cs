﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class aspx_Workflownotification : System.Web.UI.Page
{
    ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
    ASB.WebService asbWebService = new ASB.WebService();
    ASBExt.WebServiceExt asbExt = new ASBExt.WebServiceExt();
    LogFile.Log logobj = new LogFile.Log();
    ArrayList globalVariables = new ArrayList();
    string transId = "";
    string recordId = "";
    string actName = "";
    string workflowId = "";
    string lno = "";
    string elno = "";
    string comments = "";
    bool isChanged = false;
    string proj = "";
    string username = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["enc"] != null)
        {
            string details = Decrypt(Request.QueryString["enc"]);
            string[] workFlowDtls = details.Split(',');
            proj = workFlowDtls[0];
            transId = workFlowDtls[1];
            recordId = workFlowDtls[2];
            workflowId = workFlowDtls[3];
            lno = workFlowDtls[4];
            elno = workFlowDtls[5];
            actName = workFlowDtls[6];
        }
    }

    public string getlicDetails()
    {
        var result = asbExt.CallAxpString();
        var macid = "";
        var licfile = "";

        if (result != string.Empty)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(result);
            }
            catch (Exception ex)
            {
            }
            XmlNode resultNode = null;


            resultNode = xmlDoc.SelectSingleNode("//result");
            if (resultNode != null)
            {
                foreach (XmlNode childNode in resultNode.ChildNodes)
                {
                    if (childNode.Name == "macid")
                        macid = childNode.InnerText;
                    else if (childNode.Name == "licfile")
                        licfile = childNode.InnerText;
                }
            }


        }
        var licDetails = " macid='" + macid + "' licfile='" + licfile + "'";
        return licDetails;
    }

    private string GetBrowserDetails()
    {
        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        string userDetails = browser.Type + "¿" + browser.Browser + "¿"
            + browser.Version + "¿" + browser.MajorVersion + "¿"
            + browser.MinorVersion + "¿" + browser.Platform + "¿"
            + Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"];

        if (userDetails.Length > 200)
            userDetails = userDetails.Substring(0, 200);

        return userDetails;
    }

    private string GetAxProps()
    {
        string contents = "";
        string scriptsPath = HttpContext.Current.Application["ScriptsPath"].ToString();
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(scriptsPath);

        if (di.Exists)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(scriptsPath + "axprops.xml");

            while (reader.Read())
            {
                reader.MoveToContent();
                if (reader.NodeType == System.Xml.XmlNodeType.Element)
                    contents += "<" + reader.Name + ">";
                if (reader.NodeType == System.Xml.XmlNodeType.Text)
                    contents += reader.Value + "";
                else if (reader.NodeType == System.Xml.XmlNodeType.EndElement) //Display the end of the element.
                    contents += "</" + reader.Name + ">";

            }
        }
        return contents;
    }

    private string GetAxApps(string project)
    {
        string contents = "";
        string scriptsPath = string.Empty;

        if (ConfigurationManager.AppSettings["proj"] != null && ConfigurationManager.AppSettings["proj"].ToString() != "")
            project = ConfigurationManager.AppSettings["proj"].ToString();
        scriptsPath = HttpContext.Current.Application["ScriptsPath"].ToString();
        Application["axdb"] = "Oracle";
        if (string.IsNullOrEmpty(project))
            return "";
        string db = string.Empty;
        int dbIdx = -1;
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(scriptsPath);
        if (di.Exists)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(scriptsPath + "axapps.xml");
            bool startRead = false;
            string lastEmptyNode = string.Empty;
            using (reader)
            {
                while (reader.Read())
                {
                    reader.MoveToContent();

                    if (reader.NodeType == System.Xml.XmlNodeType.Element && reader.Name == project)
                    {
                        startRead = true;
                        contents += "<" + reader.Name + ">";
                    }
                    else if (reader.NodeType == System.Xml.XmlNodeType.Element && startRead == true)
                    {
                        if (reader.Name == "db")
                        {
                            dbIdx = 0;
                        }

                        contents += "<" + reader.Name + ">";
                        if (reader.IsEmptyElement)
                        {
                            lastEmptyNode = reader.Name;
                            contents += "</" + reader.Name + ">";
                        }
                    }

                    if (reader.NodeType == System.Xml.XmlNodeType.Text && startRead == true)
                    {
                        if (dbIdx != -1)
                        {
                            db = reader.Value;
                            dbIdx = -1;
                        }
                        contents += reader.Value + "";
                    }
                    else
                    {
                        if (reader.Name == lastEmptyNode)
                        {
                            lastEmptyNode = string.Empty;
                            continue;
                        }
                        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && reader.Name == project)
                        {
                            startRead = false;
                            contents += "</" + reader.Name + ">";
                            break;
                        }
                        else if (reader.NodeType == System.Xml.XmlNodeType.EndElement && startRead == true) //Display the end of the element.
                            contents += "</" + reader.Name + ">";
                    }

                }
            }

        }

        return contents;
    }

    protected void submit_Click(object sender, EventArgs e)
    {
        string result;
        username = userName.Value;
        var pwrd = hdnPwd.Value;
        string licDetails = getlicDetails();
        string userDetails = GetBrowserDetails();
        var axApps = GetAxApps(proj);
        var axProps = GetAxProps();
        Session.Abandon();
        var sid = GenerateNewSessionID();

        var seed = "1234";
        var errlog = "";
        var lang_attr = "";

        var sXml = "<login ip='' other='" + userDetails + "'  seed='" + seed + "'  axpapp='" + proj + "' sessionid='" + sid + "' username='" + username + "' password='" + pwrd + "' url='' direct='t' trace='" + errlog + "' " + lang_attr + licDetails + ">";
        sXml = sXml + axApps + axProps + "</login>";
        result = objWebServiceExt.CallLoginWS("main", sXml);
        if (result.StartsWith(Constants.ERROR) || result.Contains(Constants.ERROR))
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(result);
            string msg = string.Empty;
            XmlNode errorNode = xmldoc.SelectSingleNode("/error");
            if (result.Contains("\n"))
                result = result.Replace("\n", "");

            foreach (XmlNode msgNode in errorNode)
            {
                if (msgNode.Name == "msg")
                {
                    msg = msgNode.InnerText;
                    break;
                }
            }

            if (msg == string.Empty && errorNode.InnerText != string.Empty)
                msg = errorNode.InnerText;
            if (msg.ToLower().Contains("ora-"))
                msg = "Error occurred(2). Please try again or contact administrator.";
            ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Workflow", "<script type='text/javascript'> showAlertDialog('error','" + msg + "'); </script>", false);
        }
        else if (result.StartsWith("<result") || result.Contains("<result"))
        {
            ParseLoginResult(result);
            ViewState["AppSessionKey"] = HttpContext.Current.Session["AppSessionKey"].ToString();
            ViewState["axGlobalVars"] = HttpContext.Current.Session["axGlobalVars"].ToString();
            ViewState["axUserVars"] = HttpContext.Current.Session["axUserVars"].ToString();
            ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Workflow", "<script type='text/javascript'> createModal(); </script>", false);
        }
    }

    private string GenerateNewSessionID()
    {
        SessionIDManager manager = new SessionIDManager();

        string newID = manager.CreateSessionID(Context);
        bool redirected = false;
        bool isAdded = false;
        manager.SaveSessionID(Context, newID, out redirected, out isAdded);
        return newID;
    }

    protected void comment_Click(object sender, EventArgs e)
    {
        var sid = "";
        sid = Session.SessionID;
        var axApps = GetAxApps(proj);
        var axProps = GetAxProps();

        if (hdnComment.Value != "")
        {
            isChanged = true;
        }
        var inputXml = "<root axpapp='" + proj + "' trace=''  sessionid='" + sid + "' transid='" + transId + "'  recordid='" + recordId + "' actname='" + actName + "' comments='" + hdnComment.Value + "' appsessionkey='" + ViewState["AppSessionKey"] + "' username='" + userName.Value + "' changed ='" + isChanged + "' lno='" + lno + "'  elno='" + elno + "'>";
        inputXml = inputXml + ViewState["axGlobalVars"] + ViewState["axUserVars"] + axApps + axProps + "</root>";

        var workflowResult = objWebServiceExt.CallWorkFlowActionWS(transId, inputXml);
        if (workflowResult.StartsWith(Constants.ERROR) || workflowResult.Contains(Constants.ERROR))
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(workflowResult);
            string msg = string.Empty;
            XmlNode errorNode = xmldoc.SelectSingleNode("/error");
            if (workflowResult.Contains("\n"))
                workflowResult = workflowResult.Replace("\n", "");

            foreach (XmlNode msgNode in errorNode)
            {
                if (msgNode.Name == "msg")
                {
                    msg = msgNode.InnerText;
                    break;
                }
            }

            if (msg == string.Empty && errorNode.InnerText != string.Empty)
                msg = errorNode.InnerText;
            if (msg.ToLower().Contains("ora-"))
                msg = "Error occurred(2). Please try again or contact administrator.";
            ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Workflow", "<script type='text/javascript'> showAlertDialog('error','" + msg.Replace("'", "") + "'); </script>", false);
        }
        else if (workflowResult == "done")
        {
            string action;
            if (actName == "approve")
            {
                action = actName + "d";
            }
            else
            {
                action = actName + "ed";
            }
            string msg = "Workflow " + action + " successfully.";
            ScriptManager.RegisterClientScriptBlock(Page, GetType(), "Workflow", "showAlertDialog('success','" + msg + "');", true);
        }
    }

    private string md5(string sPassword)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(sPassword);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x2").ToLower());
        }
        return s.ToString();
    }


    public static string Decrypt(string inp)
    {
        string codes64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/!@#$%^&*(){}|[];:<>?,.";
        int i, a, x, b;
        string result = "";
        a = 0;
        b = 0;
        for (i = 0; i < inp.Length; i++)
        {

            x = (codes64.IndexOf(inp[i]));
            if (x >= 0)
            {
                b = b * 64 + x;
                a = a + 6;
                if (a >= 8)
                {
                    a = a - 8;
                    x = b >> a;
                    b = b % (1 << a);
                    x = x % 256;
                    result = result + Convert.ToChar(x);
                }
            }
            else
            {
                System.Environment.Exit(1);
            }
        }
        return result;
    }

    private void ParseLoginResult(string result)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(result);

        XmlNode resultNode = xmlDoc.SelectSingleNode("/result");
        foreach (XmlNode childNode in resultNode.ChildNodes)
        {
            if (childNode.Name == "globalvars")
            {
                HttpContext.Current.Session["axGlobalVars"] = "<globalvars>" + childNode.InnerXml + "</globalvars>";
                //foreach (XmlNode xmlNode in childNode.ChildNodes)
                //    globalVariables.Add(xmlNode.Name + "=" + xmlNode.InnerText);
            }
            else if (childNode.Name == "uservars")
            {
                HttpContext.Current.Session["axUserVars"] = "<uservars>" + childNode.InnerXml + "</uservars>";
                //foreach (XmlNode xmlNode in childNode.ChildNodes)
                //    userVariables.Add(xmlNode.Name + "=" + xmlNode.InnerText);
            }
        }
    }

}