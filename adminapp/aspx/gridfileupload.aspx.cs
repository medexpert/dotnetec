﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Configuration;
using System.Web.UI.HtmlControls;


public partial class gridfileupload : System.Web.UI.Page
{
    string succ = string.Empty;
    string rowNo = string.Empty;
    Util.Util util = new Util.Util();
    long lMaxFileSize = 1000000;
    int attachmentSizeMB = 1;
    string atFName = string.Empty;
    public string direction = "ltr";

    public string langType = "en";
    protected override void InitializeCulture()
    {
        if (Session["language"] != null)
        {
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(Session["language"].ToString().ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        util.IsValidSession();
        HtmlLink Link = FindControl("generic") as HtmlLink;
        Link.Href = util.toggleTheme();
        //if (Request.UrlReferrer != null)
        //{
        //    if (!(Request.UrlReferrer.AbsolutePath.ToLower().Contains("tstruct.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("gridfileupload.aspx")))
        //    {
        //        Response.Redirect("../cusError/axcustomerror.aspx");
        //    }
        //}

        if (!IsPostBack)
        {
            if (Request.QueryString["attFld"] != null)
            {
                if (!util.IsUserNameValid(Request.QueryString["attFld"].ToString()))
                    Response.Redirect(Constants.PARAMERR);
                hdnAttFld.Value = Request.QueryString["attFld"].ToString();
            }

            if (Request.QueryString["atFname"] != null)
                atFName = Request.QueryString["atFname"].ToString();

            if (Request.QueryString["dcNo"] != null)
                hdnDcNo.Value = Request.QueryString["dcNo"];
        }
        float maxSize = lMaxFileSize / 1000000;
        //lblTypeInfo.Text = "[Supported file types are word, Image and PDF]";

        //to get maximum attachment size from Config app
        if (Session["AxAttachmentSize"] != null)
            attachmentSizeMB = Convert.ToInt32(Session["AxAttachmentSize"]);
        lMaxFileSize = attachmentSizeMB * 1024 * 1024; //convert MB to Bytes
        fileuploadsts.Text = string.Format(GetLocalResourceObject("fileuploadsts.Text").ToString(), attachmentSizeMB); //replace filesize(in MB) using parameters
        lblfilecn.Text = string.Format(GetLocalResourceObject("lblfilecn.Text").ToString(), attachmentSizeMB);

    }
    protected void cmdSend_Click(object sender, EventArgs e)
    {
        HttpFileCollection httpAttFiles = Request.Files;
        string fNames = string.Empty;
        string sid = string.Empty;

        for (int i = 0; i < httpAttFiles.Count; i++)
        {
            HttpPostedFile httpAttFile = httpAttFiles[i];

            if ((httpAttFile != null) && (httpAttFile.ContentLength > 0))
            {
                int idx = hdnAttFld.Value.LastIndexOf("F");
                rowNo = hdnAttFld.Value.Substring(idx - 3, 3);
                if (Session["nsessionid"] != null)
                    sid = Session["nsessionid"].ToString();
                else
                {
                    SessExpires();
                    return;
                }
                string ScriptsPath = HttpContext.Current.Application["ScriptsPath"].ToString();
                try
                {
                    hdnFilePath.Value = HttpContext.Current.Application["ScriptsUrlPath"].ToString() + "Axpert//" + sid + "//";
                    DirectoryInfo di = new DirectoryInfo(ScriptsPath + "Axpert\\" + sid);
                    //' Determine whether the directory exists.
                    if (!di.Exists)
                        di.Create();

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string sFileName = System.IO.Path.GetFileName(httpAttFile.FileName);
                string Ext = sFileName.Substring(sFileName.LastIndexOf("."));
                sFileName = sFileName.Substring(0, sFileName.LastIndexOf("."));
                hdnType.Value = Ext;
                //   sFileName = sFileName + rowNo + Ext;
                sFileName = sFileName + Ext;
                string sFileDir = ScriptsPath + "Axpert\\" + sid + "\\";
              
                try
                {
                    if (util.IsFileTypeValid(httpAttFile))
                    {
                        if (httpAttFile.ContentLength <= lMaxFileSize)
                        {
                            if ((sFileDir + sFileName).Length > 260)//display warning message if file path exceeds 260 characters
                            {
                                fileuploadsts.Text = GetLocalResourceObject("lblFileSaveErr.Text").ToString();
                                upsts.Value = "";
                        }
                        if (Constants.fileTypes.Contains(httpAttFile.FileName.Substring(sFileName.LastIndexOf(".")).ToLower()) == false)
                            {
                                fileuploadsts.Text = "[Invalid File Extension]";
                                cmdSend.Enabled = false;
                                break;
                            }
                            else
                            {
                                //Save File on disk
                                httpAttFile.SaveAs(sFileDir + sFileName);//+ rowNo
                                fileuploadsts.Text = "[" + lblFileUp.Text + "]";
                                fileuploadsts.ForeColor = System.Drawing.Color.Green;
                                upsts.Value = "Uploaded Successfully";

                                if (fname.Value != "")
                                    fname.Value = fname.Value + ',' + sFileName;
                                else
                                    fname.Value = sFileName;// + rowNo

                                Page.ClientScript.RegisterStartupScript(this.GetType(), "DoClientFunction", "DoClientFunction()", true);
                            }
                        }
                        else
                        {
                            upsts.Value = "";
                            fileuploadsts.Text = "[" + lblfilecn.Text+"]";
                        }
                    }
                }
                catch (Exception ex)//in case of an error
                {
                    fileuploadsts.Text = lblAnError.Text;
                    upsts.Value = fileuploadsts.Text;
                }
            }
        }

    }

    private void SessExpires()
    {
        string url = Convert.ToString(HttpContext.Current.Application["SessExpiryPath"]);
        Response.Write("<script>" + Constants.vbCrLf);
        Response.Write("parent.parent.location.href='" + url + "';");
        Response.Write(Constants.vbCrLf + "</script>");
    }

}
