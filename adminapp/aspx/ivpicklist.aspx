<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ivpicklist.aspx.vb" Inherits="ivpicklist" %>

<!DOCTYPE html>
<html>

<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Pick List">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>IView Picklist</title>
    <link id="themecss" type="text/css" rel="Stylesheet" href="" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" type="text/css" />
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" id="generic" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="Link1" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <%End If%>
    <%--<link rel="Stylesheet" href="../Css/cssbutton.css" type="text/css" />--%>
    <%-- <link type="text/css" href="../Css/jQuery/jquery.tab.all.css" rel="stylesheet" />--%>
    <%-- <link href="../Css/jQuery/jquery.ui.all.css" rel="stylesheet" type="text/css" />--%>
    <%-- <link href="../Css/jQuery/jquery.ui.theme.css" rel="stylesheet" type="text/css" />--%>
    <%--<link href="../Css/jQuery/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.theme.min.css" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/JDate.min.js?v=2" type="text/javascript"></script>
    <%--<script src="../Js/jQueryUi/jquery-1.6.2.js" type="text/javascript"></script>--%>

    <%--     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>--%>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <script src="../Js/noConflict.min.js?v=1"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2"></script>
    <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <%--custom alerts end--%>
    <%-- <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>--%>
    <%-- <link href="../Css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <%-- <script src="../Js/jQueryUi/jquery-ui.js" type="text/javascript"></script>--%>
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="../Js/jQueryUi/jquery.ui.core.js"></script>--%>

    <script type="text/javascript">
        var srchFld = "";
    </script>
    <script src="../Js/ivpicklist.min.js?v=11"></script>
    <script src="../Js/common.min.js?v=62"></script>
</head>

<body>
    <form id="form1" runat="server" dir="<%=direction%>">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="../Js/iview.min.js?v=175" />
                </Scripts>
                <Services>
                    <asp:ServiceReference Path="../WebService.asmx" />
                </Services>
            </asp:ScriptManager>
        </div>
        <div id="dvPickList" runat="server" style="position: absolute; height: 242px; width: 500px; top: 1px;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:ListBox ID="searchlist" Visible="true" runat="server" Height="0px" Width="0px"
                        AutoPostBack="True"></asp:ListBox>
                    <asp:ListBox ID="searchlistval" Visible="true" runat="server" Height="0px" Width="0px"
                        AutoPostBack="True"></asp:ListBox>
                    <asp:ListBox ID="lstValues" Visible="true" runat="server" Height="0px" Width="0px"
                        AutoPostBack="True"></asp:ListBox>
                    <div style="border: solid #497D9E 1px;">
                        <asp:Panel ScrollBars="Auto" runat="server" ID="Panel1" Width="500px">
                            <asp:GridView CellSpacing="-1" ID="GridView1" runat="server" CellPadding="2"
                                ForeColor="black" GridLines="Vertical" Width="100%" AllowSorting="false"
                                RowStyle-Wrap="false" AutoGenerateColumns="false" PageSize="10" CssClass="Grid">
                                <Columns>
                                </Columns>
                                <HeaderStyle ForeColor="#000000" CssClass="GridHead" />
                                <AlternatingRowStyle CssClass="GridAltPage" />
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ScrollBars="Auto" runat="server" ID="Panel2" Width="498px">
                            <asp:Label ID="records" runat="server" Text="" CssClass="totrec"></asp:Label>
                            &nbsp;&nbsp;
                            <asp:Label ID="pgCap" Text="Page no : " runat="server" Visible="false" CssClass="totrec">
                            </asp:Label>
                            <asp:DropDownList ID="lvPage" runat="server" AutoPostBack="true" Visible="false">
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Label ID="lblErrMsg" runat="server" CssClass="seartem" Visible="false"></asp:Label>
                        <asp:TextBox ID="pgno" runat="server" Text="0" BorderStyle="None" ForeColor="white" Width="0px"
                            Height="0px" BackColor="white"></asp:TextBox>
                    </div>
                    <div style="display: none">
                        <asp:Button ID="btnTemp" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="paramXml" runat="server" />
            <asp:HiddenField ID="srchFld" runat="server" />
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <div id="progressArea" style="text-align: left; background-color: White;">
                        <asp:Label ID="lblgetdata" meta:resourcekey="lblgetdata" runat="server"> Getting the data,
                            please wait...</asp:Label>
                        <asp:Image ID="LoadingImage" runat="server" ImageUrl="../AxpImages/icons/5-1.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <asp:Label ID="lblNodata" runat="server" meta:resourcekey="lblNodata" Visible="false">No data found.</asp:Label>
    </form>

</body>

</html>