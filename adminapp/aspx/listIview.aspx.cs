﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using CacheMgr;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Web.UI.HtmlControls;


public partial class aspx_listIview : System.Web.UI.Page
{
    LogFile.Log logobj;
    Util.Util util;
    string _xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
    string gridPageSize = "0";
    public string lParams;
    public string lName;
    public string actBut;
    public string defaultBut;
    public string CustomListViewbtn;
    public string taskBut;
    public string proj;
    public string AxRole;
    public string user;
    public string sid;
    public string language;
    public string listDataCache;
    public string headerXmlVar;
    public string lViewCaption = string.Empty;
    public string tstScripts = string.Empty;
    public string errorLog = string.Empty;
    public StringBuilder tskList = new StringBuilder();
    public string findList = string.Empty;
    public string TaskBut = string.Empty;
    public string noRec = string.Empty;
    public string cac_order = string.Empty;
    public string cac_header = string.Empty;
    public string viewList = string.Empty;
    public string tblSubtotlLst = string.Empty;
    ArrayList custViews = new ArrayList();
    public string enableBackForwButton = string.Empty;
    public StringBuilder strFieldTypes = new StringBuilder();
    public StringBuilder strBreadCrumb = new StringBuilder();
    public string strBreadCrumbBtns = string.Empty;
    public string breadCrumb = string.Empty;
    public int resrows;
    public string direction = "ltr";
    public string btn_direction = "right";
    public string bread_direction = "left";

    public TStructDef strobj;
    public string idField = string.Empty;
    int totalRows = 0;
    LogFile.Log logObj = new LogFile.Log();
    ArrayList yesNoColumns = new ArrayList();
    bool isFilterEnabled;
    public static string ColsToFilter = string.Empty;
    public static DataTable dtFilterConds = new DataTable();
    public StringBuilder filterCondDispText = new StringBuilder();
    public StringBuilder filterCondDispFullText = new StringBuilder();

    //int defaultColWidth = 50;

    ArrayList headName = new ArrayList();
    public ArrayList defaultfldName = new ArrayList();
    ArrayList colWidth = new ArrayList();
    ArrayList colType = new ArrayList();
    ArrayList colHide = new ArrayList();
    ArrayList rtfCol = new ArrayList();
    ArrayList colFld = new ArrayList();
    ArrayList colDec = new ArrayList();
    ArrayList colHlink = new ArrayList();
    ArrayList colMap = new ArrayList();
    ArrayList cancelFlag = new ArrayList();
    ArrayList colAlign = new ArrayList();

    DataTable navigationInfo = new DataTable();
    ArrayList fldName = new ArrayList();
    ArrayList Caption = new ArrayList();
    ArrayList dcNo = new ArrayList();
    ArrayList fldType = new ArrayList();
    ArrayList HiddenCols = new ArrayList();

    string dbType = string.Empty;
    string lvKey = string.Empty;

    List<string> lstRecordIds = new List<string>();
    Dictionary<int, string> lvRecordListing = new Dictionary<int, string>();

    string fileName = string.Empty;
    static string[] filterCondText = { "Equal To", "Not Equal To", "Containing", "Not Containing", "Less than", "Greater than", "Less than or equal to", "Greater than or equal to", "Between", "Starts with", "Ends with" };
    static string[] filterCondValues = { "=", "!=", "contains", "not contains", "lessthan", "greaterthan", "lessthan equal", "greaterthan equal", "between", "start with", "end with" };
    ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
    public ListviewData objListViewData = new ListviewData();
    public bool isCloudApp = false;

    public string langType = "en";
    protected override void InitializeCulture()
    {
        if (Session["language"] != null)
        {
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(Session["language"].ToString().ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        util = new Util.Util();
        util.IsValidSession();
        ResetSessionTime();

        HtmlLink Link = FindControl("generic") as HtmlLink;
        Link.Href = util.toggleTheme();
        if (Session["project"] == null || Session["project"] == string.Empty)
        {
            SessExpires();
            return;
        }
        else
        {
            if (!util.licencedValidSessionCheck())
            {
                HttpContext.Current.Response.Redirect(util.ERRPATH + Constants.SESSIONEXPMSG, false);
                return;
            }
            //if (Request.UrlReferrer != null)
            //{
            //   if (!(Request.UrlReferrer.AbsolutePath.ToLower().Contains("tstruct.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("listiview.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("homepage.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("iview.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("cpwd.aspx") || Request.UrlReferrer.AbsolutePath.ToLower().Contains("ParamsTstruct.aspx")))
            //    {
            //       Response.Redirect("../CusError/axcustomerror.aspx");
            //   }
            // }
            if (Session["axdb"] != null)
                dbType = Session["axdb"].ToString();
            logobj = new LogFile.Log();
            util = new Util.Util();
            defaultBut = "";
            taskBut = "";
            actBut = "";
            lParams = "";
            lViewCaption = "";
            lName = "";
            sid = Session["nsessionid"].ToString();
            listDataCache = sid + "order";
            headerXmlVar = sid + "header";
            fileName = "openlview-" + lName;
            objListViewData.HeadRowXML = string.Empty;
            objListViewData.Listres = string.Empty;


            if (!(string.IsNullOrEmpty(Session["language"].ToString())))
            {
                language = Session["language"].ToString();
            }
            else
            {
                language = string.Empty;
            }

            if (!IsPostBack)
            {
                if (util.IsValidQueryString(Request.RawUrl) == false)
                HttpContext.Current.Response.Redirect(util.ERRPATH + Constants.INVALIDURL);
                //refreshFilter.Visible = false;
                GetGlobalVariables();
                // GetBreadCrumb();
                CleardtFilterCond();
                gridPageSize = Session["AxDefaultPageSize"].ToString().Trim();
                errorLog = logobj.CreateLog("Loading Listliview.aspx", sid, fileName, "new");
                string pageNo = "1";
                if (Request.QueryString["pgno"] != null)
                    pageNo = Convert.ToString(Request.QueryString["pgno"]);
                if (navigationInfo.Rows.Count > 0)
                    Session["navigationInfoTable"] = navigationInfo;

                CacheManager cacheMgr = GetCacheObject();
                strobj = GetStrObject(cacheMgr);

                GetLViewData(pageNo);
                FillColumns();
                GetDcAndFieldData();
                Session["FilteredData"] = null;
                objListViewData.FilterXml = string.Empty;

                lvKey = util.GetLviewId(lName);
                Session[lvKey] = objListViewData;
                hdnlvKey.Value = lvKey;

            }
            else
            {
                lvKey = hdnlvKey.Value;
                objListViewData = (ListviewData)Session[lvKey];
                gridPageSize = GridView1.PageSize.ToString();
                SetGlobalVariables();
                //To make check all checkbox visible even after postback
                GridView1.HeaderRow.Cells[0].Text = "<input name='chkall' id='chkall' style=\"float:left; height:12px;\" type=checkbox onclick='javascript:CheckAll()'>";

            }
            SetLangStyles();
            ConstructBreadCrumb();

            tstScripts = tstScripts + "<script language=\"javascript\" type=\"text/javascript\" >function setDocht(){ Adjustwin();}ScrollToTop();</script>";
            tstScripts += "<Script language=javascript> var proj = '" + proj + "';var user='" + user + "';var sid='" + sid + "';var iName='" + lName + "'; var ivna='" + lName + "';var tst ='" + lName + "'; var ivtype='listview';var trace = '" + Session["AxTrace"] + "';var AxRole='" + AxRole + "';var gl_language = '" + language + "';</script>";
            btnClear.Attributes.Add("onclick", "javascript:pgRefresh();");
            logobj.CreateLog("End Time : " + DateTime.Now.ToString(), sid, fileName, "");
            ValidateFilterCols();
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "ParamValues", "<script>AxWaitCursor(false);ShowDimmer(false);</script>", false);
            string frameName = string.Empty;
            if (Request.QueryString["axpfrm"] != null)
                frameName = Convert.ToString(Request.QueryString["axpfrm"]);
            if ((Session["backForwBtnPressed"] != null && !Convert.ToBoolean(Session["backForwBtnPressed"])) && Request.QueryString["homeicon"] == null && frameName!="t")
                util.UpdateNavigateUrl(HttpContext.Current.Request.Url.AbsoluteUri);
            //remove key from querystring
            if (Request.QueryString["homeicon"] != null)
            {
                System.Reflection.PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                isreadonly.SetValue(this.Request.QueryString, false, null);
                this.Request.QueryString.Remove("homeicon");
            }
            Session["backForwBtnPressed"] = false;
            enableBackForwButton = "<script language=\'javascript\' type=\'text/javascript\' > enableBackButton='" + Convert.ToBoolean(Session["enableBackButton"]) + "';" + " enableForwardButton='" + Convert.ToBoolean(Session["enableForwardButton"]) + "';</script>";

            if (ConfigurationManager.AppSettings["isCloudApp"] != null)
            {
                isCloudApp = Convert.ToBoolean(ConfigurationManager.AppSettings["isCloudApp"].ToString()); ;
            }
            Page.ClientScript.RegisterStartupScript(GetType(), "set global var in iview", "<script>var isCloudApp = '" + isCloudApp.ToString() + "';</script>");
        }
        if (isFilterEnabled)
        {
            if (hdnIsParaVisible.Value != string.Empty)
            {
                if (hdnIsParaVisible.Value == "0")
                {
                    //   leftPanel.Style.Add("display", "none");
                    // showFilter.Style.Add("display", "block");
                }
                else
                {
                    //    leftPanel.Style.Add("display", "block");
                    //    showFilter.Style.Add("display", "none");
                }
            }
            else
            {
                //    leftPanel.Style.Add("display", "none");
                //   showFilter.Style.Add("display", "block");
            }
        }
        CloseDimmerJS();
        printRowsMaxLimitField.Value = Session["AxPrintRowsMaxLimit"].ToString();
        objListViewData.IsDirectDBcall = false;
        AddCustomLinks();


    }

    private void ResetSessionTime()
    {
        if (Session["AxSessionExtend"] != null && Session["AxSessionExtend"].ToString() == "true")
        {
            HttpContext.Current.Session["LastUpdatedSess"] = DateTime.Now.ToString();
            ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "eval(callParent('ResetSession()', 'function'));", true);
        }
    }

    protected override PageStatePersister PageStatePersister
    {
        get
        {
            return new SessionPageStatePersister(Page);
        }
    }
    public void setPageDirection()
    {
        if (language.ToLower() == "arabic")
        {
            direction = "rtl";
            btn_direction = "left";
            bread_direction = "right";
        }
    }
    public void CleardtFilterCond()
    {
        dtFilterConds.Rows.Clear();
        dtFilterConds.Columns.Clear();
        dtFilterConds.Columns.Add("FilterColumn", typeof(string));
        dtFilterConds.Columns.Add("Value", typeof(string));

    }

    public void AddGridViewHeaderDimmer(GridView gridView, GridViewRow gridViewRow)
    {
        for (int i = 0; i < gridView.Columns.Count; i++)
        {
            string sortExpression = gridView.Columns[i].SortExpression;
            TableCell tableCell = gridViewRow.Cells[i];

            //Make sure the column we are working with has a sort expression
            if (!string.IsNullOrEmpty(sortExpression))
            {
                //Enumerate the controls within the current cell and find the link button.
                foreach (Control gridViewRowCellControl in gridViewRow.Cells[i].Controls)
                {
                    LinkButton linkButton = gridViewRowCellControl as LinkButton;

                    if ((linkButton != null) && (linkButton.CommandName == "Sort"))
                    {
                        //Add an onclick attribute to the current cell
                        linkButton.Attributes.Add("onclick", "ShowDimmer(true);");
                        break;
                    }
                }
            }
        }
    }

    private void ValidateFilterCols()
    {
        if (ddlFilcond.SelectedValue != "between")
        {
            filVal2.Enabled = false;
            filVal2.CssClass = "txtfilter flddis Family";
        }
        else
        {
            filVal2.Enabled = true;
            filVal2.CssClass = "txtfilter tem Family";
        }
    }

    private void ClearGridData()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }

    private void GetLViewData(string pageNo)
    {
        ClearGridData();
        util.ClearSession();

        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        Boolean IsYesNoColumn = false;
        string paramFld = string.Empty;
        string iXml = string.Empty;

        if ((ddlFilter.SelectedIndex > 0))
        {
            filterColumn = ddlFilter.SelectedValue;
        }
        if (GetColIndex(filterColumn) > -1)
        {
            IsYesNoColumn = true;
        }
        if ((ddlFilcond.SelectedIndex > 0))
        {
            filterOpr = ddlFilcond.SelectedValue;
            filterOpr = util.CheckSpecialChars(filterOpr);
            if (ddlFilcond.SelectedValue == "between")
            {
                filterValue1 = filVal2.Text;
                filterValue1 = util.CheckSpecialChars(filterValue1);
            }
        }
        if (!string.IsNullOrEmpty(txtFilter.Text))
        {
            filterValue = txtFilter.Text;
            filterValue = util.CheckSpecialChars(filterValue);
        }
        if (IsYesNoColumn == true)
        {
            //If the value is not yes or no it will take the original value entered by the user.
            if (filterValue.ToLower() == "yes")
            {
                filterValue = "T";
            }
            else if (filterValue.ToLower() == "no")
            {
                filterValue = "F";
            }
        }
        if (dbType.ToLower() == "oracle")
        {
            if (ViewState["SortColumnName"] != null)
                sortCol = ViewState["SortColumnName"].ToString();
        }
        else
        {
            if (ViewState["SortColumn"] != null)
                sortCol = ViewState["SortColumn"].ToString();
        }
        if (ViewState["SortDir"] != null)
            sortOrder = ViewState["SortDir"].ToString();
        string ixml1 = "";
        bool backForwBtnPressed = Convert.ToBoolean(Session["backForwBtnPressed"]);
        if (backForwBtnPressed == true)
        {
            if (Convert.ToString(Session["CustomView"]) != string.Empty && Convert.ToString(Session["CustViewTransId"]) == lName)
                hdnSelectedView.Value = Convert.ToString(Session["CustomView"]);
            else
                hdnSelectedView.Value = lName;
        }
        else
        {
            if (hdnSelectedView.Value.ToLower().Trim() == "default" || hdnSelectedView.Value == string.Empty)
            {
                hdnSelectedView.Value = lName;
                Session["CustomView"] = lName;
            }
        }

        iXml = "<root name =\"" + lName + "\"  custviewname =\"" + hdnSelectedView.Value + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey=\"" + HttpContext.Current.Session["AppSessionKey"].ToString() + "\" username=\"" + HttpContext.Current.Session["username"].ToString() + "\" trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
        iXml = iXml + "</params>";
        iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString() + "</root>";
        param.Value = paramFld;
        paramxml.Value = ixml1;
        param.Value = "listview";

        Session["sOrder"] = sortOrder;
        Session["sColumn"] = sortCol;
        Session["fCol"] = filterColumn;
        Session["fColVal"] = filterValue;
        Session["fColVal2"] = filterValue1;
        Session["fcolopr"] = filterOpr;

        ClearArrays();

        logobj.CreateLog("Start Time " + DateTime.Now.ToString(), sid, fileName, "");
        logobj.CreateLog("", sid, fileName, "");
        string ires = string.Empty;
        //Call service

        ires = objWebServiceExt.CallGetLViewWS(lName, iXml, objWebServiceExt.asbIview.Timeout);

        string errMsg = string.Empty;
        errMsg = util.ParseXmlErrorNode(ires);
        if (errMsg != string.Empty)
        {
            if (errMsg == Constants.SESSIONERROR)
            {
                Session.RemoveAll();
                Session.Abandon();
                SessExpires();
                return;
            }
            else if (errMsg == Constants.SESSIONEXPMSG)
            {
                SessExpires();
                return;
            }
            else
            {
                Response.Redirect(util.ERRPATH + errMsg);
            }
        }
        else
        {
            logobj.CreateLog("Setting Lview components", sid, fileName, "");
            ires = _xmlString + ires;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(ires);
            objListViewData.Ires = ires;

            if (!IsPostBack)
            {
                CreateToolbarButtons(xmlDoc);
                objListViewData.ToolBarBtns = defaultBut;
            }
            CreateHeaderRow(xmlDoc, pageNo);

            //Remove headrow
            XmlNode rnode = default(XmlNode);
            rnode = xmlDoc.SelectSingleNode("//headrow");
            rnode.ParentNode.RemoveChild(rnode);

            //Remove Comps
            XmlNode cNode = default(XmlNode);
            cNode = xmlDoc.SelectSingleNode("//comps");

            string cNodeXml = cNode.OuterXml;
            //Prepares header for printing filtered records.
            objListViewData.Listres = cNodeXml;

            cNode.ParentNode.RemoveChild(cNode);

            //For Customize view list
            XmlNode vNode = default(XmlNode);
            vNode = xmlDoc.SelectSingleNode("//customizeview");
            vNode.ParentNode.RemoveChild(vNode);
            //--------------

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);


            string nXml = null;
            nXml = sw.ToString();

            // assign XML Val to input textbox
            rXml.Value = nXml;
            rXml.Value = rXml.Value.Replace("\"", "'");

            if (nXml == "<?xml version=\"1.0\" encoding=\"utf-8\"?><data></data>")
            {
                //records.Text = Constants.REC_NOT_FOUND;


                // add and remove a hidden buttons after no records string
                string norecordsClass = "norecords";
                // add a hidden buttons after no records class
                searchBar.Attributes.Add("class", String.Join(" ", searchBar
                           .Attributes["class"]
                           .Split(' ')
                           .Except(new string[] { "", norecordsClass })
                           .Concat(new string[] { norecordsClass })
                           .ToArray()
                   ));

                noRecccord.Style.Add("display", "table-cell");
                GridView1.Visible = false;
            }
            else
            {
                // nXml = util.RemoveAppSessKeyAtt(nXml, "appsessionkey");
                GridView1.Visible = true;
                BindData(nXml, totalRows, pageNo);
            }

        }
        if (!IsPostBack)
            CreateTreeView();
        UpdateRecordsPerPage();
        // VisibleGridViewData();
    }

    private void CloseDimmerJS()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "closeParentFrame();", true);
    }
    private void BindData(string data, int totalRows, string pageNo)
    {
        objListViewData.GridPageNo = pageNo;
        hdnTotalRec.Value = totalRows.ToString();
        logobj.CreateLog("Binding data to grid", sid, fileName, "");
        DataSet ds = new DataSet();
        StringReader sr = new StringReader(data);
        try
        {
            ds.ReadXml(sr);
        }
        catch (Exception ex)
        {
            CloseDimmerJS();
            Response.Redirect(util.ERRPATH + ex.Message);
        }

        //see line 1244 for details
        //ds.Tables[0].Rows.RemoveAt(0);
        // Important : the datasource store in session as datatable. for paging and sorting

        // IMP : Create a new dataset - use clone - which create new structure then change
        //       Column datatype to int, double,string and date - which is needed for Sorting


        int n = 0;
        n = 0;
        DataSet ds1 = new DataSet();
        ds1 = ds.Clone();

        foreach (DataColumn dc1 in ds1.Tables[0].Columns)
        {
            if (colType[n].ToString() == "n")
            {
                dc1.DataType = typeof(string);
            }
            else if (colType[n].ToString() == "d")
            {
                dc1.DataType = typeof(string);
            }
            else
            {
                dc1.DataType = typeof(string);
            }
            n = n + 1;
        }


        //Dim culture As System.Globalization.CultureInfo
        //culture = New System.Globalization.CultureInfo("en-GB")
        int rno = 0;
        rno = 0;
        foreach (DataRow dr1 in ds.Tables[0].Rows)
        {
            // Before import ds to ds1 change the row value from str to date while datacol type is date
            // rno for find row no and id for col ... make new date then attach to dr1 -datarow then import 
            int id = 0;
            for (id = 0; id <= colType.Count - 1; id++)
            {
                if (colType[id].ToString() == "d")
                {
                    string actDt = null;
                    actDt = ds.Tables[0].Rows[rno][id].ToString();
                    if (string.IsNullOrEmpty(actDt))
                    {
                        //    actDt = "01/01/1900"
                        actDt = "";
                    }

                    dr1[id] = actDt;

                    // handle Zero off
                }
                else if (colType[id].ToString() == "n")
                {
                    string nVal = null;
                    nVal = ds.Tables[0].Rows[rno][id].ToString();
                    if (string.IsNullOrEmpty(nVal))
                    {
                        nVal = "0";
                    }
                    dr1[id] = nVal;
                }
            }

            ds1.Tables[0].ImportRow(dr1);
            int recPos = (Convert.ToInt32(ds1.Tables[0].Rows[rno]["rowno"]) + ((Convert.ToInt32(objListViewData.GridPageNo) - 1) * Convert.ToInt32(gridPageSize)));
            lstRecordIds.Add(ds1.Tables[0].Rows[rno]["recordid"].ToString());
            lvRecordListing.Add(recPos, ds1.Tables[0].Rows[rno]["recordid"].ToString());
            rno = rno + 1;
        }
        Session["lstRecordIds"] = lstRecordIds.ToList();

        Session["lvRecordListing-" + lName] = lvRecordListing;
        Session["lvPageNo"] = pageNo;
        Session["lvPageSize"] = gridPageSize;
        Session["lvTotalRows"] = totalRows;
        //Session["lvRecPos"] = gridPageSize;


        Session["recordTransId"] = lName;
        Cache.Insert(cac_order, ds1.Tables[0], null, DateTime.Now.AddMinutes(20), TimeSpan.Zero);
        Session["cac_order"] = ds1.Tables[0];
        resrows = ds1.Tables[0].Rows.Count;

        GridView1.Columns.Clear();
        DataColumn dc = null;
        if (ds1.Tables.Count > 0)
        {

            foreach (DataColumn dc_loopVariable in ds1.Tables[0].Columns)
            {
                dc = dc_loopVariable;
                BoundField bfield = new BoundField();
                //'initialiae the data field value
                bfield.DataField = dc.ColumnName;
                //'initialise the header text value
                bfield.HeaderText = dc.ColumnName;
                //' add newly created columns to gridview
                GridView1.Columns.Add(bfield);
                //For setting grid Header height
                GridView1.HeaderStyle.Height = 25;
            }
        }
        GridView1.DataSource = ds1;

        objListViewData.DtListData = ds1.Tables[0];

        ClearChartColumns();
        if ((!IsPostBack))
        {
            ddlFilter.Items.Clear();
            ddlFilter.Items.Add("");
        }

        int index = 0;

        // to change the header Name and set the column width
        int idx = 0;
        for (idx = 0; idx <= headName.Count - 1; idx++)
        {
            if (colHide[idx].ToString() == "false")
            {
                // For change the Column Heading from fld name to Caption
                //GridView1.Columns[idx].HeaderText = headName[idx].ToString();
                GridView1.Columns[idx].HeaderText = headName[idx].ToString().Replace("~", "<br/>");
                //For Sorting set sortexp to newly set column heading
                //     GridView1.Columns[idx].SortExpression = colFld[idx].ToString();
                // For set width of the Column                       
                GridView1.Columns[idx].ItemStyle.Width = Convert.ToInt32(colWidth[idx]);

                GridView1.Columns[idx].HeaderStyle.Wrap = false;
                //GridView1.Columns[idx].ItemStyle.Wrap = false;

                if (headName[idx].ToString() != string.Empty)
                    AddChartColumns(headName[idx].ToString(), idx, colType[idx].ToString());

                if ((!IsPostBack))
                {
                    if ((!string.IsNullOrEmpty(GridView1.Columns[idx].HeaderText)))
                    {
                        foreach (DataColumn dc_loopVariable in ds1.Tables[0].Columns)
                        {
                            dc = dc_loopVariable;
                            if (index == idx)
                            {
                                ddlFilter.Items.Add(new ListItem(headName[idx].ToString(), dc.ColumnName.ToString()));
                                index = 0;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                            index = index + 1;
                        }
                    }
                }
            }
            else
            {
                GridView1.Columns[idx].Visible = false;
            }
            //Code to remove sorting for textarea fields.
            if (colType[idx].ToString() == "t")
                GridView1.Columns[idx].SortExpression = "";

        }

        if (gridPageSize == "0" | string.IsNullOrEmpty(gridPageSize))
        {
            gridPageSize = "10";
        }

        try
        {
            GridView1.PageSize = Convert.ToInt32(gridPageSize);
        }
        catch (Exception ex)
        {
            GridView1.PageSize = 10;
        }

        GridView1.DataBind();

        double pg = Convert.ToInt32(totalRows) / Convert.ToInt32(GridView1.PageSize);
        int pg1 = Convert.ToInt32(Math.Floor(pg));
        if ((totalRows % GridView1.PageSize) > 0)
        {
            pg1 += 1;
        }

        // add and remove a hidden buttons after no records string
        string norecordsClass = "norecords";

        if (totalRows > 0)
        {
            //remove a hidden buttons after no records class
            searchBar.Attributes.Add("class", String.Join(" ", searchBar
                     .Attributes["class"]
                     .Split(' ')
                     .Except(new string[] { "", norecordsClass })
                     .ToArray()
             ));

            noRecccord.Style.Add("display", "none");


            records.Text = "Total rows: " + totalRows;
            pages.Text = " of " + pg1;
            pgCap.Visible = true;
            lvPage.Visible = true;
            GridView1.Visible = true;
        }
        else
        {
            // add a hidden buttons after no records class
            searchBar.Attributes.Add("class", String.Join(" ", searchBar
                       .Attributes["class"]
                       .Split(' ')
                       .Except(new string[] { "", norecordsClass })
                       .Concat(new string[] { norecordsClass })
                       .ToArray()
               ));

            noRecccord.Style.Add("display", "table-cell");


            // records.Text = Constants.REC_NOT_FOUND;
            hdnNoOfRecords.Value = "";
            GridView1.Visible = false;
            pgCap.Visible = false;
            lvPage.Visible = false;
            pages.Text = "";
        }

        int Newpgno = 0;
        lvPage.Items.Clear();
        Newpgno = Convert.ToInt32(pageNo);
        for (Newpgno = 1; Newpgno <= pg1; Newpgno++)
        {
            lvPage.Items.Add(Newpgno.ToString());
        }
        lvPage.SelectedValue = pageNo;


        logobj.CreateLog("Binding data to grid completed", sid, fileName, "");

    }

    /// Rowdatabound function of a gridview where the first column is made checkbox as per requirement and also binds other data.
    protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        System.Data.DataRowView drv = null;
        int i = 0;
        string sstr = null;
        string fstr = string.Empty;
        drv = (System.Data.DataRowView)e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (string.IsNullOrEmpty(e.Row.Cells[0].Text) | e.Row.Cells[0].Text == " " | e.Row.Cells[0].Text == "&nbsp;")
            {
                e.Row.Cells[0].Text = "<input name='chkall' id='chkall' style=\"float:left;height:12px;\" type=checkbox onclick='javascript:CheckAll()'>";
            }
            int rtfIndex = 0;
            for (rtfIndex = 0; rtfIndex <= colType.Count - 1; rtfIndex++)
            {
                if (e.Row.Cells[rtfIndex].Text.StartsWith("rtf_") || e.Row.Cells[rtfIndex].Text.StartsWith("rtfm_") || e.Row.Cells[rtfIndex].Text.StartsWith("fr_rtf_"))
                {
                    rtfCol.Add("true");
                }
                else
                {
                    rtfCol.Add("false");
                }
            }
            AddGridViewHeaderDimmer(GridView1, e.Row);
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (drv != null)
            {
                // first change  col 1 to check box
                string catName = drv[0].ToString();
                // for change the content to component like check box or input box
                //to Remove checkbox from first column
                e.Row.Cells[0].Text = "<input name='chkItem' style=\"float:left; height:12px;\" type=checkbox value=" + catName + " onclick='javascript:ChkHdrCheckbox();'>";

                //for  hide the columns
                int idx = 0;
                for (idx = 0; idx <= colHide.Count - 1; idx++)
                {
                    if (colHide[idx].ToString() == "true")
                    {
                        e.Row.Cells[idx].Visible = false;
                    }
                    if (colType[idx].ToString() == "n" & colDec[idx].ToString() == "0")
                    {
                        if (language.ToLower() == "arabic")
                            e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Left;
                        else
                            e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Right;
                    }
                    else if (colType[idx].ToString() == "n")
                    {
                        if (language.ToLower() == "arabic")
                            e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Left;
                        else
                            e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Right;
                        //format numeric with decimal pts
                        e.Row.Cells[idx].Text = string.Format("{0:n" + colDec[idx] + "}", Convert.ToDouble(drv[idx].ToString()));

                    }
                    else if (colType[idx].ToString() == "d")
                    {
                        string tdt = null;
                        if ((!string.IsNullOrEmpty(drv[idx].ToString())))
                        {
                            tdt = drv[idx].ToString();
                            e.Row.Cells[idx].Text = tdt.ToString();
                        }
                        else
                        {
                            e.Row.Cells[idx].Text = "&nbsp;";
                        }

                        e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Center;
                    }
                    else if (colType[idx].ToString() == "t")
                    {
                        string decodedText = HttpUtility.HtmlDecode(e.Row.Cells[idx].Text);
                        decodedText = HttpUtility.HtmlDecode(decodedText);
                        e.Row.Cells[idx].Text = decodedText;
                        if (drv[idx].ToString().IndexOf("\n") != -1)
                        {

                            e.Row.Cells[idx].Text = drv[idx].ToString().Replace("\n", "<br>");
                        }

                    }

                    // cal map
                    // First split the Map value and then find the params value
                    // check only if colhide is false

                    string @params = null;
                    @params = "";
                    if (colHide[idx].ToString() == "false")
                    {
                        if (colMap[idx].ToString() != "-")
                        {
                            // spilt colmap 
                            string[] spiltOut = colMap[idx].ToString().Split(',');
                            int s = 0;
                            for (s = 0; s < spiltOut.Length; s++)
                            {
                                int pos;
                                pos = (spiltOut[s].ToString().IndexOf("=:", 0) + 1);
                                if(pos == 0)
                                {
                                    pos = (spiltOut[s].ToString().LastIndexOf("=", 0) + 1);
                                }
                                if (pos > 0)
                                {
                                    fstr = spiltOut[s].ToString().Substring(0, (pos - 1));

                                    sstr = spiltOut[s].ToString().Substring(pos + 1);
                                }
                                if ((sstr == "recordid"))
                                {
                                    fstr = "recordid";
                                }
                                int g = 0;
                                for (g = 0; g <= colFld.Count - 1; g++)
                                {

                                    if (colFld[g].ToString() == sstr)
                                    {
                                        break; // TODO: might not be correct. Was : Exit For
                                    }
                                }
                                string sstrval = string.Empty;
                                if (colFld.Count != g)
                                {
                                    sstrval = drv[g].ToString();
                                    if (sstrval.Contains("&"))
                                    {
                                        sstrval = sstrval.Replace("&", "amp;");
                                    }
                                }
                                int recPos = (Convert.ToInt32(drv["rowno"]) + ((Convert.ToInt32(objListViewData.GridPageNo) - 1) * ((GridView)sender).PageSize));
                                string pageType = string.Empty;

                                if (recPos == 1 && Convert.ToInt32(Session["lvTotalRows"]) == 1)
                                {
                                    pageType = "single";
                                }
                                else if (recPos == 1)
                                {
                                    pageType = "first";
                                }
                                else if (recPos == Convert.ToInt32(Session["lvTotalRows"]))
                                {
                                    pageType = "last";
                                }

                                else
                                {
                                    pageType = "middle";
                                }
                                @params = @params + Server.HtmlEncode("&" + fstr + "=" + sstrval + "&recPos=" + recPos.ToString() + "&curPage=" + Session["lvPageNo"] + "&pageType=" + pageType);
                            }
                        }
                    }

                    Session["colHlink"] = colHlink;

                    // set for hyper link
                    if (colHlink[idx].ToString() != "-" & drv[idx].ToString() != "Grand Total")
                    {
                        string hl = null;
                        string na = null;
                        hl = colHlink[idx].ToString().Substring(0, 1);
                        na = colHlink[idx].ToString().Substring(1);
                        if (((drv[idx].ToString() == string.Empty) | (drv[idx] == null)))
                        {
                            e.Row.Cells[idx].Text = " ";
                        }
                        else
                        {
                            drv[idx] = util.CheckSpecialChars(drv[idx].ToString());
                            if (hl == "p")
                            {
                                e.Row.Cells[idx].Text = "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();SetColumnName(\"" + sstr + "\",\"" + e.Row.RowIndex + "\");' href='./tstruct.aspx?name=" + na + @params + "' class=l2 target=_parent alt=\"" + drv[idx].ToString() + "\">" + drv[idx].ToString() + "</a>";
                            }
                            else if (hl == "t")
                            {
                                if (rtfCol[idx] == "true")
                                {
                                    e.Row.Cells[idx].Text = "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();SetColumnName(\"" + sstr + "\",\"" + e.Row.RowIndex + "\");loadAxTstruct(\"tstruct.aspx?transid=" + na + @params + "\");' class=l2 alt=\"{Rich Text Box...}\" >" + drv[idx].ToString() + "</a>";
                                }
                                else
                                {
                                    e.Row.Cells[idx].Text = "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();SetColumnName(\"" + sstr + "\",\"" + e.Row.RowIndex + "\");loadAxTstruct(\"tstruct.aspx?transid=" + na + @params + "\")' class=l2 alt=\"" + drv[idx].ToString() + "\" >" + drv[idx].ToString() + "</a>";
                                }
                            }
                            else if (hl == "i")
                            {
                                e.Row.Cells[idx].Text = "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();loadAxTstruct(\"ivtoivload.aspx?ivname=" + na + @params + "\");'  class=l2 target=_parent alt=\"" + drv[idx].ToString() + "\">" + drv[idx].ToString() + "</a>";
                            }
                        }
                        if (i == 0)
                        {
                            if (Session["recordTransId"].ToString() == na)
                            {
                                i++;
                                string spn = "<span style=\"float:left;width:62px\">";
                                e.Row.Cells[0].Text = spn + e.Row.Cells[0].Text + "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();loadAxTstruct(\"tstruct.aspx?transid=" + na + @params + "\")'>"
                                    + "<span class=\"icon-software-pencil\" style=\"color:black;font-size:12px;border:0px;\" title=\"edit record\" ></span></a>"
                                    + "<a onclick='javascript:LoadPopPage(\"tstruct.aspx?transid=" + na + @params + "\",\"" + lvPage.SelectedValue + "\"," + "\"true" + "\")' style=\"margin-left:3px;cursor:pointer;\">"
                                    + "<span class=\"icon-basic-sheet-multiple\" style=\"color:black;font-weight:bold;border:0px;\" title=\"popup record\" ></span></a>"
                                    + "</span>";
                                //   e.Row.Cells[0].Text = spn + e.Row.Cells[0].Text + "<a onclick='javascript:AxWaitCursor(true);window.parent.loadFrame();' href='./tstruct.aspx?transid=" + na + @params + "'>" + "<img src=\"../AxpImages/icons/16x16/edittstruct.png\" title=\"edit record\" border=0></a><img src=\"../AxpImages/icons/16x16/poptstruct.png\" style=\"margin-left:3px;cursor:pointer;\" title=\"popup record\" border=0 onclick='javascript:LoadPopPage(\"tstruct.aspx?transid=" + na + @params + "\", \"\"," + "\"true" + "\")'></span>";
                            }
                            else
                            {
                                if (sstr != null)
                                {
                                    if (navigationInfo.Columns.Contains(sstr))
                                    {
                                        if (navigationInfo.Columns.IndexOf(sstr) == 0)
                                            navigationInfo.Rows.Add();
                                        navigationInfo.Rows[navigationInfo.Rows.Count - 1][sstr] = "transid=" + na + @params.Replace("amp;", "");
                                    }
                                    else
                                    {
                                        navigationInfo.Columns.Add(sstr, typeof(string));
                                        if (navigationInfo.Rows.Count == 0)
                                            navigationInfo.Rows.Add("transid=" + na + @params.Replace("amp;", ""));
                                        else
                                            navigationInfo.Rows[navigationInfo.Rows.Count - 1][sstr] = "transid=" + na + @params.Replace("amp;", "");
                                    }
                                }
                            }
                        }
                    }

                    if (colAlign[idx].ToString() != string.Empty)
                    {
                        string alignStr = colAlign[idx].ToString();
                        if (alignStr == "Center")
                        {
                            e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Center;
                        }
                        else
                        {
                            if (language.ToLower() == "arabic")
                            {
                                if (alignStr == "Right")
                                    e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Left;
                                else if (alignStr == "Left")
                                    e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Right;
                            }
                            else
                            {
                                if (alignStr == "Right")
                                    e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Right;
                                else if (alignStr == "Left")
                                    e.Row.Cells[idx].HorizontalAlign = HorizontalAlign.Left;

                            }
                        }
                    }

                    if (cancelFlag[idx].ToString() == "T")
                    {
                        if (e.Row.Cells[idx].Text == "T")
                        {
                            e.Row.Cells[idx].Text = "<img src=\"../AxpImages/cancelicon.jpg\" >";
                        }
                        else
                        {
                            e.Row.Cells[idx].Text = "";
                        }
                    }

                    if ((!string.IsNullOrEmpty(util.IviewWrap)) && (util.IviewWrap.ToLower() == "true"))
                        e.Row.Cells[idx].Attributes.Add("style", "white-space: normal;");

                }

            }

            //for NOWRAP in IE
            int m = 0;
            for (m = 0; m <= e.Row.Cells.Count - 1; m++)
            {
                string rowText = e.Row.Cells[m].Text.ToString();

                //For Standared KeyWords
                if (e.Row.Cells[m].Text == "Review")
                {
                    e.Row.Cells[m].Text = "Forwarded";
                }
                if (e.Row.Cells[m].Text == "T")
                {
                    e.Row.Cells[m].Text = "Yes";
                    AddToYesNoArray(drv.DataView.Table.Columns[m].ColumnName);
                }
                if (e.Row.Cells[m].Text == "F")
                {
                    e.Row.Cells[m].Text = "No";
                    AddToYesNoArray(drv.DataView.Table.Columns[m].ColumnName);
                }

                if (rowText.Length > 1 && rowText.ToString().Substring(0, 2) != "<a")
                {
                    if (e.Row.Cells[m].Text.Length > 0 & e.Row.Cells[m].Text.Length < 100)
                    {
                        e.Row.Cells[m].Text = "<nobr>" + e.Row.Cells[m].Text + "</nobr>";
                    }
                }
                else
                {
                    e.Row.Cells[m].Text = "<nobr>" + e.Row.Cells[m].Text + "</nobr>";
                }
            }
        }
    }

    private void AddToYesNoArray(string columnName)
    {
        int idx = -1;
        idx = yesNoColumns.IndexOf(columnName);

        if (idx == -1)
            yesNoColumns.Add(columnName);
    }


    /// Function to handle gridview sorting.
    protected void GridView1_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        DataTable dtorders = new DataTable();
        dtorders = (DataTable)Cache.Get(cac_order);
        if (objListViewData.SortColumnName.ToString().Equals(e.SortExpression) && (objListViewData.SortDir.ToString().Equals("ASC")))
        {
            dtorders.DefaultView.Sort = e.SortExpression + " " + "DESC";
            objListViewData.SortDir = "DESC";
        }
        else
        {
            dtorders.DefaultView.Sort = e.SortExpression + " " + "ASC";
            objListViewData.SortDir = "ASC";
        }

        objListViewData.SortColumnName = e.SortExpression;
        int colindex = GetColumnIndexByHeaderText(dtorders, e.SortExpression);
        objListViewData.SortColumn = colindex.ToString();
        Cache.Insert(cac_order, dtorders, null, DateTime.Now.AddMinutes(20), TimeSpan.Zero);
        lblerr.Visible = false;
        dvStatus.Style.Add("display", "none");
        string spgno = "1";
        if (!string.IsNullOrEmpty(lvPage.SelectedValue))
        {
            spgno = lvPage.SelectedValue;

        }
        fileName = "openlview-" + lName;
        errorLog = logobj.CreateLog("Loading Listliview.aspx", sid, fileName, "new");
        GetLViewData(spgno);
        Session["navigationInfoTable"] = navigationInfo;
    }

    /// Function to get the column index by knowing the header text.
    public static int GetColumnIndexByHeaderText(DataTable aGridView, string ColumnText)
    {
        string Cell = null;
        for (int Index = 0; Index <= aGridView.Columns.Count - 1; Index++)
        {
            Cell = aGridView.Columns[Index].ToString();
            if (Cell == ColumnText)
            {
                return Index - 1;
            }
        }
        return -1;
    }



    /// Function to call the web service on change of drop down.i.e. paging is handled onchange of the dropdown.
    protected void lvPage_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        string id = (sender as DropDownList).ClientID;
        if ((ddlFilter.SelectedIndex != 0) && ((ddlFilcond.SelectedIndex == 0) | (string.IsNullOrEmpty(txtFilter.Text))))
        {
            lblerr.Visible = true;
            dvStatus.Style.Add("display", "block");
        }
        else if (txtFilter.Text != string.Empty && (ddlFilter.SelectedIndex == 0 || ddlFilcond.SelectedIndex == 0))
        {
            lblerr.Visible = true;
            dvStatus.Style.Add("display", "block");
        }
        else
        {
            string pgNo = string.Empty;
            pgNo = lvPage.SelectedValue;
            lblerr.Visible = false;
            dvStatus.Style.Add("display", "none");
            fileName = "openlview-" + lName;
            errorLog = logobj.CreateLog("Loading Listliview.aspx for pagination", sid, fileName, "new");
            if (Convert.ToString(objListViewData.Mode) == "apply")
                Createxml(0, pgNo);
            else
                GetLViewData(pgNo);
            Session["navigationInfoTable"] = navigationInfo;
            tstScripts += "<script type=\"text/javascript\">ShowDimmer(false);ScrollToTop();</script>";
        }
    }

    protected void btnFilter_Click(object sender, System.EventArgs e)
    {
        if ((ddlFilter.SelectedIndex != 0) && ((ddlFilcond.SelectedIndex == 0) | (string.IsNullOrEmpty(txtFilter.Text))))
        {
            lblerr.Visible = true;
            dvStatus.Style.Add("display", "block");
        }
        else if (txtFilter.Text != string.Empty && (ddlFilter.SelectedIndex == 0 || ddlFilcond.SelectedIndex == 0))
        {
            lblerr.Visible = true;
            dvStatus.Style.Add("display", "block");
        }
        else
        {
            lblerr.Visible = false;
            dvStatus.Style.Add("display", "none");
            fileName = "openlview-" + lName;
            errorLog = logobj.CreateLog("Loading ListView.aspx", sid, fileName, "new");
            GetLViewData("1");
            Session["navigationInfoTable"] = navigationInfo;
        }

    }

    protected void button1_Click(object sender, System.EventArgs e)
    {
        string spgno = "1";
        if ((!string.IsNullOrEmpty(lvPage.SelectedValue)))
        {
            spgno = lvPage.SelectedValue;
        }
        fileName = "openlview-" + lName;
        errorLog = logobj.CreateLog("Loading Listliview.aspx", sid, fileName, "new");
        GetLViewData(spgno);
        Session["navigationInfoTable"] = navigationInfo;
    }

    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedFld = ddlFilter.SelectedItem.Text;
        if (headName.Count == 0)
            headName = (ArrayList)objListViewData.HeadName;
        if (colType.Count == 0)
            colType = (ArrayList)objListViewData.ColType;
        int indx = headName.IndexOf(selectedFld);
        if (indx != -1)
        {
            string type = colType[indx].ToString();
            if (type != "")
                FillFilterConditions(type);
        }
    }

    private void FillFilterConditions(string sourceType)
    {
        ddlFilcond.Items.Clear();

        ListItem emptyItem = new ListItem();
        emptyItem.Text = "";
        emptyItem.Value = "";
        ddlFilcond.Items.Add(emptyItem);
        txtFilter.Text = string.Empty;
        for (int i = 0; i < filterCondText.Length; i++)
        {
            if (sourceType == "c" && (i == 0 || i == 1 || i == 2 || i == 3 || i == 9 || i == 10))
            {
                ListItem item = new ListItem();
                item.Text = filterCondText[i].ToString();
                item.Value = filterCondValues[i].ToString();
                ddlFilcond.Items.Add(item);
            }
            else if (sourceType != "c" && (i == 0 || i == 1 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8))
            {
                ListItem item = new ListItem();
                item.Text = filterCondText[i].ToString();
                item.Value = filterCondValues[i].ToString();
                ddlFilcond.Items.Add(item);
            }
        }
        ddlFilcond.Focus();
    }

    protected void ddlFilcond_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlFilcond.SelectedValue == "between")
        {
            lblfilVal2.Visible = true;
            filVal2.Visible = true;
            filVal2.Text = "";
        }
        else
        {
            lblfilVal2.Visible = false;
            filVal2.Visible = false;
        }
        if (ddlFilcond.SelectedValue != string.Empty)
        {
            txtFilter.ReadOnly = false;
            txtFilter.Focus();
        }
        else
        {
            ddlFilcond.Focus();
            txtFilter.ReadOnly = true;
            txtFilter.Text = string.Empty;
            filVal2.Text = string.Empty;

        }
    }

    //Creating Custom view list div
    private void CreateViewLisDiv(string views)
    {
        StringBuilder strView = new StringBuilder();
        if (views != string.Empty)
        {
            string[] arrViews = views.Split(',');
            if (arrViews.Length > 0)
            {
                if (!defaultBut.Contains("<li class=\"gropuedBtn\"><div class=\"dropdown\"><button class=\"btn actionsBtn dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Views"))
                {

                    StringBuilder actionbarBtnHtml = new StringBuilder();

                    defaultBut += "<li class=\"gropuedBtn\"><div class=\"dropdown\"><button class=\"btn actionsBtn dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Views<span class=\"icon-arrows-down\"></span></button> <ul id=\"uldropelements\" class=\"dropdown-menu\">";
                    for (int i = 0; i < arrViews.Length; i++)
                    {
                        if (arrViews[i] == lName)
                            arrViews[i] = "Default";
                        //actionbarBtnHtml.Append("<li style='align-items: center;width:100%;display:inline-flex;'><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + arrViews[i] + "\");' style='width:15%;border:0px;'></a><a href='javascript:void(0)' class='PopTd2' onclick='javascript:BindGrid(\"" + arrViews[i] + "\");' style='width:85%;border:0px;'>" + arrViews[i] + "</a></li>");
                        actionbarBtnHtml.Append("<li style=''><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + arrViews[i] + "\");' style='width:15%;border:0px;display:none;'></a><a href='javascript:void(0)' class='PopTd2 noaction' onclick='javascript:BindGrid(\"" + arrViews[i] + "\");' style=''>" + arrViews[i] + "</a></li>");
                        defaultBut += actionbarBtnHtml.ToString();
                        defaultBut += "</ul></div></li>";

                    }

                    objListViewData.ViewList = viewList = strView.ToString();
                }
            }
            objListViewData.Views = views;
        }
    }


    private void CreateViewListHtml()
    {
        custViews = (ArrayList)objListViewData.CustViews;
        StringBuilder strView = new StringBuilder();


        StringBuilder actionbarBtnHtml = new StringBuilder();

        CustomListViewbtn += "<li class=\"gropuedBtn\"><div class=\"dropdown\"><button class=\"btn actionsBtn dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" title=\"Views\">Views<span class=\"icon-arrows-down\"></span></button> <ul id=\"uldropelements\" class=\"dropdown-menu\">";
        // actionbarBtnHtml.Append("<li style='align-items: center;width:100%;display:inline-flex;'><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + lName + "\");' style='width:15%;border:0px;'></a><a href='javascript:void(0)' class='PopTd2' onclick='javascript:BindGrid(\"" + lName + "\");' style='width:85%;border:0px;'>Default</a></li>");
        actionbarBtnHtml.Append("<li style=''><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + lName + "\");' style='width:15%;border:0px;display:none;'></a><a href='javascript:void(0)' class='PopTd2 noaction' onclick='javascript:BindGrid(\"" + lName + "\");' style=''>Default</a></li>");
        for (int i = 0; i < custViews.Count; i++)
        {

            if (custViews[i].ToString() != lName)
            {
                //actionbarBtnHtml.Append("<li style='align-items: center;width:100%;display:inline-flex;'><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + custViews[i] + "\");' style='width:15%;border:0px;'></a><a href='javascript:void(0)' class='PopTd2' onclick='javascript:BindGrid(\"" + custViews[i] + "\");' style='width:85%;border:0px;'>" + custViews[i] + "</a></li>");
                actionbarBtnHtml.Append("<li style=''><a href='javascript:void(0)' class='icon-basic-magnifier PopTd1' onclick='javascript:BindXml(\"" + custViews[i] + "\");' style='width:15%;border:0px;display:none;'></a><a href='javascript:void(0)' class='PopTd2 noaction' onclick='javascript:BindGrid(\"" + custViews[i] + "\");' style=''>" + custViews[i] + "</a></li>");
            }

        }
        CustomListViewbtn += actionbarBtnHtml.ToString();
        CustomListViewbtn += "</ul></div></li>";

        viewList = strView.ToString();
    }

    private void CreateToolbarButtons(XmlDocument xmlDoc1)
    {
        XmlNodeList compNodes = default(XmlNodeList);
        XmlNodeList cbaseDataNodes = default(XmlNodeList);
        string Lsttask = null;
        string action = null;
        string tskCap = null;

        XmlElement viewElement = xmlDoc1.DocumentElement;
        string views = viewElement.SelectSingleNode("//data/customizeview") == null ? string.Empty : viewElement.SelectSingleNode("//data/customizeview").InnerText;


        if (views.Split(',').Length > 0 && views.Trim() != string.Empty)
        {
            custViews.AddRange(views.Split(','));
            custViews.Remove(lName);
            objListViewData.CustViews = custViews;
            CreateViewListHtml();
        }
        BindViewList();
        compNodes = xmlDoc1.SelectNodes("//data/comps");


        tskList.Length = 0;

        foreach (XmlNode compNode in compNodes)
        {
            cbaseDataNodes = compNode.ChildNodes;
            int cnt1 = 0;
            cnt1 = 0;
            int compNodeCnt = 0;
            int ToolbarBtnCnt = 0;
            ToolbarBtnCnt = cbaseDataNodes.Count - 1;

            //Button creation loop is reversed because the buttons in the xml come in the reverse order.

            for (compNodeCnt = ToolbarBtnCnt; compNodeCnt >= 0; compNodeCnt--)
            {
                if (cbaseDataNodes[compNodeCnt].Name.Length >= 6)
                {
                    if (cbaseDataNodes[compNodeCnt].Name.Substring(0, 7) == "x__head")
                    {
                        lViewCaption = cbaseDataNodes[compNodeCnt].Attributes["caption"].Value;
                        objListViewData.LviewCaption = lViewCaption;
                    }
                }
                if (cbaseDataNodes[compNodeCnt].Name.Substring(0, 3) == "btn")
                {
                    string task = null;
                    task = cbaseDataNodes[compNodeCnt].Attributes["task"].Value;
                    string actionVal = null;
                    string hint = null;
                    hint = cbaseDataNodes[compNodeCnt].Attributes["hint"].Value;
                    string sname = null;
                    sname = cbaseDataNodes[compNodeCnt].Attributes["sname"].Value;
                    actionVal = cbaseDataNodes[compNodeCnt].Attributes["action"].Value;
                    string confirm = null;
                    XmlNode cnf = cbaseDataNodes[compNodeCnt].Attributes["desc"];
                    if (cnf == null)
                    {
                        confirm = "";
                    }
                    else
                    {
                        confirm = cnf.Value;
                    }

                    string appl = null;
                    XmlNode ap = cbaseDataNodes[compNodeCnt].Attributes["apply"];
                    if (ap == null)
                    {
                        appl = "";
                    }
                    else
                    {
                        appl = ap.Value;
                    }

                    if (task == "new" & hint == "New")
                    {
                        defaultBut += "<li><a class='add' href=\"javascript:pgReload();\" title=\" " + hint + "\">" + hint + "</a></li>";
                    }
                    else if (task == "save")
                    {
                        defaultBut += "<li><a class='save' href=\"javascript:formsubmit();\" title=\" " + hint + "\">" + hint + "</a></li>";
                    }
                    else if (task == "search")
                    {
                        // defaultBut = defaultBut & "&nbsp;<a href=""javascript:openSearch('" & tid & "');""><img src=""./AxpImages/search.png"" border=0 alt=" & hint & "></a>"
                    }
                    else if (task == "remove" | task == "delete")
                    {
                        defaultBut = defaultBut + "<li><a class='delete' href=\"javascript:callDelete('" + lName + "','lview');\" title=\" " + hint + "\">" + hint + "</a></li>";
                    }
                    else if (task == "print")
                    {
                        defaultBut += "<li><a class='print' title='Print' href=\"javascript:toHTML('" + lName + "','lview','true');\">" + hint + "</a></li>";
                    }
                    else if (task == "view")
                    {
                        defaultBut = defaultBut + "<li><a class='view' href=\"javascript:OpenPopup('divAction','New');\" title=\" " + hint + "\">" + hint + "</a></li>";
                    }
                    else if (task == "find")
                    {
                        //defaultBut &= "&nbsp;<a href=''><img src=""./AxpImages/find.png"" border=0 alt=" & hint & "></a>"
                    }
                    else if (task == "printersetting")
                    {
                        //defaultBut &= "&nbsp;<a href=''><img src=""./AxpImages/print.png"" border=0 alt=" & hint & "></a>"
                    }
                    else if (((task == "preview") | (task == "close") | (task == "gofirst") | (task == "gonext") | (task == "goprior") | (task == "golast")))
                    {
                        if (task == "preview")
                        {
                            //Currently not available in this version
                        }
                        else
                        {
                            defaultBut = defaultBut + "";
                        }

                    }
                    else if (task == "tasks")
                    {
                        XmlNodeList taskListNodes = default(XmlNodeList);
                        taskListNodes = cbaseDataNodes[compNodeCnt].ChildNodes;

                        if (taskListNodes.Count > 0)
                        {
                            Custom custObj = Custom.Instance;
                            try
                            {
                                hint = custObj.AxBeforeAddTaskTitle(hint);
                            }
                            catch (Exception ex)
                            {
                                hint = "Tasks";
                            }
                            //defaultBut = defaultBut + "<a class='tasks' onClick='javascript:FindPos();ShowTaskList();'  class=handCur>&nbsp;" + hint + "</a>";
                        }


                        defaultBut += (string.Empty);



                        //Task list div





                        foreach (XmlNode taskItem in taskListNodes)
                        {
                            Lsttask = taskItem.Attributes["task"].Value;
                            tskCap = taskItem.Attributes["caption"].Value;
                            action = taskItem.Attributes["action"].Value;
                            Lsttask = Lsttask.ToLower();
                            if (!string.IsNullOrEmpty(Lsttask) & string.IsNullOrEmpty(action))
                            {
                                if (Lsttask == "attach")
                                {
                                    tskList.Append("<li><a href='javascript:void(0)' onclick='javascript:AttachFiles();'>");
                                    tskList.Append("" + tskCap + "</a></li>");
                                }
                                else if (Lsttask == "email")
                                {
                                    tskList.Append("<li><a href='javascript:void(0)' onclick='javascript:openEMail(\"" + lName + "\",\"tstruct\",\"0\");'>");
                                    tskList.Append("" + tskCap + "</a></li>");

                                }
                                else if (Lsttask == "print")
                                {
                                    //Currently not available for this version
                                }
                                else if (Lsttask == "pdf")
                                {
                                    tskList.Append("<li><a href='javascript:void(0)' class='pdf'  title='PDF' onclick='javascript:toPDF(\"" + lName + "\",\"lview\");'>");
                                    tskList.Append("" + tskCap + "</a></li>");

                                }
                                else if (Lsttask == "save as")
                                {
                                    tskList.Append("<li><a href='javascript:void(0)' class='pdf' title='PDF' onclick='javascript:toPDF(\"" + lName + "\",\"lview\");'>");
                                    tskList.Append(lblTaskPDF.Text + "</a></li>");
                                }
                                else if (Lsttask == "preview")
                                {
                                    //Currently not available for this version
                                }
                                else if (Lsttask == "view history")
                                {
                                    tskList.Append("<li><a class=\"popDivBg\" \"PopTd1\">");
                                    tskList.Append("" + tskCap + "</a></li>");
                                }
                                else if (Lsttask == "to xl")
                                {
                                    //tskList = tskList + "<tr class=popDivBg onclick='javascript:toExcel(\"" + lName + "\",\"lview\")'><td class=PopTd1>";
                                    //tskList = tskList + "<img src=\"../AxpImages/icons/16x16/csv-export.png\" border=0></td><td class=PopTd2>&nbsp;" + tskCap + "</td></tr>";
                                    tskList.Append("<li><a href='javascript:void(0)' class='excel' title='Save as Excel' onclick='javascript:toExcelWeb(\"" + lName + "\",\"lview\");'>");
                                    tskList.Append(lblTaskExcel.Text + "</a></li>");

                                }
                            }
                        }
                        defaultBut += (tskList);

                        objListViewData.TaskList = tskList.ToString();

                        //end of task list div

                        //findList = "<div id='findListPopUp' style=\"display:none;width:100px;\" class=handCur onclick='javascript:HideFindList()'>";
                        //findList += "<div class=popDivBg><table style=\"width:100%\">";

                        //findList = findList + "<tr class=popDivBg onclick=\"javascript:displayLviewWin('dvViewColumn', 'Search View Columns');\">";
                        //findList = findList + "<td class=PopTd2> View Column</td></tr>";

                        //findList += "</table></div></div>";

                    }
                    else if (string.IsNullOrEmpty(task))
                    {
                        string caption = string.Empty;
                        string imgName = string.Empty;
                        if ((cbaseDataNodes[compNodeCnt].Attributes["caption"] != null))
                        {
                            caption = cbaseDataNodes[compNodeCnt].Attributes["caption"].Value;
                        }

                        if ((cbaseDataNodes[compNodeCnt].Attributes["img"] != null))
                        {
                            imgName = cbaseDataNodes[compNodeCnt].Attributes["img"].Value;
                        }

                        if (!string.IsNullOrEmpty(actionVal))
                        {
                            if (cnt1 == 0)
                            {
                                if ((string.IsNullOrEmpty(cbaseDataNodes[compNodeCnt].Attributes["caption"].Value)) && (cbaseDataNodes[compNodeCnt].Attributes["hint"].Value == "New"))
                                {
                                    defaultBut += "<li><a class='add'  href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\">" + hint + "</a></li>";
                                }
                                else if (actionVal == "csvimport")
                                {
                                    defaultBut += "<input type=hidden name=cb_sactbu><li><a class=\"squarebutton\"  href=\"javascript:callFileUploadAction('" + actionVal + "','" + lName + "','" + confirm + "');\"><span>" + cbaseDataNodes[compNodeCnt].Attributes["caption"].Value + "</span></a></li>&nbsp;&nbsp;";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(caption) & !string.IsNullOrEmpty(imgName))
                                    {
                                        //Display Image with caption as hint.
                                        defaultBut += "<li><a href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\"><img src=\"../AxpImages/" + imgName + ".png\" style='border:0px;'  alt='" + caption + "'></a></li>&nbsp;";
                                    }
                                    else if (!string.IsNullOrEmpty(imgName) & string.IsNullOrEmpty(caption))
                                    {
                                        //Display only image
                                        defaultBut += "<li><a href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\"><img src=\"../AxpImages/" + imgName + ".png\" style='border:0px;' alt='" + caption + "'></a></li>&nbsp;";
                                    }
                                    else if (!string.IsNullOrEmpty(caption) & string.IsNullOrEmpty(imgName))
                                    {
                                        //Display button with Caption
                                        defaultBut += "<li><a><input type=button onclick=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\" value=\"" + caption + "\"></a></li>&nbsp;&nbsp;";
                                    }
                                    else if (string.IsNullOrEmpty(caption) & string.IsNullOrEmpty(imgName))
                                    {
                                        defaultBut += "<li><a><input type=button onclick=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\" value=\"" + caption + "\"></a></li>&nbsp;&nbsp;";
                                    }
                                }

                            }
                            else
                            {
                                if ((string.IsNullOrEmpty(cbaseDataNodes[compNodeCnt].Attributes["caption"].Value)) & (cbaseDataNodes[compNodeCnt].Attributes["hint"].Value == "New"))
                                {
                                    defaultBut += "<li><a class=\"squarebutton\"  style=\"margin-left: 6px\" href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\"><span>New</span></a></li>&nbsp;&nbsp;";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(caption) & !string.IsNullOrEmpty(imgName))
                                    {
                                        //Display Image with caption as hint.
                                        defaultBut += "<li><a href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\"><img src=\"../AxpImages/" + imgName + ".png\" style='border:0px;' alt='" + caption + "' title='" + caption + "'></a></li>&nbsp;";
                                    }
                                    else if (!string.IsNullOrEmpty(imgName) & string.IsNullOrEmpty(caption))
                                    {
                                        //Display only image
                                        defaultBut += "<li><a href=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\"><img src=\"../AxpImages/" + imgName + ".png\" style='border:0px;' alt='" + caption + "' title='" + caption + "'></a></li>&nbsp;";
                                    }
                                    else if (!string.IsNullOrEmpty(caption) & string.IsNullOrEmpty(imgName))
                                    {
                                        //Display button with Caption
                                        defaultBut += "<li><a><input type=button onclick=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\" value=\"" + caption + "\"></a></li>&nbsp;&nbsp;";
                                    }
                                    else if (string.IsNullOrEmpty(caption) & string.IsNullOrEmpty(imgName))
                                    {
                                        defaultBut += "<li><a><input type=button onclick=\"javascript:callAction('" + actionVal + "','" + lName + "','" + confirm + "','" + appl + "');\" value=\"" + caption + "\"></a></li>&nbsp;&nbsp;";
                                    }
                                    cnt1 = 1;
                                }
                            }

                        }
                    }
                }
            }
        }
        if (Session["AxpExcelExport"] != null && Session["AxpExcelExport"].ToString() == "true")
            defaultBut += "<li><a href='javascript:void(0)' class='excel' title='Save as Excel' onclick='javascript:toExcelWeb(\"" + lName + "\",\"lview\");'>Save as Excel</a></li>";
    }

    private void CreateHeaderRow(XmlDocument xmlDoc, string pageNo)
    {
        string headrowXml = "";

        XmlNodeList productNodes = default(XmlNodeList);
        XmlNodeList baseDataNodes = default(XmlNodeList);

        productNodes = xmlDoc.SelectNodes("//headrow");
        int reccount = 0;
        foreach (XmlNode productNode in productNodes)
        {
            if (pageNo == "1")
            {
                XmlNode tnode = productNode.Attributes["totalrows"];
                XmlNode rnode1 = productNode.Attributes["reccount"];
                if (tnode == null)
                {
                    totalRows = 0;
                }
                else
                {
                    totalRows = Convert.ToInt32(tnode.Value);
                }

                if (rnode1 == null)
                {
                    reccount = 0;
                }
                else
                {
                    reccount = Convert.ToInt32(rnode1.Value);
                }

                objListViewData.Lv_noofpages = totalRows;
            }
            else
            {
                totalRows = (int)objListViewData.Lv_noofpages;
            }
            headrowXml = productNode.InnerXml;
            baseDataNodes = productNode.ChildNodes;

            foreach (XmlNode baseDataNode in baseDataNodes)
            {
                colFld.Add(baseDataNode.Name);
                if (baseDataNode.Name == "rowno")
                {
                    // change here to add "-"
                    headName.Add("");
                    //For list view default node
                    defaultfldName.Add("");

                    colWidth.Add("10");
                    colType.Add("c");
                    //change here to hide first column heading
                    colHide.Add("false");
                    colDec.Add("0");
                    colHlink.Add("-");
                    colMap.Add("-");
                    cancelFlag.Add("");
                    colAlign.Add("");
                }
                else
                {

                    colWidth.Add(baseDataNode.Attributes["width"].Value);
                    colHide.Add(baseDataNode.Attributes["hide"].Value);
                    headName.Add(baseDataNode.InnerText);
                    //For list view default node
                    defaultfldName.Add(baseDataNode.Name);
                    cancelFlag.Add("");

                    colType.Add(baseDataNode.Attributes["type"].Value);
                    colDec.Add(baseDataNode.Attributes["dec"].Value);
                    if (baseDataNode.Attributes["align"] != null)
                        colAlign.Add(baseDataNode.Attributes["align"].Value);
                    else
                        colAlign.Add("");
                    //IMPORATANT TO CHECK ATTRIBUTES .. SOMETIMES MISSED IN NODE
                    //----------------------------------------------------------

                    XmlNode node = baseDataNode.Attributes["hlink"];
                    if (node == null)
                    {
                        colHlink.Add("-");
                    }
                    else
                    {
                        colHlink.Add(node.Value);
                    }

                    XmlNode node1 = baseDataNode.Attributes["map"];
                    if (node1 == null)
                    {
                        colMap.Add("-");
                    }
                    else
                    {
                        colMap.Add(node1.Value);
                    }
                }
            }
        }

        objListViewData.HeadRowXML = "<headrow>" + headrowXml + "</headrow>";
        headrowXml = "<data><headrow>" + headrowXml + "</headrow></data>";
        objListViewData.CancelFlag = cancelFlag;
        objListViewData.ColDec = colDec;
        objListViewData.ColHlink = colHlink;
        objListViewData.ColMap = colMap;
        objListViewData.ColWidth = colWidth;
        objListViewData.ColHide = colHide;
        objListViewData.HeadName = headName;
        objListViewData.ColType = colType;
        objListViewData.ColFld = colFld;
        objListViewData.ColAlign = colAlign;
        objListViewData.DefaultfldName = defaultfldName;


    }

    private int GetColIndex(string columnName)
    {
        yesNoColumns = (ArrayList)objListViewData.YesNoColumns;
        int indx = -1;
        indx = yesNoColumns.IndexOf(columnName);
        return indx;
    }

    private void GetBreadCrumb()
    {
        objListViewData.MenuBreadcrumb = string.Empty;
        string strv = Session["MenuData"].ToString();
        XmlDocument xmlDoc1 = new XmlDocument();
        xmlDoc1.LoadXml(strv);
        string code = "tstruct.aspx?transid=" + lName;
        XmlNode node = null;
        XmlElement basicDataRoot = xmlDoc1.DocumentElement;
        if (((basicDataRoot.SelectSingleNode("descendant::child[@target='" + code + "']") != null)))
        {
            node = basicDataRoot.SelectSingleNode("descendant::child[@target='" + code + "']");
        }
        else if ((basicDataRoot.SelectSingleNode("descendant::parent[@target='" + code + "']") != null))
        {
            node = basicDataRoot.SelectSingleNode("descendant::parent[@target='" + code + "']");
        }
        if (node != null)
        {
            string s = node.Attributes["name"].Value;
            string s1 = string.Empty;
            XmlNode parnode = default(XmlNode);
            if ((node.ParentNode != null))
            {
                parnode = node.ParentNode;
                while (parnode.Name != "root")
                {
                    s1 = parnode.Attributes["name"].Value + " > " + s1;
                    parnode = parnode.ParentNode;
                }
            }
            objListViewData.MenuBreadcrumb = s1;
        }
        else
        {
            objListViewData.MenuBreadcrumb = string.Empty;
        }
    }
    private void ConstructBreadCrumb()
    {
        string direction1 = "left";

        if (language.ToLower() == "arabic")
            direction1 = "right";
        if (language.ToLower() == "arabic")
            strBreadCrumbBtns = "<div id='backforwrdbuttons' class='hide backbutton " + direction1 + "'>" +
                "<span class='navLeft icon-arrows-left-double-32 handCur' onclick='javascript:BackForwardButtonClicked(\"back\");' id='" + "goback" + "' src=\'" + Constants.FORWARDIMG + "' style='border:0' alt=\"Previous Page\" title=\"Click here to go back\"></span>" +
                "</div>";
        else
            strBreadCrumbBtns = "<div id='backforwrdbuttons' class='hide backbutton " + direction1 + " '>" +
        "<span class='navLeft icon-arrows-left-double-32 handCur' onclick='javascript:BackForwardButtonClicked(\"back\");' id='" + "goback" + "' src=\'" + Constants.BACKIMG + "'style='border:0' alt=\"Previous Page\" title=\"Click here to go back\"></span>" +
        "</div>";

        if (!(string.IsNullOrEmpty(objListViewData.LviewCaption.ToString())))
        {
            breadCrumb = objListViewData.LviewCaption.ToString();
        }
        else
        {
            breadCrumb = string.Empty;
        }

        //if (util.BreadCrumb)
        //    strBreadCrumb.Append("<div class='icon-services " + direction1 + " bcrumb pd10'><h3>" + objListViewData.MenuBreadcrumb.ToString() + objListViewData.LviewCaption.ToString() + "</h3></div>");
        //else
        strBreadCrumb.Append("<div class='icon-services " + direction1 + " bcrumb pd5'><h3><span style=\"position: relative;top: 8px;font-size: 20px !important;left: 10px;\">" + breadCrumb + "</span></h3></div>");

    }


    private void GetGlobalVariables()
    {
        lName = Request.QueryString["tid"];
        hdnTransId.Value = lName;
        lName = util.CheckSpecialChars(lName);
        if (!util.IsValidIvName(lName))
            Response.Redirect(Constants.PARAMERR);

        objListViewData.Tid = lName;
        proj = Session["project"].ToString();
        proj = util.CheckSpecialChars(proj);
        AxRole = Session["AxRole"].ToString();
        AxRole = util.CheckSpecialChars(AxRole);
        user = Session["user"].ToString();
        user = util.CheckSpecialChars(user);

        objListViewData.YesNoColumns = yesNoColumns;
    }

    private void SetGlobalVariables()
    {
        if (objListViewData.Tid != null)
        {
            lName = objListViewData.Tid.ToString(); ;
        }
        proj = Session["project"].ToString();
        proj = util.CheckSpecialChars(proj);
        AxRole = Session["AxRole"].ToString();
        AxRole = util.CheckSpecialChars(AxRole);
        user = Session["user"].ToString();
        user = util.CheckSpecialChars(user);
        if (objListViewData.ToolBarBtns != null)
            defaultBut = objListViewData.ToolBarBtns.ToString();
        if (tskList.ToString() == string.Empty)
        {
            tskList.Length = 0;
            tskList.Append(objListViewData.TaskList.ToString());
        }
        if (viewList == string.Empty)
            viewList = objListViewData.ViewList.ToString();

    }
    private void SetLangStyles()
    {

        if (language.ToLower() == "arabic")
        {
            direction = "rtl";
            //dvColumnName.Attributes["style"] = "float:right";
            //dvFilterWith.Attributes["style"] = "float:right";
            //dvValue1.Attributes["style"] = "float:right";
            //dvValue2.Attributes["style"] = "float:right";

            searchoverlay.Attributes["class"] = "arabicoverlay";
            //dvsearchdir.Attributes["class"] = "lstsrcharabic";
            dvStatus.Attributes["style"] = "margin-right:25px;";
            searchoverlay.Attributes["style"] = "float:right";
            //Panel2.Attributes["style"] = "padding-right:10px;";
            btn_direction = "left";
            bread_direction = "right";
        }
        else
        {
            //dvColumnName.Attributes["style"] = "float:left";
            //dvFilterWith.Attributes["style"] = "float:left";
            //dvValue1.Attributes["style"] = "float:left";
            //dvValue2.Attributes["style"] = "float:left";

            searchoverlay.Attributes["class"] = "overlay";
            //dvsearchdir.Attributes["class"] = "lstsrch";
            dvStatus.Attributes["style"] = "margin-left:25px;";
            searchoverlay.Attributes["style"] = "float:left";
            //Panel2.Attributes["style"] = "padding-left:10px;";
            btn_direction = "right";
            bread_direction = "left";
        }
    }
    private void ClearArrays()
    {
        headName.Clear();
        colWidth.Clear();
        colType.Clear();
        colHide.Clear();
        colFld.Clear();
        colDec.Clear();
        colHlink.Clear();
        colMap.Clear();
        cancelFlag.Clear();
    }

    private void SessExpires()
    {
        string url = Convert.ToString(HttpContext.Current.Application["SessExpiryPath"]);
        Response.Write("<script>" + Constants.vbCrLf);
        Response.Write("parent.parent.location.href='" + url + "';");
        Response.Write(Constants.vbCrLf + "</script>");
    }

    #region Treeview

    private void CreateTreeView()
    {
        hdnViewName.Value = lName + "01";
        string result = string.Empty;
        result = strobj.structRes;

        strobj = new TStructDef(result, new DataTable(), new DataSet());

        for (int dcIdx = 0; dcIdx < strobj.dcs.Count; dcIdx++)
        {
            TreeNode root1 = new TreeNode();
            root1.ShowCheckBox = true;
            TStructDef.DcStruct dc = (TStructDef.DcStruct)strobj.dcs[dcIdx];
            string[] dcRange = strobj.GetDcFieldRange(dc.frameno.ToString()).Split(',');
            int startIdx = Convert.ToInt32(dcRange[0]);
            int endIdx = Convert.ToInt32(dcRange[1]);
            StringBuilder strDcMRFields = new StringBuilder();

            if (dc.isgrid)
                root1.Text = dc.caption + " = Grid DC";
            else
                root1.Text = dc.caption;
            root1.Value = dc.name;
            root1.ToolTip = root1.Text;
            for (int i = startIdx; i <= endIdx; i++)
            {
                TreeNode root2 = new TreeNode();
                TStructDef.FieldStruct fld = (TStructDef.FieldStruct)strobj.flds[i];

                if (!(fld.caption.StartsWith("axp_recid")) && !fld.visibility && (fld.datatype != "Image") && (fld.caption != string.Empty) && (fld.savevalue != false))
                {
                    root2.Text = fld.caption;
                    root2.Value = fld.name;
                    root2.Expanded = true;
                    root2.ShowCheckBox = true;
                    root2.ToolTip = fld.caption;

                    //Add child node to parent
                    root1.ChildNodes.Add(root2);
                }
            }
            root1.Expanded = true;
            tvList.Nodes.Add(root1);
        }
        CheckSelectedCols();
    }
    //Add filter dropdown
    private void FillColumns()
    {
        ddlFilter.Items.Clear();
        ListItem lstEmpty = new ListItem();
        lstEmpty.Value = "";
        lstEmpty.Text = "";
        ddlFilter.Items.Add(lstEmpty);
        for (int i = 0; i < colFld.Count; i++)
        {
            if (colHide[i].ToString() == "false")
            {
                if (headName[i].ToString() != "")
                {
                    ListItem lst = new ListItem();
                    lst.Text = headName[i].ToString();
                    lst.Value = colFld[i].ToString();
                    ddlFilter.Items.Add(lst);
                }
            }
        }
    }

    private void GetCheckedNode()
    {
        StringBuilder strChild = new StringBuilder();
        int nodecount = tvList.Nodes.Count;
        for (int i = 0; i < nodecount; i++)
        {
            foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
            {
                string nod = node.Text;
                if (node.Checked == true)
                {
                    strChild.Append(node.Value);
                    strChild.Append(',');
                }
                else
                {
                    HiddenCols.Add(node.Value);
                }
            }
        }
        if (strChild.ToString().Length > 0)
        {
            hdnColumns.Value = strChild.ToString().Remove(strChild.ToString().Length - 1, 1).ToString();
        }
        objListViewData.HiddenCols = HiddenCols;
    }

    //Check selected columns from grid
    private void CheckSelectedCols()
    {
        int tvNodecount = tvList.Nodes.Count;
        for (int i = 0; i < tvNodecount; i++)
        {
            tvList.Nodes[i].Checked = false;
            foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
            {
                node.Checked = false;
            }
        }
        int idx = 0;
        if (objListViewData.DefaultfldName != null)
            defaultfldName = (ArrayList)(objListViewData.DefaultfldName);

        for (idx = 0; idx <= defaultfldName.Count - 1; idx++)
        {
            string headerName = defaultfldName[idx].ToString();
            int nodecount = tvList.Nodes.Count;
            for (int i = 0; i < nodecount; i++)
            {
                foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
                {
                    string nod = node.Value;
                    if (nod == headerName)
                        node.Checked = true;
                }
            }
        }
        CheckParentNode();
    }

    //Check parnt node on check of all sub node and uncheck else
    private void CheckParentNode()
    {
        StringBuilder strParent = new StringBuilder();
        StringBuilder strParentFld = new StringBuilder();
        StringBuilder strChild = new StringBuilder();
        int nodecount = tvList.Nodes.Count;
        TreeNode node1 = tvList.Nodes[0];

        TreeNodeCollection parentnode = tvList.Nodes;
        for (int i = 0; i < nodecount; i++)
        {
            int checkCount = 0;
            int childcnt = tvList.Nodes[i].ChildNodes.Count;
            foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
            {
                if (node.Checked)
                    checkCount++;
                strChild.Append(node.Text);
                strChild.Append(',');
            }
            strParent.Append(tvList.Nodes[i].Text);
            strParentFld.Append(tvList.Nodes[i].Value);
            strParent.Append(',');
            if (childcnt == checkCount)
                tvList.Nodes[i].Checked = true;
        }
        hdnParent.Value = strParent.ToString();
        hdnParentFld.Value = strParentFld.ToString();
        hdnChild.Value = strChild.ToString();
    }

    #endregion

    #region Listview actions

    //Custom view apply conditions
    protected void btnApply1_Click(object sender, System.EventArgs e)
    {
        Createxml(0, "1");
    }

    protected void btnDiscard_Click(object sender, System.EventArgs e)
    {
        CreateViewListHtml();
        CheckSelectedCols();
    }


    //Create xml input string
    private void Createxml(int issave, string pageNo)
    {
        lblFltrCondition.Text = string.Empty;
        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        string paramFld = string.Empty;
        string iXml = string.Empty;
        string mode = issave.ToString() == "1" ? "True" : "False";
        string action = hdnSaveMode.Value == "New" ? "t" : "f";
        StringBuilder inputxml = new StringBuilder();
        inputxml.Append("<customizeview name='" + txtViewName.Text + "' save='" + mode + "' new='" + action + "'>");
        //Condition
        inputxml.Append("<conditions>");
        //Column
        //Get selected colums
        GetCheckedNode();
        inputxml.Append("<column>" + hdnColumns.Value);
        //inputxml.ToString().Remove(inputxml.Length - 1);
        inputxml.Append("</column>");
        //No repeat
        inputxml.Append("<norepeat>");
        inputxml.Append(hdnNoRepeat.Value);
        inputxml.Append("</norepeat>");

        inputxml.Append("<cond status='" + rbtnStatus.SelectedItem + "'  ordtype='" + rbtnOrder.SelectedItem + "' groupbycap='" + hdnGroupCap.Value + "' orderbycap='" + hdnSortCap.Value + "' groupby='" + hdnGroup.Value + "' orderby='" + hdnSort.Value + "'>");
        //Subtotal
        inputxml.Append("<subtotals>" + hdnSubtotal.Value + "</subtotals>" + hdnCondTxt.Value + "</cond>");

        inputxml.Append("</conditions>");
        inputxml.Append("</customizeview>");
        string CustomViewXml = inputxml.ToString();
        if (hdnGridCond.Value.Trim() != string.Empty && rbtnStatus.SelectedItem.ToString().ToUpper() == "ON")
        {
            lblFltrCondition.Visible = true;
            lblFltrCondition.Text = hdnGridCond.Value;
            objListViewData.FilterText = hdnGridCond.Value;
        }
        else
        {
            lblFltrCondition.Visible = false;
            objListViewData.FilterText = string.Empty;
        }
        fileName = "SaveCustomView";
        errorLog = logobj.CreateLog("saving customized view", sid, fileName, "new");
        iXml = "<root name =\"" + lName + "\"  custviewname = \"" + txtViewName.Text + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "' trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
        iXml = iXml + "</params>";
        iXml += CustomViewXml;

        iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString() + "</root>";

        //Call service
        string ires = string.Empty;

        ires = objWebServiceExt.SaveCustomizeView(iXml);

        if (mode == "True")
        {
            objListViewData.CustomView = txtViewName.Text;
            objListViewData.Mode = "save";
            objListViewData.CustViewTransId = lName;
        }
        else
        {
            objListViewData.Mode = "apply";
        }
        string errMsg = string.Empty;
        if (ires != string.Empty)
            errMsg = util.ParseXmlErrorNode(ires);

        if (errMsg != string.Empty)
        {
            if (errMsg == Constants.SESSIONERROR)
            {
                Session.RemoveAll();
                Session.Abandon();
                SessExpires();
            }
            else
            {
                if (errMsg == "Exists")
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "New Window", "OpenopupWithCond();", true);
                }
                else
                {
                    Response.Redirect(util.ERRPATH + errMsg);
                }
            }
        }
        else
        {
            objListViewData.FilterXml = ires;
            logobj.CreateLog("Setting Lview components", sid, fileName, "");

            if (issave == 1 && hdnSaveMode.Value == "New")
            {
                custViews = (ArrayList)objListViewData.CustViews == null ? custViews : (ArrayList)objListViewData.CustViews;
                custViews.Add(txtViewName.Text);
                objListViewData.CustViews = custViews;
                hdnSelectedView.Value = txtViewName.Text;
                BindViewList();
            }
            CreateViewListHtml();
            objListViewData.ToolBarBtns = defaultBut;
            ires = _xmlString + ires;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(ires);
            CreateHeaderRow(xmlDoc, pageNo);
            //Remove headrow
            XmlNode rnode = default(XmlNode);
            rnode = xmlDoc.SelectSingleNode("//headrow");
            if (rnode != null)
                rnode.ParentNode.RemoveChild(rnode);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);

            string nXml = null;
            nXml = sw.ToString();

            rXml.Value = nXml;
            rXml.Value = rXml.Value.Replace("\"", "'");

            if (nXml == _xmlString + "<data></data>")
            {
                records.Text = lblNodataServer.Text;// Constants.REC_NOT_FOUND;
                GridView1.Visible = false;
            }
            else
            {
                //nXml = util.RemoveAppSessKeyAtt(nXml, "appsessionkey");
                GridView1.Visible = true;
                BindData(nXml, totalRows, pageNo);
                CheckSelectedCols();
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "window", "CloseListActionWin();", true);
            }
        }
    }


    //Delete custom view
    protected void btnRemv_Click(object sender, EventArgs e)
    {
        if (lName == txtViewName.Text)
        {
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "window", "<script language=JavaScript> showAlertDialog('error',4012,'clients'); </script>");
            return;
        }
        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        string paramFld = string.Empty;
        string iXml = string.Empty;
        string pageNo = "1";

        errorLog = logobj.CreateLog("deleting customized view", sid, fileName, "new");

        string CustomViewXml = "<customizeview name='" + txtViewName.Text + "'/>";

        iXml = "<root name =\"" + lName + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "' trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
        iXml = iXml + "</params>";
        iXml += CustomViewXml;

        iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString() + "</root>";

        //Call service
        string ires = string.Empty;

        ires = objWebServiceExt.DeleteCustomizeView(iXml);

        string errMsg = string.Empty;
        errMsg = util.ParseXmlErrorNode(ires);

        if (errMsg != string.Empty)
        {
            if (errMsg == Constants.SESSIONERROR)
            {
                Session.RemoveAll();
                Session.Abandon();
                SessExpires();
            }
            else
            {
                Response.Redirect(util.ERRPATH + errMsg);
            }
        }
        else
        {
            custViews = (ArrayList)objListViewData.CustViews;
            custViews.Remove(txtViewName.Text);
            objListViewData.CustViews = custViews;
            logobj.CreateLog("deleted customize view", sid, fileName, "");
            string viewList = Convert.ToString(objListViewData.Views);

            string[] arr = viewList.Split(',');
            StringBuilder strView = new StringBuilder();
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] != txtViewName.Text)
                    strView.Append(arr[i] + ",");
            }
            if (strView.Length > 0)
                viewList = strView.Remove(strView.Length - 1, 1).ToString();
            objListViewData.ViewList = viewList;
            BindViewList();
            CreateViewListHtml();
            hdnSelectedView.Value = lName;
            GetListViewData("1");
            CheckSelectedCols();
        }

    }

    //Save custom view
    protected void btnSav_Click(object sender, EventArgs e)
    {
        Createxml(1, "1");
    }

    //Show Binded grid with custom view conditions
    protected void btnShowGrid_Click(object sender, EventArgs e)
    {
        objListViewData.Mode = string.Empty;
        lblFltrCondition.Text = string.Empty;
        lblFltrCondition.Visible = false;
        CreateViewListHtml();
        GetListViewData("1");
    }

    //Binding gridview 
    private void GetListViewData(string pageNo)
    {
        ClearGridData();
        util.ClearSession();
        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        Boolean IsYesNoColumn = false;
        string paramFld = string.Empty;
        string iXml = string.Empty;

        if ((ddlFilter.SelectedIndex > 0))
        {
            filterColumn = ddlFilter.SelectedValue;
        }
        if (GetColIndex(filterColumn) > -1)
        {
            IsYesNoColumn = true;
        }
        if ((ddlFilcond.SelectedIndex > 0))
        {
            filterOpr = ddlFilcond.SelectedValue;
            filterOpr = util.CheckSpecialChars(filterOpr);
            if (ddlFilcond.SelectedValue == "between")
            {
                filterValue1 = filVal2.Text;
                filterValue1 = util.CheckSpecialChars(filterValue1);
            }
        }
        if (!string.IsNullOrEmpty(txtFilter.Text))
        {
            filterValue = txtFilter.Text;
            filterValue = util.CheckSpecialChars(filterValue);
        }
        if (IsYesNoColumn == true)
        {
            //If the value is not yes or no it will take the original value entered by the user.
            if (filterValue.ToLower() == "yes")
            {
                filterValue = "T";
            }
            else if (filterValue.ToLower() == "no")
            {
                filterValue = "F";
            }
        }
        if (dbType.ToLower() == "oracle")
        {
            if (objListViewData.SortColumnName != null)
                sortCol = objListViewData.SortColumnName.ToString();
        }
        else
        {
            if (objListViewData.SortColumn != null)
                sortCol = objListViewData.SortColumn.ToString();
        }
        if (objListViewData.SortDir != null)
            sortOrder = objListViewData.SortDir.ToString();

        string ixml1 = "";

        string viewName = string.Empty;
        if (hdnSelectedView.Value.ToLower() == "default")
            viewName = lName;
        else
            viewName = hdnSelectedView.Value;
        objListViewData.CustomView = viewName;
        objListViewData.CustViewTransId = lName;

        errorLog = logobj.CreateLog("Loading Listliview.aspx", sid, "loadCustView" + hdnSelectedView.Value, "new");
        string CustomViewXml = "<customizeview name='" + viewName + "'/>";

        iXml = "<root name =\"" + lName + "\"  custviewname=\"" + viewName + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey=\"" + HttpContext.Current.Session["AppSessionKey"].ToString() + "\" username=\"" + HttpContext.Current.Session["username"].ToString() + "\" trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
        iXml = iXml + "</params>";
        iXml += CustomViewXml;

        iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString() + "</root>";

        param.Value = paramFld;
        paramxml.Value = ixml1;
        param.Value = "listview";

        Session["sOrder"] = sortOrder;
        Session["sColumn"] = sortCol;
        Session["fCol"] = filterColumn;
        Session["fColVal"] = filterValue;
        Session["fColVal2"] = filterValue1;
        Session["fcolopr"] = filterOpr;

        ClearArrays();

        logobj.CreateLog("Start Time " + DateTime.Now.ToString(), sid, fileName, "");
        logobj.CreateLog("", sid, fileName, "");
        string ires = string.Empty;
        //Call service

        ires = objWebServiceExt.CallGetLViewWS(lName, iXml, objWebServiceExt.asbIview.Timeout);

        string errMsg = string.Empty;
        errMsg = util.ParseXmlErrorNode(ires);

        if (errMsg != string.Empty)
        {
            if (errMsg == Constants.SESSIONERROR)
            {
                Session.RemoveAll();
                Session.Abandon();
                SessExpires();
            }
            else
            {
                Response.Redirect(util.ERRPATH + errMsg);
            }
        }
        else
        {
            logobj.CreateLog("Setting Lview components", sid, fileName, "");
            ires = _xmlString + ires;
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(ires);

            objListViewData.Ires = ires;
            if (!IsPostBack)
                objListViewData.ToolBarBtns = defaultBut;
            CreateHeaderRow(xmlDoc, pageNo);


            //Remove headrow
            XmlNode rnode = default(XmlNode);
            rnode = xmlDoc.SelectSingleNode("//headrow");
            if (rnode != null)
                rnode.ParentNode.RemoveChild(rnode);

            //Remove Comps
            XmlNode cNode = default(XmlNode);
            cNode = xmlDoc.SelectSingleNode("//comps");
            cNode.ParentNode.RemoveChild(cNode);


            //For Customize view list

            XmlNode vNode = default(XmlNode);
            vNode = xmlDoc.SelectSingleNode("//customizeview");
            if (vNode != null)
                vNode.ParentNode.RemoveChild(vNode);

            //--------------

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xmlDoc.WriteTo(xw);

            string nXml = null;
            nXml = sw.ToString();

            // assign XML Val to input textbox
            rXml.Value = nXml;
            rXml.Value = rXml.Value.Replace("\"", "'");

            if (nXml == "<?xml version=\"1.0\" encoding=\"utf-8\"?><data></data>")
            {
                records.Text = lblNodataServer.Text;// Constants.REC_NOT_FOUND;
                GridView1.Visible = false;
            }
            else
            {
                //nXml = util.RemoveAppSessKeyAtt(nXml, "appsessionkey");
                GridView1.Visible = true;
                BindData(nXml, totalRows, pageNo);
            }

            GetSetCustomViewCondition(viewName);
        }
    }

    #endregion


    #region Stub
    //Get custom view xml details to bind popup for edit/view
    protected void btnListView_Click(object sender, EventArgs e)
    {
        objListViewData.Mode = string.Empty;
        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        string paramFld = string.Empty;
        string iXml = string.Empty;

        if (hdnListViewName.Value != "New view")
        {
            string pageNo = "1";
            string CustomViewXml = "<customizeview name='" + hdnListViewName.Value + "'/>";

            iXml = "<root name =\"" + lName + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "' trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
            iXml = iXml + "</params>";
            iXml += CustomViewXml;

            iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString() + "</root>";

            //Call service
            string ires = string.Empty;

            ires = objWebServiceExt.GetCustomizeViews(iXml);

            string errMsg = string.Empty;
            if (ires != string.Empty)
                errMsg = util.ParseXmlErrorNode(ires);

            if (errMsg != string.Empty)
            {
                if (errMsg == Constants.SESSIONERROR)
                {
                    Session.RemoveAll();
                    Session.Abandon();
                    SessExpires();
                }
                else
                {
                    Response.Redirect(util.ERRPATH + errMsg);
                }
            }
            else
            {
                if (ires != string.Empty)
                {
                    ires = _xmlString + ires;
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(ires);
                    objListViewData.ToolBarBtns = defaultBut;
                    //Remove headrow
                    XmlNode rnode = default(XmlNode);
                    rnode = xmlDoc.SelectSingleNode("//headrow");
                    if (rnode != null)
                        rnode.ParentNode.RemoveChild(rnode);

                    //Remove Comps
                    XmlNode cNode = default(XmlNode);
                    cNode = xmlDoc.SelectSingleNode("//comps");
                    if (cNode != null)
                        cNode.ParentNode.RemoveChild(cNode);

                    //Get the root element
                    XmlElement root = xmlDoc.DocumentElement;


                    CreateColumnNorepeat(xmlDoc);
                    if (ddlViewList.Items.Count > 0)
                        ddlViewList.SelectedItem.Selected = false;

                    CreateSortDetails(xmlDoc);

                    string colName = CreateSubTotalString(xmlDoc);
                    CreateConditionString(xmlDoc);
                    if (colName.Length > 0)
                    {
                        hdnSubtotalList.Value = colName.Remove(colName.Length - 1, 1).ToString();
                    }
                    CreateViewListHtml();
                    if (lName == hdnListViewName.Value)
                    {
                        CheckSelectedCols();
                    }
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "New Window", "OpenPopup('divAction','Edit');", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "norepeat", "checkDc();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "subtotal", "bindSubtotalList();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Edit", "EditCondPopUp();", true);
                }

            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "New Window", "OpenPopup('divAction','EditNew');", true);
        }
    }

    string condTxt = string.Empty;

    //Create no repeat and column details to show binded data in popup
    private void CreateColumnNorepeat(XmlDocument xmlDoc)
    {
        string column = string.Empty;
        string noRepeat = string.Empty;
        XmlElement root = xmlDoc.DocumentElement;
        if (xmlDoc.SelectSingleNode("/root/conditions/column") != null)
            column = xmlDoc.SelectSingleNode("/root/conditions/column").InnerText;
        if (xmlDoc.SelectSingleNode("/root/conditions/norepeat") != null)
            noRepeat = xmlDoc.SelectSingleNode("/root/conditions/norepeat").InnerText;
        string[] coln = column.Split(',');
        hdnNoRepeat.Value = noRepeat;
        CheckCols(coln);

    }

    //Create  sort details to show binded data in popup
    private void CreateSortDetails(XmlDocument xmlDoc)
    {
        if (xmlDoc.SelectSingleNode("/root/conditions/cond") != null)
        {
            XmlNode sortDetails = xmlDoc.SelectSingleNode("/root/conditions/cond");
            hdnListViewName.Value = sortDetails.Attributes["name"] == null ? "" : sortDetails.Attributes["name"].Value;

            ddlViewList.Items.FindByText(hdnListViewName.Value).Selected = true;
            XmlNode status = xmlDoc.SelectSingleNode("/root/conditions/cond");
            hdnStatus.Value = sortDetails.Attributes["status"] == null ? "" : sortDetails.Attributes["status"].Value.ToUpper();

            ddlSortBy.SelectedValue = hdnSort.Value = sortDetails.Attributes["orderby"] == null ? "" : sortDetails.Attributes["orderby"].Value;
            hdnSortCap.Value = sortDetails.Attributes["orderbycap"] == null ? "" : sortDetails.Attributes["orderbycap"].Value;

            ddlGroupBy.SelectedValue = hdnGroup.Value = sortDetails.Attributes["groupby"] == null ? "" : sortDetails.Attributes["groupby"].Value;
            hdnGroupCap.Value = sortDetails.Attributes["groupbycap"] == null ? "" : sortDetails.Attributes["groupbycap"].Value;

            rbtnOrder.Text = hdnOrder.Value = sortDetails.Attributes["ordtype"] == null ? "" : sortDetails.Attributes["ordtype"].Value;
        }
    }
    //Create subtotal details to show binded data in popup
    private string CreateSubTotalString(XmlDocument xmlDoc)
    {
        hdnSubtotalList.Value = string.Empty;
        int nodecount = 0;
        //Get the root element
        StringBuilder strColName = new StringBuilder();
        StringBuilder strCaption = new StringBuilder();

        // XmlElement root = xmlDoc.DocumentElement;
        if (xmlDoc.SelectSingleNode("/root/conditions/cond/subtotals") != null)
        {
            XmlElement root = xmlDoc.DocumentElement;
            XmlNodeList list = xmlDoc.SelectNodes("/root/conditions/cond/subtotals");
            nodecount = xmlDoc.SelectSingleNode("/root/conditions/cond/subtotals").ChildNodes.Count;
            hdnSubtotal.Value = xmlDoc.SelectSingleNode("/root/conditions/cond/subtotals").InnerXml;

            for (int i = 0; i < nodecount; i++)
            {
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(0).InnerText);//a1
                strColName.Append(',');
                //for checkbox select
                strCaption.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(0).InnerText);
                strCaption.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(2).InnerText);//a4
                strColName.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(3).InnerText);//a5
                strColName.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(1).InnerText);//a12
                strColName.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(4).InnerText);//a13
                strColName.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).ChildNodes.Item(5).InnerText);//a18
                strColName.Append(',');
                strColName.Append(list.Item(0).ChildNodes.Item(i).Attributes["totalontop"].Value);
                strColName.Append(';');
            }
        }
        if (strCaption.Length > 0)
            hdnSubtotlCaptn.Value = strCaption.Remove(strCaption.Length - 1, 1).ToString();
        return strColName.ToString();
    }

    //Create conition details to show binded data in popup
    private void CreateConditionString(XmlDocument xmlDoc)
    {
        hdnCondTxt.Value = string.Empty;
        int nodecount = 0;
        XmlElement root = xmlDoc.DocumentElement;
        if (root.SelectSingleNode("/root/conditions/cond") != null)
        {
            nodecount = root.SelectSingleNode("/root/conditions/cond").ChildNodes.Count;

            XmlNode status = root.SelectSingleNode("/root/conditions/cond");
            hdnStatus.Value = status.Attributes["status"] == null ? "" : status.Attributes["status"].Value;

            for (int i = 1; i <= nodecount - 1; i++)
            {
                XmlNode subroot = root.SelectSingleNode("/root/conditions/cond/c" + i);

                string op = subroot.Attributes["op"] == null ? "" : subroot.Attributes["op"].Value;
                string v1 = subroot.Attributes["v1"] == null ? "" : subroot.Attributes["v1"].Value;
                string v2 = subroot.Attributes["v2"] == null ? "" : subroot.Attributes["v2"].Value;
                string fname = subroot.Attributes["fname"] == null ? "" : subroot.Attributes["fname"].Value;
                string lopr = subroot.Attributes["lopr"] == null ? "" : subroot.Attributes["lopr"].Value;

                if (condTxt == string.Empty)
                    condTxt = fname + "^" + op + "^" + v1 + "^" + v2 + "^" + lopr;
                else
                    condTxt += "|" + fname + "^" + op + "^" + v1 + "^" + v2 + "^" + lopr;
            }
            rbtnStatus.Text = hdnStatus.Value;
            hdnCondTxt.Value = condTxt;
        }
    }

    private void GetSetCustomViewCondition(string viewName)
    {
        string sortOrder = string.Empty;
        string filterColumn = string.Empty;
        string filterValue = string.Empty;
        string filterOpr = string.Empty;
        string filterValue1 = string.Empty;
        string sortCol = string.Empty;
        Boolean IsYesNoColumn = false;
        string paramFld = string.Empty;

        if ((ddlFilter.SelectedIndex > 0))
        {
            filterColumn = ddlFilter.SelectedValue;
        }
        if (GetColIndex(filterColumn) > -1)
        {
            IsYesNoColumn = true;
        }
        if ((ddlFilcond.SelectedIndex > 0))
        {
            filterOpr = ddlFilcond.SelectedValue;
            filterOpr = util.CheckSpecialChars(filterOpr);
            if (ddlFilcond.SelectedValue == "between")
            {
                filterValue1 = filVal2.Text;
                filterValue1 = util.CheckSpecialChars(filterValue1);
            }
        }
        if (!string.IsNullOrEmpty(txtFilter.Text))
        {
            filterValue = txtFilter.Text;
            filterValue = util.CheckSpecialChars(filterValue);
        }
        if (IsYesNoColumn == true)
        {
            //If the value is not yes or no it will take the original value entered by the user.
            if (filterValue.ToLower() == "yes")
            {
                filterValue = "T";
            }
            else if (filterValue.ToLower() == "no")
            {
                filterValue = "F";
            }
        }
        if (dbType.ToLower() == "oracle")
        {
            if (objListViewData.SortColumnName != null)
                sortCol = objListViewData.SortColumnName.ToString();
        }
        else
        {
            if (objListViewData.SortColumn != null)
                sortCol = objListViewData.SortColumn.ToString();
        }
        if (objListViewData.SortDir != null)
            sortOrder = objListViewData.SortDir.ToString();

        string pageNo = "1";
        string CustomViewXml = "<customizeview name='" + viewName + "'/>";

        string iXml = "<root name =\"" + lName + "\" axpapp = \"" + proj + "\" sessionid = \"" + sid + "\" appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "' trace = \"" + errorLog + "\" list=\"true\"  pageno=\"" + pageNo + "\" pagesize=\"" + gridPageSize + "\" sorder=\"" + sortOrder + "\" scol=\"" + sortCol + "\" fcolnm=\"" + filterColumn + "\" fcolopr=\"" + filterOpr + "\" fcolval1=\"" + filterValue + "\" fcolval2=\"" + filterValue1 + "\"><params>";
        iXml = iXml + "</params>";
        iXml += CustomViewXml;

        iXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString() + "</root>";

        //Call service
        string ires = string.Empty;
        ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
        ires = objWebServiceExt.GetCustomizeViews(iXml);

        string errMsg = string.Empty;
        if (ires != string.Empty)
            errMsg = util.ParseXmlErrorNode(ires);

        if (errMsg != string.Empty)
        {
            if (errMsg == Constants.SESSIONERROR)
            {
                Session.RemoveAll();
                Session.Abandon();
                SessExpires();
            }
            else
            {
                Response.Redirect(util.ERRPATH + errMsg);
            }
        }
        else
        {
            if (ires != string.Empty)
            {

                ires = _xmlString + ires;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(ires);

                //commented because toolbar getting added many times
                //ViewState["toolbarBtns"] = defaultBut;

                //Remove headrow
                XmlNode rnode = default(XmlNode);
                rnode = xmlDoc.SelectSingleNode("//headrow");
                if (rnode != null)
                    rnode.ParentNode.RemoveChild(rnode);

                //Remove Comps
                XmlNode cNode = default(XmlNode);
                cNode = xmlDoc.SelectSingleNode("//comps");
                if (cNode != null)
                    cNode.ParentNode.RemoveChild(cNode);

                string conditionStr = string.Empty;
                string condStatus = string.Empty;
                int nodecount = 0;
                XmlElement root = xmlDoc.DocumentElement;
                if (root.SelectSingleNode("/root/conditions/cond") != null)
                {
                    nodecount = root.SelectSingleNode("/root/conditions/cond").ChildNodes.Count;

                    XmlNode status = root.SelectSingleNode("/root/conditions/cond");
                    condStatus = status.Attributes["status"] == null ? "" : status.Attributes["status"].Value;

                    for (int i = 1; i <= nodecount - 1; i++)
                    {
                        XmlNode subroot = root.SelectSingleNode("/root/conditions/cond/c" + i);

                        string op = subroot.Attributes["op"] == null ? "" : subroot.Attributes["op"].Value;
                        string v1 = subroot.Attributes["v1"] == null ? "" : subroot.Attributes["v1"].Value;
                        string v2 = subroot.Attributes["v2"] == null ? "" : subroot.Attributes["v2"].Value;
                        string fname = subroot.Attributes["fname"] == null ? "" : subroot.Attributes["fname"].Value;
                        string lopr = subroot.Attributes["lopr"] == null ? "" : subroot.Attributes["lopr"].Value;

                        string opstr = string.Empty;
                        if (op == "=")
                            op = "equal to";
                        else if (op == "<>")
                            op = "not like";
                        else if (op == "like")
                            op = "containing";
                        else if (op == "not like")
                            op = "not containing";
                        else if (op == "<")
                            op = "less than";
                        else if (op == ">")
                            op = "greater than";
                        else if (op == "<=")
                            op = "less than or equal to";
                        else if (op == ">")
                            op = "greater than or equal to";

                        if (op.ToLower() == "between")
                            conditionStr = lopr + " (" + fname + " " + op + " " + v1 + "," + v1 + ") ";
                        else
                            conditionStr += lopr + " (" + fname + " " + op + " " + v1 + ") ";
                    }
                }

                if (conditionStr != string.Empty && condStatus.ToUpper() == "ON")
                {
                    lblFltrCondition.Visible = true;
                    lblFltrCondition.Text = conditionStr;
                }
                else
                {
                    lblFltrCondition.Visible = false;
                }
            }
        }
    }


    //Bind dropdown for List view popup
    private void FillListviewColumns()
    {
        ddlListFilter.Items.Clear();
        ddlSortBy.Items.Clear();
        ddlFldTypes.Items.Clear();
        ListItem lstEmpty = new ListItem();
        lstEmpty.Value = "";
        lstEmpty.Text = "";
        ddlListFilter.Items.Add(lstEmpty);
        ddlSortBy.Items.Add(lstEmpty);
        ddlFldTypes.Items.Add(lstEmpty);
        for (int i = 0; i < fldName.Count; i++)
        {
            if (Caption[i].ToString() != "")
            {
                ListItem lst = new ListItem();
                lst.Text = Caption[i].ToString();
                lst.Value = fldName[i].ToString();
                ddlListFilter.Items.Add(lst);
                ddlSortBy.Items.Add(lst);
                ListItem lst1 = new ListItem();
                lst1.Text = fldType[i].ToString();
                lst1.Value = lst1.Text;
                ddlFldTypes.Items.Add(lst1);
            }
        }
    }

    //Adding Dc and field data to an array
    private void GetDcAndFieldData()
    {
        ArrayList arr = new ArrayList();
        ddlGroupBy.Items.Clear();
        ListItem lstEmpty = new ListItem();
        lstEmpty.Value = "";
        lstEmpty.Text = "";
        ddlGroupBy.Items.Add(lstEmpty);
        for (int dcIdx = 0; dcIdx < strobj.dcs.Count; dcIdx++)
        {
            TStructDef.DcStruct dc = (TStructDef.DcStruct)strobj.dcs[dcIdx];
            string[] dcRange = strobj.GetDcFieldRange(dc.frameno.ToString()).Split(',');
            int startIdx = Convert.ToInt32(dcRange[0]);
            int endIdx = Convert.ToInt32(dcRange[1]);
            int fIdx = 0;
            for (int i = startIdx; i <= endIdx; i++)
            {
                TStructDef.FieldStruct fld = (TStructDef.FieldStruct)strobj.flds[i];
                fIdx = i;
                if (!(fld.caption.StartsWith("axp_recid")) && (!fld.visibility) && (fld.datatype != "Image") && (fld.caption != string.Empty) && (fld.savevalue != false))
                {
                    AddTodcNoArray(dc.frameno);
                    AddToFldNameArray(fld.name);
                    AddToCaptionArray(fld.caption);
                    AddToFldTypeArray(fld.datatype);
                    if (dc.frameno == 1)
                    {
                        ListItem lst = new ListItem();
                        lst.Text = fld.caption;
                        lst.Value = fld.name;
                        ddlGroupBy.Items.Add(lst);
                    }
                }
                strobj.flds[fIdx] = fld;
            }
        }
        FillListviewColumns();
        ddlGroupBy.ClearSelection();
    }

    //Addig field name to array
    private void AddToFldNameArray(string name)
    {
        int idx = fldName.IndexOf(name);
        if (idx == -1)
            fldName.Add(name);
    }

    //Addig DC number to array
    private void AddTodcNoArray(int dcNumber)
    {
        int idx = dcNo.IndexOf(dcNumber);

        if (idx == -1)
            dcNo.Add(dcNumber);
    }

    //Addig caption to array
    private void AddToCaptionArray(string caption)
    {
        Caption.Add(caption);
    }

    //Addig field type to array
    private void AddToFldTypeArray(string type)
    {
        int idx = fldType.IndexOf(type);

        fldType.Add(type);
    }

    //Bind dropdown list view
    private void BindViewList()
    {
        StringBuilder str = new StringBuilder();
        hdnViewNames.Value = str.ToString();
        //Create a new Dataset
        DataSet objDS = new DataSet();
        //Create a new datatable
        DataTable objTable = objDS.Tables.Add();
        //Add coloms

        objTable.Columns.Add("id", typeof(int));
        objTable.Columns.Add("filename", typeof(string));
        //Add rows
        for (int i = 0; i < custViews.Count; i++)
        {
            objTable.Rows.Add(i, custViews[i]);
            str.Append(custViews[i]);
            str.Append(',');
        }
        if (str.Length > 0)
            hdnViewNames.Value = str.ToString();
        ddlViewList.DataValueField = "id";
        ddlViewList.DataTextField = "filename";

        ddlViewList.DataSource = objDS;
        ddlViewList.DataBind();
    }


    #region GetCacheObject
    private CacheManager GetCacheObject()
    {
        CacheManager cacheMgr = null;

        try
        {
            cacheMgr = new CacheManager(errorLog);
        }
        catch (Exception ex)
        {
            CloseDimmerJS();
            Response.Redirect(util.ERRPATH + ex.Message);
        }

        if (cacheMgr == null)
        {
            CloseDimmerJS();
            Response.Redirect(util.ERRPATH + "Unknown error. Please try again later");
        }

        return cacheMgr;
    }
    #endregion

    #region GetStrObject
    private TStructDef GetStrObject(CacheManager cacheMgr)
    {
        TStructDef strObj = null;
        // cachemanager and TStructDef objects throw exceptions
        try
        {
            strObj = cacheMgr.GetStructDef(proj, sid, user, lName, AxRole);
        }
        catch (Exception ex)
        {
            CloseDimmerJS();
            Response.Redirect(util.ERRPATH + ex.Message);
        }

        if (strObj == null)
            Response.Redirect(util.ERRPATH + "Unknown error. Please try again later");


        return strObj;
    }

    #endregion


    //Check uncheck treeview node
    private void CheckCols(string[] arr)
    {
        int nodecount = tvList.Nodes.Count;
        for (int i = 0; i < nodecount; i++)
        {
            tvList.Nodes[i].Checked = false;
            foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
            {
                node.Checked = false;
            }
        }

        for (int i = 0; i < nodecount; i++)
        {
            foreach (TreeNode node in tvList.Nodes[i].ChildNodes)
            {
                string nod = node.Value;
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[j] == nod)
                        node.Checked = true;
                }
            }
        }
        CheckParentNode();
    }

    #endregion

    #region Charts

    private void ClearChartColumns()
    {
        //clear the chart columns and add one empty item
        ddlChartCol1.Items.Clear();
        ddlChartCol2.Items.Clear();
        ddlChartCol1.Items.Add("");
        ddlChartCol2.Items.Add("");
    }

    private void AddChartColumns(string columnName, int index, string type)
    {
        ListItem lst = new ListItem();
        lst.Text = columnName;
        lst.Value = index.ToString();
        ddlChartCol1.Items.Add(lst);
        if (type == "n")
            ddlChartCol2.Items.Add(lst);
    }

    protected void btnGetChart_Click(object sender, EventArgs e)
    {
        if (ddlChartCol1.SelectedValue != "" && ddlChartCol2.SelectedValue != "")
        {
            try
            {
                PrepareChart(rbtnChartType.SelectedValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            string chartScript = "<script type='text/javascript'>ShowCharts('LView','show');</script>";
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "myChart", chartScript, false);
        }
    }

    private void PrepareChart(string type)
    {
        StringBuilder strChart = new StringBuilder();
        strChart.Append("<script type='text/javascript'>");

        DataTable dt = new DataTable();
        dt = (DataTable)Session["FilteredData"];
        if (dt == null) dt = (DataTable)Session["cac_order"];
        if (dt == null) return;
        string col1 = string.Empty; string col2 = string.Empty;
        string colData = string.Empty;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["axrowtype"].ToString() != "subhead" && dt.Rows[i]["axrowtype"].ToString() != "stot")
            {
                col2 = dt.Rows[i][Convert.ToInt32(ddlChartCol2.SelectedValue)].ToString().Replace(",", "");

                if (colData == string.Empty)
                    colData = "['" + dt.Rows[i][Convert.ToInt32(ddlChartCol1.SelectedValue)].ToString() + "', " + col2 + "]";
                else
                    colData += ",['" + dt.Rows[i][Convert.ToInt32(ddlChartCol1.SelectedValue)].ToString() + "', " + col2 + "]";
            }
        }

        string chWidth = "550";
        string chHeight = "200";
        if (hdnChartSize.Value == "full")
        {
            chWidth = "800";
            chHeight = "300";
        }

        if (type == "line")
            strChart.Append(GetLineChart(colData, chWidth, chHeight));
        else if (type == "bar")
            strChart.Append(GetBarChart(colData, chWidth, chHeight));
        else if (type == "pie")
            strChart.Append(GetPieChart(colData, chWidth, chHeight));

        strChart.Append("</script>");
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "myChart", strChart.ToString(), false);
    }


    private string GetLineChart(string data, string width, string height)
    {
        StringBuilder strChart = new StringBuilder();
        strChart.Append("google.setOnLoadCallback(drawChart);");
        strChart.Append("function drawChart() {var data = google.visualization.arrayToDataTable([");
        strChart.Append("['" + ddlChartCol1.SelectedItem.Text + "', '" + ddlChartCol2.SelectedItem.Text + "'],");
        strChart.Append(data);
        strChart.Append("]);");
        strChart.Append("data = google.visualization.data.group(data,[0],[{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number'}]);");
        strChart.Append("new google.visualization.LineChart(document.getElementById('chart_div'))");
        strChart.Append(".draw(data, {title: '" + objListViewData.LviewCaption.ToString() + "',width: " + width + ",height:" + height + ",fontName:'Segoe UI',fontSize:10});}");
        strChart.Append("drawChart();ShowCharts('LView','show');");
        return strChart.ToString();
    }

    private string GetBarChart(string data, string width, string height)
    {
        StringBuilder strChart = new StringBuilder();
        strChart.Append("google.setOnLoadCallback(drawChart);function drawChart() {");
        strChart.Append("var data = google.visualization.arrayToDataTable([");
        strChart.Append("['" + ddlChartCol1.SelectedItem.Text + "', '" + ddlChartCol2.SelectedItem.Text + "'],");
        strChart.Append(data);
        strChart.Append("]);");
        strChart.Append("data = google.visualization.data.group(data,[0],[{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number'}]);");
        strChart.Append("new google.visualization.BarChart(document.getElementById('chart_div'))");
        strChart.Append(".draw(data, {title: '" + objListViewData.LviewCaption.ToString() + "',width: " + width + ",height:" + height + ",fontName:'Segoe UI',fontSize:10});}");
        strChart.Append("drawChart();ShowCharts('LView','show');");
        return strChart.ToString();
    }

    private string GetPieChart(string data, string width, string height)
    {
        StringBuilder strChart = new StringBuilder();
        strChart.Append("google.setOnLoadCallback(drawChart);");
        strChart.Append("function drawChart() {var data = google.visualization.arrayToDataTable([");
        strChart.Append("['" + ddlChartCol1.SelectedItem.Text + "', '" + ddlChartCol2.SelectedItem.Text + "'],");
        strChart.Append(data);
        strChart.Append("]);");
        strChart.Append("data = google.visualization.data.group(data,[0],[{'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number'}]);");
        strChart.Append("new google.visualization.PieChart(document.getElementById('chart_div'))");
        strChart.Append(".draw(data, {title: '" + objListViewData.LviewCaption.ToString() + "',width: " + width + ",height:" + height + ",fontName:'Segoe UI',fontSize:10});}");
        strChart.Append("drawChart();ShowCharts('LView','show');");

        return strChart.ToString();
    }

    #endregion

    #region ListView Filter
    public void VisibleGridViewData()
    {
        string ColsToFilter = string.Empty;

        isFilterEnabled = false;
        DataTable dt = (DataTable)Session["cac_order"];
        //tst:cil1,col2~tst2:col1,col2
        if (ConfigurationManager.AppSettings["AxListviewFilterCols"] != null)
        {
            string filterCols = ConfigurationManager.AppSettings["AxListviewFilterCols"].ToString();
            string[] filterDetails = filterCols.Split('~');
            for (int i = 0; i < filterDetails.Length; i++)
            {
                string[] filterTst = filterDetails[i].ToString().Split(':');
                if (filterTst[0].ToString().Trim() == lName)
                {
                    ColsToFilter = filterTst[1].ToString().Trim();
                    hdnFilterColumns.Value = ColsToFilter;
                    isFilterEnabled = true;
                    break;
                }
            }
        }
        if (isFilterEnabled)
        {
            Session["FilteredData"] = null;
            string[] rowsToExclude = new string[] { "subhead", "gtot", "stot" };
            string typeColumn = "axrowtype";
            int pageSize = Convert.ToInt32(gridPageSize);
            int visibleRows = 0;
            var allGridCol = string.Empty;
            var hiddenGridCol = string.Empty;
            var hiddenGridFldCol = string.Empty;
            for (int i = 0; i < headName.Count; i++)
            {

                if ((headName[i].ToString() != string.Empty) && (!headName[i].ToString().StartsWith("axp")))
                    allGridCol = (allGridCol == string.Empty) ? headName[i].ToString() : allGridCol + "^" + headName[i].ToString();

                if ((colHide[i].ToString() == "false") && (headName[i].ToString() != string.Empty))
                {
                    hiddenGridCol = (hiddenGridCol == string.Empty) ? headName[i].ToString() : hiddenGridCol + "^" + headName[i].ToString();
                    hiddenGridFldCol = (hiddenGridFldCol == string.Empty) ? colFld[i].ToString() : hiddenGridFldCol + "^" + colFld[i].ToString();
                }
            }

            hdnGridColumns.Value = allGridCol;
            hdnHideColumns.Value = hiddenGridCol;
            hdnHideShowFld.Value = hiddenGridFldCol;

            hdnJsonData.Value = util.DataTableToJson(dt, headName, colFld, colHide, rowsToExclude, typeColumn, ColsToFilter, pageSize, out visibleRows);

            if (totalRows == 0 || visibleRows == 0)
                hdnNoOfRecords.Value = "";
            else
                hdnNoOfRecords.Value = "Filter will be applied on " + visibleRows + " of " + totalRows;
        }
        else
        {
            hdnJsonData.Value = "[{\"enablefilter\":\"false\"}]";
        }
    }
    protected void btnSortGrid_Click(object sender, EventArgs e)
    {

        pgCap.Visible = lvPage.Visible = Session["FilteredData"] == null ? true : false;
        pages.Text = Session["FilteredData"] == null ? pages.Text : "";
        DataTable dt = Session["FilteredData"] == null ? (DataTable)Session["cac_order"] : (DataTable)Session["FilteredData"];
        if (dt.Rows.Count > 0 && objListViewData.Ires != null && objListViewData.Ires.ToString() != "")
        {
            //refreshFilter.Visible = true;
            string ires = objListViewData.Ires.ToString();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(ires);
            CreateHeaderRow(xmlDoc, lvPage.SelectedItem.Text);

            if (dt != null && headName.IndexOf(hdnSortColName.Value) > -1)
            {
                string columnName = colFld[headName.IndexOf(hdnSortColName.Value)].ToString();
                if (objListViewData.SortDir != null && objListViewData.SortDir.ToString() == "ASC")
                {
                    dt.DefaultView.Sort = columnName + " " + "DESC";
                    objListViewData.SortDir = "DESC";
                }
                else
                {
                    dt.DefaultView.Sort = columnName + " " + "ASC";
                    objListViewData.SortDir = "ASC";
                }
                dt = dt.DefaultView.ToTable();
                GridView1.DataSource = dt;
                GridView1.DataBind();
                records.Text = "Total no. of records: " + dt.Rows.Count;
            }
        }
    }

    protected void btnFilterGrid_Click(object sender, EventArgs e)
    {
        if (Session["cac_order"] != null && objListViewData.Ires != null)
        {

            try
            {
                //refreshFilter.Visible = true;
                string strUnChecked = "unchecked";
                string ires = objListViewData.Ires.ToString();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(ires);
                CreateHeaderRow(xmlDoc, lvPage.SelectedItem.Text);
                DataTable dtFilterData = new DataTable();
                DataTable dt = (DataTable)Session["cac_order"];
                if (dt != null && headName.IndexOf(hdnFilterColName.Value) > -1)
                {
                    string columnName = colFld[headName.IndexOf(hdnFilterColName.Value)].ToString();
                    string value = hdnFilterBy.Value;
                    //adding  checked column name and value to dtFilterConds
                    if (hdnFilterChecked.Value != strUnChecked)
                    {
                        dtFilterConds.Rows.Add(columnName, value);
                    }
                    //removeing  unchecked column name and value from dtFilterConds
                    else
                    {
                        var rows = dtFilterConds.Select("FilterColumn = '" + columnName + "' AND Value ='" + value + "' ");
                        foreach (var row in rows)
                            row.Delete();
                    }


                    dt = Filterdatarecords();
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    pgCap.Visible = lvPage.Visible = false;
                    records.Text = "Total no. of records: " + dt.Rows.Count;
                    pages.Text = "";
                }
            }
            catch (Exception ex)
            {
                //Response.Redirect(util.ERRPATH + ex.Message);
            }

        }
    }
    private DataTable Filterdatarecords()
    {
        DataTable dtFilterData = new DataTable();
        DataTable dt = (DataTable)Session["cac_order"];
        string[] arrFilterColumns = new string[100];

        if (hdnFilterColumns.Value != string.Empty)
        {
            arrFilterColumns = hdnFilterColumns.Value.Split(',');

            int columnCount = 0;
            DataTable dtsession;
            DisplayFilterCondition(dtFilterConds);
            for (int i = 0; i < arrFilterColumns.Length; i++)
            {
                String serachColumnName = arrFilterColumns[i].ToString();
                bool contains = dtFilterConds.AsEnumerable().Any(row => serachColumnName == row.Field<String>("FilterColumn"));
                if (contains)
                {

                    Session["FilteredData"] = null;
                    columnCount++;// column count from arrFilterColumns
                    DataRow[] dr = dtFilterConds.Select("FilterColumn='" + serachColumnName + "'");
                    int colValueCount = 1; //value count for each column

                    for (int j = 0; j < dr.Length; j++)
                    {

                        //filters for first value in first column
                        if ((Session["FilteredData"] == null) && (columnCount <= 1))
                        {
                            bool containsvalue = dt.AsEnumerable().Any(row => dr[j]["Value"].ToString() == row.Field<string>(dr[j]["FilterColumn"].ToString()));
                            if (containsvalue)
                            {
                                dtFilterData = dt.AsEnumerable().Where(row => row.Field<String>(dr[j]["FilterColumn"].ToString()) == dr[j]["Value"].ToString()).CopyToDataTable();
                                Session["FilteredData"] = dtFilterData;
                                dt = dtFilterData;
                            }
                        }
                        else
                        {
                            //filters from second column
                            if (columnCount > 1)
                            {
                                DataTable dtfilterdata = new DataTable();
                                DataTable dtfirstfilterdata = new DataTable();
                                dtfirstfilterdata = Session["FilterFirstdata"] as DataTable;
                                bool containsvalueexist = dtfirstfilterdata.AsEnumerable().Any(row => dr[j]["Value"].ToString() == row.Field<String>(dr[j]["FilterColumn"].ToString()));
                                if (containsvalueexist)
                                    dtfilterdata = dtfirstfilterdata.AsEnumerable().Where(row => row.Field<String>(dr[j]["FilterColumn"].ToString()) == dr[j]["Value"].ToString()).CopyToDataTable();
                                //filter for first value from second column
                                if (colValueCount <= 1)
                                {
                                    dt = dtfilterdata;
                                    Session["filteruniondata"] = dt;
                                    colValueCount++;
                                }
                                //filter for others value  from second column
                                else
                                {
                                    colValueCount++;
                                    DataTable dt1 = Session["filteruniondata"] as DataTable;
                                    if (dt1.Rows.Count != 0)
                                    {
                                        dt = dtfilterdata.AsEnumerable().Union(dt1.AsEnumerable()).CopyToDataTable();
                                        Session["filteruniondata"] = dt;
                                    }
                                    else
                                    {
                                        Session["filteruniondata"] = dtfilterdata;
                                        dt = dtfilterdata;
                                    }
                                    if (dr.Length == (colValueCount - 1))
                                    {
                                        Session["FilterFirstdata"] = dt;
                                    }
                                }
                                Session["FilteredData"] = dt;
                            }
                            //filters for others values in first column
                            else
                            {
                                dtsession = Session["FilteredData"] as DataTable;
                                dt = (DataTable)Session["cac_order"];
                                bool containsvaluesecol = dt.AsEnumerable().Any(row => dr[j]["Value"].ToString() == row.Field<String>(dr[j]["FilterColumn"].ToString()));
                                if (containsvaluesecol)
                                {
                                    dtFilterData = dt.AsEnumerable().Where(row => row.Field<String>(dr[j]["FilterColumn"].ToString()) == dr[j]["Value"].ToString()).CopyToDataTable();
                                    dt = dtsession.AsEnumerable().Union(dtFilterData.AsEnumerable()).CopyToDataTable();
                                    Session["FilteredData"] = dt;
                                }
                            }
                        }
                        //store first column filter value
                        if (columnCount == 1)
                            Session["FilterFirstdata"] = dt;

                    }

                }
            }


            if (dt.Rows.Count == 0)
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);

            }
        }

        return dt;
    }


    private void DisplayFilterCondition(DataTable dtFilterConds)
    {
        //division for displaying selected items
        if (dtFilterConds.Rows.Count > 0)
        {
            filterCondDispText.Append("<div class='filteritem'>");
            filterCondDispText.Append("<span style='font-weight: bold'>Filtered on: </span>");
            var grouped = from table in dtFilterConds.AsEnumerable()

                          group table by new { filterCol = table["FilterColumn"] } into groupby

                          select new

                          {

                              Value = groupby.Key,

                              ColumnValues = groupby

                          };

            foreach (var key in grouped)
            {
                int idx = colFld.IndexOf(key.Value.filterCol);
                string strColName = headName[idx].ToString();

                filterCondDispText.Append(strColName + "-");
                filterCondDispFullText.Append(strColName + "-");

                foreach (var columnValue in key.ColumnValues)
                {
                    filterCondDispText.Append(columnValue["Value"].ToString() + ",");
                    filterCondDispFullText.Append(columnValue["Value"].ToString() + ",");
                }
            }


            if (filterCondDispText.Length > 190)
            {
                filterCondDispText.Remove(190, ((filterCondDispText.Length - 1) - 190));
                filterCondDispText.Append("<a  onclick='javascript:ShowMessageBox(\"" + filterCondDispFullText + "\");'> More</a>");
            }
            filterCondDispText.Append("</div>");
            //divFilterCondlist.InnerHtml = filterCondDispText.ToString();
        }
        else
        {
            // divFilterCondlist.InnerHtml = string.Empty;

        }
    }


    protected void RecordsPerPage_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        // gridPageSize = recPerPage.SelectedItem.Text;
        GetLViewData("1");
    }

    protected void btnClearFilter_Clicked(object sender, System.EventArgs e)
    {
        // refreshFilter.Visible = false;
        // divFilterCondlist.InnerHtml = string.Empty;
        //gridPageSize = ConfigurationManager.AppSettings["PageRowCount"].ToString().Trim();
        GetLViewData(lvPage.SelectedItem.Text);
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(), "StopDimmer", "<script>AxWaitCursor(false);</script>", false);
    }

    protected void btnHideColumn_Click(object sender, System.EventArgs e)
    {
        // refreshFilter.Visible = true;
        if (headName.Count == 0)
            headName = (ArrayList)objListViewData.HeadName;
        if (colFld.Count == 0)
            colFld = (ArrayList)objListViewData.ColFld;
        string colName = colFld.IndexOf(hdnHideColName.Value).ToString();
        DataTable dt = (DataTable)Session["cac_order"];
        int idx = dt.Columns.IndexOf(hdnHideColName.Value);
        if (GridView1.Columns[idx].Visible)
            GridView1.Columns[idx].Visible = false;
        else
            GridView1.Columns[idx].Visible = true;
    }

    private void UpdateRecordsPerPage()
    {
        //int recIdx = recPerPage.Items.IndexOf(new ListItem(gridPageSize));
        int recIdx = 0; //murali
        if (recIdx == -1)
        {
            List<int> list = new List<int> { 50, 100, 200, 500, 1000, 2000 };
            int ivPageSize = Convert.ToInt32(gridPageSize);
            int closest = list.OrderBy(item => Math.Abs(ivPageSize - item)).First();
            if (closest > ivPageSize && list.IndexOf(closest) > 0)
                recIdx = list.IndexOf(closest) - 1;
            else if (closest > ivPageSize && list.IndexOf(closest) == 0)
                recIdx = 0;
            else
                recIdx = list.IndexOf(closest) + 1;

            //  recPerPage.Items.Insert(recIdx, new ListItem(gridPageSize));
            //  recPerPage.SelectedIndex = recIdx;
        }
        else
        {
            // recPerPage.SelectedIndex = recIdx;
        }
    }


    #endregion

    private void AddCustomLinks()
    {
         Custom cusObj = Custom.Instance;
        string projName = HttpContext.Current.Session["Project"].ToString();
        for (int i = 0; i < cusObj.jsPageName.Count; i++)
        {
            string fileName=string.Empty;
            if (cusObj.jsPageName[i].ToString() == "listIview.aspx")
            {
                fileName = cusObj.jsPageFiles[i].ToString();
                HtmlGenericControl js = new HtmlGenericControl("script");
                js.Attributes["type"] = "text/javascript";
                string path = "../" + projName + "/" + fileName;
                js.Attributes["src"] = path;
                ScriptManager1.Controls.Add(js);
            }
        }

        for (int j = 0; j < cusObj.cssPageName.Count; j++)
        {
            string fileName = string.Empty;
            if (cusObj.cssPageName[j].ToString() == "listIview.aspx")
            {
                fileName = cusObj.cssPageFiles[j].ToString();
                HtmlGenericControl css = new HtmlGenericControl("link");
                css.Attributes["type"] = "text/css";
                css.Attributes["rel"] = "stylesheet";
                string path = "../" + projName + "/" + fileName;
                css.Attributes["href"] = path;
                ScriptManager1.Controls.Add(css);
            }
        }
    }
}

