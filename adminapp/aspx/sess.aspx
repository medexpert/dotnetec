<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sess.aspx.vb" Inherits="sess" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="Session Expire">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">

    <title>
        <%=appTitle%></title>
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" id="generic" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="Link1" />
    <%End If%>
    <link href="../Css/login.min.css?v=3" rel="stylesheet" type="text/css" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62"></script>
     <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <script>
        //localStorage.clear();
        sessionStorage.removeItem("homeJsonObj");
        sessionStorage.clear();
        clearLocalStorage = function (exceptions, contains) {
            contains = contains || false;
            var storage = localStorage
            var keys = [];
            var exceptions = [].concat(exceptions) //prevent undefined

            //get storage keys
            $.each(localStorage, function (key, val) {
                keys.push(key);
            });

            //loop through keys
            for (i = 0; i < keys.length; i++) {
                var key = keys[i]
                var deleteItem = true
                //check if key excluded
                for (j = 0; j < exceptions.length; j++) {
                    var exception = exceptions[j];
                    if (key.indexOf(exception) > -1 && contains) { deleteItem = false; }
                    else if (key == exception) { deleteItem = false; }
                }
                //delete key
                if (deleteItem) {
                    localStorage.removeItem(key)
                }
            }
        }

        clearLocalStorage(['projInfo-', 'versionInfo-', 'langInfo-'], true);
        $(document).ready(function () {
            $("#lblsesslogin").parent().prop('title', lcm[389]);
        })
    </script>

</head>
<body id="main_body" runat="server">
    <video id="bgvid" runat="server" playsinline="" autoplay="" muted="" loop="">
        <source src="" type="video/mp4" id="bgvidsource" runat="server">
    </video>
    <div id="login-container">
        <div style="display: none;" id="login-logo">
            <img src="../AxpImages/logo.png" alt="Logo" />
        </div>
        <div style="display: none;" id="login-prouct-name">
            <%=appName%>
        </div>
        <div id="container-arrow">
            <div id="info-box">
                <h2>
                    <asp:Label ID="lblsessexp" runat="server" meta:resourcekey="lblsessexp">Your session has expired</asp:Label></h2>
                <ul>
                    <asp:Label ID="lblsessrsn" runat="server" meta:resourcekey="lblsessrsn">This has happened due to the following reasons:</asp:Label>
                    <li>
                        <asp:Label ID="lblsessbrwsr" runat="server" meta:resourcekey="lblsessbrwsr">a. You have kept the browser window idle for a long time.</asp:Label></li>
                    <li>
                        <asp:Label ID="lblsessstatic" runat="server" meta:resourcekey="lblsessstatic">b. You are accessing the application URL from a saved or static page.</asp:Label></li>
                    <li>
                        <asp:Label ID="lblsessdup" runat="server" meta:resourcekey="lblsessdup">c. You might have logged into another session.</asp:Label></li>
                </ul>
                <ul>
                    <li><a href="<%=loginStr %>" class="btn hotbtn ">
                        <asp:Label ID="lblsesslogin" runat="server" meta:resourcekey="lblsesslogin">Log in</asp:Label></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>
