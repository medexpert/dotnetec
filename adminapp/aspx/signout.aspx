<%@ Page Language="VB" AutoEventWireup="false" CodeFile="signout.aspx.vb" Inherits="signout"
    Debug="true" %>

<!DOCTYPE html>

<html>
<head runat="server">

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta name="description" content="Axpert Signout">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title><%=appTitle%></title>
    <%If EnableOldTheme = "true" Then%>

    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />
    <%End If%>
    <link href="../Css/login.min.css?v=3" rel="stylesheet" type="text/css" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62"></script>
     <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <script>
        $(document).ready(function(){
            $("#lnkLogin").prop('title', lcm[389]);
        })
    </script>
    <style>
        .bdir-rtl form{
            direction:rtl;
        }
    </style>
</head>

<body id="main_body" runat="server">
    <video id="bgvid" runat="server" playsinline="" autoplay="" muted="" loop="">
        <source src="" type="video/mp4" id="bgvidsource" runat="server">
    </video>
    <form id="form1" runat="server"  >
        <div id="login-container">
            <div id="container-arrow">
                <div id="info-box" style="height: 200px">
                    <h2>
                        <asp:Label ID="lblMsg" runat="server" Style="text-align: center; margin-left: 40px; margin-right: 40px"> </asp:Label>
                    </h2>
                    <ul style="text-align: center; margin-left: 0px">
                        <li><a href="<%=loginStr %>" class="hotbtn btn" id="lnkLogin">Login</a></li>
                    </ul>
                </div>
            </div>
            <asp:Label ID="lblLoggedout" runat="server" meta:resourcekey="lblLoggedout" Visible="false">You have successfully logged out.</asp:Label>
            <asp:Label ID="lblCustomerror" runat="server" meta:resourcekey="lblCustomerror" Visible="false">Server error. Please try again.If the problem continues, please contact your administrator.</asp:Label>
        </div>
        <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    </form>
</body>
</html>
