﻿using CacheMgr;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;


public partial class Tstruct : System.Web.UI.Page
{
    #region Variable Declaration
    Util.Util util;
    public string proj = string.Empty;
    public string sid = string.Empty;
    public string language = string.Empty;
    public string trace = string.Empty;
    public string user = string.Empty;
    public string transId = string.Empty;
    public string AxRole = string.Empty;
    public string rid = string.Empty;
    public string searchVal = string.Empty;
    public string direction = "ltr";
    public string classdir = "left";
    public string appsessionKey = string.Empty;
    public int actbtncount = 0;
    int docHgt = 0;
    public string optStr = string.Empty;
    //Variables to store toolbar buttons and their left values
    ArrayList leftBtns = new ArrayList();    //left index of the btns, to arrange the order
    ArrayList tmpLeftBtns = new ArrayList();
    ArrayList toolBarBtns = new ArrayList();
    ArrayList actionBarBtns = new ArrayList();
    ArrayList sortedBtns = new ArrayList();
    ArrayList paramNames = new ArrayList();
    ArrayList paramValues = new ArrayList();
    ArrayList clientParamValues = new ArrayList();
    ArrayList headNames = new ArrayList();
    ArrayList customBtnHtml = new ArrayList();
    string AxOnApproveDisable = "false";
    string AxOnRejectDisable = "false";
    string AxOnReturnSave = "false";
    string AxOnRejectSave = "false";

    string AxLogTimeTaken = "false";
    //variables to store the Html in the page
    string submitBtn = string.Empty;
    string cancelBtn = string.Empty;
    StringBuilder tstHTML = new StringBuilder();
    public StringBuilder submitCancelBtns = new StringBuilder();
    StringBuilder taskBtnHtml = new StringBuilder();
    StringBuilder attHtml = new StringBuilder();
    public StringBuilder toolbarBtnHtml = new StringBuilder();


    public StringBuilder dcHtml = new StringBuilder();
    public StringBuilder tstHeader = new StringBuilder();
    public String tstCss = string.Empty;
    StringBuilder tstSavedHtml = new StringBuilder();


    //Public varaibles declaration     
    public Custom customObj = null;
    string actstr = string.Empty;
    static string actstrType = "open";
    string loadResult = string.Empty;
    string strGlobalVar = string.Empty;
    string fileName = string.Empty;
    public string errorLog = string.Empty;
    string queryStr = string.Empty;
    public string tstCaption = string.Empty;
    public string tstName = string.Empty;
    public StringBuilder tstVars = new StringBuilder();
    public StringBuilder tstJsArrays = new StringBuilder();
    public StringBuilder tstScript = new StringBuilder();
    public StringBuilder tstTabScript = new StringBuilder();
    public string tstDraftsScript = string.Empty;
    public string traceLog = string.Empty;
    // public string tstTimeVars = string.Empty;
    public string btnFunction = string.Empty;
    string btnStyle = "handCur";
    string btnHTML = string.Empty;
    string customFolder = string.Empty;
    string customPage = string.Empty;
    LogFile.Log logobj = new LogFile.Log();
    ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
    public string structXml = string.Empty;
    public string jsFromCache = string.Empty;
    public string htmlFromCache = string.Empty;
    TStructData dataObjFromCache;
    Boolean isTstInCache = false;
    Boolean isTstructCached = false;
    string draftsPath = string.Empty;
    public static string enableBackForwButton = string.Empty;
    public bool isTstPop = false;
    public string axpRefreshParent = "false";
    public string axRefreshSelect = "false";
    public string axRefreshSelectID = "";
    public string axSrcSelectID = "";
    public string axRefreshSelectType = "";
    public string langauge = "ENGLISH";
    // public string ptransid = string.Empty;
    public string exportTallyTid = string.Empty;
    TStructDef strObj = null;
    public string dcGridOnSave = "true";
    public string designModeBtnHtml = String.Empty;

    public string isAxpImagePath = "false";
    #endregion

    public StringBuilder getLang = new StringBuilder();
    public StringBuilder strLogTime = new StringBuilder();
    public float formLogTime;
    public float pageLogTime;
    DateTime stTime;
    DateTime edTime;
    public int FetchPickListRows = 1000;
    public bool isRapidLoad = false;
    public bool loadFromMem = true;
    public string defaultDepFlds = String.Empty;
    public string langType = "en";
    //public bool newDesigner = false;
    public bool theModeDesign = false;
    public string axWizardType = "";
    public string wizardClass = "";
    public string schemaName = string.Empty;
    string isTstFromHyperLink = "false";
    string designJson = string.Empty;
    DataTable axpConfigStr = new DataTable();
    string AxpIsAutoSplit = string.Empty;
    string AxpIviewDisableSplit = string.Empty;
    string AxpFileUploadlmt = string.Empty;
    string AxpCameraOption = string.Empty;
    string AxpSaveImageDb = "false";
    public string dvFooterHtml = string.Empty;
    protected override void InitializeCulture()
    {
        if (Session["language"] != null)
        {
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(Session["language"].ToString().ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
            }
        }
    }

    #region PageLoad
    /// <summary>
    /// Page Load event of the tstruct page where the tstruct construction is initialized.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "eval(callParent(\"loadFrame\", \"function\"));", true);
        util = new Util.Util();
        stTime = DateTime.Now;
        DateTime startTime = DateTime.Now;
        //logobj.CreateLog("Page load " , sid, "LogTimeTaken", "new");
        util.IsValidSession();
        ResetSessionTime();

        DateTime webStart = DateTime.Now;
        Response.ExpiresAbsolute = DateTime.Now;
        Response.Expires = -1441;
        Response.CacheControl = "no-cache";
        Response.AddHeader("Pragma", "no-cache");
        Response.AddHeader("Pragma", "no-store");
        Response.AddHeader("cache-control", "no-cache");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoServerCaching();

        if (Session["project"] == null)
        {
            SessionExpired();
            return;
        }
        else
        {
            if (util.IsValidQueryString(Request.RawUrl) == false)
                HttpContext.Current.Response.Redirect(util.ERRPATH + Constants.INVALIDURL);
            SetSessionVariables();
            if (!util.licencedValidSessionCheck())
            {
                HttpContext.Current.Response.Redirect(util.ERRPATH + Constants.SESSIONEXPMSG, false);
                return;
            }

            //TODO: ask malakonda and remove the below code if not used
            if (HttpContext.Current.Session["fd-HugeFlds"] != null)
                util.ClearFDFldSession();

            DateTime sdate = DateTime.Now;
            ConstructTstruct();
            DateTime enddate = DateTime.Now;
            strLogTime.Append("TotConstructTstruct-" + sdate.Subtract(enddate).TotalMilliseconds.ToString());

            CustomDiv.InnerHtml = customObj.GetCustomDivHtml();
            IncludeJsFiles();
            SetLangStyles();
        }
        util.DeleteKeyOnRefreshSave();
        UpdateNavigation();

        //Code to store the timetaken details
        TStructData tstData = (TStructData)Session[hdnDataObjId.Value];
        if (tstData != null && AxLogTimeTaken == "true")
        {
            tstData.strServerTime = stTime.Subtract(webStart).TotalMilliseconds + "," + edTime.Subtract(stTime).TotalMilliseconds + "," + DateTime.Now.Subtract(edTime).TotalMilliseconds;
            Session[hdnDataObjId.Value] = tstData;
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "closeParentFrame();", true);
        edTime = DateTime.Now;
        strLogTime.Append("PageLoad FUll-" + startTime.Subtract(edTime).TotalMilliseconds.ToString());
        pageLogTime = float.Parse(edTime.Subtract(startTime).TotalMilliseconds.ToString());
        pageLogTime = pageLogTime - formLogTime;
        if (Session["AxDcGridOnSave"] != null)
            dcGridOnSave = HttpContext.Current.Session["AxDcGridOnSave"].ToString();
        Page.ClientScript.RegisterStartupScript(GetType(), "set Grid DC Pop Up Visible On Save", "<script>var dcGridOnSave = '" + dcGridOnSave.ToString() + "';var theModeDesign = '" + theModeDesign.ToString().ToLower() + "';var AxpIsAutoSplit = '" + AxpIsAutoSplit.ToLower() + "';var AxpIviewDisableSplit = '" + AxpIviewDisableSplit.ToLower() + "';var AxpFileUploadlmt='" + AxpFileUploadlmt.ToString() + "';var AxpCameraOption ='" + AxpCameraOption.ToString() + "';</script>");

    }

    private void UpdateNavigation()
    {
        if (Session["AxFromHypLink"] != null)
            isTstFromHyperLink = Session["AxFromHypLink"].ToString();

        if (!string.IsNullOrEmpty(Request.QueryString["axp_IsSaveUrl"]))
        {
            if (Request.QueryString["axp_IsSaveUrl"].ToString() == "true")
                Session["axp_IsSaveUrl"] = "true";
        }
        string frameName = string.Empty;
        if (Request.QueryString["axpfrm"] != null)
            frameName = Convert.ToString(Request.QueryString["axpfrm"]);
        if (Session["backForwBtnPressed"] == null || (Session["backForwBtnPressed"] != null && !Convert.ToBoolean(Session["backForwBtnPressed"])) && (Request.QueryString.Count < 2 || Request.UrlReferrer != null && (Request.UrlReferrer.AbsolutePath.Contains("listIview.aspx") || Request.UrlReferrer.AbsolutePath.Contains("iview.aspx") || Request.UrlReferrer.AbsolutePath.Contains("tstruct.aspx") || Request.UrlReferrer.AbsolutePath.Contains("tstructdesign.aspx") || Request.UrlReferrer.AbsolutePath.Contains("mainnew.aspx"))) && frameName != "t")
        {
            if (isTstPop)
                Session["enableBackButton"] = "false";
            else if (Session["AxHypTstRefresh"] != null && Session["AxHypTstRefresh"].ToString() == "true")
            {
                Session["AxHypTstRefresh"] = "false";
            }
            else if (Request.QueryString["AxHypTstRefresh"] != null && Request.QueryString["AxHypTstRefresh"].ToString() == "true")
            {

            }
            else
                util.UpdateNavigateUrl(HttpContext.Current.Request.Url.AbsoluteUri);
        }
        if (Session["axp_IsSaveUrl"] != null)
            Session["axp_IsSaveUrl"] = null;
        Session["backForwBtnPressed"] = false;
        if (Session["RapidTsTruct"] != null && Session["RapidTsTruct"].ToString() == "true")
            enableBackForwButton = "<script language=\'javascript\' type=\'text/javascript\' > enableBackButton='" + Convert.ToBoolean(Session["enableBackButton"]) + "';" + " enableForwardButton='" + Convert.ToBoolean(Session["enableForwardButton"]) + "'; var fromHyperLink='" + isTstFromHyperLink + "';var isRapidLoad='" + isRapidLoad + "';var defaultDepFlds='" + defaultDepFlds + "';</script>";
        else
            enableBackForwButton = "<script language=\'javascript\' type=\'text/javascript\' > enableBackButton='" + Convert.ToBoolean(Session["enableBackButton"]) + "';" + " enableForwardButton='" + Convert.ToBoolean(Session["enableForwardButton"]) + "'; var fromHyperLink='" + isTstFromHyperLink + "';var isRapidLoad='" + false + "';var defaultDepFlds='" + defaultDepFlds + "';</script>";
    }

    private void ResetSessionTime()
    {
        if (Session["AxSessionExtend"] != null && Session["AxSessionExtend"].ToString() == "true")
        {
            HttpContext.Current.Session["LastUpdatedSess"] = DateTime.Now.ToString();
            ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "eval(callParent('ResetSession()', 'function'));", true);
        }
    }

    #endregion


    #region formload functions

    #region ConstructTstruct
    /// <summary>
    /// Function to call all formload related functions
    /// </summary>
    private void ConstructTstruct()
    {
        // To set the values for the global variables like transid,sessionid, user etc..
        SetGlobalVariables();
        //Check desing access 
        CheckDesignAccess();
        stTime = DateTime.Now;
        //Design json and Config string will be retrieved from direct db instead of node js
        Boolean designMode = Convert.ToBoolean(HttpContext.Current.Session[transId + "IsDesignMode"]);
        GetDesignModeData();
        //GetStructDesignAndConfig(); //Design & Config data merged into structure XML(service call), so this direct db call not required for run mode. 
        CacheManager cacheMgr = GetCacheObject();
        strObj = GetStrObject(cacheMgr);
        GetStructConfig();
        customObj = Custom.Instance;

        if (goval.Value == "go")
        {
            if (lvPage.SelectedValue != "")
                callWebservice(lvPage.SelectedValue);
        }
        else
        {
            Session["StructureHtml"] = "";
            Session["ToolbarBtnIcons"] = "";
            // To write the tstruct details to the client.
            WriteGlobalVariables();
            // to get the language from login page
            GetLanguage();

            if (strObj == null)
                return;
            structXml = strObj.structRes;
            edTime = DateTime.Now;
            strLogTime.Append("GetStructure-" + stTime.Subtract(edTime).TotalMilliseconds.ToString());
            formLogTime = float.Parse(edTime.Subtract(stTime).TotalMilliseconds.ToString());
            //logobj.CreateLog("Get Structure- timetaken-" + stTime.Subtract(edTime).TotalMilliseconds.ToString(), sid, "LogTimeTaken", "");
            try
            {
                SaveID.Value = strObj.save_id;
                PublishID.Value = strObj.Publish_id;
                IsPublish.Value = strObj.Is_Publish;
            }
            catch (Exception ex) { }

            parseXMLDoc(strObj.pdfList);

            WriteTstJsArrayDef(strObj);

            stTime = DateTime.Now;
            //if (strObj.IsObjFromCache )
            //{
            //    getDesignedData(strObj);
            //}

            // To get structure details from the object
            GetStructureDetails(strObj, cacheMgr);

            if (strObj.axdesignJObject != null)
            {
                if (Session["Axp_DesignJson"] != null && Session["Axp_DesignJson"].ToString() != "" && designMode)
                {
                    designHidden.Value = Session["Axp_DesignJson"].ToString();
                }
                else
                {
                    designHidden.Value = "[" + JsonConvert.SerializeObject(cacheMgr.axdesignJObcmg) + "]";
                }
                if ((strObj.axdesignJObject.wizardDC == true && strObj.dcs.Count > 1) || (Session["MobileView"] != null && Session["MobileView"].ToString() == "True" && strObj.dcs.Count > 1))
                    strObj.isWizardTstruct = true;
            }

            // To fill search dropdown list
            FillSearchList(strObj);
            edTime = DateTime.Now;
            strLogTime.Append("Constructing Tstrct/FromCache-" + stTime.Subtract(edTime).TotalMilliseconds.ToString());
            // To write the jsondata to the client  
            stTime = DateTime.Now;

            LoadStructure(strObj);

            if (strObj.isWizardTstruct)
                wizardClass = "wizardTstruct";

            edTime = DateTime.Now;
            strLogTime.Append("LoadStructure-" + stTime.Subtract(edTime).TotalMilliseconds.ToString());
        }
    }

    private void GetDesignModeData()
    {
        try
        {
            Boolean designMode = Convert.ToBoolean(HttpContext.Current.Session[transId + "IsDesignMode"]);
            if (designMode)
            {
                DataSet dsDesign = new DataSet();
                DataTable dtDesignData = new DataTable();
                DBContext objdb = new DBContext();
                dsDesign = objdb.GetAxConfigurations(transId, Constants.STRUCTTYPE_TSTRUCT, designMode, Constants.CONFIGTYPE_DESIGN);
                dtDesignData = dsDesign.Tables["Table0"];
                if (dtDesignData.Rows.Count > 0)
                {
                    var newDesign = dtDesignData.AsEnumerable().Select(x => new { newdesignJson = x.Field<string>("CONTENT") }).ToList();
                    designJson = newDesign[0].newdesignJson;
                }
                Session["Axp_DesignJson"] = designJson;
            }
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            string sessID = Constants.GeneralLog;
            if (HttpContext.Current.Session != null)
                sessID = HttpContext.Current.Session.SessionID;
            logObj.CreateLog("GetDesignModeData -" + ex.Message, sessID, "GetDesignModeData", "new");
        }
    }
    private void GetStructConfig()
    {
        try
        {
            string axpStructKey = Constants.AXCONFIGTSTRUCT;
            if (HttpContext.Current.Session["AxDtConfigs"] != null)
                axpConfigStr = (DataTable)HttpContext.Current.Session["AxDtConfigs"];
            HttpContext.Current.Session["AxDtConfigs"] = null;
            if (axpConfigStr != null && axpConfigStr.Rows.Count > 0)
                SetAxpStructConfigProps();
            else
            {
                FDR fObj = (FDR)HttpContext.Current.Session["FDR"];
                axpConfigStr = fObj.DataTableFromRedis(util.GetConfigCacheKey(axpStructKey, transId, "", AxRole, "ALL"));
                SetAxpStructConfigProps();
            }
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            string sessID = Constants.GeneralLog;
            if (HttpContext.Current.Session != null)
                sessID = HttpContext.Current.Session.SessionID;
            logObj.CreateLog("GetStructConfig -" + ex.Message, sessID, "GetStructConfig", "new");
        }
    }

    private void SetAxpStructConfigProps()
    {
        try
        {
            if (axpConfigStr != null && axpConfigStr.Rows.Count > 0)
            {
                var strAutoSPlit = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "autosplit")
                   .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (strAutoSPlit.Count > 0)
                    AxpIsAutoSplit = strAutoSPlit[0].splitVal;

                var strDisSPlit = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "disablesplit")
                   .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (strDisSPlit.Count > 0)
                    AxpIviewDisableSplit = strDisSPlit[0].splitVal;

                var fldAlign = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "text")
                  .Select(x => new { fldName = x.Field<string>("SFIELD"), alignProp = x.Field<string>("PROPSVAL") }).ToList();



                var autoCompPatt = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "autocomplete search pattern")
                 .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (autoCompPatt.Count > 0)
                    Session["AxpautoCompPatt"] = autoCompPatt[0].splitVal;


                var strFileUpload = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "file upload limit")
                   .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (strFileUpload.Count > 0)
                    AxpFileUploadlmt = strFileUpload[0].splitVal;

                var strcameraopt = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "camera option")
                  .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (strcameraopt.Count > 0)
                    AxpCameraOption = strcameraopt[0].splitVal;

                var strSaveImagedb = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "saveimage")
                 .Select(x => new { splitVal = x.Field<string>("PROPSVAL") }).ToList();
                if (strSaveImagedb.Count > 0)
                    AxpSaveImageDb = strSaveImagedb[0].splitVal;
                if (AxpSaveImageDb.ToLower() == "true")
                {
                    isAxpImagePath = "false";
                    isAxpImagePathHidden.Value = isAxpImagePath;
                }
                Session["AxpSaveImageDb"] = AxpSaveImageDb;

                var ExcelExport = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "excel export")
                    .Select(x => new { excel = x.Field<string>("PROPSVAL") }).ToList();
                if (ExcelExport.Count > 0)
                    Session["AxpExcelExport"] = ExcelExport[0].excel;
                else
                    Session["AxpExcelExport"] = null;

                JavaScriptSerializer ser = new JavaScriptSerializer();
                hdnFldAlgnProp.Value = ser.Serialize(fldAlign);

                //Tstruct grid edit option - popup/inline
                var gridEditOption = axpConfigStr.AsEnumerable().Where(x => x.Field<string>("PROPS").ToLower() == "gridedit")
                 .Select(x => new { alignProp = x.Field<string>("PROPSVAL") }).ToList();

                if ((Session["MobileView"] == null || Session["MobileView"].ToString() != "True"))
                {
                    if (gridEditOption.Count > 0)
                        Session["AxInlineGridEdit"] = gridEditOption[0].alignProp == "inline" ? "true" : "false";
                    else
                        Session["AxInlineGridEdit"] = "true";
                }
            }
            if ((Session["MobileView"] != null && Session["MobileView"].ToString() == "True"))
            {
                Session["AxInlineGridEdit"] = "false";
                AxpIsAutoSplit = "false";
            }
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            string sessID = Constants.GeneralLog;
            if (HttpContext.Current.Session != null)
                sessID = HttpContext.Current.Session.SessionID;
            logObj.CreateLog("SetAxpStructConfigProps -" + ex.Message, sessID, "SetAxpStructConfigProps", "new");
        }
    }


    #endregion

    #region SetGlobalVariables
    /// <summary>
    /// Function to Set the Global variables like transid, user, role etc...
    /// </summary>
    private void SetGlobalVariables()
    {
        if (!IsPostBack)
        {
            GetWorkflowGlobalVars();

            hdnAxIsPerfCode.Value = Session["AxIsPerfCode"].ToString();

            proj = Session["project"].ToString();
            ViewState["proj"] = proj;
            user = Session["user"].ToString();
            ViewState["user"] = user;
            sid = Session["nsessionid"].ToString();
            ViewState["sid"] = sid;
            AxRole = Session["AxRole"].ToString();
            ViewState["AxRole"] = AxRole;
            language = Session["language"].ToString();
            ViewState["language"] = language;
            transId = Request.QueryString["transid"].ToString();
            if (!util.IsTransIdValid(transId))
                Response.Redirect(Constants.PARAMERR);
            Session.Add("transid", transId);
            ViewState["tid"] = transId;
            fileName = "opentstruct-" + transId;
            errorLog = logobj.CreateLog("Loading Structure.", sid, fileName, "new");

            if ((!string.IsNullOrEmpty(Request.QueryString["recordid"])))
            {
                rid = Request.QueryString["recordid"];
                rid = CheckSpecialChars(rid);
                ViewState["rid"] = rid;
            }
            else
            {
                rid = "0";
            }
            if (Request.QueryString.Count < 2 && (Session["lstRecordIds"] != null || Session["recordTransId"] != null || Session["navigationInfoTable"] != null) || Request.QueryString.ToString().Contains("axpdraftid"))
                util.ClearSession();

            //remove key from querystring
            if (Request.QueryString["AxIsPop"] != null)
            {
                System.Reflection.PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                isreadonly.SetValue(this.Request.QueryString, false, null);
                this.Request.QueryString.Remove("AxIsPop");
            }
            //The AxPop is used to identify if the tstruct is opened as pop up and hide navigate buttons
            if (Request.QueryString["AxPop"] != null)
                isTstPop = Convert.ToBoolean(Request.QueryString["AxPop"].ToString());

            if (Request.QueryString["AxHypTstRefresh"] != null)
                Session["AxHypTstRefresh"] = Request.QueryString["AxHypTstRefresh"].ToString();

            if (Session["AxLogging"] != null)
                AxLogTimeTaken = Session["AxLogging"].ToString().ToLower();

        }
        else
        {
            proj = ViewState["proj"].ToString();
            sid = ViewState["sid"].ToString();
            user = ViewState["user"].ToString();
            transId = ViewState["tid"].ToString();
            AxRole = ViewState["AxRole"].ToString();
            language = ViewState["language"].ToString();
        }
        if ((Request.QueryString["act"] != null))
        {
            actstr = " act='" + Request.QueryString["act"].ToString() + "'";
            actstrType = Request.QueryString["act"].ToString();
        }
        if (HttpContext.Current.Session["dbuser"] != null)
            schemaName = HttpContext.Current.Session["dbuser"].ToString();
    }

    private void GetWorkflowGlobalVars()
    {
        if (Application["AxOnApproveDisable"].ToString().ToLower() == "true")
            AxOnApproveDisable = "true";
        if (Application["AxOnRejectDisable"].ToString().ToLower() == "true")
            AxOnRejectDisable = "true";
        if (Application["AxOnReturnSave"].ToString().ToLower() == "true")
            AxOnReturnSave = "true";
        if (Application["AxOnRejectSave"].ToString().ToLower() == "true")
            AxOnRejectSave = "true";
    }

    private void SetSessionVariables()
    {
        if (Session["AxDisplayAutoGenVal"] != null)
            hdnShowAutoGenFldValue.Value = Session["AxDisplayAutoGenVal"].ToString();

        if (Session["MobileView"] != null && Session["MobileView"].ToString() == "True")
            axWizardType = "modern";
        else if (Session["AxWizardType"] != null)
            axWizardType = Session["AxWizardType"].ToString();

        if (Session["AxDrafts"] != null)
            tstDraftsScript = Session["AxDrafts"].ToString();

        if (!String.IsNullOrEmpty(Session["AxExportTallyTid"].ToString()))
            exportTallyTid = Session["AxExportTallyTid"].ToString().ToLower();
        if (Session["AxIsPerfCode"] != null && Session["AxIsPerfCode"].ToString().ToLower() == "true")
            isRapidLoad = true;

        traceLog = logobj.CreateLog("AsbDefineRest-savetstruct", Session.SessionID.ToString(), "tstruct-add-field", "new");
        traceLog = traceLog.Replace("\\", "\\\\");
        //If a Tstruct is opened from HypLink, then, it should not be loaded from In Mem during Rapid Load.
        if (HttpContext.Current.Session["AxFromHypLink"] != null && HttpContext.Current.Session["AxFromHypLink"].ToString().ToUpper() == "TRUE")
            loadFromMem = false;

        //If a Tstruct is opened from Actions, then, it should not be loaded from In Mem during Rapid Load.
        if (Request.QueryString["IsFrmAct"] != null && Request.QueryString["IsFrmAct"].ToString().ToUpper() == "TRUE")
            loadFromMem = false;

        if (Request.QueryString["theMode"] != null && Request.QueryString["theMode"].ToString().ToLower() == "design")
            theModeDesign = true;

        HttpContext.Current.Session[Request.QueryString["transid"] + "IsDesignMode"] = theModeDesign;

        //AutoSave and publish
        if (theModeDesign == true && Session["AutoSavePublish"] != null)
        {
            if (Session["AutoSavePublish"].ToString().ToLower() == "true")
            {
                SaveDesign.Visible = false;
                PublishDesignID.Visible = false;
                PublishSaveDesignID.Visible = true;
            }
        }
        string scriptsUrlPath = Application["ScriptsurlPath"].ToString();
        if (HttpContext.Current.Session["AxDraftSavePath"] != null)
            draftsPath = HttpContext.Current.Session["AxDraftSavePath"].ToString() + "axpert\\drafts\\";
        hdnScriptsUrlpath.Value = scriptsUrlPath;
        hdnScriptspath.Value = scriptsUrlPath;

        if (Session["AppSessionKey"] != null)
            appsessionKey = Session["AppSessionKey"].ToString();

        if (ConfigurationManager.AppSettings["FetchPickListRows"] != null && ConfigurationManager.AppSettings["FetchPickListRows"].ToString() != "")
            FetchPickListRows = int.Parse(ConfigurationManager.AppSettings["FetchPickListRows"].ToString());
        langauge = Session["language"].ToString();

        if (HttpContext.Current.Session["AxImagePath"] != null && HttpContext.Current.Session["AxImagePath"].ToString() != "")
            isAxpImagePath = "true";
        if (HttpContext.Current.Session["AxpImagePathGbl"] != null && HttpContext.Current.Session["AxpImagePathGbl"].ToString() != "")
            isAxpImagePath = "true";
        if (AxpSaveImageDb.ToLower() == "true")
            isAxpImagePath = "false";

        isAxpImagePathHidden.Value = isAxpImagePath;
    }

    #endregion

    #region WriteGlobalVariables
    /// <summary>
    /// Function to write the Global variables info to the javascript.
    /// </summary>
    private void WriteGlobalVariables()
    {
        tstVars.Append("<script type='text/javascript'>");
        tstVars.Append("function GetFormDetails() { var a = '" + proj + "';var b='" + user + "';var c='" + transId + "';var d='" + sid + "';var e = '" + AxRole + "';var f='" + Session["AxTrace"] + "';SetTstProps(a,b,c,d,e,f);}");
        tstVars.Append("</script>");
    }

    private void GetLanguage()
    {
        getLang.Append("<script type='text/javascript'>");
        getLang.Append("function Getlanguage() { var l = '" + language + "';SetLangProps(l);}");
        getLang.Append("</script>");
    }

    private void WriteTstJsArrayDef(TStructDef strObj)
    {
        tstJsArrays.Append("<script type='text/javascript'>");
        if (strObj.tstHyperLink)
        {
            tstJsArrays.Append("var HLinkPop = new Array();var HLinkName = new Array();var HLinkSource = new Array();var HLinkLoad = new Array();var HLinkParamName = new Array();var HLinkParamValue = new Array();");
        }
        if (strObj.popdcs.Count > 0)
        {
            tstJsArrays.Append("TstructHasPop = true; var PopParentDCs = new Array();var PopParentFlds = new Array();var PopSqlFill = new Array();var PopSummaryParent = new Array();");
            tstJsArrays.Append("var PopSummaryFld = new Array();var PopSummDelimiter = new Array();var PopGridDCs = new Array();var PopGridDCFirm = new Array();");
            tstJsArrays.Append("var ParentDcNo = new Array();var ParentClientRow = new Array();var PopGridDcNo = new Array();var PopCondition = new Array();");
            tstJsArrays.Append("var PopParentsStr = new Array();var PopRows = new Array();");
        }
        if (strObj.dcs.Count > 0)
        {
            tstJsArrays.Append("var DCName = new Array();var DCCaption = new Array();var DCFrameNo = new Array();var DCIsGrid = new Array();var DCIsPopGrid = new Array();var DCHasDataRows = new Array();var DCAllowEmpty = new Array();var DCAllowChange= new Array();");
            tstJsArrays.Append("var DcIsFormatGrid = new Array();var DcKeyColumns = new Array(); var DcSubTotCols = new Array();var DcKeyColValues = new Array();var DcMultiSelect = new Array();var DcAllowAdd = new Array();var DcAcceptMRFlds = new Array();");
        }

        tstJsArrays.Append("var TabDCs = new Array();var TabDCStatus = new Array();var TabDCAlignmentStatus = new Array();var PagePositions = new Array();");

        if (strObj.flds.Count > 0)
        {
            tstJsArrays.Append("var FNames = new Array();var FldsFrmLst = new Array();var ExprPosArray= new Array();var FLowerNames = new Array();var FToolTip = new Array();var FDataType = new Array();var FCustDatatype = new Array();");
            tstJsArrays.Append("var FMaxLength = new Array();var FDecimal = new Array();var FDupDecimals=new Array(); var FldValidateExpr = new Array();var FCaption = new Array();var HTMLFldNames = new Array();");
            tstJsArrays.Append("var FldFrameNo = new Array();var FldDcRange = new Array();var FProps = new Array(); var ExpFldNames = new Array();");
            tstJsArrays.Append("var Expressions = new Array();var Formcontrols = new Array();var PatternNames = new Array();var Patterns = new Array();");
            tstJsArrays.Append("var FMoe = new Array();var FldDependents = new Array();var FldParents = new Array();var ClientFldParents = new Array();var FldAutoSelect = new Array();var FldIsSql = new Array();var FldAlignType = new Array();");
            tstJsArrays.Append("var FldRapidDeps = new Array();var FldRapidDepType = new Array();var FldRapidExpDeps = new Array();var FldRapidParents = new Array();var FldPurpose= new Array();var FSetCarry=new Array();");
        }
        //Add dependency arrays 
        tstJsArrays.Append("var DArray = new Array();var PArray = new Array();var CArray = new Array();var FldChkSeparator = new Array();");
        //add general arrays
        tstJsArrays.Append("var Parameters = new Array();var VisibleDCs = new Array();var FillAutoShow = new Array();var FillMultiSelect = new Array();var FillParamFld = new Array();var FillParamDCs = new Array();var FillCondition = new Array();var FillSourceDc = new Array();var FillGridName = new Array();var FillGridVExpr = new Array();var FillGridExecOnSave = new Array();");
        tstJsArrays.Append("</script>");
    }
    #endregion

    #region GetCacheObject
    private CacheManager GetCacheObject()
    {
        CacheManager cacheMgr = null;

        try
        {
            cacheMgr = new CacheManager(errorLog);
        }
        catch (Exception ex)
        {
            Response.Redirect(util.ERRPATH + ex.Message);
        }

        if (cacheMgr == null)
            Response.Redirect(util.ERRPATH + "Server error. Please try again later");

        return cacheMgr;
    }
    #endregion

    #region GetStrObject
    private TStructDef GetStrObject(CacheManager cacheMgr)
    {
        TStructDef strObj = null;
        // cachemanager and TStructDef objects throw exceptions
        try
        {
            string language = HttpContext.Current.Session["language"].ToString();
            strObj = cacheMgr.GetStructDef(proj, sid, user, transId, AxRole);
            ClearDcHasDataRows(strObj);
        }
        catch (Exception ex)
        {
            if (ex.Message == Constants.SESSIONEXPMSG)
            {
                //SessionExpired();
                Response.Redirect(util.ERRPATH + Constants.SESSIONEXPMSG);
                return null;
            }
            else
            {
                Response.Redirect(util.ERRPATH + ex.Message.Replace(Environment.NewLine, ""));
            }
        }

        if (strObj == null)
            Response.Redirect(util.ERRPATH + "Server error. Please try again later");


        return strObj;
    }

    #endregion

    #region GetStructureDetails

    /// <summary>
    /// Gets the structure of the tstruct either from the customized folder, or the Cache or Database
    /// </summary>
    /// <param name="strObj"></param>
    private void GetStructureDetails(TStructDef strObj, CacheManager cacheMgr)
    {
        Boolean objFromCache = strObj.IsObjFromCache;

        tstScript = ClearStringBuilders(tstScript);
        tstTabScript = ClearStringBuilders(tstTabScript);
        if (objFromCache)
        {
            tstTabScript.Append(strObj.GenerateTabScript(strObj));
            cacheMgr.GetStructureHTML(transId, AxRole, sid, language);
            tstHTML.Append(cacheMgr.StructureHtml);
            tstHeader.Append(cacheMgr.TstHeaderHtml);
            toolbarBtnHtml.Append(cacheMgr.ToolbarBtnIcons);
            tstScript.Append(cacheMgr.StructureScript);
            tstCaption = cacheMgr.StructureCaption;
            tstName = cacheMgr.StructureName;

            if (ShowSubCanBtns() && !theModeDesign)
            {

                // submitCancelBtns.Append(cacheMgr.StructureSubmitCancel);

                //if (submitCancelBtns != null)

                submitCancelBtns.Append("<div id = 'dvsubmitCancelBtns' style = 'position:relative;height:auto;display: block;width:100%;top:10px;padding-bottom: 15px;margin-top: -9px;' class=''><center><table style = 'width:100%;' ><input id = 'btnSaveTst' type=button class='saveTask btn btn-primary hotbtn'  onclick='javascript:FormSubmit();'  value='Submit' title='Submit'>&nbsp;&nbsp;<input id = 'New' type=button class='newTask btn btn-primary coldbtn'  onclick='javascript:NewTstruct();'  value='Reset' title='Reset'>&nbsp;&nbsp;</table></center></div>");
                dvFooter.Visible = true;
                heightframe.Attributes.Add("data-submitcancel", "true");

            }
            else
            {
                dvFooter.Visible = false;
                submitCancelBtns = ClearStringBuilders(submitCancelBtns);
                heightframe.Attributes.Add("data-submitcancel", "false");
            }
            dvFooterHtml = submitCancelBtns.ToString();

            //vFooterHtml = submitCancelBtns.ToString();
            if (string.IsNullOrEmpty(tstHTML.ToString()) || string.IsNullOrEmpty(tstHeader.ToString()) || string.IsNullOrEmpty(tstScript.ToString()))
            {
                objFromCache = false;
                attHtml = ClearStringBuilders(attHtml);
                taskBtnHtml = ClearStringBuilders(taskBtnHtml);
                dcHtml = ClearStringBuilders(dcHtml);
                tstHeader = ClearStringBuilders(tstHeader);
                submitCancelBtns = ClearStringBuilders(submitCancelBtns);
                tstScript = ClearStringBuilders(tstScript);
                tstCaption = string.Empty;
                tstName = string.Empty;
            }
            else
            {
                if (isTstructCached && isTstInCache)
                {
                    //tstScript.Append(cacheMgr.TabScript);
                    tstHTML = ClearStringBuilders(tstHTML);
                    WriteHtml(strObj);
                }
                else
                    wBdr.InnerHtml = "<script type='text/javascript'>$j('div#wBdr').hide();</script>" + Constants.WIZARD_TEMPLATE + tstHTML.ToString();
            }
        }

        if (!objFromCache)
        {
            // GetBreadCrumb(strObj.transId);
            tstCaption = strObj.tstCaption;
            //tstCaption = Session["menubreadcrumb"].ToString() + strObj.tstCaption;
            ParseStructure(strObj);
            tstName = strObj.transId;
            cacheMgr.StructureHtml = tstHTML.ToString();
            Session["StructureHtml"] = tstHTML.ToString();
            cacheMgr.axdesignJObcmg = strObj.axdesignJObject;
            cacheMgr.StructureScript = tstScript.ToString();
            cacheMgr.StructureCaption = tstCaption;
            cacheMgr.StructureName = tstName;
            cacheMgr.StructureSubmitCancel = submitCancelBtns.ToString();
            cacheMgr.TstHeaderHtml = tstHeader.ToString();
            cacheMgr.ToolbarBtnIcons = toolbarBtnHtml.ToString();
            Session["ToolbarBtnIcons"] = toolbarBtnHtml.ToString();
            string language = HttpContext.Current.Session["language"].ToString();
            string fdKey = Constants.REDISTSTRUCT;
            if (HttpContext.Current.Session["MobileView"] != null && HttpContext.Current.Session["MobileView"].ToString() == "True")
                fdKey = Constants.REDISTSTRUCTMOB;
            string pgKey = Constants.AXPAGETITLE;
            ArrayList redisvalues = new ArrayList();
            Boolean designMode = Convert.ToBoolean(HttpContext.Current.Session[transId + "IsDesignMode"]);
            if (!designMode)
            {
                cacheMgr.fdwObj.SaveInRedisServer(util.GetRedisServerkey(fdKey, transId), strObj, Constants.REDISTSTRUCT, schemaName);

                FDR fObj = (FDR)HttpContext.Current.Session["FDR"];

                var redisvalues1 = fObj.ObjectJsonFromRedis(util.GetRedisServerkey(pgKey, ""));
                if (redisvalues1 == null)
                    redisvalues.Add(Title + "♠" + tstCaption + "♠" + transId);
                else
                {
                    redisvalues = (ArrayList)redisvalues1;
                    string newValue = Title + "♠" + tstCaption + "♠" + transId;
                    if (!redisvalues.Contains(newValue))
                        redisvalues.Add(newValue);

                }
                cacheMgr.fdwObj.SaveInRedisServer(util.GetRedisServerkey(pgKey, transId), redisvalues, Constants.AXPAGETITLE, schemaName);

            }
            cacheMgr.SetStructureHTML(transId, AxRole, language);
        }

        if (HttpContext.Current.Session["globalvarstring"] != null)
        {
            strGlobalVar = HttpContext.Current.Session["globalvarstring"].ToString();
            string global_Scripts = "<script language='javascript' type='text/javascript' >" + strGlobalVar + "</script>";
            tstScript.Append(global_Scripts);
        }
    }

    private StringBuilder ClearStringBuilders(StringBuilder strName)
    {
        strName.Remove(0, strName.ToString().Length);
        return strName;
    }

    #endregion

    #region Structure Related Methods

    #region ParseStructure
    /// <summary>
    /// Function to create Html for the Structure.
    /// </summary>
    /// <param name="strObj"></param>
    private void ParseStructure(TStructDef strObj)
    {
        if (!strObj.hideToolBar)
            CreateToolbarButtons(strObj);
        CreateHeaderHtml(strObj);
        CreateDcHtml(strObj);
        strObj.CreateTabArrays();
        tstScript.Append(strObj.GetJScriptArrays(strObj));
        WriteHtml(strObj);
        tstScript.Append(tstTabScript.ToString());
    }

    #endregion

    #region Toolbar Creation Methods

    /// <summary>
    /// Function to create the toolbar buttons from the button array in the structdef object.
    /// <RELEASENOTE> For action button, the image of the button needs tobe available at AxpImages folder. 
    /// Note on png files are supported.</RELEASENOTE>
    /// </summary>
    /// <param name="strObj">StructDef object returned for the structure</param>
    //TODO: Steps needed to add new task
    private void CreateToolbarButtons(TStructDef strObj)
    {
        //The loop is reversed to sort the toolbar button left values which come in the descending order.   
        toolbarBtnHtml.Length = 0;


        for (int i = strObj.btns.Count - 1; i >= 0; i--)
        {
            TStructDef.ButtonStruct btn = (TStructDef.ButtonStruct)strObj.btns[i];
            string[] arrLeft = null;
            string tlhw = string.Empty;

            tlhw = btn.dimension;
            if (!string.IsNullOrEmpty(tlhw))
            {
                arrLeft = tlhw.Split(',');
                if ((arrLeft.Length > 0 & arrLeft[1] != string.Empty))
                {
                    if (leftBtns.IndexOf(arrLeft[1].ToString()) != -1)
                        arrLeft[1] = Convert.ToString(Convert.ToInt32(arrLeft[1], 10) + 1);
                    leftBtns.Add(arrLeft[1]);
                    //if (!(btn.task.ToLower() == "" && btn.action.ToLower() != ""))
                    tmpLeftBtns.Add(arrLeft[1]);
                }
                else
                {
                    logobj.CreateLog("    Button left value is missing:  " + btn.caption, sid, fileName, "");
                }
            }
            string hint = btn.hint;
            string caption = btn.caption;
            string task = btn.task.ToLower();
            string action = btn.action.ToLower();
            string btnName = "";
            if (!string.IsNullOrEmpty(caption)) btnName = caption;
            else if (!string.IsNullOrEmpty(hint)) btnName = hint;
            btnFunction = string.Empty;
            btnHTML = string.Empty;
            btnStyle = string.Empty;
            btnStyle = "";
            switch (task)
            {
                case "new":
                    if (string.IsNullOrEmpty(btnName)) btnName = "New";
                    if (hint == "New")
                    {
                        btnFunction = " onclick='javascript:NewTstruct();' ";
                        toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='new' " + btnFunction.ToString() + " title='" + hint + "' class='handCur'>" + btnName + "</a></li>");
                        btnStyle = "newTask";
                    }
                    else
                    {
                        btnFunction = " onclick='javascript:NewTstruct();' ";
                        toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='new' " + btnFunction.ToString() + "  title='" + hint + "' class='handCur'>" + btnName + "</a></li>");
                        //toolBarBtns.Add("");
                    }

                    if (cancelBtn == String.Empty) cancelBtn += "<input id='New' type=button class='" + btnStyle + " btn btn-primary coldbtn' " + btnFunction.ToString() + " value='Reset' title='Reset'>&nbsp;&nbsp;";
                    break;

                case "save":
                    if (string.IsNullOrEmpty(btnName)) btnName = "Save";
                    btnFunction = " onclick='javascript:FormSubmit();' ";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='imgSaveTst' " + btnFunction.ToString() + " title='" + hint + "' class='handCur'>" + btnName + "</a></li>");

                    if (submitBtn == String.Empty) submitBtn += "<input id='btnSaveTst' type=button class='saveTask btn btn-primary hotbtn' " + btnFunction.ToString() + " value='Submit' title='Submit'>&nbsp;&nbsp;";

                    break;

                case "search":
                    if (string.IsNullOrEmpty(btnName)) btnName = "Search";
                    btnFunction = " onclick='javascript:OpenSearch(" + (char)34 + transId + (char)34 + ");' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='search' " + btnFunction.ToString() + " title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;

                case "remove":
                    if (string.IsNullOrEmpty(btnName)) btnName = "Remove";
                    btnFunction = " onclick='javascript:DeleteTstruct();' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='remove' " + btnFunction.ToString() + " alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;
                case "print":
                    if (string.IsNullOrEmpty(btnName)) btnName = "Print";
                    btnFunction = " onclick='javascript:OpenPrint(" + (char)34 + transId + (char)34 + ");' ";
                    btnStyle = "handCur";
                    if (strObj.tstPform == "yes")
                    {
                        toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='print' " + btnFunction.ToString() + "  alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    }
                    else
                    {
                        toolBarBtns.Add("");
                    }
                    break;

                case "pdf":
                    if (string.IsNullOrEmpty(btnName)) btnName = "PDF";
                    string hideClass = string.Empty;
                    if (strObj.pdfList == "")
                    {
                        hideClass = " class=\"hide\" ";
                    }
                    btnFunction = " onclick='javascript:OpenPdfDocList();'";
                    //btnFunction = " onclick='javascript:ProcessRow();' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li" + hideClass + "><a href=\"javascript:void(0)\" id='pdf' " + btnFunction.ToString() + " alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;

                case "view history":
                    if (string.IsNullOrEmpty(btnName)) btnName = "View History";
                    btnFunction = " onclick='javascript:OpenHistory(" + (char)34 + transId + (char)34 + ");' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='viewhistory' " + btnFunction.ToString() + "  alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;

                case "listview":
                    if (string.IsNullOrEmpty(btnName)) btnName = "List View";
                    btnFunction = " onclick='javascript:CallListView(" + (char)34 + transId + (char)34 + ");' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" src=\"../AxpImages/toolicons/view2.png\" " + btnFunction.ToString() + " title='List View' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;

                case "attach":
                    if (string.IsNullOrEmpty(btnName)) btnName = "Attach";
                    btnFunction = " onclick='javascript:AttachFiles();' ";
                    btnStyle = "handCur";
                    toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='attach' " + btnFunction.ToString() + " title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    break;

                case "tasks":

                    btnFunction = " onclick='javascript:FindPos();ShowTaskList();' ";
                    btnStyle = "handCur";
                    if (strObj.taskBtns.Count > 0)
                    {
                        StringBuilder TaskBtnHTML = new StringBuilder();
                        TaskBtnHTML.Append("<li class='dropdown'><a href='javascript:void(0)' id='tasks' class='dropdown-toggle' data-toggle='dropdown' data-hover='dropdown' title='Tasks' data-close-others='true'>" + lblTaskBtn.Text + "&nbsp;<span class='caret'></span></a><ul class='dropdown-menu'>");
                        //TaskBtnHTML.Append("<li class='dropdown'><button id='btnTasks' class='dropdown-toggle' type='button' data-toggle='dropdown' data-hover='dropdown' title='Tasks' data-close-others='true' aria-expanded='true' id='btnTasks'>Tasks&nbsp;<span class='caret'></span></button><ul class='dropdown-menu'>");
                        TaskBtnHTML.Append(CreateTaskButtons(strObj));
                        TaskBtnHTML.Append("</ul></li>");
                        toolBarBtns.Add(TaskBtnHTML.ToString());
                        //toolBarBtns.Add("<a href=\"javascript:void(0)\" id='imgTsk' " + btnFunction.ToString() + " border=0 alt='Tasks' title='Tasks' class='" + btnStyle + "'>" + hint + "</a>");
                    }
                    else
                    {
                        toolBarBtns.Add("");
                    }
                    break;

                case "preview":

                    btnFunction = " onclick='javascript:OpenPrint(" + (char)34 + transId + (char)34 + ");' ";
                    if (string.IsNullOrEmpty(btnName)) btnName = "Preview";
                    btnStyle = "handCur";
                    if (strObj.tstPform == "yes")
                    {
                        toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='preview' " + btnFunction.ToString() + " alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'>" + btnName + "</a></li>");
                    }
                    else
                    {
                        toolBarBtns.Add("");
                    }
                    break;

                case "":
                    if (btn.action != "")
                    {
                        if (!string.IsNullOrEmpty(btn.fileupload))
                        {

                            btnStyle = "handCur";
                            string actConfirmMsg = string.Empty;
                            string actRem = string.Empty;
                            string manRem = string.Empty;
                            for (int m = 0; m <= strObj.actions.Count - 1; m++)
                            {
                                TStructDef.ActionStruct actn = (TStructDef.ActionStruct)strObj.actions[m];
                                if (actn.actname == btn.action)
                                {
                                    actConfirmMsg = actn.actdesc;
                                    actRem = actn.actRem;
                                    manRem = actn.manRem;
                                }
                            }
                            if (!util.IsImageAvailable(btn.image))
                                btn.image = "";

                            if (btn.fileupload == "y")
                            {

                                btnFunction = " onclick='javascript:CallFileUploadAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fmessage + (char)34 + "," + (char)34 + btn.ftype + (char)34 + "," + (char)34 + actConfirmMsg + (char)34 + ");' ";

                                if (!string.IsNullOrEmpty(caption) && !string.IsNullOrEmpty(btn.image))
                                {
                                    //Display Image with caption as hint.
                                    toolBarBtns.Add("<input type=hidden id=cb_sactbu name=cb_sactbu/><li><a href=\"javascript:void(0)\" id='" + hint + "'  alt='" + caption + "' " + btnFunction.ToString() + " class='" + btnStyle + "'>" + caption + "</a></li>");
                                }
                                else if (!string.IsNullOrEmpty(btn.image) && string.IsNullOrEmpty(caption))
                                {
                                    //Display only image
                                    toolBarBtns.Add("<input href=\"javascript:void(0)\" type=hidden id=cb_sactbu name=cb_sactbu/><li><a id='" + hint + "' alt='" + hint + "' " + btnFunction.ToString() + " class='" + btnStyle + "'>" + hint + "</a></li>");
                                }
                                else
                                {
                                    toolBarBtns.Add("<input type=hidden id=cb_sactbu name=cb_sactbu/><li><a href=\"javascript:void(0)\" class='fileupload' " + btnFunction.ToString() + "><span class=\"glyphicon glyphicon-picture icon-basic-elaboration-folder-picture\" style=\"padding-right: 0px;\"></span></a></li>");
                                }

                            }
                            else if (btn.fileupload == "a")
                            {

                                //btnFunction = " onclick='javascript:AttachFiles();' ";
                                //toolBarBtns.Add("<li><input type=hidden id=cb_sactbu name=cb_sactbu/><input id=\"" + btn.caption + "\" type=button " + btnFunction.ToString() + " class=\"actionBtn handCur\" value=\"" + btn.caption + "\"/></li>");
                                btnFunction = " onclick='javascript:AttachFiles();' ";
                                toolBarBtns.Add("<input type=hidden id=cb_sactbu name=cb_sactbu/><li><a href=\"javascript:void(0)\" class='attach' id='attach' " + btnFunction.ToString() + " alt='" + hint + "' title='" + hint + "' class='attach " + btnStyle + "'>" + btnName + "</a></li>");

                            }
                            else
                            {

                                btnFunction = " onclick='javascript:CallAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fileupload + (char)34 + "," + (char)34 + actConfirmMsg + (char)34 + "," + (char)34 + actRem + (char)34 + "," + (char)34 + manRem + (char)34 + ");' ";
                                if ((btn.fileupload.IndexOf("\\") != -1))
                                {
                                    btn.fileupload = btn.fileupload.Replace("\\", "\\\\");
                                }
                                actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id=\"" + btn.caption + "\" " + btnFunction.ToString() + " class=\"action actionBtn handCur\" alt=\"" + btn.caption + "\" title=\"" + btn.caption + "\">" + btn.caption + "</a></li>");
                            }
                            btn.fileupload = "";
                        }
                        //Note: CancelTstruct, Task function is no more supported.
                        //else if (!string.IsNullOrEmpty(btn.cancelBtn))
                        //{
                        //    if (btn.cancelBtn == "y")
                        //    {
                        //        btnFunction = " onclick='javascript:CancelTstruct();' ";
                        //        toolBarBtns.Add("<li><a href=\"javascript:void(0)\" class='cancel'><img  id='Cancel' " + btnFunction.ToString() + " src='../AxpImages/spacer.gif' border=0 alt='" + hint + "' title='" + hint + "' class='" + btnStyle + "'/></a></li>");
                        //    }
                        //    btn.cancelBtn = "";
                        //}
                        else
                        {
                            CreateActionButtons(btn, strObj);
                            if (leftBtns.Count > toolBarBtns.Count)
                            {
                                toolBarBtns.Add("");
                            }
                            btn.fileupload = "";
                            btn.cancelBtn = "";
                        }
                    }
                    else
                    {
                        if (!util.IsImageAvailable(btn.image))
                            btn.image = "";
                        //TODO: provide the button like any other case, on click it should alert "No task defined"                     
                        btnStyle = "handCur";
                        if (!string.IsNullOrEmpty(caption) && !string.IsNullOrEmpty(btn.image))
                        {
                            //Display Image with caption as hint.
                            toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + hint + "'  alt='" + caption + "' class='" + btnStyle + "'>" + caption + "</a></li>");
                        }
                        else if (!string.IsNullOrEmpty(btn.image) && string.IsNullOrEmpty(caption))
                        {
                            //Display only image
                            toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + hint + "'  alt='" + hint + "' class='" + btnStyle + "'>" + hint + "</a></li>");
                        }
                        else
                        {
                            //toolBarBtns.Add("<li><a href=\"javascript:void(0)\" style=\"width:auto\"><input id=\"" + btn.caption + "\" type=button class=\"actionBtn handCur\" value=\"" + btn.caption + "\">" + btn.caption + "</a></li>");
                            toolBarBtns.Add("<li><a href=\"javascript:void(0)\" id=\"" + hint + "\" class=\"actionBtn handCur\">" + btn.caption + "</a></li>");
                        }
                    }
                    break;

                default:
                    if ((task == "close") || (task == "gofirst") || (task == "gonext") || (task == "goprior") || (task == "golast"))
                    {
                        toolBarBtns.Add("");
                    }
                    else
                    {
                        CreateActionButtons(btn, strObj);
                        if (leftBtns.Count > toolBarBtns.Count)
                        {
                            toolBarBtns.Add("");
                        }
                    }

                    break;
            }
        }

        try
        {
            if (strObj.customBtns.Count > 0)
            {
                customBtnHtml = customObj.AxGetCustomTstBtns(strObj);
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "closeParentFrame();", true);
            throw ex;
        }

        leftBtns.Sort(new Util.CustomComparer());

        AlignToolbarBtns(strObj);

        PrintHTMLtoPDF(strObj);

        ConstructDraftButton();

    }

    private void ConstructDraftButton()
    {
        toolbarBtnHtml.Append("<li><a href=\"javascript:void(0)\" id=\"saveasdraft\" onclick=\"SaveAsDraft()\" class=\"action singleaction \" title=\"Save as Draft\">Save As Draft</a></li>");
        toolbarBtnHtml.Append("<li><div id=\"dvSavedDrafts\"  class=\"dropdown action singleaction\"><a id=\"btnSavedDrafts\" class=\"btn btn-default dropdown-toggle\" onclick=\"getDraftsList(this);\" data-toggle=\"dropdown\">Saved Drafts<span class=\"caret\"></span></a></div></li>");
    }

    /// <summary>
    /// tstruct html to pdf buttons append to defaultbut variable.
    /// </summary>
    /// <param name="strObj"></param>
    private void PrintHTMLtoPDF(TStructDef strObj)
    {
        try
        {
            if (!string.IsNullOrEmpty(strObj.htmlToPDF))
            {
                String[] htmlToPDFBtns = strObj.htmlToPDF.Split('~');
                foreach (var htmlbtnlen in htmlToPDFBtns)
                {
                    string htmlToPDFBtnName = string.Empty;
                    List<string> htmlToPDFForms = new List<string>();
                    if (htmlbtnlen.Contains("-"))
                    {
                        htmlToPDFBtnName = htmlbtnlen.Split('-')[0];
                        String[] htmlToPDFParams = new String[] { };
                        htmlToPDFParams = htmlbtnlen.Split('-')[1].Split('^');
                        foreach (var formlen in htmlToPDFParams)
                        {
                            if (formlen.Contains("{"))
                                htmlToPDFForms.Add(formlen.Split('{')[0]);
                            else
                                htmlToPDFForms.Add(formlen);
                        }
                    }
                    if (htmlToPDFForms.Count > 1)
                    {
                        StringBuilder btnHtml = new StringBuilder();
                        toolbarBtnHtml.Append("<ul><li class=\"gropuedBtn\"><div class=\"dropdown\"><button class=\"actionsBtn dropdown-toggle printhtmltopdfbtn\" type=\"button\" data-toggle=\"dropdown\">" + htmlToPDFBtnName + "<span class=\"icon-arrows-down\"></span></button> <ul style=\"margin-left: -12px;padding: 1px 0px; left: 6px;top: 25px;\" id=\"uldropelements\" class=\"dropdown-menu\">");
                        foreach (var htpForms in htmlToPDFForms)
                        {
                            string formName = htpForms.ToString();
                            if (formName.Contains("."))
                                formName = formName.Split('.')[0];
                            btnHtml.Append("<li class=\"printhtmltopdf\" id=" + htpForms.ToString() + ">" + formName + "</li>");
                        }
                        toolbarBtnHtml.Append(btnHtml.ToString());
                        toolbarBtnHtml.Append("</ul></div></li>");
                    }
                    else if (htmlToPDFForms.Count == 1)
                    {
                        toolbarBtnHtml.Append("<li><a href=\"javascript:void(0)\" style=\"width:auto\" class=\"printhtmltopdf\" id=" + htmlToPDFForms[0].ToString() + ">" + htmlToPDFBtnName + "</a></li></ul>");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "closeParentFrame();", true);
            //throw ex;
        }
    }

    private void CheckDesignAccess()
    {
        try
        {
            Session["axDesign"] = "false";
            if (HttpContext.Current.Session["AxResponsibilities"] != null && HttpContext.Current.Session["AxDesignerAccess"] != null)
            {
                if (user.ToLower() == "admin")
                {
                    Session["axDesign"] = "true";
                    designModeBtnHtml = Constants.DESIGN_MODE_BTN_HTML;
                }
                else
                {
                    string[] arrAxResp = HttpContext.Current.Session["AxResponsibilities"].ToString().ToLower().Split(',');
                    string[] arrAxDesignerResp = HttpContext.Current.Session["AxDesignerAccess"].ToString().ToLower().Split(',');
                    foreach (string designerResp in arrAxDesignerResp)
                    {
                        if (arrAxResp.Contains(designerResp))
                        {
                            Session["axDesign"] = "true";
                            designModeBtnHtml = Constants.DESIGN_MODE_BTN_HTML;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logobj.CreateLog("CheckDesignAccess -" + ex.Message, HttpContext.Current.Session.SessionID, "CheckDesignAccess", "new");
        }
    }

    /// <summary>
    /// Function to align the buttons and append to defaultbut variable.
    /// </summary>
    /// <remarks></remarks>
    private void AlignToolbarBtns(TStructDef strObj)
    {
        int tempLftCnt = 0;
        int BtnLftCnt = 0;

        for (BtnLftCnt = 0; BtnLftCnt < leftBtns.Count; BtnLftCnt++)
        {
            for (tempLftCnt = 0; tempLftCnt < tmpLeftBtns.Count; tempLftCnt++)
            {
                if (leftBtns[BtnLftCnt].ToString() == tmpLeftBtns[tempLftCnt].ToString())
                {
                    if (toolBarBtns.Count > tempLftCnt)
                        sortedBtns.Add(toolBarBtns[tempLftCnt]);
                    //else if (strObj.grpActBtns != "" && actionBarBtns.Count > tempLftCnt)
                    //{
                    //    sortedBtns.Add(actionBarBtns[tempLftCnt]);
                    //}
                    break;
                }
            }
        }

        for (int j = 0; j < customBtnHtml.Count; j++)
        {
            toolbarBtnHtml.Append(customBtnHtml[j]);

        }

        if (HttpContext.Current.Session["language"].ToString() == "ARABIC")
        {
            for (int j = 0; j <= sortedBtns.Count - 1; j++)
            {
                if ((!string.IsNullOrEmpty(sortedBtns[j].ToString())))
                {
                    toolbarBtnHtml.Append(sortedBtns[j]);
                }
            }
        }

        else
        {
            for (int j = 0; j <= sortedBtns.Count - 1; j++)
            {
                if ((!string.IsNullOrEmpty(sortedBtns[j].ToString())))
                {
                    toolbarBtnHtml.Append(sortedBtns[j]);
                }
            }
        }

        if (strObj.HlpText != "")
        {
            btnFunction = "onclick='javascript:ShowTstHelp(" + (char)34 + transId + (char)34 + ");' ";
            btnStyle = "handCur";
            //toolbarBtnHtml.Append("<li><a href=\"javascript:void(0)\" id='showHelp' " + btnFunction.ToString() + " border=0  alt='Hint' class='" + btnStyle + "'>Help(Info)</a></li>");
        }


        string groupButtons = strObj.grpActBtns;
        if (actionBarBtns.Count > 0)
        {

            if (groupButtons != "")
            {
                ArrayList tempChecker = actionBarBtns;

                String[] btnGroups = groupButtons.Split('~');

                for (int i = 0; i < btnGroups.Length; i++)
                {
                    StringBuilder actionbarBtnHtml = new StringBuilder();

                    toolbarBtnHtml.Append("<li class=\"gropuedBtn\"><div class=\"dropdown\"><button class=\"btn actionsBtn dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">" + btnGroups[i].Split('-')[0].ToString() + "<span class=\"icon-arrows-down\"></span></button> <ul style=\"left: 7px;top: 19px;\" id=\"uldropelements\" class=\"dropdown-menu\">");

                    string[] seperateButton = (btnGroups[i].Split('-')[1]).ToString().Split(',');
                    for (int j = 0; j < seperateButton.Length; j++)
                    {

                        for (int k = 0; k < actionBarBtns.Count; k++)
                        {
                            // 
                            //if (actionBarBtns[k].ToString().Contains(">" + seperateButton[j] + "<"))
                            if (actionBarBtns[k].ToString().Contains("onclick='javascript:CallAction(\"" + seperateButton[j] + "\","))
                            {
                                actionbarBtnHtml.Append(actionBarBtns[k].ToString());
                                tempChecker.RemoveAt(k);
                            }
                        }
                    }

                    //actionbarBtnHtml.ToString()
                    toolbarBtnHtml.Append(actionbarBtnHtml.ToString());
                    toolbarBtnHtml.Append("</ul></div></li>");
                }

                foreach (string item in tempChecker)
                {
                    if (item != "")
                    {
                        toolbarBtnHtml.Append(item.Replace("class=\"action \"", "class=\"action singleaction \""));
                    }
                }
            }
            else
            {
                StringBuilder actionbarBtnHtml = new StringBuilder();
                for (int i = 0; i < actionBarBtns.Count; i++)
                {

                    toolbarBtnHtml.Append(actionBarBtns[i].ToString().Replace("class=\"action \"", "class=\"action singleaction \""));
                }

            }
        }
        if (Session["AxExportTallyTid"] != null)
        {
            if (transId.ToLower() == Session["AxExportTallyTid"].ToString())
                toolbarBtnHtml.Append("<li><a id=\"covcxtally\" onclick=\"javascript:AxCustomExportToXml();\"  class=\"action singleaction handCur\" alt=\"Export Tally XML\" title=\"Export Tally XML\">Export Tally XML</a></li>");
        }
    }

    /// <summary>
    /// Function to create buttons in the task list i.e. buttons under the task toolbar button.
    /// </summary>
    private string CreateTaskButtons(TStructDef strObj)
    {
        if (strObj.taskBtns.Count > 0)
        {
            string task = string.Empty;
            string action = string.Empty;
            //taskBtnHtml.Append("<div id='taskListPopUp' style='display:none;min-width:100px;z-index:30000;' onclick=\"javascript:HideTaskList('true')\">");
            //taskBtnHtml.Append("<div><table style='width:100%'>");
            for (int i = 0; i < strObj.taskBtns.Count; i++)
            {
                TStructDef.ButtonStruct btn = (TStructDef.ButtonStruct)strObj.taskBtns[i];
                task = btn.task;
                task = task.ToLower();
                action = btn.action.ToLower();
                string hint = btn.hint;
                string caption = btn.caption;
                btnFunction = string.Empty;
                btnHTML = string.Empty;
                btnStyle = "handCur";

                switch (task)
                {
                    case "attach":

                        btnFunction = " onclick='javascript:AttachFiles();' ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" id='attach' title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "email":

                        btnFunction = " onclick=\"javascript:openEMail('" + transId + "','tstruct',0);\" ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" id='email' title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "print":

                        btnFunction = " onclick=\"javascript:OpenPrint('" + transId + "');\" ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" id='print' title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "save as":

                        btnFunction = " onclick='javascript:CallSaveAs();' ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "preview":
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a id='preview' class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "view history":

                        btnFunction = " onclick=\"javascript:OpenHistory('" + transId + "');\" ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "pdf":

                        btnFunction = " onclick='javascript:OpenPdfDocList();'";
                        //btnFunction = " onclick='javascript:ProcessRow(1);' ";
                        string hideClass = string.Empty;
                        if (strObj.pdfList == "")
                        {
                            hideClass = " hide ";
                        }
                        taskBtnHtml.Append("<li  class='liTaskItems " + hideClass + "' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\"  title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;

                    case "new":
                        btnFunction = " onclick='javascript:NewTstruct();' ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "search":
                        btnFunction = " onclick=\"javascript:OpenSearch('" + transId + "');\" ";
                        taskBtnHtml.Append("<li  class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "save":
                        btnFunction = " onclick='javascript:FormSubmit();' ";
                        taskBtnHtml.Append("<li class='liTaskItems'  " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "remove":
                        btnFunction = " onclick='javascript:DeleteTstruct();' ";
                        taskBtnHtml.Append("<li class='liTaskItems' " + btnFunction + "><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " >" + btn.caption + "</a></li>");
                        break;
                    case "":
                        if (btn.action != "")
                        {
                            string actConfirmMsg = string.Empty;
                            string actRem = string.Empty;
                            string manRem = string.Empty;
                            for (int m = 0; m <= strObj.actions.Count - 1; m++)
                            {
                                TStructDef.ActionStruct actn = (TStructDef.ActionStruct)strObj.actions[m];
                                if (actn.actname == btn.action)
                                {
                                    actConfirmMsg = actn.actdesc;
                                    actRem = actn.actRem;
                                    manRem = actn.manRem;
                                }
                            }
                            if (!string.IsNullOrEmpty(btn.fileupload))
                            {
                                if (btn.fileupload == "y")
                                {
                                    btnFunction = " onclick='javascript:CallFileUploadAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fileupload + (char)34 + ");' ";
                                    taskBtnHtml.Append("<li class='liTaskItems'><a class='TaskItems' href=\"javascript:void(0)\" " + btnFunction.ToString() + "");
                                    taskBtnHtml.Append(">" + btn.caption + "</a></li><input type=hidden id='cb_sactbu' name='cb_sactbu'>");
                                }
                                else if (btn.fileupload == "a")
                                {
                                    btnFunction = " onclick='javascript:AttachFiles();' ";
                                    taskBtnHtml.Append("<li class='liTaskItems'><a class='TaskItems' href=\"javascript:void(0)\" " + btnFunction.ToString() + "");
                                    taskBtnHtml.Append(">" + btn.caption + "</a></li>");
                                }
                                else
                                {
                                    if ((btn.fileupload.IndexOf("\\") != -1))
                                    {
                                        btn.fileupload = btn.fileupload.Replace("\\", "\\\\");
                                    }

                                    btnFunction = " onclick='javascript:CallAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fileupload + (char)34 + "," + (char)34 + actConfirmMsg + (char)34 + "," + (char)34 + actRem + (char)34 + "," + (char)34 + manRem + (char)34 + ");' ";
                                    actionBarBtns.Add("<li><a href=\"javascript:void(0)\" " + btnFunction.ToString() + " class=\"action \">" + btn.caption + "</a></li>");
                                }
                                btn.fileupload = "";
                            }
                            //Note: CancelTstruct, Task function is no more supported.
                            //else if (!string.IsNullOrEmpty(btn.cancelBtn))
                            //{
                            //    if (btn.cancelBtn == "y")
                            //    {
                            //        btnFunction = " onclick='javascript:CancelTstruct();' ";
                            //        taskBtnHtml.Append("<tr><td><a><img  id=" + hint + " src='../AxpImages/icons/16x16/cancel.png' " + btnFunction.ToString() + " border=0 alt=" + hint + " title=" + hint + " class='" + btnStyle + "'></a>&nbsp;</td></tr>");
                            //    }
                            //    btn.cancelBtn = "";
                            //}
                            else
                            {
                                btnFunction = " onclick='javascript:CallAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fileupload + (char)34 + "," + (char)34 + actConfirmMsg + (char)34 + "," + (char)34 + actRem + (char)34 + "," + (char)34 + manRem + (char)34 + ");' ";
                                taskBtnHtml.Append("<li class='liTaskItems' " + btnFunction + " ><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " ");
                                taskBtnHtml.Append(" >" + btn.caption + "</a></li>");
                            }
                        }
                        else
                        {
                            taskBtnHtml.Append("<li class='liTaskItems'><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " onclick='javascript:AlertNoAction();' ");
                            taskBtnHtml.Append(">" + btn.caption + "</a></li>");
                        }
                        break;
                    default:
                        taskBtnHtml.Append("<li class='liTaskItems'><a class='TaskItems' href=\"javascript:void(0)\" title=" + btn.caption + " onclick='javascript:AlertNoAction();' ");
                        taskBtnHtml.Append(">" + btn.caption + "</a></li>");
                        break;
                }
                //taskBtnHtml.Append("</tr>");
            }
            //taskBtnHtml.Append("</table></div>");
            //taskBtnHtml.Append("</div>");
            return taskBtnHtml.ToString();
        }
        else return string.Empty;
    }

    /// <summary>
    /// Function to append the action buttons to the toolbar buttons.
    /// </summary>
    /// <param name="btn">Button structure as defined in the StructDef object.</param>
    private void CreateActionButtons(TStructDef.ButtonStruct btn, TStructDef strObj)
    {
        string actConfirmMsg = string.Empty;
        string actRem = string.Empty;
        string manRem = string.Empty;
        for (int m = 0; m <= strObj.actions.Count - 1; m++)
        {
            TStructDef.ActionStruct actn = (TStructDef.ActionStruct)strObj.actions[m];
            if (actn.actname == btn.action)
            {
                actConfirmMsg = actn.actdesc;
                actRem = actn.actRem;
                manRem = actn.manRem;
                break;
            }
        }
        btnFunction = string.Empty;
        btnStyle = "handCur";

        btnFunction = " onclick='javascript:CallAction(" + (char)34 + btn.action + (char)34 + "," + (char)34 + btn.fileupload + (char)34 + "," + (char)34 + actConfirmMsg + (char)34 + "," + (char)34 + actRem + (char)34 + "," + (char)34 + manRem + (char)34 + ");' ";

        if (!util.IsImageAvailable(btn.image))
            btn.image = "";

        string btnId = "actbtn_" + btn.action;

        if (!string.IsNullOrEmpty(btn.caption) & !string.IsNullOrEmpty(btn.image))
        {
            //Display Image with caption as hint.
            actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + btnId + "' " + btnFunction.ToString() + " class=\"action \" title='" + btn.hint + "' >" + btn.hint + "</a></li>");
        }
        else if (!string.IsNullOrEmpty(btn.image) & string.IsNullOrEmpty(btn.caption))
        {
            //Display only image
            actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + btnId + "'  " + btnFunction.ToString() + " class=\"action \" title='" + btn.hint + "' >" + btn.hint + "</a></li>");
        }
        else if (!string.IsNullOrEmpty(btn.caption) & string.IsNullOrEmpty(btn.image))
        {
            //Display button with Caption
            actbtncount++;
            //Check the image icon of png format with the same name as caption and apply
            if (util.IsImageAvailable("toolicons\\actionbtns\\" + btn.caption.ToLower() + ".png"))
            {
                actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id='attach' " + btnFunction.ToString() + " class=\"action \" title='" + btn.caption + "' class='" + btnStyle + "'>" + btn.caption + "</a></li>");
            }
            else
                actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + btnId + "' " + btnFunction.ToString() + " class=\"action \" title=\"" + btn.caption + "\" >" + btn.caption + "</a></li>");

        }
        else if (!string.IsNullOrEmpty(btn.hint) && string.IsNullOrEmpty(btn.image))
        {
            actionBarBtns.Add("<li><a href=\"javascript:void(0)\" id='" + btnId + "' " + btnFunction.ToString() + " class=\"action \" title=\"" + btn.hint + "\" >" + btn.hint + "</a></li>");
        }
        else
        {
            //throw error
            actionBarBtns.Add("");
        }

        if (strObj.grpActBtns == null || strObj.grpActBtns == "")
        {
            toolBarBtns.Add(actionBarBtns[actionBarBtns.Count - 1].ToString().Replace("class=\"action \"", "class=\"action singleaction \""));
            actionBarBtns.RemoveAt(actionBarBtns.Count - 1);
        }
    }

    #endregion

    #region CreateTstructHtml

    /// <summary>
    /// Function to return the tstruct header html.
    /// </summary>
    /// <param name="strObj"></param>
    public void CreateHeaderHtml(TStructDef strObj)
    {
        tstHeader.Length = 0;
        string navButtons = string.Empty;
        string adirction = "left";
        if (Session["language"].ToString() == "ARABIC")
            adirction = "right";
        if (language.ToLower() == "arabic")
            tstHeader.Append("<div id='backforwrdbuttons' class='hide backbutton " + adirction + " ' style='background: transparent;margin-top: 2px;padding-top: 4px;position:fixed;right: 4px;'><span class='navLeft icon-arrows-right-double handCur' onclick='javascript:BackForwardButtonClicked(\"back\");' id='" + "goback" + "' style=\"border:0px;\" alt=\"Previous Page\" title=\"Click here to go back\" ></span></div>");
        else
            tstHeader.Append("<div id='backforwrdbuttons' class='hide backbutton " + adirction + " ' style='background: transparent;margin-top: 2px;padding-top: 4px;position:fixed; ' ><span class='navLeft icon-arrows-left-double-32 handCur' onclick='javascript:BackForwardButtonClicked(\"back\");' id='" + "goback" + "'  title=\"Click here to go back\"></span></div>");

        //searchBar.InnerHtml = string.Empty;
        /* searchBar.InnerHtml =*/
        tstHeader.Append("<div id='backtohm' class='backbutton " + adirction + "' style='display:none;float:left'><a><img id='" + "homeico" + "' src='../AxpImages/icons/24X24/home.png'  alt=\"Go to List\" title=\"Go to List\" class=\"handCur\"/></a></div>");
        tstHeader.Append(strObj.GetHeaderHtml(tstCaption, ""));
        divmainheader.InnerHtml = string.Empty;
        //divmainheader.InnerHtml = strObj.headerHtml.ToString();
        bbcrumb.InnerHtml = strObj.headerHtml.ToString();
    }

    /// <summary>
    /// Function to construct the DC Html for all the Dc's in the structdef.
    /// </summary>
    /// <param name="strObj"></param>
    private void CreateDcHtml(TStructDef strObj)
    {
        if (strObj.tstLayout == Constants.TILE)
            CreateTileDcHtml(strObj);
        else
        {
            int dcCount = strObj.pagePositions.Count;
            for (int i = 0; i < dcCount; i++)
            {
                dcHtml.Append(strObj.GetPagePositionHtml(strObj.pagePositions[i].ToString()));

                if (i == dcCount - 1)
                    dcHtml.Append("<div id='waitDiv' style='display:none;'><div id='backgroundDiv' style='background: url(../Axpimages/loadingBars.gif) center center no-repeat rgba(255, 255, 255, 0.4); background-size: 135px;'></div></div>");
            }
            tstTabScript.Append(strObj.GenerateTabScript(strObj));
        }
    }

    private void CreateTileDcHtml(TStructDef strObj)
    {
        StringBuilder strDcHtml = new StringBuilder();
        StringBuilder popGridHtml = new StringBuilder();
        int dcCnt = 0;
        int totWidth = 0;
        for (int i = 0; i < strObj.dcs.Count; i++)
        {
            TStructDef.DcStruct dc = (TStructDef.DcStruct)strObj.dcs[i];
            if (!dc.ispopgrid)
            {
                dcCnt++;
                if (dcCnt % 2 == 0)
                {
                    totWidth += dc.dcWidht;
                    strDcHtml.Append("<div id=\"dvRightWrapper" + dc.frameno + "\" style=\"width:" + dc.dcWidht + "px;\" class=\"Rightdiv\" >");
                    strDcHtml.Append(strObj.GetPagePositionHtml(strObj.pagePositions[i].ToString()) + "</div>");
                    // Close wrapper row div
                    dcHtml.Append("<div id=dcRow" + dc.frameno + " style=\"width:" + totWidth + "px;\" class=\"TileWrapper\">");
                    dcHtml.Append(strDcHtml + "</div>");
                    strDcHtml = new StringBuilder();
                    //Add clear div
                    strDcHtml.Append("<div class=\"clear\"></div>");
                    totWidth = 0;
                }
                else
                {
                    totWidth += dc.dcWidht + 10;
                    //TODO:Add a expand and collapse button for the wrapper
                    //dcHtml.Append("<div id=dcRow" + dc.frameno + " class=\"TileWrapper\">");
                    strDcHtml.Append("<div id=\"dvLeftWrapper" + dc.frameno + "\" style=\"width:" + dc.dcWidht + "px;\" class=\"Leftdiv\" >");
                    strDcHtml.Append(strObj.GetPagePositionHtml(strObj.pagePositions[i].ToString()) + "</div>");
                    if (strObj.dcs.Count == dc.frameno)
                    {
                        dcHtml.Append("<div id=dcRow" + dc.frameno + " style=\"width:" + totWidth + "px;\" class=\"TileWrapper\">");
                        dcHtml.Append(strDcHtml + "</div>");
                    }
                }
            }
            else
            {
                popGridHtml.Append(strObj.GetPagePositionHtml(strObj.pagePositions[i].ToString()));
            }
        }
        if (strObj.dcs.Count % 2 != 0)
            dcHtml.Append("</div>");

        dcHtml.Append(popGridHtml.ToString());
        tstTabScript.Append(strObj.GenerateTabScript(strObj));
    }

    #endregion

    #region WriteHtml
    /// <summary>
    /// Function to write the html on to the response page.
    /// </summary>
    /// <param name="strObj"></param>
    private void WriteHtml(TStructDef strObj)
    {
        string docHt = strObj.docHeight.ToString() + "px";
        int dcTop = strObj.docHeight;

        //Check for submit and cancel while writing from html
        if (ShowSubCanBtns() && !theModeDesign)
        {
            if (!(strObj.dcs.Count >= 1) && Session["MobileView"] != null)
            {
                submitCancelBtns.Append("<div id = 'dvsubmitCancelBtns' style = 'position:relative;height:auto;display: block;width:100%;top:10px;padding-bottom: 15px;margin-top: -9px;' class=''><center><table style = 'width:100%;' ><input id = 'btnSaveTst' type=button class='saveTask btn btn-primary hotbtn'  onclick='javascript:FormSubmit();'  value='Submit' title='Submit'>&nbsp;&nbsp;<input id = 'New' type=button class='newTask btn btn-primary coldbtn'  onclick='javascript:NewTstruct();'  value='Reset' title='Reset'>&nbsp;&nbsp;</table></center></div>");
                // submitCancelBtns.Append("<div id='dvsubmitCancelBtns' style ='position:relative;height:auto;display: block;width:100%;top:10px;padding-bottom: 15px;margin-top: -9px;' class=''><center><table style='width:100%;'><tr><td align='center' valign='middle'>" + submitBtn + cancelBtn + "</td></tr></table></center></div>");
                dvFooter.Visible = true;
                heightframe.Attributes.Add("data-submitcancel", "true");
            }
        }
        else
        {
            dvFooter.Visible = false;
            heightframe.Attributes.Add("data-submitcancel", "false");
            submitCancelBtns = ClearStringBuilders(submitCancelBtns);
        }
        dvFooterHtml = submitCancelBtns.ToString();


        // Code for attachments div '            
        attHtml.Append("<div id=\"attachment-overlay\" class=\"frmAtt container-fluid attachmentsContainer hide\" ></div>");
        attHtml.Append("<div id=\"FrameAttach\" class=\"frmAtt2 hide\" >");
        string attachfield = "<input type=hidden id=hdnattach\"+nrno+\" name=hdnattach\"+nrno+\"><span class=attachlbl id=attach\"+nrno+\" name=attach\"+nrno+\"></span>&nbsp;&nbsp;";
        string attachrowone = "<input type=hidden id=\"hdnattach001\" name=\"hdnattach001\"/><span class=\"attachlbl\" id=\"attach001\"></span>&nbsp;&nbsp;";
        attachfield = "<td style={display:inline;width:30;} id=spattachR\"+nrno+\" class=gridcolslno ><span class=tem1><a href=\"javascript:void(0)\"  title='delete' class=\"rowdelete\" onclick=\"DetachRow(\"+dnrno+\")\"></a></span></td><td id=spattach\"+nrno+\">" + attachfield + "</td>";
        attachrowone = "<td style={display:inline;width:30;} id=\"spattachR001\" class=gridcolslno ><span class=tem1><a href=\"javascript:void(0)\"  title='delete' class=\"rowdelete\" onclick=\"DetachRow('001')\"></a></span></td><td id=\"spattach001\">" + attachrowone + "</td>";
        string attachbut = "";
        attHtml.Append("<table style=\"border:0px; border-spacing:0px; padding:1px;\" >");
        attHtml.Append("<tr>");
        attHtml.Append("<td class=\"gridcolslno\" style='width:30'></td>");
        attHtml.Append("</tr><tr>" + attachrowone + "</tr></table>");
        attHtml.Append(attachbut.ToString());
        attHtml.Append("</div>");

        if (isTstInCache)
        {
            dcHtml = new StringBuilder();
            dcHtml.Append(htmlFromCache);
        }

        string gridElementsHeightScript = "<script type='text/javascript'>SetGridElementsHeight();</script>";
        tstHTML.Append(attHtml.ToString() + dcHtml.ToString() + gridElementsHeightScript);
        wBdr.InnerHtml = "<script type='text/javascript'>$j('div#wBdr').hide();</script>" + Constants.WIZARD_TEMPLATE + tstHTML.ToString();
    }
    #endregion

    private string GetParamValues()
    {
        StringBuilder paramXml = new StringBuilder();
        for (int qs = 0; qs <= paramNames.Count - 1; qs++)
        {
            paramValues[qs] = CheckSpecialChars(paramValues[qs].ToString());
            if (paramNames[qs].ToString().ToLower() != "transid" && paramNames[qs].ToString().ToLower() != "themode" && paramNames[qs].ToString().ToLower() != "hltype" && paramNames[qs].ToString().ToLower() != "torecid" && paramNames[qs].ToString().ToLower() != "layout" && paramNames[qs].ToString().ToLower() != "act")
            {
                paramXml.Append("<" + paramNames[qs].ToString() + ">" + paramValues[qs].ToString() + "</" + paramNames[qs].ToString() + ">");
            }
        }
        return paramXml.ToString();
    }

    private string GetGlobalParamsXml(string gloParams)
    {
        StringBuilder gloParamXml = new StringBuilder();
        string[] arrGloParams = gloParams.Split(',');
        for (int i = 0; i < arrGloParams.Length; i++)
        {
            string paramName = arrGloParams[i].ToString().Trim();
            if (Session["axGloVars_" + paramName] != null)
                gloParamXml.Append("<" + paramName + ">" + Session["axGloVars_" + paramName].ToString() + "</" + paramName + ">");
        }

        if (HttpContext.Current.Session["user"] != null)
            gloParamXml.Append("<username>" + HttpContext.Current.Session["user"].ToString() + "</username>");

        return gloParamXml.ToString();
    }

    #region LoadStructureData
    /// <summary>
    /// Function to check if the transaction is new or to load a selected transaction.
    /// </summary>
    /// <param name="structRes">Structure result stored in the struct def object.</param>
    private void LoadStructure(TStructDef strObj)
    {
        string structRes = strObj.structRes;
        string loadXml = string.Empty;
        string loadRes = string.Empty;
        bool isDraft = false;
        string draftID = string.Empty;
        string draftLoadStr = string.Empty;
        string AxDisplayAutoGenVal = Session["AxDisplayAutoGenVal"].ToString();
        string isTstParamLoad = string.Empty;
        UpdateParamsFrmQueryStr();
        bool wsPerfFormLoad = strObj.wsPerfFormLoadCall;
        if (Session["DraftName"] != null)
        {
            isDraft = true;
            draftID = Session["DraftName"].ToString();
            draftLoadStr = "IsDraftLoad = true;";
            Session["DraftName"] = null;
        }
        else
        {
            string queryString = string.Empty;
            queryString = GetParamValues();
            string visibleDCs = string.Empty;
            visibleDCs = strObj.GetVisibleDCs();
            logobj.CreateLog("    Recordid = " + rid, sid, fileName, "");

            if (rid != "0")
            {
                LoadRecidFromList();
                loadXml = loadXml + "<root" + actstr + " axpapp='" + proj + "' sessionid='" + sid + "' appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "' transid='" + transId + "' recordid='" + rid + "' dcname='" + visibleDCs + "' trace='" + errorLog + "'>";
                logobj.CreateLog("    Loading Tstruct.", sid, fileName, "");
                loadXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString();
                loadXml += "</root>";
                loadXml = util.ReplaceImagePath(loadXml);//AxpImagePath needs to be replaced with empty if the "save Image in DB" key exist in advance configuration. 
                stTime = DateTime.Now;
                loadRes = objWebServiceExt.CallLoadDataWS(transId, loadXml, structRes, rid, proj);
                wsPerfFormLoad = true;
                edTime = DateTime.Now;
                strLogTime.Append("LoadData-" + stTime.Subtract(edTime).TotalMilliseconds.ToString());
                formLogTime = formLogTime + float.Parse(edTime.Subtract(stTime).TotalMilliseconds.ToString());
                //logobj.CreateLog("LoadData-" + stTime.Subtract(edTime).TotalMilliseconds.ToString(), sid, "LogTimeTaken", "");               
                Page.Title = "Load Tstruct";
                Session["axp_lockonrecid"] = transId + "~" + rid;
            }
            else
            {
                string strDoForm = String.Empty;
                strDoForm = GetTstructFromInMemOrSess();

                if (Session["AxIsPerfCode"].ToString() == "true" && !string.IsNullOrEmpty(strDoForm) && queryString == "")
                {
                    AxDisplayAutoGenVal = "false";
                    loadRes = strDoForm;
                    dvRefreshFromLoad.Visible = true;
                    wsPerfFormLoad = true;
                }
                else
                {
                    #region
                    //Commented below code to avoid formload service call, Earlier checking WSFLD 3rd node having visible dc then calling formload service.
                    //bool isFillgridDf = false;
                    //if (visibleDCs != "" && strObj.wsPerfFGDcName != null)
                    //{
                    //    string[] flGridDc = strObj.wsPerfFGDcName;
                    //    string[] visibDCName = visibleDCs.Split(',').ToArray();
                    //    var FillgridDf = flGridDc.Where(fd => visibDCName.Any(vd => fd == vd)).ToList();
                    //    if (FillgridDf.Count > 0)
                    //        isFillgridDf = true;
                    //}
                    #endregion
                    if (queryString != "" && actstrType.ToLower() == "load")
                        isTstParamLoad = "true";
                    else if (queryString != "" && actstrType.ToLower() == "open")
                        isTstParamLoad = "false";
                    loadXml = loadXml + "<root" + actstr + " axpapp='" + proj + "' sessionid='" + sid + "' transid='" + transId + "' recordid='" + rid + "' dcname='" + visibleDCs + "' trace='" + errorLog + "' appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "'>";
                    logobj.CreateLog("    Opening Tstruct.", sid, fileName, "");
                    loadXml += queryString;
                    loadXml += Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString();
                    loadXml += "</root>";
                    DateTime stTime11 = DateTime.Now;
                    // Call service
                    //if (isTstParamLoad == "true" || (isTstParamLoad == "false" && strObj.wsPerfFormLoadCall) || isFillgridDf)
                    if (isTstParamLoad == "true" || (isTstParamLoad == "false" && strObj.wsPerfFormLoadCall))
                    {
                        AxDisplayAutoGenVal = "false";
                        wsPerfFormLoad = true;
                        loadRes = objWebServiceExt.CallDoFormLoadWS(transId, loadXml, structRes);
                        HandleFormLoadErr(loadRes, queryString);
                    }
                    else if (strObj.wsPerfFormLoadCall)
                    {
                        AxDisplayAutoGenVal = "false";
                        dvRefreshFromLoad.Visible = true;
                        wsPerfFormLoad = true;
                        loadRes = util.GetCachedFormLoadData(transId);
                        if (loadRes == string.Empty)
                        {
                            loadRes = objWebServiceExt.CallDoFormLoadWS(transId, loadXml, structRes);
                            HandleFormLoadErr(loadRes, queryString);
                            if ((strObj.formLoadCache == "" || strObj.formLoadCache == "None") && loadRes != "")
                                util.CacheFormLoadData(loadRes, transId);
                        }
                    }
                    DateTime edTime11 = DateTime.Now;
                    strLogTime.Append("DoFormLoad-" + stTime11.Subtract(edTime11).TotalMilliseconds.ToString());
                    formLogTime = formLogTime + float.Parse(edTime11.Subtract(stTime11).TotalMilliseconds.ToString());
                    //logobj.CreateLog("DoFormLoad-" + stTime11.Subtract(edTime11).TotalMilliseconds.ToString(), sid, "LogTimeTaken", "");                       
                }

                if ((!string.IsNullOrEmpty(queryString)))
                    Page.Title = "Load TStruct with QS";
                else
                    Page.Title = "Tstruct";
            }

            loadRes = loadRes.Trim();
            loadRes = loadRes.Replace("\\n", "");
            loadRes = loadRes.Replace("\\", ";bkslh");
        }
        string key = string.Empty;
        Session.Add(key, null);

        tstScript.Append("<script language='javascript' type='text/javascript' >var rSFlds='" + strObj.fastDataFlds + "'</script>");

        //The empty check for result has been removed since the tstruct data object will not be created.
        TStructData strDataObj = null;
        string attachJson = string.Empty;
        Session["AxDraftId"] = null;
        try
        {
            if (isDraft)
            {
                loadRes = GetLoadResultJson(draftID, ref strDataObj);
            }
            else
            {
                DateTime wstime = DateTime.Now;
                strDataObj = new TStructData(loadRes, transId, rid, strObj);
                GetTabDcHTMLOnFormLoad(strObj, strDataObj, rid);
                DateTime wetime = DateTime.Now;
                strLogTime.Append("TSTDATAOBJ-" + wstime.Subtract(wetime).TotalMilliseconds.ToString());
                //logobj.CreateLog("Tstruct data object-" + wstime.Subtract(wetime).TotalMilliseconds.ToString(), sid, "LogTimeTaken", "");
            }
            key = util.GetTstDataId(transId);
            loadRes = loadRes.Replace("'", "&quot;");
            strDataObj.transid = transId.ToString();

            //if a tstruct data is loading from another source for a new record then take the recordid as axp_recid1 column from json response
            try
            {
                var axp_recid1 = strDataObj.GetFieldValue("1", "axp_recid1");
                if (rid == "0" && axp_recid1 != string.Empty && axp_recid1 != "0")
                    rid = axp_recid1.ToString();
            }
            catch (Exception ex)
            {
                logobj.CreateLog("Exception in Tstruct data get axp_recid1 from dataset :--- " + ex.StackTrace, HttpContext.Current.Session["nsessionid"].ToString(), "Exception-" + transId, "");
            }

            strDataObj.recordid = rid.ToString();
            Session.Add(key, strDataObj);
            hdnDataObjId.Value = key;
            GetImageArrays(strDataObj);
            //On loading a record, the format grid dc html will also be constructed and set to the div, 
            //since there is no format grid construction method in the client.
            if (rid != "0" && !isTstInCache)
                GetFormatGridHtml(strObj, strDataObj);

            if (strDataObj.attachDirPath != string.Empty && rid != "0")
                attachJson = LoadAttachFromLoc(transId, rid, strDataObj);
        }
        catch (Exception ex)
        {
            if (util.sysErrorlog)
                logobj.CreateLog("Exception in Tstruct data object creation :--- " + ex.StackTrace, HttpContext.Current.Session["nsessionid"].ToString(), "Exception-" + transId, "");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "controlDimmer", "closeParentFrame();", true);
            Session["DraftHTML"] = Constants.DRAFT_REFRESH;
            Response.Redirect("err.aspx?errmsg=" + ex.Message.Replace(Environment.NewLine, ""));
        }
        if (AxDisplayAutoGenVal != null)
            hdnShowAutoGenFldValue.Value = AxDisplayAutoGenVal;

        //Removeing script tag code
        var localloadRes = loadRes;
        localloadRes = util.CheckScriptTag(localloadRes);
        StringBuilder tmpSB = new StringBuilder();
        tmpSB.Append("<script language='javascript' type='text/javascript'>");
        tmpSB.Append("var LoadResult = '");
        tmpSB.Append(localloadRes + attachJson);
        tmpSB.Append("'; </script>");
        loadResult += tmpSB.ToString();
        loadResult = loadResult.Replace("\n", "");
        StringBuilder xHtm = new StringBuilder();
        xHtm.Append("<table style='width:500px;border:0px;padding:0px;border-spacing:2px;'>");
        xHtm.Append("<tr><td colspan=2>");
        xHtm.Append("<input type=hidden id='recordid000F0' name='recordid000F0' value='" + rid + "'>");
        xHtm.Append("<input type=hidden id='hdnTraceValue' name='hdnTraceValue' value='" + errorLog.ToString() + "'>");
        xHtm.Append("<INPUT type='hidden' id='html_transid000F0' name='html_transid000F0' value='" + transId + "'>");
        xHtm.Append("<INPUT type='hidden' id='pickfld000F0' name='pickfld000F0' value=''>");
        xHtm.Append("</td></tr></table>");
        string strObjwsPerfFields = string.Empty;
        if (strObj.wsPerfFields != null)
            strObjwsPerfFields = string.Join("','", strObj.wsPerfFields.Split(','));
        string strObjwsPerfEvalExpClient = string.Empty;
        if (strObj.wsPerfEvalExpClient != null)
            strObjwsPerfEvalExpClient = string.Join("','", strObj.wsPerfEvalExpClient.Split(','));
        StringBuilder regTransIdRecId = new StringBuilder();
        regTransIdRecId.Append("<script language='javascript' type='text/javascript'>");
        regTransIdRecId.Append("transid = '" + transId + "';recordid = '" + rid + "';gl_language = '" + language + "';" + draftLoadStr + "var tstDataId='" + hdnDataObjId.Value + "';var axTheme='" + Session["themeColor"].ToString() + "';AxIsTstructCached =" + isTstructCached.ToString().ToLower() + "; displayAutoGenVal='" + AxDisplayAutoGenVal + "';var axpRefreshParent='" + axpRefreshParent + "';var axpRefSelect='" + axRefreshSelect + "';var axpRefSelectID='" + axRefreshSelectID + "';var axpSrcSelectID='" + axSrcSelectID + "';var axRefreshSelectType='" + axRefreshSelectType + "';var wsPerfFormLoadCall=" + wsPerfFormLoad.ToString().ToLower() + ";var wsPerfEnabled=" + strObj.wsPerfEnabled.ToString().ToLower() + ";var wsPerfFields=['" + strObjwsPerfFields + "'];");
        if (strObj.wsPerfFGDcName != null && strObj.wsPerfFGDcName.Count() > 0)
            regTransIdRecId.Append("var wsPerfFGDcName =['" + string.Join("','", strObj.wsPerfFGDcName) + "'];");
        if (wsPerfFormLoad == false)
            regTransIdRecId.Append("var wsPerfEvalExpClient =['" + strObjwsPerfEvalExpClient + "'];");
        if (wsPerfFormLoad == false && isTstParamLoad == "false")
            regTransIdRecId.Append("var formParamFlds =['" + string.Join("','", paramNames.ToArray()) + "'];var formParamVals =['" + string.Join("','", clientParamValues.ToArray()) + "'];");
        regTransIdRecId.Append("AxOnApproveDisable=" + AxOnApproveDisable + ";AxOnReturnSave=" + AxOnReturnSave + ";AxOnRejectSave=" + AxOnRejectSave + ";AxOnRejectDisable=" + AxOnRejectDisable + ";");
        regTransIdRecId.Append("AxLogTimeTaken='" + AxLogTimeTaken + "';var headerAttachDir = '" + strDataObj.attachDir + "';");
        regTransIdRecId.Append("var isWizardTstruct = " + strObj.isWizardTstruct.ToString().ToLower() + "; var hideToolBar = " + strObj.hideToolBar.ToString().ToLower() + ";");
        regTransIdRecId.Append("</script>");
        tstScript.Append(regTransIdRecId.ToString());
        if (isTstInCache)
            tstScript.Append(jsFromCache);

        tstScript.Append("<script language='javascript' type='text/javascript' >function setDocht(){ }</script>");
        tstScript.Append(loadResult + xHtm.ToString());
        logobj.CreateLog("Loading tstruct.aspx completed", sid, fileName, "");
        logobj.CreateLog("End Time : " + DateTime.Now.ToString(), sid, fileName, "");
    }

    private void ClearDcHasDataRows(TStructDef strObj)
    {
        for (int i = 0; i < strObj.dcs.Count; i++)
        {
            TStructDef.DcStruct dc = (TStructDef.DcStruct)strObj.dcs[i];
            if (dc.isgrid)
            {
                dc.DCHasDataRows = false;
                strObj.dcs[i] = dc;
            }
        }
    }

    private void GetTabDcHTMLOnFormLoad(TStructDef strObj, TStructData strDataObj, string recordId)
    {
        string tabDcsHtml = string.Empty;
        for (int i = 0; i < strObj.tabDCs.Count; i++)
        {
            if (strObj.tabDCStatus[i].ToString() == "0")
            {
                if (recordId == "0" && strObj.wsPerfFGDcName != null)
                {
                    string[] flGridDc = strObj.wsPerfFGDcName;
                    var loaddcFG = flGridDc.Where(d => d.ToLower() == "dc" + strObj.tabDCs[i]).ToList();
                    if (loaddcFG.Count > 0)
                        continue;
                }
                string temptabHTML = string.Empty;
                string tempDcDesignJson = string.Empty;
                try
                {
                    temptabHTML = strObj.GetTabDcHTML(Convert.ToInt32(strObj.tabDCs[i]), strDataObj, "false");
                    Dc curDc = strDataObj.tstStrObj.axdesignJObject.dcs.FirstOrDefault(elm => elm.dc_id == strObj.tabDCs[i].ToString());
                    tempDcDesignJson = new JavaScriptSerializer().Serialize(curDc);
                }
                catch (Exception ex)
                {

                }
                if (tabDcsHtml == string.Empty)
                    tabDcsHtml += strDataObj.AxDepArrays.ToString() + "*♦*" + strDataObj.GetMasterRowFlds() + "*♠**♠*" + temptabHTML + "*♠*" + tempDcDesignJson;
                else
                    tabDcsHtml += "*♠♠*" + strDataObj.AxDepArrays.ToString() + "*♦*" + strDataObj.GetMasterRowFlds() + "*♠**♠*" + temptabHTML + "*♠*" + tempDcDesignJson;
            }
        }
        hdnTabHtml.Value = tabDcsHtml;
    }

    private string GetLoadResultJson(string draftID, ref TStructData strDataObj)
    {
        FDR fObj = (FDR)Session["FDR"];
        strDataObj = fObj.TstructDataFromRedis(draftID, true);
        if (strDataObj == null)
        {
            throw new Exception("REDIS might have reset/Flushed. Please Try Again.");
        }
        if (Session["AxAutoPurge"] != null && Session["AxAutoPurge"].ToString() == "true" && draftID != string.Empty)
        {
            Session["AxDraftId"] = draftID;// To purge after saving only
        }
        ASBExt.WebServiceExt objExt = new ASBExt.WebServiceExt();
        //strDataObj.tstStrObj = strObj;
        strDataObj.objWebServiceExt = objExt;
        strDataObj.sessionid = Session["nsessionid"].ToString();
        strDataObj.IsDraftObj = true;
        string loadRes = strDataObj.CreateJsonForDraft();
        logobj.CreateLog(loadRes, sid, "LoadDraft", "new");
        Session["DraftHTML"] = Constants.DRAFT_REFRESH;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "setFormDirty", "window.parent.globalChange = true;SetFormDirty(true); ", true);
        return loadRes;
    }

    private void HandleFormLoadErr(string loadRes, string queryString)
    {
        if (loadRes.Contains("\"error\"") == true && loadRes.Contains(Constants.SESSIONEXPMSG))
        {
            Response.Redirect(util.ERRPATH + Constants.SESSIONEXPMSG);
            return;
        }
        else if (loadRes.Contains("\"error\"") == true && loadRes.Contains(Constants.ERAUTHENTICATION))
        {
            Response.Redirect(util.ERRPATH + Constants.ERAUTHENTICATION);
            return;
        }
        else if (!loadRes.Contains("\"error\"") && Session["AxIsPerfCode"].ToString() == "true" && queryString == "")
        {
            if (strObj.formLoadCache == "InMemory")
            {
                string fdKey = Constants.REDISTSTRUCTDOFORM;
                FDW fdwObj = FDW.Instance;
                fdwObj.SaveInRedisServer(util.GetRedisServerkey(fdKey, transId), loadRes, Constants.REDISTSTRUCTDOFORM, schemaName);
            }
            else if (strObj.formLoadCache == "Session")
                HttpContext.Current.Session[transId + "_" + sid] = loadRes;
        }
    }

    private string GetTstructFromInMemOrSess()
    {
        string strDoForm = string.Empty;
        if (strObj.IsObjFromCache == true)
        {
            string fdKey = Constants.REDISTSTRUCTDOFORM;
            try
            {
                if (strObj.formLoadCache == "InMemory")
                {
                    FDR fObj = (FDR)HttpContext.Current.Session["FDR"];
                    strDoForm = fObj.StringFromRedis(util.GetRedisServerkey(fdKey, transId));
                }
                else if (strObj.formLoadCache == "Session")
                {
                    if (HttpContext.Current.Session[transId + "_" + sid] != null)
                    {
                        strDoForm = HttpContext.Current.Session[transId + "_" + sid].ToString();
                    }
                }
            }
            catch (Exception Ex) { }
        }
        return strDoForm;
    }

    private void LoadRecidFromList()
    {
        if (Session["lvRecordListing-" + transId] != null)
        {
            Dictionary<int, string> lvRecordListing = new Dictionary<int, string>();
            try
            {
                lvRecordListing = (Dictionary<int, string>)Session["lvRecordListing-" + transId];
                if (lvRecordListing != null)
                {
                    var recordkeyValuePair = lvRecordListing.Single(x => x.Value == rid);
                    int recordKey = recordkeyValuePair.Key;
                    Session["lvRecordKey"] = recordKey;
                }
            }
            catch (Exception ex)
            {
                logobj.CreateLog(ex.Message, sid, "Exception in LoadStructure ListViewNavigation Details", "new");
            }
        }
    }

    private void UpdateParamsFrmQueryStr()
    {
        for (int qn = 1; qn <= Request.QueryString.Count - 1; qn++)
        {
            if (Request.QueryString.AllKeys[qn] == null)
                continue;
            if (Request.QueryString.Keys[qn] == "axp_refresh")
            {
                axpRefreshParent = Request.QueryString.Get(qn);
            }
            else if (Request.QueryString.Keys[qn] == "AxRefSelect")
            {
                axRefreshSelect = Request.QueryString.Get(qn);
            }
            else if (Request.QueryString.Keys[qn] == "AxRefSelectID")
            {
                axRefreshSelectID = Request.QueryString.Get(qn);
            }
            else if (Request.QueryString.Keys[qn] == "AxSrcSelectID")
            {
                axSrcSelectID = Request.QueryString.Get(qn);
            }
            else if (Request.QueryString.Keys[qn] == "AxRefType")
            {
                axRefreshSelectType = Request.QueryString.Get(qn);
            }
            else
            {
                if (Request.QueryString.AllKeys[qn].ToLower() == "axfromhyperlink" || Request.QueryString.AllKeys[qn].ToLower() == "axpop" || Request.QueryString.AllKeys[qn].ToLower() == "axhyptstrefresh" || Request.QueryString.AllKeys[qn].ToLower() == "recpos" || Request.QueryString.AllKeys[qn].ToLower() == "pagetype" || Request.QueryString.AllKeys[qn].ToLower() == "curpage" || Request.QueryString.AllKeys[qn].ToLower() == "openeriv")
                    continue;
                // eliminate Name from querystring            
                paramNames.Add(Request.QueryString.Keys[qn]);
                string val = string.Empty;
                val = Request.QueryString.Get(qn);
                val = val.Replace("--.--", "&");
                val = val.Replace("amp;", "&");

                //val = val.Replace("%2b", "+");
                val = util.CheckReverseUrlSpecialChars(val);
                val = util.ReverseCheckSpecialChars(val);
                paramValues.Add(val);
                if (val.Contains("'")) val = val.Replace("'", "%27");
                val = val.Replace(";bkslh", "%5C");
                val = val.Replace("\\", "%5C");
                clientParamValues.Add(val);
            }
        }
    }

    //Function to construct the image arrays for all the images in the tstruct with values from the tstruct data object.
    private void GetImageArrays(TStructData tstData)
    {
        StringBuilder strImgArr = new StringBuilder();
        strImgArr.Append("<script language='javascript' type='text/javascript'>");
        for (int i = 0; i < tstData.imageFldNames.Count; i++)
        {
            strImgArr.Append("imgNames[" + i + "]='" + tstData.imageFldNames[i].ToString() + "';");
            string src = tstData.imageFldSrc[i].ToString();
            src = src.Replace("\\", ";bkslh");
            strImgArr.Append("imgSrc[" + i + "]='" + src + "';");
        }
        strImgArr.Append("</script>");
        loadResult += strImgArr.ToString();
    }

    /// <summary>
    /// Function to construct the html for the format grid dc on Loading a record.
    /// </summary>
    /// <param name="strObj"></param>
    /// <param name="tstData"></param>
    private void GetFormatGridHtml(TStructDef strObj, TStructData tstData)
    {
        StringBuilder strDcHtml = new StringBuilder();
        for (int i = 0; i < strObj.visibleDCs.Count; i++)
        {
            int dcNo = Convert.ToInt32(strObj.visibleDCs[i].ToString());
            if (strObj.IsDcFormatGrid(dcNo))
            {
                strDcHtml.Append(strObj.GetTabDcHTML(dcNo, tstData, "false"));
            }
        }

        dvFormatDc.InnerHtml = strDcHtml.ToString();
    }


    #endregion

    #endregion

    #endregion

    #region General Functions


    /// <summary>
    /// Function to include the js files in the aspx page.
    /// </summary>
    private void IncludeJsFiles()
    {
        string projName = HttpContext.Current.Session["Project"].ToString();
        for (int i = 0; i < customObj.jsFiles.Count; i++)
        {
            string[] jsFileStr = customObj.jsFiles[i].ToString().Split('¿');
            string tid = jsFileStr[0].ToString().ToLower();
            string fileName = jsFileStr[1].ToString();
            if (transId.ToLower() == tid)
            {
                HtmlGenericControl js = new HtmlGenericControl("script");
                js.Attributes["type"] = "text/javascript";
                string path = "../" + projName + "/" + fileName;
                js.Attributes["src"] = path;
                ScriptManager1.Controls.Add(js);
            }
        }

        for (int j = 0; j < customObj.jsGlobalFiles.Count; j++)
        {
            HtmlGenericControl js = new HtmlGenericControl("script");
            js.Attributes["type"] = "text/javascript";
            string path = "../" + projName + "/" + customObj.jsGlobalFiles[j].ToString();
            js.Attributes["src"] = path;
            ScriptManager1.Controls.Add(js);

        }

        for (int i = 0; i < customObj.cssFiles.Count; i++)
        {
            string[] jsFileStr = customObj.cssFiles[i].ToString().Split('¿');
            string tid = jsFileStr[0].ToString().ToLower();
            string fileName = jsFileStr[1].ToString();
            if (transId.ToLower() == tid)
            {
                HtmlGenericControl js = new HtmlGenericControl("link");
                js.Attributes["type"] = "text/css";
                js.Attributes["rel"] = "stylesheet";
                string path = "../" + projName + "/" + fileName;
                js.Attributes["href"] = path;
                ScriptManager1.Controls.Add(js);

            }
        }

        for (int i = 0; i < customObj.cssGlobalFiles.Count; i++)
        {
            HtmlGenericControl js = new HtmlGenericControl("link");
            js.Attributes["type"] = "text/css";
            js.Attributes["rel"] = "stylesheet";
            string path = "../" + projName + "/" + customObj.cssGlobalFiles[i].ToString();
            js.Attributes["href"] = path;
            ScriptManager1.Controls.Add(js);

        }
    }

    /// <summary>
    /// function for replacing the special characters in a given string.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static string CheckSpecialChars(string str)
    {
        str = Regex.Replace(str, "&", "&amp;");
        str = Regex.Replace(str, "<", "&lt;");
        str = Regex.Replace(str, ">", "&gt;");
        str = Regex.Replace(str, "'", "&apos;");
        str = Regex.Replace(str, "\"", "&quot;");

        return str;
    }

    /// <summary>
    /// function for handling session timeout.
    /// </summary>
    /// <remarks></remarks>
    public void SessionExpired()
    {
        string url = util.SESSEXPIRYPATH;
        Response.Write("<script language='javascript'>");
        Response.Write("parent.parent.location.href='" + url + "';");
        Response.Write("</script>");
    }

    public void btnHtml_Click(object sender, EventArgs e)
    {

    }

    private Boolean ShowSubCanBtns()
    {
        Boolean ShowBtns = true;
        if (Session["AxShowSubmitCancel"] != null)
        {

            if (Session["MobileView"] != null && Session["MobileView"].ToString() == "True")
            {
                ShowBtns = false;
                return ShowBtns;
            }
            string ShowSubmitCancelBtns = HttpContext.Current.Session["AxShowSubmitCancel"].ToString();
            if (ShowSubmitCancelBtns != string.Empty)
            {
                if (ShowSubmitCancelBtns.ToLower() == "false")
                    ShowBtns = false;
            }

        }
        return ShowBtns;

    }
    private void SetLangStyles()
    {
        if (Session["language"].ToString() == "ARABIC")
        {
            direction = "rtl";
            classdir = "right";
            //searchoverlay.Attributes["class"] = "arabicoverlay hide";
            //dvsrchclose.Attributes["style"] = "text-align: left;";
            //dvsrchfor.Attributes["style"] = "margin-right: 50px;";
            //dvsrchfor.Attributes["class"] = "right";
        }
        else
        {
            classdir = "left";
            //searchoverlay.Attributes["class"] = "overlay hide";
            //dvsrchclose.Attributes["style"] = "text-align: right;";
            //dvsrchfor.Attributes["style"] = "margin-left: 50px;";
            //dvsrchfor.Attributes["class"] = "left";
        }
    }

    #endregion

    #region Search Methods

    /// <summary>
    /// Function to fill the search drop down with fields.
    /// </summary>
    /// <param name="strObj"></param>
    private void FillSearchList(TStructDef strObj)
    {

        ddlSearch.Items.Clear();
        for (int i = 0; i < strObj.searchDataCaptions.Count; i++)
        {
            if (strObj.searchDataCaptions[i].ToString() != string.Empty)
                ddlSearch.Items.Add(new ListItem(strObj.searchDataCaptions[i].ToString(), strObj.searchDataNames[i].ToString()));
        }

        for (int j = 0; j < ddlSearch.Items.Count; j++)
        {
            string strSearch = string.Empty;
            if (Request.Form["ddlSearch"] != null)
                strSearch = Request.Form["ddlSearch"].ToString();
            if (ddlSearch.Items[j].Text == strSearch)
            {
                ddlSearch.SelectedIndex = j;
            }
        }
    }

    /// <summary>
    /// function to bind the gridview while searching the transactions.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="totRows"></param>
    /// <param name="curPageNo"></param>
    private void BindDataGrid(string searchResult, int totRows, string curPageNo)
    {
        grdSearchRes.Columns.Clear();
        DataSet ds = new DataSet();
        StringReader sr = new StringReader(searchResult);

        ds.ReadXml(sr);

        // Important : the datasource store in session as datatable. for paging and sorting

        // IMP : Create a new dataset - use clone - which create new structure then change
        // Column datatype to int, double,string and date - which is needed for Sorting
        int colCnt = 0;
        DataSet ds1 = new DataSet();
        ds1 = ds.Clone();
        foreach (DataColumn dc1 in ds1.Tables[0].Columns)
        {
            dc1.DataType = typeof(string);

            colCnt = colCnt + 1;
        }

        int rowNo = 0;
        foreach (DataRow dr1 in ds.Tables[0].Rows)
        {
            // Before import ds to ds1 change the row value from str to date while datacol type is date
            // rno for find row no and id for col ... make new date then attach to dr1 -datarow then import

            ds1.Tables[0].ImportRow(dr1);
            rowNo = rowNo + 1;
        }

        Session["order"] = ds1.Tables[0];
        int resRows = ds1.Tables[0].Rows.Count;

        if (ds1.Tables.Count > 0)
        {
            foreach (DataColumn dc in ds1.Tables[0].Columns)
            {
                BoundField field = new BoundField();
                //'initialiae the data field value
                field.DataField = dc.ColumnName;
                //'initialise the header text value
                field.HeaderText = dc.ColumnName;
                //' add newly created columns to gridview
                grdSearchRes.Columns.Add(field);
            }
        }
        string fldId = Request.Form["ddlSearch"];
        try
        {
            ds1 = customObj.axAfterSearch(transId, ds1, searchVal, fldId);
        }
        catch (Exception ex)
        {
        }
        if (ds1.Tables[0].Rows.Count < 1)
        {
            totRows = 0;
            pgCap.Visible = false;
            lvPage.Visible = false;
        }
        grdSearchRes.DataSource = ds1;

        // to change the header Name and set the column width
        int idx = 0;
        for (idx = 0; idx <= headNames.Count - 1; idx++)
        {
            if (idx == 0)
                grdSearchRes.Columns[idx].HeaderText = "Select";
            // For change the Column Heading from fld name to Caption
            else
                grdSearchRes.Columns[idx].HeaderText = headNames[idx].ToString();
        }

        grdSearchRes.DataBind();
        double pg = (int)totRows / (int)grdSearchRes.PageSize;
        int pg1 = (int)Math.Floor(pg);
        if ((totRows % grdSearchRes.PageSize) > 0)
        {
            pg1 += 1;
        }

        if (totRows > 0)
        {
            records.Text = "Total no. of records: " + totRows;
            pages.Text = " of " + pg1;
            records.CssClass = "seartotrecords";
            pgCap.Visible = true;
            lvPage.Visible = true;
        }
        else
        {
            records.Text = lblNodata.Text;
            records.CssClass = "searnorecords";
            pages.Text = "";
        }

        int pgNo = 0;
        if (curPageNo == "1")
        {
            lvPage.Items.Clear();
            for (pgNo = 1; pgNo <= pg1; pgNo++)
            {
                lvPage.Items.Add(pgNo.ToString());
            }
        }


    }

    /// <summary>
    /// Function to bind the resulting search data into the grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSearchRes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        System.Data.DataRowView drv = default(System.Data.DataRowView);
        drv = (System.Data.DataRowView)e.Row.DataItem;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (drv != null)
            {
                // first change col 1 to check box
                string catName = drv[0].ToString();
                // for change the content to component like check box or input box
                //to Remove checkbox from first column
                e.Row.Cells[0].CssClass = "text-center";
                e.Row.Cells[0].Text = "<input style=\"width:10px;\" type=radio name='radioselect' value=" + catName + " onclick=loadTstruct(this.value);>";
                int n = 0;
                for (n = 0; n <= e.Row.Cells.Count - 1; n++)
                {
                    if (e.Row.Cells[n].Text == "~!@*")
                    {
                        e.Row.Cells[n].Text = "";
                    }
                }
            }
        }
        //for NOWRAP in IE
        int m = 0;
        for (m = 0; m <= e.Row.Cells.Count - 1; m++)
        {
            if (e.Row.Cells[m].Text.Length > 0)
            {
                e.Row.Cells[m].Text = "<nobr>" + e.Row.Cells[m].Text + "</nobr>";
            }
        }
    }

    /// <summary>
    /// Handles pagination for search grid.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void grdSearchRes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        DataTable dtOrders = new DataTable();
        dtOrders = (DataTable)Session["order"];
        grdSearchRes.PageIndex = e.NewPageIndex;
        grdSearchRes.DataSource = dtOrders.DefaultView;
        grdSearchRes.DataBind();
    }

    /// <summary>
    /// Function to handle Pagination page changed event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lvPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        string pgNo = lvPage.SelectedValue;
        callWebservice(pgNo);
    }

    bool IsAllDigits(string s)
    {
        return s.All(char.IsDigit);
    }
    /// <summary>
    /// Function to call service for filling the grid with search result.
    /// </summary>
    /// <param name="pgno"></param>
    public void callWebservice(string pageNo)
    {
        string pageSize = string.Empty;
        grdSearchRes.Columns.Clear();
        string qs = queryStr;
        float f;
        headNames.Clear();

        //searchVal = Request.Form["searstr"].Replace("&", "&amp;");
        searchVal = Request.Form["hdnSearchStr"];
        searchVal = util.CheckReverseUrlSpecialChars(searchVal);
        searchVal = util.CheckSpecialChars(searchVal);

        fileName = "Search-" + transId;
        errorLog = logobj.CreateLog("Loading Search List.", sid, fileName, "new");
        pageSize = grdSearchRes.PageSize.ToString();
        try
        {
            pageSize = customObj.axBeforeSearch(transId, pageSize);
        }
        catch (Exception ex)
        {
        }

        int fIdx = strObj.GetFieldIndex(Request.Form["ddlSearch"]);
        TStructDef.FieldStruct fld = (TStructDef.FieldStruct)strObj.flds[fIdx];
        //fld.datatype;

        ;
        if (fld.datatype != "Date/Time" && fld.datatype != "Numeric" && ((fld.datatype == "Character" && !(searchVal.GetType().Name == "String" || searchVal.GetType().Name == "Character")) || (fld.datatype != "Character" && searchVal.GetType().Name != fld.datatype)))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "searchInTstruct", "$('#records').text('Enter " + fld.datatype.ToLower() + " values')", true);
            return;
        }

        else if (fld.datatype == "Numeric" && (!IsAllDigits(searchVal) && !float.TryParse(searchVal, out f)))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "searchInTstruct", "$('#records').text('Enter " + fld.datatype.ToLower() + " values')", true);
            return;
        }

        string iXml = string.Empty;
        iXml = "<sqlresultset axpapp=\"" + proj + "\" transid=\"" + ViewState["tid"] + "\" sessionid=\"" + sid + "\" trace=\"" + errorLog + "\" pageno=\"" + pageNo + "\" pagesize=\"" + int.Parse(pageSize) + "\" appsessionkey='" + Session["AppSessionKey"].ToString() + "' username='" + Session["username"].ToString() + "'>";
        iXml = iXml + "<fields>" + qs + "</fields><searchfor>" + Request.Form["ddlSearch"] + "</searchfor><value>" + searchVal + "</value>";
        iXml = iXml + Session["axApps"].ToString() + Application["axProps"].ToString() + Session["axGlobalVars"].ToString() + Session["axUserVars"].ToString() + "</sqlresultset>";
        string res = string.Empty;

        //Call service
        res = objWebServiceExt.CallGetSearchValWS(transId, iXml, structXml);

        if (res.ToLower().Contains("ora-"))
        {
            string strErrMsg = "Error occurred(2). Please try again or contact administrator.";
            Response.Redirect("err.aspx?errmsg" + strErrMsg);
        }

        Session["srchdata"] = res;

        if ((res.IndexOf(Constants.ERROR) == -1))
        {
            XmlDocument xmlDoc1 = new XmlDocument();
            xmlDoc1.LoadXml(res);

            XmlNode cNode = default(XmlNode);
            cNode = xmlDoc1.SelectSingleNode("//response");

            int totalRows = 0;
            if (pageNo == "1")
            {
                XmlNode tnode = cNode.Attributes["totalrows"];
                if (tnode == null)
                {
                    totalRows = 0;
                }
                else
                {
                    totalRows = Convert.ToInt32(tnode.Value);
                    cNode.Attributes.RemoveNamedItem("totalrows");
                }
                Session["s_noofpages"] = totalRows;
            }
            else
            {
                totalRows = Convert.ToInt32(Session["s_noofpages"]);
            }

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            cNode.WriteTo(xw);

            string ires2 = null;
            ires2 = sw.ToString();

            XmlDocument xmlDoc2 = new XmlDocument();
            XmlNodeList productNodes2 = default(XmlNodeList);
            XmlNodeList baseDataNodes2 = default(XmlNodeList);
            xmlDoc2.LoadXml(ires2);

            productNodes2 = xmlDoc2.SelectNodes("//row");

            int p = 0;
            foreach (XmlNode productNode2 in productNodes2)
            {
                if (p > 0)
                {
                    break; // TODO: might not be correct. Was : Exit For
                }
                baseDataNodes2 = productNode2.ChildNodes;
                foreach (XmlNode baseDataNode2 in baseDataNodes2)
                {
                    headNames.Add(baseDataNode2.Attributes["cap"].Value);
                }
                p = p + 1;
            }

            //Remove attribute Cap
            XmlDocument xmlDoc3 = new XmlDocument();
            xmlDoc3.LoadXml(sw.ToString());

            XmlNodeList productNodes3 = default(XmlNodeList);
            XmlNodeList baseDataNodes3 = default(XmlNodeList);

            productNodes3 = xmlDoc3.SelectNodes("//row");

            foreach (XmlNode productNode3 in productNodes3)
            {
                baseDataNodes3 = productNode3.ChildNodes;
                foreach (XmlNode baseDataNode3 in baseDataNodes3)
                {
                    baseDataNode3.Attributes.RemoveNamedItem("cap");
                }
            }

            string nXml = null;
            nXml = xmlDoc3.OuterXml;

            if (nXml == "<response />")
            {
                records.Text = lblNodata.Text;
                records.CssClass = "searnorecords";
                grdSearchRes.Visible = false;
                pgCap.Visible = false;
                lvPage.Visible = false;
                pages.Text = "";
            }
            else
            {
                records.Text = string.Empty;
                grdSearchRes.Visible = true;
                BindDataGrid(nXml, totalRows, pageNo);
            }
        }
        else
        {
            if (util.sysErrorlog)
            {
                logobj.CreateLog("Error in Search Tstruct Service :--- " + res, sid, fileName, "");
            }
            res = res.Replace(Constants.ERROR, string.Empty);
            res = res.Replace("</error>", string.Empty);
            res = res.Replace("\n", string.Empty);
            Response.Redirect(util.ERRPATH + res);
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "searchInTstruct", "if(jQuery('#grdSearchRes').length)bindUpdownEvents('grdSearchRes','single');", true);
        //  Page.ClientScript.RegisterStartupScript(this.GetType(), "searchInTstruct", "alert();", true);
    }

    /// <summary>
    /// Function to fill the search result.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnGo_Click(object sender, EventArgs e)
    {
        callWebservice("1");
    }

    #endregion

    protected void colBtn1_Click(object sender, EventArgs e)
    {

        Session["layoutstyle"] = "onecolumn";
        if (Request.QueryString["transid"] != null)
            Response.Redirect("tstruct.aspx?transid=" + Request.QueryString["transid"].ToString());
    }
    protected void colBtn2_Click(object sender, EventArgs e)
    {
        Session["layoutstyle"] = "twocolumn";
        if (Request.QueryString["transid"] != null)
            Response.Redirect("tstruct.aspx?transid=" + Request.QueryString["transid"].ToString());

    }

    #region pdf Docs
    private void parseXMLDoc(string ires)
    {
        if (ires != String.Empty)
        {
            try
            {
                string _xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
                ires = _xmlString + ires;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(ires);
                XmlDocument gcxmlDoc = new XmlDocument();
                XmlNodeList gcproductNodes;
                XmlNodeList gcbaseDataNodes;
                gcxmlDoc.LoadXml(ires);
                gcproductNodes = gcxmlDoc.SelectNodes("/" + transId + "_pdflist");
                string selectStr = "<select id=\"pdfFName\" name=\"fname\" class==\"combotem Family\" style=\"width:65%;height:20px;margin-bottom:10px;\">";
                foreach (XmlNode gcproductNode in gcproductNodes[0])
                {
                    gcbaseDataNodes = gcproductNodes[0].ChildNodes;
                    int tNo = 1;
                    if (gcproductNode.Name != "transid")
                    {
                        optStr = optStr + "<option value= " + gcproductNode.Attributes["source"].Value + "$" + tNo + " class=\"" + gcproductNode.InnerText + "\">" + gcproductNode.InnerText + "</option>";
                        tNo = tNo + 1;
                    }
                }
                selectStr += optStr + "</select>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "myScript", "LoadPdfDDL('" + selectStr + "');", true);
            }
            catch (Exception ex)
            {
                logobj.CreateLog("parseXMLDoc -" + ex.Message + "--" + ires, HttpContext.Current.Session.SessionID, "parseXMLDoc", "new");
            }
        }
    }
    #endregion

    [WebMethod]
    public static string GetFilterFastData(string SessKey, string FldName, string FldValue, string fltValue)
    {
        string json = string.Empty;
        try
        {
            //FDR fObj = (FDR)HttpContext.Current.Session["FDR"];
            //json = fObj.GetAutoFilterFastData(SessKey, FldName, FldValue, fltValue);
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            logObj.CreateLog("GetFilterFastData -" + ex.Message, HttpContext.Current.Session.SessionID, "GetFilterFastData", "new");
        }
        return json;
    }
    [WebMethod]
    public static string GetAutoCompleteData(string tstDataId, string FldName, string FltValue, ArrayList ChangedFields, ArrayList ChangedFieldDbRowNo, ArrayList ChangedFieldValues, ArrayList DeletedDCRows, string pageData, string fastdll, string fldNameAc, string refreshAC, string pickArrow, string parentsFlds, string rfSave)
    {
        string json = string.Empty;
        DateTime stTime = DateTime.Now;
        try
        {
            ASB.WebService objws = new ASB.WebService();
            json = objws.GetdllAutoComplete(tstDataId, FldName, FltValue, ChangedFields, ChangedFieldDbRowNo, ChangedFieldValues, DeletedDCRows, pageData, fastdll, fldNameAc, refreshAC, pickArrow, parentsFlds, rfSave);
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            string sessID = Constants.GeneralLog;
            if (HttpContext.Current.Session != null)
                sessID = HttpContext.Current.Session.SessionID;
            logObj.CreateLog("Get AutoComplete Data -" + ex.Message, sessID, "GetAutoCompleteData", "new");
        }
        DateTime etTime = DateTime.Now;

        //return "Ajax call total-" + etTime.Subtract(stTime).TotalMilliseconds.ToString() + json;
        return json;
    }
    [WebMethod]
    public static string GetWrkFlwCmmt(string tid, string rid)
    {
        string wrkXml = string.Empty;
        string wrkflwTblStr = string.Empty;
        wrkXml = "<root axpapp='" + HttpContext.Current.Session["project"].ToString() + "' sessionid='" + HttpContext.Current.Session["nsessionid"].ToString() + "' transid='" + tid + "' recordid='" + rid + "' trace='' appsessionkey='" + HttpContext.Current.Session["AppSessionKey"].ToString() + "' username='" + HttpContext.Current.Session["username"].ToString() + "'>";
        wrkXml += HttpContext.Current.Session["axApps"].ToString() + HttpContext.Current.Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString();
        wrkXml += "</root>";
        string jsonText = string.Empty;
        try
        {
            ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
            wrkflwTblStr = objWebServiceExt.CallViewCommentsWS(tid, wrkXml);
            DataSet ds = new DataSet();
            StringReader stringReader = new StringReader(wrkflwTblStr);
            ds.ReadXml(stringReader);
            DataTable dt = ds.Tables["row"];
            jsonText = JsonConvert.SerializeObject(dt);
        }
        catch (Exception ex)
        {
            LogFile.Log logObj = new LogFile.Log();
            string sessID = Constants.GeneralLog;
            if (HttpContext.Current.Session != null)
                sessID = HttpContext.Current.Session.SessionID;
            logObj.CreateLog("Get workflow comments -" + ex.Message, sessID, "GetWorkFolwComment", "new");
        }
        return jsonText;
    }


    public string LoadAttachFromLoc(string transId, string recId, TStructData strDataObj)
    {
        string attachDirPath = strDataObj.attachDirPath;
        if (!attachDirPath.Contains(":\\") && !attachDirPath.StartsWith("\\"))
        {
            strDataObj.CreateDirectoryInCommonDir(ref attachDirPath);
        }

        DirectoryInfo dir = new DirectoryInfo(attachDirPath + "\\" + transId + "\\");
        string fileValues = string.Empty;
        string attachJson = string.Empty;
        if (dir.Exists)
        {
            FileInfo[] info = dir.GetFiles(recId + "-*.*");
            foreach (FileInfo FI in info)
                fileValues += FI.ToString().Substring(FI.ToString().IndexOf("-") + 1) + ",";
            if (fileValues.Length > 0)
            {
                fileValues = fileValues.TrimEnd(',');
                attachJson = "*$*{\"attachment\":[{\"att\":\"" + fileValues + "\"}]}";
            }
        }

        return attachJson;
    }

    private void getDesignedData(TStructDef strObj)
    {
        var designMode = false;
        if (HttpContext.Current.Session[transId + "IsDesignMode"] != null && HttpContext.Current.Session[transId + "IsDesignMode"].ToString() != string.Empty)
        {
            designMode = Convert.ToBoolean(HttpContext.Current.Session[transId + "IsDesignMode"]);
        }
        string session_id = HttpContext.Current.Session["nsessionid"].ToString();
        string utl = HttpContext.Current.Session["utl"].ToString();
        string userName = HttpContext.Current.Session["username"].ToString();
        string nodeAccessToken = string.Empty;
        if (HttpContext.Current.Session["nodeAccessToken"] != null)
        {
            nodeAccessToken = HttpContext.Current.Session["nodeAccessToken"].ToString();
        }
        string appsesskey = HttpContext.Current.Session["AppSessionKey"].ToString();
        if (designHidden.Value == string.Empty && !designMode && nodeAccessToken != string.Empty)
        {
            designHidden.Value = strObj.GetPublishNodeApiCall(nodeAccessToken, session_id, utl, userName, appsesskey);
        }
        else if (designHidden.Value == string.Empty && designMode && nodeAccessToken != string.Empty)
        {
            designHidden.Value = strObj.GetSaveNodeApiCall(nodeAccessToken, session_id, utl, userName, appsesskey);
        }
        if (designHidden.Value == string.Empty)
        {
            designHidden.Value = strObj.axdesignJson;
        }
        strObj.axdesignJObject = strObj.getDesignObjectFromJson(designHidden.Value);
    }


    [WebMethod]
    public static string SavePublishDesign(string Transid, string DesignType, string Content, string SavedId)
    {
        string tstDesign = string.Empty;
        DBContext objdb = new DBContext();
        if (DesignType == "SAVE")
            tstDesign = objdb.SaveDesignJson(Transid, Content);
        else if (DesignType == "PUBLISH")
        {
            tstDesign = objdb.PublishDesignJson(Transid, SavedId);
            ClearCacheDesignKeys(Transid);
        }
        else if (DesignType == "SAVEPUBLISH")
        {
            tstDesign = objdb.SavePublishDesign(Transid, Content, SavedId);
            ClearCacheDesignKeys(Transid);
        }
        else if (DesignType == "RESET")
        {
            tstDesign = objdb.ResetDesignJson(Transid, SavedId);
            ClearCacheDesignKeys(Transid);
        }
        return tstDesign;
    }
    public static void ClearCacheDesignKeys(string Transid)
    {
        FDW fdwObj = FDW.Instance;
        string fdKey = Constants.REDISTSTRUCT;
        string fdKeyMob = Constants.REDISTSTRUCTMOB;
        string designKey = Constants.REDISTSTRUCTAXDESIGN;
        string fdkey1 = Constants.AXPAGETITLE;
        string schemaName = string.Empty;
        if (HttpContext.Current.Session["dbuser"] != null)
            schemaName = HttpContext.Current.Session["dbuser"].ToString();
        Util.Util utilObj = new Util.Util();
        fdwObj.ClearRedisServerDataByKey(utilObj.GetRedisServerkey(fdkey1, Transid), "", false, schemaName);
        fdwObj.ClearRedisServerDataByKey(utilObj.GetRedisServerkey(fdKey, Transid), "", false, schemaName);
        fdwObj.ClearRedisServerDataByKey(utilObj.GetRedisServerkey(fdKeyMob, Transid), "", false, schemaName);
        fdwObj.ClearRedisServerDataByKey(utilObj.GetRedisServerkey(designKey, Transid), "", false, schemaName);
    }


    [WebMethod]
    public static string GetFormLoadValues(string key, string tstQureystr)
    {
        string result = string.Empty;
        LogFile.Log logobj = new LogFile.Log();
        Util.Util utils = new Util.Util();
        string queryString = string.Empty;
        string isTstParamLoad = string.Empty;
        string actstrType = string.Empty;
        string AxDisplayAutoGenVal = HttpContext.Current.Session["AxDisplayAutoGenVal"].ToString();
        bool wsPerfFormLoad = false;
        try
        {
            utils.DeleteKeyOnRefreshSave();
            string queryStringParamsData = QueryStringParams(tstQureystr);
            queryString = queryStringParamsData.Split('♠')[0];
            actstrType = queryStringParamsData.Split('♠')[1];
            DateTime sTime1 = DateTime.Now;
            if (HttpContext.Current.Session["project"] == null)
                return utils.SESSTIMEOUT;
            TStructData tstData = (TStructData)HttpContext.Current.Session[key];
            if (tstData == null)
                return Constants.DUPLICATESESS;
            string transId = tstData.transID;
            string errorLog = logobj.CreateLog("Form Load Service", HttpContext.Current.Session["nsessionid"].ToString(), "FormData-" + transId, "new");
            DateTime eTime1 = DateTime.Now;
            TStructDef strObj = tstData.tstStrObj;
            wsPerfFormLoad = strObj.wsPerfFormLoadCall;
            if (queryString != "" && actstrType.ToLower() == "load")
                isTstParamLoad = "true";
            else if (queryString != "" && actstrType.ToLower() == "open")
                isTstParamLoad = "false";

            string actstr = " act='" + actstrType + "'";

            string rid = "0";

            string loadXml = string.Empty;
            loadXml += "<root" + actstr + " axpapp='" + HttpContext.Current.Session["project"].ToString() + "' sessionid='" + HttpContext.Current.Session["nsessionid"].ToString() + "' transid='" + transId + "' recordid='" + rid + "' dcname='" + strObj.GetVisibleDCs() + "' trace='" + errorLog + "' appsessionkey='" + HttpContext.Current.Session["AppSessionKey"].ToString() + "' username='" + HttpContext.Current.Session["username"].ToString() + "'>";
            loadXml += queryString;
            loadXml += HttpContext.Current.Session["axApps"].ToString() + HttpContext.Current.Application["axProps"].ToString() + HttpContext.Current.Session["axGlobalVars"].ToString() + HttpContext.Current.Session["axUserVars"].ToString();
            loadXml += "</root>";

            DateTime stTime11 = DateTime.Now;
            ASBExt.WebServiceExt objWebServiceExt = new ASBExt.WebServiceExt();
            if (isTstParamLoad == "true" || (isTstParamLoad == "false" && strObj.wsPerfFormLoadCall))
            {
                result = objWebServiceExt.CallDoFormLoadWS(transId, loadXml, strObj.structRes);
                HandleFormLoadErrNew(result, queryString);
                wsPerfFormLoad = true;
                if (isTstParamLoad == "true")
                {
                    AxDisplayAutoGenVal = "true";
                }
                else
                {
                    string stsResult = result.Replace("*$*", "¿");
                    stsResult = stsResult.Split('¿').Last();
                    Util.Util utilities = new Util.Util();
                    string stsGlobal = utilities.ParseJSonResultNode(stsResult);

                    if (stsGlobal == string.Empty)
                    {
                        AxDisplayAutoGenVal = "false";
                    }
                    else
                    {
                        AxDisplayAutoGenVal = "true";
                    }
                }
            }
            else if (strObj.wsPerfFormLoadCall)
            {
                AxDisplayAutoGenVal = "false";
                result = utils.GetCachedFormLoadData(transId);
                if (result == string.Empty)
                {
                    result = objWebServiceExt.CallDoFormLoadWS(transId, loadXml, strObj.structRes);
                    HandleFormLoadErrNew(result, queryString);
                    if ((strObj.formLoadCache == "" || strObj.formLoadCache == "None") && result != "")
                        utils.CacheFormLoadData(result, transId);
                }
            }
            if (result != "")
            {
                result = result.Trim();
                result = result.Replace("\\n", "");
                result = result.Replace("\\", ";bkslh");
            }

            TStructData strDataObj = null;
            strDataObj = new TStructData(result, transId, "0", strObj);
            string dcHmtl = GetTabDcHTMLFormLoad(strObj, strDataObj, "0");
            HttpContext.Current.Session.Add(key, null);
            key = utils.GetTstDataId(transId);
            result = result.Trim();
            result = result.Replace("\\n", "");
            result = result.Replace("\\", ";bkslh");
            result = result.Replace("'", "&quot;");
            strDataObj.transid = transId.ToString();
            strDataObj.recordid = "0";
            HttpContext.Current.Session.Add(key, strDataObj);

            string tstVarScript = string.Empty;
            string strObjwsPerfEvalExpClient = string.Empty;
            if (strObj.wsPerfEvalExpClient != null && wsPerfFormLoad == false)
                strObjwsPerfEvalExpClient = string.Join("','", strObj.wsPerfEvalExpClient.Split(','));
            tstVarScript = wsPerfFormLoad.ToString().ToLower() + ";";
            tstVarScript += strObjwsPerfEvalExpClient + ";";
            tstVarScript += strDataObj.attachDir;

            result = key + "*$*" + result;
            DateTime sTime2 = DateTime.Now;
            DateTime eTime2 = DateTime.Now;
            if (tstData.logTimeTaken)
                tstData.strServerTime = (tstData.webTime1 + (eTime1.Subtract(sTime1).TotalMilliseconds)) + "," + tstData.asbTime + "," + (tstData.webTime2 + (eTime2.Subtract(sTime2).TotalMilliseconds));
            logobj.CreateLog("Form Load Service completed", HttpContext.Current.Session["nsessionid"].ToString(), "FormData-" + transId, "");
            string wbdrHtml = string.Empty;
            wbdrHtml = GetTstHtml(transId, errorLog);
            string tabDCStatus = string.Join(",", strObj.tabDCStatus.ToArray());
            string tstCancelled = strObj.tstCancelled;
            result = wbdrHtml + "*$*" + dcHmtl + "*$*" + tstVarScript.ToString() + "*$*" + tabDCStatus + "*$*" + AxDisplayAutoGenVal + "*$*" + tstCancelled + "*$*" + result;
        }
        catch (Exception ex)
        {

        }
        return result;
    }

    private static void HandleFormLoadErrNew(string loadRes, string queryString)
    {
        Util.Util objUtil = new Util.Util();
        if (loadRes.Contains("\"error\"") == true && loadRes.Contains(Constants.SESSIONEXPMSG))
        {
            HttpContext.Current.Response.Redirect(objUtil.ERRPATH + Constants.SESSIONEXPMSG);
            return;
        }
        else if (loadRes.Contains("\"error\"") == true && loadRes.Contains(Constants.ERAUTHENTICATION))
        {
            HttpContext.Current.Response.Redirect(objUtil.ERRPATH + Constants.ERAUTHENTICATION);
            return;
        }
    }

    [WebMethod]
    public static string GetLoadDataValues(string key, string recordid, string tstQureystr)
    {
        string result = string.Empty;
        LogFile.Log logobj = new LogFile.Log();
        Util.Util utils = new Util.Util();
        try
        {
            utils.DeleteKeyOnRefreshSave();
            QueryStringParams(tstQureystr);
            if (HttpContext.Current.Session["project"] == null)
                return utils.SESSTIMEOUT;
            TStructData tstData = (TStructData)HttpContext.Current.Session[key];
            if (tstData == null)
                return Constants.DUPLICATESESS;
            string transId = tstData.transID;
            string errorLog = logobj.CreateLog("Load Data service.", HttpContext.Current.Session["nsessionid"].ToString(), "LoadData-" + transId, "new");
            LoadRecidFromListNew(transId, recordid);
            result = tstData.CallGetLoadData(tstData, recordid, errorLog);
            TStructData strDataObj = null;
            TStructDef strObj = tstData.tstStrObj;
            strDataObj = new TStructData(result, transId, recordid, strObj);
            string dcHmtl = GetTabDcHTMLFormLoad(strObj, strDataObj, recordid);
            HttpContext.Current.Session.Add(key, null);
            key = utils.GetTstDataId(transId);
            result = result.Trim();
            result = result.Replace("\\n", "");
            result = result.Replace("\\", ";bkslh");
            result = result.Replace("'", "&quot;");
            strDataObj.transid = transId.ToString();
            strDataObj.recordid = recordid;
            HttpContext.Current.Session.Add(key, strDataObj);
            string strDcHtml = string.Empty;
            if (recordid != "0")
                strDcHtml = GetFormatGridHtmlNew(strObj, strDataObj);

            if (strDataObj.attachDirPath != string.Empty && recordid != "0")
                result += LoadAttachFromLocNew(transId, recordid, strDataObj);

            result = key + "*$*" + result;
            string ImgVals = GetImageArraysNew(strDataObj);
            string tstVarScript = string.Empty;
            string strObjwsPerfEvalExpClient = string.Empty;
            tstVarScript = "true;";
            tstVarScript += strObjwsPerfEvalExpClient + ";";
            tstVarScript += strDataObj.attachDir;
            string wbdrHtml = string.Empty;
            wbdrHtml = GetTstHtml(transId, errorLog);
            string tabDCStatus = string.Join(",", strObj.tabDCStatus.ToArray());
            string tstCancelled = strObj.tstCancelled;
            result = wbdrHtml + "*$*" + dcHmtl + "*$*" + tstVarScript.ToString() + "*$*" + ImgVals + "*$*" + tabDCStatus + "*$*" + tstCancelled + "*$*" + result;
            logobj.CreateLog("Load Data service completed.", HttpContext.Current.Session["nsessionid"].ToString(), "LoadData-" + transId, "");
        }
        catch (Exception ex)
        {

        }
        return result;
    }

    private static string GetTstHtml(string transId, string errorLog)
    {
        string AjaxTstHTML = string.Empty;
        try
        {
            CacheManager cacheMgr = new CacheManager(errorLog);
            cacheMgr.GetStructureHTML(transId, HttpContext.Current.Session["AxRole"].ToString(), HttpContext.Current.Session["nsessionid"].ToString(), HttpContext.Current.Session["language"].ToString());
            if (cacheMgr.StructureHtml == "")
                AjaxTstHTML = "<script type='text/javascript'>$j('div#wBdr').hide();</script>" + Constants.WIZARD_TEMPLATE + HttpContext.Current.Session["StructureHtml"].ToString() + "*$*" + HttpContext.Current.Session["ToolbarBtnIcons"].ToString();
            else
                AjaxTstHTML = "<script type='text/javascript'>$j('div#wBdr').hide();</script>" + Constants.WIZARD_TEMPLATE + cacheMgr.StructureHtml.ToString() + "*$*" + cacheMgr.ToolbarBtnIcons.ToString();
            if (HttpContext.Current.Session["axDesign"] != null && HttpContext.Current.Session["axDesign"].ToString() == "true")
                AjaxTstHTML += Constants.DESIGN_MODE_BTN_HTML;
        }
        catch (Exception ex)
        { }
        return AjaxTstHTML;
    }

    public static string GetTabDcHTMLFormLoad(TStructDef strObj, TStructData strDataObj, string recordId)
    {
        string tabDcsHtml = string.Empty;
        for (int i = 0; i < strObj.tabDCs.Count; i++)
        {
            if (strObj.tabDCStatus[i].ToString() == "0")
            {
                if (recordId == "0" && strObj.wsPerfFGDcName != null)
                {
                    string[] flGridDc = strObj.wsPerfFGDcName;
                    var loaddcFG = flGridDc.Where(d => d.ToLower() == "dc" + strObj.tabDCs[i]).ToList();
                    if (loaddcFG.Count > 0)
                        continue;
                }
                string temptabHTML = string.Empty;
                string tempDcDesignJson = string.Empty;
                try
                {
                    temptabHTML = strObj.GetTabDcHTML(Convert.ToInt32(strObj.tabDCs[i]), strDataObj, "false");
                    Dc curDc = strDataObj.tstStrObj.axdesignJObject.dcs.FirstOrDefault(elm => elm.dc_id == strObj.tabDCs[i].ToString());
                    tempDcDesignJson = new JavaScriptSerializer().Serialize(curDc);
                }
                catch (Exception ex)
                {

                }
                if (tabDcsHtml == string.Empty)
                    tabDcsHtml += strDataObj.AxDepArrays.ToString() + "*♦*" + strDataObj.GetMasterRowFlds() + "*♠**♠*" + temptabHTML + "*♠*" + tempDcDesignJson;
                else
                    tabDcsHtml += "*♠♠*" + strDataObj.AxDepArrays.ToString() + "*♦*" + strDataObj.GetMasterRowFlds() + "*♠**♠*" + temptabHTML + "*♠*" + tempDcDesignJson;
            }
        }
        return tabDcsHtml;
    }

    private static string QueryStringParams(string tstQureystr)
    {
        StringBuilder returnString = new StringBuilder();
        string hltype = "open";
        if (tstQureystr != string.Empty)
        {
            string[] quertStr = tstQureystr.Split('?');
            string[] arrParams = null;
            if (quertStr.Length > 0)
            {
                arrParams = quertStr[quertStr.Length - 1].Split('♠');
            }
            if (arrParams != null)
                for (int i = 0; i < arrParams.Length; i++)
                {
                    if (arrParams[i] != "")
                    {
                        string prName = arrParams[i].Split('=')[0];
                        string prValue = arrParams[i].Split('=')[1];
                        switch (prName.ToLower())
                        {
                            case "axp_issaveurl":
                                HttpContext.Current.Session["axp_IsSaveUrl"] = prValue;
                                break;
                            case "axfromhyplink":
                                HttpContext.Current.Session["AxFromHypLink"] = prValue;
                                break;
                            case "axpfrm":
                                HttpContext.Current.Session["axpfrm"] = prValue;
                                break;
                            case "axpop":
                                HttpContext.Current.Session["isTstPop"] = prValue;
                                break;
                            case "hltype":
                            case "act":
                                hltype = prValue;
                                break;
                            case "transid":
                            case "themode":
                            case "torecid":
                            case "layout":
                            case "tstname":
                            case "axfromhyperlink":
                            case "axhyptstrefresh":
                            case "recpos":
                            case "pagetype":
                            case "curpage":
                            case "openeriv":
                                break;
                            default:
                                returnString.Append("<" + prName.ToString() + ">" + CheckSpecialChars(prValue.ToString()) + "</" + prName.ToString() + ">");
                                break;
                        }
                    }
                }
            UpdateNavigationNew();
        }
        return returnString.ToString() + "♠" + hltype;
    }

    private static void UpdateNavigationNew()
    {
        Util.Util utils = new Util.Util();
        string isTstFromHyperLink = string.Empty;
        if (HttpContext.Current.Session["AxFromHypLink"] != null)
            isTstFromHyperLink = HttpContext.Current.Session["AxFromHypLink"].ToString();

        string frameName = string.Empty;
        if (HttpContext.Current.Session["axpfrm"] != null)
            frameName = HttpContext.Current.Session["axpfrm"].ToString();
        if (HttpContext.Current.Session["backForwBtnPressed"] == null || (HttpContext.Current.Session["backForwBtnPressed"] != null && !Convert.ToBoolean(HttpContext.Current.Session["backForwBtnPressed"])) && frameName != "t")
        {
            if (HttpContext.Current.Session["isTstPop"] != null && HttpContext.Current.Session["isTstPop"].ToString() == "true")
                HttpContext.Current.Session["enableBackButton"] = "false";
            else if (HttpContext.Current.Session["AxHypTstRefresh"] != null && HttpContext.Current.Session["AxHypTstRefresh"].ToString() == "true")
            {
                HttpContext.Current.Session["AxHypTstRefresh"] = "false";
            }
            else
                utils.UpdateNavigateUrl(HttpContext.Current.Request.Url.AbsoluteUri);
        }
        if (HttpContext.Current.Session["axp_IsSaveUrl"] != null)
            HttpContext.Current.Session["axp_IsSaveUrl"] = null;
        if (HttpContext.Current.Session["axpfrm"] != null)
            HttpContext.Current.Session["axpfrm"] = null;
        HttpContext.Current.Session["backForwBtnPressed"] = false;
        enableBackForwButton = "<script language=\'javascript\' type=\'text/javascript\' > enableBackButton='" + Convert.ToBoolean(HttpContext.Current.Session["enableBackButton"]) + "';" + " enableForwardButton='" + Convert.ToBoolean(HttpContext.Current.Session["enableForwardButton"]) + "'; var fromHyperLink='" + isTstFromHyperLink + "';var isRapidLoad='" + false + "';var defaultDepFlds='';</script>";
    }

    private static void LoadRecidFromListNew(string transid, string rcId)
    {
        if (HttpContext.Current.Session["lvRecordListing-" + transid] != null)
        {
            Dictionary<int, string> lvRecordListing = new Dictionary<int, string>();
            try
            {
                lvRecordListing = (Dictionary<int, string>)HttpContext.Current.Session["lvRecordListing-" + transid];
                if (lvRecordListing != null)
                {
                    var recordkeyValuePair = lvRecordListing.Single(x => x.Value == rcId);
                    int recordKey = recordkeyValuePair.Key;
                    HttpContext.Current.Session["lvRecordKey"] = recordKey;
                }
            }
            catch (Exception ex)
            {
                //logobj.CreateLog(ex.Message, sid, "Exception in LoadStructure ListViewNavigation Details", "new");
            }
        }
    }

    //Function to construct the image arrays for all the images in the tstruct with values from the tstruct data object.
    private static string GetImageArraysNew(TStructData tstData)
    {
        string strImg = string.Empty;
        StringBuilder strImgArr = new StringBuilder();
        for (int i = 0; i < tstData.imageFldNames.Count; i++)
        {
            if (strImgArr.ToString() != string.Empty)
                strImgArr.Append("♠");
            strImgArr.Append(tstData.imageFldNames[i].ToString() + "♦");
            string src = tstData.imageFldSrc[i].ToString();
            src = src.Replace("\\", ";bkslh");
            strImgArr.Append(src);
        }
        strImg = strImgArr.ToString();
        return strImg;
    }

    private static string GetFormatGridHtmlNew(TStructDef strObj, TStructData tstData)
    {
        StringBuilder strDcHtml = new StringBuilder();
        for (int i = 0; i < strObj.visibleDCs.Count; i++)
        {
            int dcNo = Convert.ToInt32(strObj.visibleDCs[i].ToString());
            if (strObj.IsDcFormatGrid(dcNo))
            {
                strDcHtml.Append(strObj.GetTabDcHTML(dcNo, tstData, "false"));
            }
        }
        return strDcHtml.ToString();
    }

    private static string LoadAttachFromLocNew(string transId, string recId, TStructData strDataObj)
    {
        string attachDirPath = strDataObj.attachDirPath;
        if (!attachDirPath.Contains(":\\") && !attachDirPath.StartsWith("\\"))
        {
            strDataObj.CreateDirectoryInCommonDir(ref attachDirPath);
        }

        DirectoryInfo dir = new DirectoryInfo(attachDirPath + "\\" + transId + "\\");
        string fileValues = string.Empty;
        string attachJson = string.Empty;
        if (dir.Exists)
        {
            FileInfo[] info = dir.GetFiles(recId + "-*.*");
            foreach (FileInfo FI in info)
                fileValues += FI.ToString().Substring(FI.ToString().IndexOf("-") + 1) + ",";
            if (fileValues.Length > 0)
            {
                fileValues = fileValues.TrimEnd(',');
                attachJson = "*$*{\"attachment\":[{\"att\":\"" + fileValues + "\"}]}";
            }
        }
        return attachJson;
    }
}
