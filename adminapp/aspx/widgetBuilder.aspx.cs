﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aspx_widgetBuilder : System.Web.UI.Page
{
    public string globalVars = string.Empty;
    public string appsessionKey = string.Empty;
    public string appsessionKeyWOL = string.Empty;
    public string traceLog = string.Empty;
    public string axWizardType = "";
    LogFile.Log logobj = new LogFile.Log();

    Util.Util util;
    public string direction = "ltr";
    public string langType = "en";

    protected override void InitializeCulture()
    {
        if (Session["language"] != null)
        {
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(Session["language"].ToString().ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Util.Util util = new Util.Util();
        if (Session["project"] == null || Convert.ToString(Session["project"]) == string.Empty)
        {
            SessExpires();
            return;
        }
        else
        {
            if (!util.licencedValidSessionCheck())
            {
                HttpContext.Current.Response.Redirect(util.ERRPATH + Constants.SESSIONEXPMSG, false);
                return;
            }
        }
        if (Session["AppSessionKey"] != null)
        {
            if (Session["lictype"] != null && Session["lictype"].ToString() == "limited")
                appsessionKey = "l~";
            else
                appsessionKey = "u~";
            appsessionKey += Session["AppSessionKey"].ToString();
            appsessionKeyWOL = Session["AppSessionKey"].ToString();
            traceLog = logobj.CreateLog("AsbDefineRest-saveivew", Session.SessionID.ToString(), "WidgetBuilder-ivew-save", "new");
            traceLog = traceLog.Replace("\\", "\\\\");
        }
        if (Session["AxWizardType"] != null)
        {
            axWizardType = Session["AxWizardType"].ToString();
        }


        if (Session["globalvarstring"] != null)
        {
            globalVars = Session["globalvarstring"].ToString();
            //ScriptManager.RegisterStartupScript(Page, this.GetType(), "assign Parameters", "var Parameters= new Array();" + Session["globalvarstring"].ToString() , true);
        }
    }
    private static void SessExpires()
    {
        string url = Convert.ToString(HttpContext.Current.Application["SessExpiryPath"]);
        HttpContext.Current.Response.Write("<script>" + Constants.vbCrLf);
        HttpContext.Current.Response.Write("parent.parent.location.href='" + url + "';");
        HttpContext.Current.Response.Write(Constants.vbCrLf + "</script>");
    }
}