﻿using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using EasyDownload.Utility;
using ICSharpCode.SharpZipLib.Zip;
using Oracle.ManagedDataAccess.Client;
using RestSharp;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace EasyDownload
{
    class Program
    {

        const int STD_OUTPUT_HANDLE = -11;
        const string Underline = "\x1B[4m";
        const string DecReset = "\x1B[0m";
        const string Red = "\x1b[31m";
        const string Green = "\x1b[32m";
        const uint ENABLE_VIRTUAL_TERMINAL_PROCESSING = 4;
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr GetStdHandle(int nStdHandle);
        [DllImport("kernel32.dll")]
        static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);
        static void Main()
        {
            double ver = 2.3;
            var handle = GetStdHandle(STD_OUTPUT_HANDLE);
            uint mode;
            GetConsoleMode(handle, out mode);
            mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            SetConsoleMode(handle, mode);
            
            string dirpath = System.AppDomain.CurrentDomain.BaseDirectory + "Data\\";
            if (!Directory.Exists(dirpath)) Directory.CreateDirectory(dirpath);
            string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\";
            if (!Directory.Exists(logpath)) Directory.CreateDirectory(logpath);
            IniFile iniFile = new IniFile("Config.ini");
            int timeintervel = Convert.ToInt32(float.Parse(iniFile.Read("TimeIntervel", "TimeIntervel")) * 60 * 100);
            DataTable orderlist;

            //string filenam = "iTero_Case_28602362_20190807042743.zip";
            //string zipfolderr = ExtractZipfile("caseno", dirpath, filenam);
            //if (zipfolderr.Substring(0, 1) == "t")
            //{
            //    zipfolderr = zipfolderr.Substring(1, zipfolderr.Length - 1);
            //    ExtractXMLfile(zipfolderr, "caseno", filenam);
            //}

            
            if (iniFile != null)
            {
                if (string.IsNullOrEmpty(Readconfigfile()))
                {
                    Updatelog("Config file not available. Press any key to close the application.", 1);
                    //Console.WriteLine("Config file not available. Press any key to close the application.");
                    Console.ReadLine();
                }
                else
                {
                    do
                    {
                        do
                        {
                            bool datasetflag;
                            do
                            {
                                //Console.WriteLine("Version " + ver);
                                Updatelog("Version " + ver, 1);
                                orderlist = ExtracrOrderNo();
                                if (orderlist == null)
                                {
                                    Updatelog("Reconnecting Database..", 1);
                                    //Console.WriteLine("Reconnecting Database..");
                                    datasetflag = false;
                                }
                                else
                                    datasetflag = true;
                            } while (!datasetflag); //Database connectivity 

                            if (orderlist.Rows.Count == 0) Updatelog("No new case found.\n", 1);//Console.WriteLine("No new case found.\n");
                            foreach (DataRow row in orderlist.Rows)
                            {
                                Updatelog("", 1);
                                //Console.WriteLine("");
                                //Console.WriteLine(Underline + row["ORDERNO"].ToString() + DecReset);
                                //Console.WriteLine(row["ORDERNO"].ToString());
                                Updatelog(row["ORDERNO"].ToString(), 1);

                                string caseno = row["ORDERNO"].ToString();
                                string bucketpath = row["fpath"].ToString();
                                string fileinbucket = row["fname"].ToString();

                                string filedata = FetchFilefromBucket(caseno, bucketpath, fileinbucket, dirpath);
                                if (filedata.Substring(0, 1) == "t")
                                {
                                    string filename = filedata.Substring(2, filedata.Length - 2);
                                    string zipfolder = ExtractZipfile(caseno, dirpath, filename);
                                    if (zipfolder.Substring(0, 1) == "t")
                                    {
                                        zipfolder = zipfolder.Substring(1, zipfolder.Length - 1);
                                        ExtractXMLfile(zipfolder, caseno, filename);
                                    }
                                }
                                //string[] allfiles = Directory.GetFiles(dirpath, "*.*", SearchOption.TopDirectoryOnly);
                                //for (int i = 0; i < allfiles.Length; i++) File.Delete(allfiles[i]);
                            }
                            System.Threading.Thread.Sleep(timeintervel);
                        }
                        while (!Console.KeyAvailable);
                    } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
                    Console.ReadLine();
                }
            }
        }
        public static string Readconfigfile()
        {
            IniFile iniFile = new IniFile("Config.ini");
            string Protocol = iniFile.Read("Protocol", "Protocol");
            string Host = iniFile.Read("Host", "Host");
            string Port = iniFile.Read("Port", "PORT");
            string Server = iniFile.Read("Server", "Server");
            string ServiceName = iniFile.Read("SERVICE_NAME", "Servicename");
            string UserId = iniFile.Read("User Id", "User_id");
            string Pass = iniFile.Read("Password", "Pass");
            string connstring = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = " + Protocol + ")(HOST = " + Host + ")(PORT = " + Port + ")))(CONNECT_DATA = (SERVICE_NAME = " + ServiceName + ")(SERVER = " + Server + " ))); User Id = " + UserId + "; Password =" + Pass;
            return connstring;
        }
        public static DataTable ExtracrOrderNo()
        {
            string connectionStrin = Readconfigfile();
            string query = "select CASENO as orderno, ds_ordersid,fpath,fname from ds_orders where ARCHIVE_STATUS = 1 and rxStatus = 0";
            //query = "select CASENO as orderno, ds_ordersid,fpath,fname from ds_orders";// where ARCHIVE_STATUS = 1 and rxStatus = 0";
            using (OracleConnection con = new OracleConnection(connectionStrin))
            {
                //Console.WriteLine("");
                Updatelog("", 1);
                var handle = GetStdHandle(STD_OUTPUT_HANDLE);
                uint mode;
                GetConsoleMode(handle, out mode);
                mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
                SetConsoleMode(handle, mode);

                //Console.Write(Underline + "Connection in progress... ");
                //Console.Write("Connection in progress... ");
                Updatelog("Connection in progress... ", 1);
                try
                {
                    OracleCommand cmd = new OracleCommand
                    {
                        CommandText = query,
                        Connection = con
                    };
                    //Console.WriteLine("Success" + DecReset);
                    //Console.WriteLine("Success");
                    Updatelog("Success", 1);
                    //// Console.WriteLine("─────────────────────────────────");
                    //con.Open();

                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }
                catch (Exception ex)
                {
                    Updatelog("Error:" + ex.Message, 1);
                    //Console.WriteLine("Error:" + ex.Message);
                    //Console.WriteLine("Error:" + ex.Message + DecReset);
                    return null;
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }

            }
        }
        public static string FetchFilefromBucket(string caseno, string bucketpath, string fileinbucket, string dirpath)
        {
            var handle = GetStdHandle(STD_OUTPUT_HANDLE);
            uint mode;
            GetConsoleMode(handle, out mode);
            mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            SetConsoleMode(handle, mode);
            //const string bright = "\x1b[1m";
            //const string DecReset = "\x1B[0m";

            Updatelog("Fetching the zipfile in progress ", 0);
            //Console.Write("Fetching the zipfile in progress ");
            var endpoint = "https://s3.wasabisys.com";
            var config = new AmazonS3Config { ServiceURL = endpoint };
            RegionEndpoint bucketRegion = RegionEndpoint.USEast1;
            //IAmazonS3 client = new AmazonS3Client(bucketRegion);
            string accessKey = "PT75SZ0M4KUWUGOKLLE9";
            string secretKey = "eBMn6KWWph8ZjcIOHQDZ7uVe5BmvQ7eDc6wBHCP7";
            AmazonS3Client s3Client = new AmazonS3Client(accessKey, secretKey, config);
            string filename = "";
            S3DirectoryInfo dir = new S3DirectoryInfo(s3Client, "easydentconnect", bucketpath.Replace("/", "\\"));
            bool fileexistflag = false;
            try
            {
                foreach (IS3FileSystemInfo file in dir.GetFileSystemInfos())
                {
                    if (file.Name == fileinbucket)
                    {
                        Updatelog(file.Name, 0);
                        //Console.Write(file.Name);
                        //Console.Write(bright + file.Name + DecReset);
                        filename = file.Name;
                        fileexistflag = true;
                    }
                }
                if (fileexistflag)
                {
                    GetObjectRequest request = new GetObjectRequest
                    {
                        BucketName = "easydentconnect",
                        Key = bucketpath + "/" + filename
                    };
                    try
                    {
                        //Newly added instead of stream download 15-04-2020
                        string FileLocation = dirpath + filename;
                        if (Directory.Exists(@dirpath))
                        {
                            Directory.Delete(@dirpath, true);
                            Directory.CreateDirectory(dirpath);
                        }
                        FileStream fs = File.Create(FileLocation);
                        fs.Close();
                        TransferUtility fileTransferUtility = new TransferUtility(s3Client);
                        fileTransferUtility.Download(FileLocation, request.BucketName, request.Key);
                        fileTransferUtility.Dispose();
                        //Up here

                        //Old streaming procedure to download the file from bucket
                        //GetObjectResponse response = s3Client.GetObject(request);
                        //if (File.Exists(dirpath + filename))
                        //    File.Delete(dirpath + filename);
                        //response.WriteResponseStreamToFile(dirpath + filename);
                        return "t " + filename;
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("");
                        Updatelog("", 1);
                        RxStatusupdate(caseno, "File not found in bucket", filename, 2);
                        Updatelog("Error:" + ex.Message + "(The file not found in bucket)", 0);
                        Console.Write("Error:" + ex.Message + "(The file not found in bucket)");
                        return "f";
                    }
                }
                else
                {
                    //Console.WriteLine("");
                    Updatelog("", 1);
                    RxStatusupdate(caseno, "File not found in bucket", filename, 2);
                    return "f";
                }

            }
            catch (Exception ex)
            {
                RxStatusupdate(caseno, "Error:" + ex.Message + "(Error in retriving the file)", filename, 2);
                //Console.WriteLine("Error:" + ex.Message + "(Error in retriving the file)");
                return "f";
            }
        }
        public static string ExtractZipfile(string caseno, string dirpath, string filename)
        {
            //Console.WriteLine("");
            Updatelog("", 1);
            Updatelog("Zipfile extraction is in progress... ", 1);
            //Console.WriteLine("Zipfile extraction is in progress... ");
            //DirectoryInfo dirinf = new DirectoryInfo(dirpath);
            string ExtractZipfileflg = "t";
            string zipfilename = filename;
            string zipFilePath = @dirpath + filename;

            if (Directory.Exists(@dirpath + zipfilename.Substring(0, zipfilename.Length - 4)))
            {
                Directory.Delete(@dirpath + zipfilename.Substring(0, zipfilename.Length - 4), true);
            }

            if (IsPasswordProtectedZipFile(zipFilePath) == 0)
            {
                try
                {
                    System.IO.Compression.ZipFile.ExtractToDirectory(zipFilePath, @dirpath);
                }
                catch (Exception ex)
                {
                    ExtractZipfileflg = "f";
                    Updatelog("Error:" + ex.Message, 1);
                    //Console.WriteLine("Error:" + ex.Message);
                    RxStatusupdate(caseno, "Unable to extract the Zip file", filename, 2);
                }
            }

            else
            {
                ExtractZipfileflg = "f";
                if (IsPasswordProtectedZipFile(zipFilePath) == 1)
                    //Console.WriteLine("Zip file is password protected");
                    RxStatusupdate(caseno, "Zip file is password protected", filename, 2);
                else
                    RxStatusupdate(caseno, "Invalid/Unformated Zip file", filename, 2);
            }



            return ExtractZipfileflg + @dirpath + filename.Substring(0, filename.Length - 4);
        }
        public static void ExtractXMLfile(string path, string caseno, string filename)
        {
            Updatelog("Parsing of XML file is in progress... ", 1);
            //Console.WriteLine("Parsing of XML file is in progress... ");
            if (Directory.Exists(path))
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                string[] xmlFiles = Directory.GetFiles(path, "*.xml", SearchOption.AllDirectories);

                if (xmlFiles.Length > 0)
                {
                    xmlFiles = xmlFiles.OrderByDescending(d => d).ToArray();
                    DataTable Data_Rx_Table = new DataTable();
                    Data_Rx_Table.Columns.Add("TeethInfo");
                    Data_Rx_Table.Columns.Add("TID");
                    Data_Rx_Table.Columns.Add("Tvalues");
                    Data_Rx_Table.Columns.Add("MaterialInfo");
                    Data_Rx_Table.Columns.Add("MID");
                    Data_Rx_Table.Columns.Add("Mvalues");
                    Data_Rx_Table.Columns.Add("RestoreInfo");
                    Data_Rx_Table.Columns.Add("RID");
                    Data_Rx_Table.Columns.Add("Rvalues");

                    string fileExt = System.IO.Path.GetExtension(xmlFiles[0]);

                    if (fileExt == ".xml")
                    {
                        string xmlfile = xmlFiles[0];
                        if (xmlFiles[0].IndexOf("Materials.xml")>0)
                            xmlfile = xmlFiles[1];
                        var xmlStr = File.ReadAllText(xmlfile);
                        var str = XElement.Parse(xmlStr);
                        string rxtype = str.ToString().Substring(str.ToString().IndexOf("<") + 1, str.ToString().IndexOf(" ")).Trim();
                        Updatelog("Rx Type : " + rxtype, 1);
                        //Console.WriteLine("Rx Type : " + rxtype);
                        string _Notesval = "";
                        DataRow Dr_Data_Rx_Table = Data_Rx_Table.NewRow();
                        switch (rxtype)
                        {
                            case "DentalContainer":
                                Dr_Data_Rx_Table = ParseDentalContainerXml(Data_Rx_Table, xmlfile);
                                break;

                            case "iTeroExport":
                                Dr_Data_Rx_Table = ParseITeroXml(Data_Rx_Table, xmlfile);
                                var _RxInfo = str.Elements("RxInfo").ToList();
                                var _Notes = _RxInfo.Elements("Notes").ToList();
                                if (_Notes.Count > 0)
                                    _Notesval = _Notes[0].Value;
                                break;
                        }

                        string result = UpdateData(caseno, Dr_Data_Rx_Table, _Notesval);
                        if (result.IndexOf("Rx Details Saved") > 0)
                            RxStatusupdate(caseno, rxtype, filename, 3);
                        //MoveDeleteFileFolder(filename, result.IndexOf("Rx Details Saved"));
                    }
                }
                else
                    RxStatusupdate(caseno, "No XML files in zip file", filename, 2);
            }
            else
                RxStatusupdate(caseno, "No Data/folders in zip file", filename, 2);
        }
        public static DataRow ParseDentalContainerXml(DataTable Data_Rx_Table, string xmlfile)
        {
            var xmlStr = File.ReadAllText(xmlfile);
            var str = XElement.Parse(xmlStr);
            var objectlist = str.Elements("Object").ToList();
            var _objectlist = objectlist.Elements("Object").ToList();
            DataTable materialtable = new DataTable();
            for (int i = 0; i < _objectlist.Count; i++)
            {
                string teethdetail = _objectlist[i].ToString();

                if (teethdetail.IndexOf("TDM_List_ModelElement") > 0)
                {
                    var tmp = _objectlist[i];
                    var _list = tmp.Elements("List").ToList();
                    var __objectlist = _list.Elements("Object").ToList();
                    var _propertylist = __objectlist.Elements("Property").ToList();

                    materialtable.Columns.Add("Id");
                    materialtable.Columns.Add("Material");
                    DataRow materialrow;

                    materialrow = materialtable.NewRow();
                    for (int j = 0; j < _propertylist.Count; j++)
                    {
                        string materialdetailes = _propertylist[j].ToString();
                        if (materialdetailes.IndexOf("ModelElementID") > 0)
                        {
                            materialrow = materialtable.NewRow();
                            materialrow["Id"] = Getvalue(materialdetailes, "value");
                        }
                        if (materialdetailes.IndexOf("CacheMaterialName") > 0)
                        {
                            materialrow["Material"] = Getvalue(materialdetailes, "value");
                            materialtable.Rows.Add(materialrow);
                            //Console.WriteLine(materialdetailes);
                        }
                    }
                }
            }
            DataRow Dr_Data_Rx_Table = Data_Rx_Table.NewRow();

            for (int i = 0; i < _objectlist.Count; i++)
            {
                string teethdetail = _objectlist[i].ToString();
                if (teethdetail.IndexOf("TDM_List_ToothElement") > 0)
                {
                    var tmp = _objectlist[i];
                    var _list = tmp.Elements("List").ToList();
                    var __objectlist = _list.Elements("Object").ToList();
                    var _propertylist = __objectlist.Elements("Property").ToList();
                    string modelelemid = "";
                    string _modelelemid = "";
                    string toothno = "";
                    string toothmater = "";
                    for (int j = 0; j < _propertylist.Count; j++)
                    {
                        string toothdetailes = _propertylist[j].ToString();
                        if (toothdetailes.IndexOf("ModelElementID") > 0)
                            modelelemid = Getvalue(toothdetailes, "value");
                        if (toothdetailes.IndexOf("toothElementTypeID") > 0)
                        {
                            _modelelemid = Getvalue(toothdetailes, "value");
                            _modelelemid = _modelelemid.Substring(_modelelemid.IndexOf("_") + 1, _modelelemid.Length - _modelelemid.IndexOf("_") - 1);
                        }
                        if (toothdetailes.IndexOf("ToothNumber") > 0)
                            toothno = Getvalue(toothdetailes, "value");
                        if (toothdetailes.IndexOf("CacheToothTypeClass") > 0)
                            toothmater = Getvalue(toothdetailes, "value");

                        if (modelelemid != "" && toothno != "" && toothmater != "")
                        {
                            Dr_Data_Rx_Table = Data_Rx_Table.NewRow();
                            Dr_Data_Rx_Table["TeethInfo"] = "AdaId";
                            Dr_Data_Rx_Table["TID"] = toothno;
                            Dr_Data_Rx_Table["Tvalues"] = toothno;

                            Dr_Data_Rx_Table["MaterialInfo"] = "ToothMaterial";
                            Dr_Data_Rx_Table["MID"] = toothmater;
                            Dr_Data_Rx_Table["Mvalues"] = toothmater;

                            for (int k = 0; k < materialtable.Rows.Count; k++)
                            {
                                Dr_Data_Rx_Table["RestoreInfo"] = "RestorationType";
                                if (materialtable.Rows[k].ItemArray[0].ToString() == modelelemid)
                                {
                                    Dr_Data_Rx_Table["RID"] = modelelemid;
                                    Dr_Data_Rx_Table["Rvalues"] = modelelemid;
                                }
                                else
                                {
                                    Dr_Data_Rx_Table["RID"] = _modelelemid;
                                    Dr_Data_Rx_Table["Rvalues"] = _modelelemid;
                                }
                            }
                            Data_Rx_Table.Rows.Add(Dr_Data_Rx_Table);
                            modelelemid = "";
                            toothno = "";
                            toothmater = "";
                        }
                    }
                }
            }
            return Dr_Data_Rx_Table;
        }
        public static DataRow ParseITeroXml(DataTable Data_Rx_Table, string xmlfile)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(utility.ITeroExport));
            XmlTextReader xmlreader = new XmlTextReader(xmlfile);
            utility.ITeroExport XmlMappedObjects = (utility.ITeroExport)serializer.Deserialize(xmlreader);
            xmlreader.Close();
            var xmlStr = File.ReadAllText(xmlfile);
            var str = XElement.Parse(xmlStr);
            var _RxInfo = str.Elements("RxInfo").ToList();
            var _Teeth = _RxInfo.Elements("Teeth").ToList();
            var _Tooth = _Teeth.Elements("Tooth").ToList();
            var _Toothno = _Tooth.Elements("AdaId").ToList();
            DataRow Dr_Data_Rx_Table = Data_Rx_Table.NewRow();
            for (int i = 0; i < _Tooth.Count; i++)
            {
                string teethdetaile = _Tooth[i].ToString();
                string _AdaId = Getvalue(teethdetaile, "AdaId");
                string _RestorationType = Getvalue(teethdetaile, "RestorationType");
                string _ToothMaterial = Getvalue(teethdetaile, "ToothMaterial");
                Dr_Data_Rx_Table = Data_Rx_Table.NewRow();
                Dr_Data_Rx_Table["TeethInfo"] = "AdaId";
                Dr_Data_Rx_Table["TID"] = _AdaId;
                Dr_Data_Rx_Table["Tvalues"] = _AdaId;

                var Materials = from EachItem in XmlMappedObjects.RxDefines.Materials.Material
                                where EachItem.Id == _ToothMaterial
                                select EachItem;
                foreach (var Material in Materials)
                {
                    Dr_Data_Rx_Table["MaterialInfo"] = "ToothMaterial";
                    Dr_Data_Rx_Table["MID"] = Material.Id;
                    Dr_Data_Rx_Table["Mvalues"] = Material.Name;
                }

                var RestorationTypes = from EachItem in XmlMappedObjects.RxDefines.RestorationTypes.Type
                                       where EachItem.Id == _RestorationType
                                       select EachItem;
                foreach (var Type in RestorationTypes)
                {
                    Dr_Data_Rx_Table["RestoreInfo"] = "RestorationType";
                    Dr_Data_Rx_Table["RID"] = Type.Id;
                    Dr_Data_Rx_Table["Rvalues"] = Type.Name;
                }
                Data_Rx_Table.Rows.Add(Dr_Data_Rx_Table);
            }
            return Dr_Data_Rx_Table;

        }
        public static string UpdateData(string caseno, DataRow dr_Data_Rx_Table, string notes)
        {
            Updatelog("Database updation is in progress ", 0);
            //Console.Write("Database updation is in progress ");
            IniFile iniFile = new IniFile("Config.ini");
            string Apihost = iniFile.Read("ApiHost", "ApiHost");
            string ApiPName = iniFile.Read("ApiPName", "ApiPName");
            string ApiUName = iniFile.Read("ApiUName", "ApiUName");
            string ApiPKey = iniFile.Read("ApiPKey", "ApiPKey");
            string Apiseed = iniFile.Read("Apiseed", "Apiseed");
            var client = new RestClient(Apihost + "/ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            int rcount1 = dr_Data_Rx_Table.Table.Rows.Count;
            string rcount = rcount1.ToString();

            string jsondata = "{\n\"_parameters\": [\n{\n\"savedata\": {\n\"axpapp\": \"" + ApiPName + "\",\n\"username\": \"" + ApiUName + "\",\n";
            jsondata += "\"password\": \"" + ApiPKey + "\",\n";
            jsondata += "\"seed\": \"" + Apiseed + "\",\n";
            jsondata += "\"s\": \"\",\n\"transid\": \"trxdt\",\n\"recordid\": \"0\",\n\"recdata\": [\n{\n";
            jsondata += "\"axp_recid1\": [\n{\n\"rowno\": \"001\",\n\"text\": \"0\",\n\"columns\": {\n\"caseno\": \"" + caseno + "\",\n\"notes\": \"" + notes + "\",\n\"rcount\": \"" + rcount + "\"\n}\n}\n]\n},";
            jsondata += "{\n\"axp_recid2\": [\n";

            for (int i = 0; i < rcount1; i++)
            {
                string productname = dr_Data_Rx_Table.Table.Rows[i].ItemArray[5].ToString();
                string designtype = dr_Data_Rx_Table.Table.Rows[i].ItemArray[8].ToString();
                string notations = dr_Data_Rx_Table.Table.Rows[i].ItemArray[2].ToString();
                jsondata += "{\n\"rowno\": \"00" + (i + 1).ToString() + "\",\n\"text\": \"0\",\n\"columns\": {\n\"product\": \"001\",\n\"productname\": \"" + productname + "\",\n\"designtype\": \"" + designtype + "\",\n\"notations\": \"" + notations + "\",\n\"model\": \"Y\"\n}\n}";
                if (i < rcount1 - 1)
                    jsondata += ",";
            }

            jsondata += "\n]\n}\n]\n}\n}\n]\n}\n\n";
            request.AddParameter("application/json", jsondata, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
            Updatelog(response.Content, 1);

            //Console.WriteLine("");
            return response.Content.ToString();
        }
        public static string RxStatusupdate(string caseno, string errormsg, string filename, int status)
        {
            //Ver 2.1
            //Rxstatus Updated with 2 with message : File not found in bucket                        file not in Bucket
            //Rxstatus Updated with 2 with message : ex.Message + "(Error in retriving the file)"    unable to read from Bucket
            //Rxstatus Updated with 2 with message : Unable to extract the Zip file                  unable to extract a valid Zip file
            //Rxstatus Updated with 2 with message : Invalid/Unformated Zip file                     Not a valid Zip file
            //Rxstatus Updated with 2 with message : Zip file is password protected                  Protected Zip file
            //Rxstatus Updated with 2 with message : No XML files in zip file                        No xml in Zip file
            //Rxstatus Updated with 2 with message : No Data/folders in zip file                     No valid data in Zip file
            //Rxstatus Updated with 3 with message : rxtype 

            var handle = GetStdHandle(STD_OUTPUT_HANDLE);
            uint mode;
            GetConsoleMode(handle, out mode);
            mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            SetConsoleMode(handle, mode);

            //Console.Write("Rx Status updation is in progress ");
            IniFile iniFile = new IniFile("Config.ini");
            string Apihost = iniFile.Read("ApiHost", "ApiHost");
            string ApiPName = iniFile.Read("ApiPName", "ApiPName");
            string ApiUName = iniFile.Read("ApiUName", "ApiUName");
            string ApiPKey = iniFile.Read("ApiPKey", "ApiPKey");
            string Apiseed = iniFile.Read("Apiseed", "Apiseed");
            var client = new RestClient(Apihost + "/ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");

            string jsondata = "{\n\"_parameters\": [\n{\n\"savedata\": {\n\"axpapp\": \"" + ApiPName + "\",\n\"username\": \"" + ApiUName + "\",\n";
            jsondata += "\"password\": \"" + ApiPKey + "\",\n";
            jsondata += "\"seed\": \"" + Apiseed + "\",\n";
            jsondata += "\"s\": \"\",\n\"transid\": \"urxst\",\n\"recordid\": \"0\",\n\"recdata\": [\n{\n";
            jsondata += "\"axp_recid1\": [\n{\n\"rowno\": \"001\",\n\"text\": \"0\",\n\"columns\": {\n\"caseno\": \"" + caseno + "\",\n\"status\": \"" + status + "\",\n\"remark\": \"" + errormsg + "\"\n}\n}\n]\n}\n]\n}\n}\n]\n}\n";

            request.AddParameter("application/json", jsondata, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string ststusmsg = "Rx Status updated with " + status;
            if (status == 2)
                //ststusmsg += " (" + Underline + Red + "error" + DecReset + ") ";
                ststusmsg += " (error) ";
            else
                //ststusmsg += " (" + Underline + Green + "success" + DecReset + ") ";
                ststusmsg += " (success) ";
            ststusmsg += response.Content;
            //Console.WriteLine(ststusmsg);
            Updatelog(ststusmsg, 1);

            if (status == 2)  //2 for error and 3 for success
            {
                //Console.WriteLine(errormsg);
                Updatelog(errormsg, 1);
                //MoveDeleteFileFolder(filename, -1);
            }

            return response.Content.ToString();
        }
        public static void Updatelog(string log, int newline)
        {
            string logfile = System.AppDomain.CurrentDomain.BaseDirectory + "Log.txt";
            if (!File.Exists(logfile))
                File.Create(logfile).Dispose();
            using (StreamWriter w = File.AppendText(logfile))
            {
                if (newline == 1)
                {
                    w.Write(DateTime.Now.ToString() + " : " + log + "\n");
                    Console.WriteLine(log);
                }
                else
                {
                    w.Write(DateTime.Now.ToString() + " : " + log);
                    Console.Write(log);
                }
            }
        }
       

        public static string Getvalue(string source, string key)
        {
            string _AdaId = source.Substring(source.IndexOf(key), source.Length - source.IndexOf(key));
            if (_AdaId.IndexOf(" ") > 0)
                _AdaId = _AdaId.Substring(0, _AdaId.IndexOf(" ") - 1);
            _AdaId = _AdaId.Remove(_AdaId.IndexOf('"'), 1);
            _AdaId = _AdaId.Remove(_AdaId.IndexOf('='), 1);
            _AdaId = _AdaId.Remove(_AdaId.IndexOf(key), key.Length);
            return _AdaId;
        }
        public static int IsPasswordProtectedZipFile(string path)
        {
            // 1 - Password protected
            // 0 - No password
            //-1 - Invalid file
            using (FileStream fileStreamIn = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (ZipInputStream zipInStream = new ZipInputStream(fileStreamIn))
            {
                try
                {
                    ZipEntry entry = zipInStream.GetNextEntry();
                    if (entry.IsCrypted)
                        return 1;
                    else
                        return 0;
                }
                catch                //(Exception e)
                {
                    return -1;
                }

            }
        }
      
        
        public static string ExtractZipfile1(string caseno, string dirpath, string filename)
        {
            Console.WriteLine("");
            Console.WriteLine("Zipfile extraction is in progress... ");
            //DirectoryInfo dirinf = new DirectoryInfo(dirpath);
            string ExtractZipfileflg = "t";
            string zipfilename = filename;
            string zipFilePath = @dirpath + filename;

            if (Directory.Exists(@dirpath + zipfilename.Substring(0, zipfilename.Length - 4)))
            {
                Directory.Delete(@dirpath + zipfilename.Substring(0, zipfilename.Length - 4), true);
            }

            if (IsPasswordProtectedZipFile(zipFilePath) == 0)
            {
                try
                {
                    System.IO.Compression.ZipFile.ExtractToDirectory(zipFilePath, @dirpath);
                }
                catch (Exception ex)
                {
                    ExtractZipfileflg = "f";
                    Console.WriteLine("Error:" + ex.Message);
                    RxStatusupdate(caseno, "Unable to extract the Zip file", filename, 2);
                }
            }
            else
            {
                ExtractZipfileflg = "f";
                RxStatusupdate(caseno, "Zip file is password protected", filename, 2);
            }

            return ExtractZipfileflg + @dirpath + filename.Substring(0, filename.Length - 4);
        }
        public static void MoveDeleteFileFolder(string filename, int result)
        {
            string dirpath = System.AppDomain.CurrentDomain.BaseDirectory + "Data\\";
            string logpath = System.AppDomain.CurrentDomain.BaseDirectory + "Log\\";

            if (result > 0)
            {
                if (File.Exists(dirpath + filename))
                    File.Delete(dirpath + filename);
            }
            else
            {
                if (File.Exists(dirpath + filename))
                {
                    if (File.Exists(logpath + filename))
                        File.Delete(logpath + filename);
                    File.Move(dirpath + filename, logpath + filename);
                }

                //File.Move(dirpath + filename, logpath + filename.Substring(0, filename.Length - 4) + " " + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip");
            }
        }
        public static void Fontandcolor()
        {
            //const string reset = "\x1b[0m";
            //const string bright = "\x1b[1m";
            //const string dim = "\x1b[2m";
            //const string underscore = "\x1b[4m";
            //const string blink = "\x1b[5m";
            //const string reverse = "\x1b[7m";
            //const string hidden = "\x1b[8m";
            //const string cursivefont = "\x1b[3m";

            //const string black = "\x1b[30m";
            //const string yellow = "\x1b[33m";
            //const string blue = "\x1b[34m";
            //const string magenta = "\x1b[35m";
            //const string cyan = "\x1b[36m";
            //const string white = "\x1b[37m";

            //const string BGblack = "\x1b[40m";
            //const string BGred = "\x1b[41m";
            //const string BGgreen = "\x1b[42m";
            //const string BGyellow = "\x1b[43m";
            //const string BGblue = "\x1b[44m";
            //const string BGmagenta = "\x1b[45m";
            //const string BGcyan = "\x1b[46m";
            //const string BGwhite = "\x1b[47m";
        }

        public static void ExtractZip()
        {
            string dirpath = System.AppDomain.CurrentDomain.BaseDirectory + "Data\\";
            //Console.WriteLine("");
            Updatelog("", 1);
            Console.WriteLine("Zipfile extraction is in progress... ");

            string[] zipFiles = Directory.GetFiles(dirpath, "*.zip", SearchOption.AllDirectories);
            for (int i = 0; i < zipFiles.Length; i++)
            {
                string zipFilePath = zipFiles[i];
                if (IsPasswordProtectedZipFile(zipFilePath) == 0)
                {
                    try
                    {
                        System.IO.Compression.ZipFile.ExtractToDirectory(zipFilePath, @dirpath);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error:" + ex.Message);
                    }
                }
                else
                {
                    if (IsPasswordProtectedZipFile(zipFilePath) == 1)
                        Console.WriteLine("Zip file is password protected");
                    else
                        Console.WriteLine("Invalid/Unformated Zip file");
                }
            }
            Console.ReadLine();

        }
        public static void version()
        {

        }
    }
}