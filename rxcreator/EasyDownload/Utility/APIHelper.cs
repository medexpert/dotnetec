﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.Threading.Tasks;

using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using Amazon.S3.Transfer;
using Amazon.Runtime;

namespace EasyDownload.Utility
{
   
    public class IniFile   // revision 11
    {
        public string Path;
        string EXE = Assembly.GetExecutingAssembly().GetName().Name;

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        public IniFile(string IniPath = null)
        {
            Path = new FileInfo(IniPath ?? EXE + ".ini").FullName.ToString();
        }

        public string Read(string Key, string Section = null)
        {
            var RetVal = new StringBuilder(255);
            GetPrivateProfileString(Section ?? EXE, Key, "", RetVal, 255, Path);
            return RetVal.ToString();
        }

        public void Write(string Key, string Value, string Section = null)
        {
            WritePrivateProfileString(Section ?? EXE, Key, Value, Path);
        }

        public void DeleteKey(string Key, string Section = null)
        {
            Write(Key, null, Section ?? EXE);
        }

        public void DeleteSection(string Section = null)
        {
            Write(null, null, Section ?? EXE);
        }

        public bool KeyExists(string Key, string Section = null)
        {
            return Read(Key, Section).Length > 0;
        }


    }

    public class utility
    {
        [XmlRoot(ElementName = "Tooth")]
        public class Tooth
        {
            [XmlAttribute(AttributeName = "AdaId")]
            public string AdaId { get; set; }
            [XmlAttribute(AttributeName = "RestorationType")]
            public string RestorationType { get; set; }
            [XmlAttribute(AttributeName = "ToothMaterial")]
            public string ToothMaterial { get; set; }
            [XmlAttribute(AttributeName = "MarginDesignInt")]
            public string MarginDesignInt { get; set; }
            [XmlAttribute(AttributeName = "MarginDesignExt")]
            public string MarginDesignExt { get; set; }
            [XmlAttribute(AttributeName = "IncisalShade")]
            public string IncisalShade { get; set; }
            [XmlAttribute(AttributeName = "MiddleShade")]
            public string MiddleShade { get; set; }
            [XmlAttribute(AttributeName = "GingivalShade")]
            public string GingivalShade { get; set; }
            [XmlAttribute(AttributeName = "DieShade")]
            public string DieShade { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Teeth")]
        public class Teeth
        {
            [XmlElement(ElementName = "Tooth")]
            public Tooth Tooth { get; set; }
        }

        [XmlRoot(ElementName = "RxInfo")]
        public class RxInfo
        {
            [XmlElement(ElementName = "CaseId")]
            public string CaseId { get; set; }
            [XmlElement(ElementName = "FilePath")]
            public string FilePath { get; set; }
            [XmlElement(ElementName = "ExportTime")]
            public string ExportTime { get; set; }
            [XmlElement(ElementName = "UtcOffset")]
            public string UtcOffset { get; set; }
            [XmlElement(ElementName = "LabTechnician")]
            public string LabTechnician { get; set; }
            [XmlElement(ElementName = "Patient")]
            public string Patient { get; set; }
            [XmlElement(ElementName = "Doctor")]
            public string Doctor { get; set; }
            [XmlElement(ElementName = "LabName")]
            public string LabName { get; set; }
            [XmlElement(ElementName = "DueDate")]
            public string DueDate { get; set; }
            [XmlElement(ElementName = "iTeroVersion")]
            public string ITeroVersion { get; set; }
            [XmlElement(ElementName = "ExportScheme")]
            public string ExportScheme { get; set; }
            [XmlElement(ElementName = "SchemeInfo")]
            public string SchemeInfo { get; set; }
            [XmlElement(ElementName = "ToothShadeSystem")]
            public string ToothShadeSystem { get; set; }
            [XmlElement(ElementName = "Teeth")]
            public Teeth Teeth { get; set; }
        }

        [XmlRoot(ElementName = "Type")]
        public class Type
        {
            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "RestorationTypes")]
        public class RestorationTypes
        {
            [XmlElement(ElementName = "Type")]
            public List<Type> Type { get; set; }
        }

        [XmlRoot(ElementName = "Material")]
        public class Material
        {
            [XmlAttribute(AttributeName = "Id")]
            public string Id { get; set; }
            [XmlAttribute(AttributeName = "Name")]
            public string Name { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Materials")]
        public class Materials
        {
            [XmlElement(ElementName = "Material")]
            public List<Material> Material { get; set; }
        }

        [XmlRoot(ElementName = "RxDefines")]
        public class RxDefines
        {
            [XmlElement(ElementName = "RestorationTypes")]
            public RestorationTypes RestorationTypes { get; set; }
            [XmlElement(ElementName = "Materials")]
            public Materials Materials { get; set; }
        }

        [XmlRoot(ElementName = "Object")]
        public class Object
        {
            [XmlAttribute(AttributeName = "ObjectType")]
            public string ObjectType { get; set; }
            [XmlAttribute(AttributeName = "SubType")]
            public string SubType { get; set; }
            [XmlAttribute(AttributeName = "JawId")]
            public string JawId { get; set; }
            [XmlAttribute(AttributeName = "Index")]
            public string Index { get; set; }
            [XmlAttribute(AttributeName = "FileName")]
            public string FileName { get; set; }
            [XmlText]
            public string Text { get; set; }
            [XmlAttribute(AttributeName = "AdaId")]
            public string AdaId { get; set; }
        }

        [XmlRoot(ElementName = "ExportedObjects")]
        public class ExportedObjects
        {
            [XmlElement(ElementName = "Object")]
            public List<Object> Object { get; set; }
        }

        [XmlRoot(ElementName = "ActualUsedTransforms")]
        public class ActualUsedTransforms
        {
            [XmlElement(ElementName = "Transformation")]
            public string Transformation { get; set; }
        }

        [XmlRoot(ElementName = "iTeroExport")]
        public class ITeroExport
        {
            [XmlElement(ElementName = "RxInfo")]
            public RxInfo RxInfo { get; set; }
            [XmlElement(ElementName = "RxDefines")]
            public RxDefines RxDefines { get; set; }
            [XmlElement(ElementName = "ExportedObjects")]
            public ExportedObjects ExportedObjects { get; set; }
            [XmlElement(ElementName = "ActualUsedTransforms")]
            public ActualUsedTransforms ActualUsedTransforms { get; set; }
            [XmlAttribute(AttributeName = "Version")]
            public string Version { get; set; }
        }
    }
}