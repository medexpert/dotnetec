﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
namespace EasyDownload.Utility
{
    class OracleDBManager
    {
        private static string connectionString = @"Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=10.0.0.2)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=ORCL_fra1fx.sub05081007350.agilecloudvcn.oraclevcn.com)(SERVER=DEDICATED)));User Id=leixir; Password=log;";

        public OracleDBManager()
        {

        }

        public OracleDBManager(string cs)
        {
            connectionString = cs;
        }

        public DataTable CallGetOrders()
        {
            string query = "select CASENO as orderno, ds_ordersid from ds_orders where ARCHIVE_STATUS=1 and rxStatus=0";
            using (OracleConnection _con = new OracleConnection(connectionString))
            {
                try
                {
                    var cmd = new OracleCommand(query, _con)
                    {
                        CommandType = System.Data.CommandType.Text
                    };
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message); return null;
                }
                finally
                {
                    _con.Close();
                    _con.Dispose();
                }
            }
        }
    }
}
